/* Copyright (c) 2014-2015, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "msm8909-qrd-cb03-jp.dtsi"
#include "dsi-panel-otm9605a-cb03-jp-video.dtsi"	//major	QHD LCM replace otm9605a CB03



/ {
	compatible = "qcom,msm8909-qrd", "qcom,msm8909", "qcom,qrd";
};

&tlmm_pinmux {
	ltr553_int_pin {
		qcom,pins = <&gp 94>;
		qcom,pin-func = <0>;
		qcom,num-grp-pins = <1>;
		label = "ltr553-irq";
		ltr553_default: ltr553_default {
			drive-strength = <6>;
			bias-pull-up;
		};
		ltr553_sleep: ltr553_sleep {
			drive-strength = <2>;
			bias-pull-down;
		};

	};

	akm_reset_pin {
		qcom,pins = <&gp 65>;
		qcom,pin-func = <0>;
		qcom,num-grp-pins = <1>;
		label = "akm_reset_pin";
		akm_default: akm_default {
			drive-strength = <6>;
			bias-pull-up;
		};
		akm_sleep: akm_sleep {
			drive-strength = <2>;
			bias-pull-down;
		};
	};
	cm36283_int_pin {
		qcom,pins = <&gp 58>;
		//qcom,pin-func = <0>;
		qcom,num-grp-pins = <1>;
		label = "cm36283-irq";
		cm36283_default: cm36283_default {
			drive-strength = <6>;
			bias-pull-up;
		};
	};
	
	gpio_led_pins {
			qcom,pins = <&gp 98>, <&gp 97>, <&gp 96>;
			qcom,num-grp-pins = <3>;
			label = "gpio-led-pins";
			gpio_led_off: led_off {
				drive-strength = <2>;
				bias-disable;
				output-low;
			};
	};

	uart2-default {
			qcom,pins = <&gp 20>, <&gp 21>;
			qcom,num-grp-pins = <2>;
			qcom,pin-func = <3>;
			label = "uart2-default";
			uart2_default: defualt {
				drive-strength = <2>;
				bias-pull-down;
			};
		};
};

&blsp1_uart2 {
	status = "ok";
	pinctrl-names = "default";
	pinctrl-0 = <&uart2_default>;
	simcom,rs232_en = <&msm_gpio 2 0>;
	simcom,rs232_sel = <&msm_gpio 23 0>;
};

/ {
	aliases {
		serial1 = &blsp1_uart2;// should be no serial1, 0 for console
	};
};

&soc {
	gpio_keys {
		compatible = "gpio-keys";
		input-name = "gpio-keys";
		pinctrl-names = "tlmm_gpio_key_active","tlmm_gpio_key_suspend";
		pinctrl-0 = <&gpio_key_active>;
		pinctrl-1 = <&gpio_key_suspend>;

		vol_up {
			label = "volume_up";
			gpios = <&msm_gpio 91 0x1>;
			linux,input-type = <1>;
			linux,code = <115>;
			gpio-key,wakeup;
			debounce-interval = <15>;
		};
		vol_down {
			label = "volume_down";
			gpios = <&msm_gpio 90 0x1>;
			linux,input-type = <1>;
			linux,code = <114>;
			gpio-key,wakeup;
			debounce-interval = <15>;
		};
	};

	gpio-leds {
		compatible = "gpio-leds";
		status = "okay";
		pinctrl-names = "default";
		pinctrl-0 = <&gpio_led_off>;
		red {
			gpios = <&msm_gpio 98 0>;
			label = "red";
			linux,default-trigger = "none";
			default-state = "off";
			retain-state-suspended;
		};
        	green {
			gpios = <&msm_gpio 97 0>;
			label = "green";
			linux,default-trigger = "none";
			default-state = "off";
			retain-state-suspended;
		};
		blue {
			gpios = <&msm_gpio 96 0>;
			label = "blue";
			linux,default-trigger = "none";
			default-state = "off";
			retain-state-suspended;
		};

	};
	i2c@78b9000 { 
		synaptics@20 {
			compatible = "synaptics,rmi4";
			reg = <0x20>;
			interrupt-parent = <&msm_gpio>;
			interrupts = <13 0x2008>;
			vdd-supply = <&pm8916_l17>;
			vcc_i2c-supply = <&pm8916_l6>;
			synaptics,reset-gpio = <&msm_gpio 12 0x00>;
			synaptics,irq-gpio = <&msm_gpio 13 0x2008>;
			synaptics,display-coords = <0 0 540 960>;
			synaptics,i2c-pull-up;
			//synaptics,button-map = <139 102 158>;
			//synaptics,name = "rmi4";
			synaptics,power-down;
			synaptics,disable-gpios;
		};

		capella@60 { 
			compatible = "capella,cm36283";
			reg = <0x60>;
			pinctrl-names = "default";
			pinctrl-0 = <&cm36283_default>;
			interrupt-parent = <&msm_gpio>;
			interrupts = <94 0x2002>;
			vdd-supply = <&pm8916_l17>;
			vio-supply = <&pm8916_l6>;
			capella,ps_en_gpio = <&msm_gpio 52 0x0>;
			capella,interrupt-gpio = <&msm_gpio 94 0x2002>;
			//capella,use-polling = <1>;
			capella,use-polling;
			capella,ls_poll_delay = <200>;
			capella,ps_poll_delay = <200>;
			capella,ps_away_thd_set = <0x10>;
			capella,ps_close_thd_set = <0x20>;
			capella,ps_conf1_val = <0x080C>;
			capella,ps_conf3_val = <0x0300>;
			capella,ps_away_cali_param = <1>;
			//capella,ps_en_gpio = <&msm_gpio 17 0x02>;
		};

		icm20645_acc@68 {
			//compatible = "inven,icm30628";
                        compatible = "inven,icm20645";
			reg = <0x68>;
			interrupt-parent = <&msm_gpio>;
			interrupts = <58 0x2>;
			inven,vdd_ana-supply = <&pm8916_l17>;
			inven,gyro_ldo_en = <&msm_gpio 112 0>;
			inven,vcc_i2c-supply = <&pm8916_l6>;
			inven,gpio_int1 = <&msm_gpio 58 0x2>;
			fs_range = <0x00>;
			axis_map_x = <1>;
			axis_map_y = <0>;
			axis_map_z = <2>;
			negate_x = <0>;
			negate_y = <1>;
			negate_z = <0>;
			poll_interval = <200>;
			min_interval = <5>;

			inven,secondary_reg = <0xc>;
			// If no compass sensor, replace "compass" with "none"
			inven,secondary_type = "compass";
                        //inven,secondary_type = "none";
			inven,secondary_name = "ak09911";
			inven,vdd_compass_ana-supply = <&pm8916_l17>;
			inven,vdd_compass_io-supply = <&pm8916_l6>;
			inven,secondary_axis_map_x = <1>;
			inven,secondary_axis_map_y = <0>;
			inven,secondary_axis_map_z = <2>;
			inven,secondary_negate_x = <1>;
			inven,secondary_negate_y = <0>;
			inven,secondary_negate_z = <0>;
                        inven,compass_rstn = <&msm_gpio 65 0x0>;
			inven,aux_reg = <0x76>;
			// If no pressure sensor, replace "pressure" with "none"
			//inven,aux_type = "pressure";
			inven,aux_type = "none";
			inven,aux_name = "bmp280";
		};
	};


        gen-vkeys {
                compatible = "qcom,gen-vkeys";
                label = "goodix-ts";
                qcom,disp-maxx = <540>;
                qcom,disp-maxy = <960>;
                qcom,panel-maxx = <540>;
                qcom,panel-maxy = <980>;
                qcom,key-codes = <158 172 139>;
                qcom,y-offset = <0>;
        };


	sound {
		compatible = "qcom,msm8x16-audio-codec";
		qcom,model = "msm8909-skue-snd-card";
		qcom,msm-snd-card-id = <0>;
		qcom,msm-codec-type = "internal";
		qcom,msm-ext-pa = "primary";
		qcom,msm-mclk-freq = <9600000>;
		qcom,msm-mbhc-hphl-swh = <1>;
		qcom,msm-mbhc-gnd-swh = <0>;
		qcom,msm-hs-micbias-type = "internal";
		qcom,msm-micbias1-ext-cap;
		qcom,msm-micbias2-ext-cap;
		qcom,audio-routing =
			"RX_BIAS", "MCLK",
			"SPK_RX_BIAS", "MCLK",
			"INT_LDO_H", "MCLK",
			"MIC BIAS Internal1", "Handset Mic",
			"MIC BIAS Internal2", "Headset Mic",
			"MIC BIAS Internal3", "Secondary Mic",
			"AMIC1", "MIC BIAS Internal1",
			"AMIC2", "MIC BIAS Internal2",
			"AMIC3", "MIC BIAS Internal3";
		pinctrl-names = "cdc_lines_act",
				"cdc_lines_sus";
		pinctrl-0 = <&cdc_pdm_lines_act>;
		pinctrl-1 = <&cdc_pdm_lines_sus>;
		asoc-platform = <&pcm0>, <&pcm1>, <&voip>, <&voice>,
				<&loopback>, <&compress>, <&hostless>,
				<&afe>, <&lsm>, <&routing>, <&lpa>,
				<&voice_svc>;
		asoc-platform-names = "msm-pcm-dsp.0", "msm-pcm-dsp.1",
				"msm-voip-dsp", "msm-pcm-voice", "msm-pcm-loopback",
				"msm-compress-dsp", "msm-pcm-hostless", "msm-pcm-afe",
				"msm-lsm-client", "msm-pcm-routing", "msm-pcm-lpa",
				"msm-voice-svc";
		asoc-cpu = <&dai_pri_auxpcm>, <&dai_hdmi>,
				<&dai_mi2s0>, <&dai_mi2s1>, <&dai_mi2s2>, <&dai_mi2s3>,
				<&sb_0_rx>, <&sb_0_tx>, <&sb_1_rx>, <&sb_1_tx>,
				<&sb_3_rx>, <&sb_3_tx>, <&sb_4_rx>, <&sb_4_tx>,
				<&bt_sco_rx>, <&bt_sco_tx>, <&int_fm_rx>, <&int_fm_tx>,
				<&afe_pcm_rx>, <&afe_pcm_tx>, <&afe_proxy_rx>, <&afe_proxy_tx>,
				<&incall_record_rx>, <&incall_record_tx>, <&incall_music_rx>,
				<&incall_music_2_rx>;
		asoc-cpu-names = "msm-dai-q6-auxpcm.1", "msm-dai-q6-hdmi.8",
				"msm-dai-q6-mi2s.0", "msm-dai-q6-mi2s.1",
				"msm-dai-q6-mi2s.2", "msm-dai-q6-mi2s.3",
				"msm-dai-q6-dev.16384",	"msm-dai-q6-dev.16385",
				"msm-dai-q6-dev.16386", "msm-dai-q6-dev.16387",
				"msm-dai-q6-dev.16390", "msm-dai-q6-dev.16391",
				"msm-dai-q6-dev.16392",	"msm-dai-q6-dev.16393",
				"msm-dai-q6-dev.12288", "msm-dai-q6-dev.12289",
				"msm-dai-q6-dev.12292", "msm-dai-q6-dev.12293",
				"msm-dai-q6-dev.224", "msm-dai-q6-dev.225",
				"msm-dai-q6-dev.241", "msm-dai-q6-dev.240",
				"msm-dai-q6-dev.32771", "msm-dai-q6-dev.32772",
				"msm-dai-q6-dev.32773", "msm-dai-q6-dev.32770";
		asoc-codec = <&stub_codec>, <&pm8916_tombak_dig>;
		asoc-codec-names = "msm-stub-codec.1", "tombak_codec";
	};


};

&rpm_bus {
	rpm-regulator-ldoa11 {
			status = "okay";
		pm8916_l11: regulator-l11 {
			status = "okay";
			regulator-min-microvolt = <2950000>;
			regulator-max-microvolt = <2950000>;
			qcom,init-voltage = <2950000>;
		};
	};

	rpm-regulator-ldoa12 {
			status = "okay";
		pm8916_l12: regulator-l12 {
			status = "okay";
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <2950000>;
			qcom,init-voltage = <2950000>;
		};
	};
};

&spmi_bus {
	qcom,pm8916@0 {
		qcom,leds@a300 {
			status = "okay";
			qcom,led_mpp_4 {
				label = "mpp";
				linux,name = "button-backlight";
				linux,default-trigger = "none";
				qcom,default-state = "off";
				qcom,max-current = <40>;
				qcom,current-setting = <5>;
				qcom,id = <6>;
				qcom,mode = "manual";
				qcom,source-sel = <1>;
				qcom,mode-ctrl = <0x60>;
			};
		};
	};

	qcom,pm8916@1 {
		qcom,vibrator@c000 {
			status = "okay";
			qcom,vib-timeout-ms = <15000>;
			qcom,vib-vtg-level-mV = <3100>;
		};
	};
};

&pm8916_mpps {
	mpp@a000 { /* MPP 1 */
		/* VDD_PX */
		status = "disabled";
	};

	mpp@a100 { /* MPP 2 */
		/* HR LED */
		status = "disabled";
	};

	mpp@a200 { /* MPP 3 */
		/* VREF DAC */
		status = "disabled";
	};

	mpp@a300 { /* MPP 4 */
		/* Backlight PWM */
		qcom,mode = <1>;		/* Digital output */
		qcom,invert = <0>;		/* Disable invert */
		qcom,src-sel = <4>;		/* DTEST1 */
		qcom,vin-sel = <0>;		/* VPH_PWR */
		qcom,master-en = <1>;		/* Enable MPP */
	};
};

&mdss_mdp {
	qcom,mdss-pref-prim-intf = "dsi";
};

//&dsi_otm9605a_qhd_video {
//	qcom,cont-splash-enabled;
//};

&pmx_mdss {
	qcom,num-grp-pins = <1>;
	qcom,pins = <&gp 8>;
};

&pmx_mdss_te {
	qcom,num-grp-pins = <1>;
	qcom,pins = <&gp 24>;
};

&mdss_dsi0 {
//	qcom,dsi-pref-prim-pan = <&dsi_otm9605a_qhd_video>;
	pinctrl-names = "mdss_default", "mdss_sleep";
	pinctrl-0 = <&mdss_dsi_active &mdss_te_active>;
	pinctrl-1 = <&mdss_dsi_suspend &mdss_te_suspend>;

	qcom,platform-reset-gpio = <&msm_gpio 8 0>;
};



&pm8916_vadc {
	chan@30 {
		qcom,scale-function = <13>;
	};
};

&pm8916_chg {
	qcom,vddmax-mv = <4350>;
	qcom,vddsafe-mv = <4380>;
	qcom,vinmin-mv = <4470>;
	qcom,batt-hot-percentage = <25>;
	qcom,batt-cold-percentage = <80>;
	qcom,tchg-mins = <360>;
	qcom,bms-controlled-charging;
	qcom,disable-vbatdet-based-recharge;
	status = "okay";

	//qcom,use-external-charger;
	qcom,charging-disabled;
};


&usb_otg {
       qcom,hsusb-otg-mode = <3>;
       qcom,usbid-gpio = <&msm_gpio 78 0>;

	interrupts = <0 134 0>,<0 140 0>,<0 136 0>;
	interrupt-names = "core_irq", "async_irq", "phy_irq";
       
       pinctrl-names = "default";
       pinctrl-0 = <&usbid_default>;
       vbus_otg-supply = <&bq24296_otg_supply>;
};


&i2c_4{
	smb1360_otg_supply: smb1360-chg-fg@14 {
             compatible = "qcom,smb1360-chg-fg";
             reg = <0x14>;

             interrupt-parent = <&msm_gpio>;
             interrupts = <58 8>;

             pinctrl-names = "default";
             pinctrl-0 = <&smb_int_default>;

             qcom,chg-inhibit-disabled;
             qcom,float-voltage-mv = <4350>;

             qcom,iterm-ma = <100>;
             qcom,charging-timeout = <1536>;
             qcom,otg-batt-curr-limit = <950>;
             qcom,recharge-thresh-mv = <100>;
             qcom,thermal-mitigation = <1500 700 600 500>;
             qcom,fg-auto-recharge-soc = <99>;

             regulator-name = "smb1360_otg_vreg";
             
             /* battery-profile selection properties */
             qcom,batt-profile-select;
             qcom,smb1360-vadc = <&pm8916_vadc>;
             qcom,batt-id-vref-uv = <1800000>;
             qcom,batt-id-rpullup-kohm = <100>;
             qcom,profile-a-rid-kohm = <2>;
             qcom,profile-b-rid-kohm = <121>;
  
             /* battery capacity */
             qcom,fg-batt-capacity-mah = <3500>;
             qcom,fg-cc-soc-coeff = <0x80AE>;

             qcom,fg-cutoff-voltage-mv = <3300>;
             qcom,fg-iterm-ma = <100>;
             qcom,fg-ibatt-standby-ma = <200>;
             qcom,fg-cc-to-cv-mv = <4330>;
             qcom,thermistor-c1-coeff = <0x85EC>;


             qcom,config-hard-thresholds;
             qcom,hot-bat-decidegc = <510>;
             qcom,cold-bat-decidegc = <(-00)>;
             qcom,soft-jeita-supported;
             qcom,warm-bat-decidegc = <440>;
             qcom,cool-bat-decidegc = <100>;

             qcom,warm-bat-mv = <4250>;
             qcom,cool-bat-mv = <4250>;
             qcom,warm-bat-ma = <550>;
             qcom,cool-bat-ma = <550>;

             qcom,fg-soc-max = <85>;
             qcom,fg-soc-min = <15>;
             qcom,fg-delta-soc = <1>;
    };

     bq24296_otg_supply:bq24296@6B {
             compatible = "ti,bq24296-charger";
             
             reg = <0x6B>;

             pinctrl-names = "default";
             pinctrl-0 = <&smb_int_default>;

			 regulator-name = "smb1360_otg_vreg";
             
             ti,chg-en-gpio = <&msm_gpio 95 0x00>;
             ti,otg-en-gpio = <&msm_gpio 3 0x00>;
             ti,psel-gpio = <&msm_gpio 20 0x00>;
             ti,chg-int = <&msm_gpio 31 0x00>;

       };

       bq27421@55 {
             compatible = "ti,bq27421-battery";
             reg = <0x55>;
       };

};

/ {
	qrd_batterydata: qcom,battery-data {
		qcom,rpull-up-kohm = <0>;
		qcom,vref-batt-therm = <1800000>;
		
		//#include "batterydata-qrd-l45q-4v35-1800mah.dtsi"
		#include "batterydata-qrd-liva-4v35-2100mah.dtsi"
		//#include "batterydata-qrd-skue-4v35-2000mah.dtsi"
	};
};

&pm8916_bms {
	status = "okay";
	qcom,resume-soc = <95>;
	//qcom,use-reported-soc;
        /delete-property/ qcom,report-charger-eoc;
	qcom,force-bms-active-on-charger;
	qcom,battery-data = <&qrd_batterydata>;
};

&tlmm_pinmux {   
  SY7803_pins {  
    qcom,pins = <&gp 9>, <&gp 111>;
    qcom,num-grp-pins = <2>;   
    qcom,pin-func = <0>;
    label = "SY7803_pins";
    SY7803_default: en_default {
      drive-strength = <2>;
      bias-pull-down;
    };
   };  
};
 
&soc {
	flash_SY7803:flashlight {
		compatible = "qcom,leds-gpio-flash";
		status = "okay";
		pinctrl-names = "flash_default";
		pinctrl-0 = <&SY7803_default>;
		qcom,flash-en = <&msm_gpio 9 0>;
		qcom,flash-now = <&msm_gpio 111 0>;
		qcom,op-seq = "flash_en", "flash_now";
		qcom,torch-seq-val = <1 0>;
		qcom,flash-seq-val = <1 1>;
		linux,name = "flashlight";
		linux,default-trigger = "flashlight-trigger";
	};
	led_flash0: qcom,camera-led-flash {
		cell-index = <0>;
		compatible = "qcom,camera-led-flash";
		qcom,flash-type = <3>;
		qcom,flash-source = <&flash_SY7803>;
		qcom,torch-source = <&flash_SY7803>;
	};
};

&i2c_3 {

	actuator0: qcom,actuator@18 {
		cell-index = <3>;
		reg = <0x18>;
		compatible = "qcom,actuator";
		qcom,cci-master = <0>;
	};

	eeprom0: qcom,eeprom@20{
		cell-index = <0>;
		reg = <0x20>;
		qcom,eeprom-name = "sunny_ov8858_q8v19w";
		compatible = "qcom,eeprom";
		qcom,slave-addr = <0x20>;
		qcom,cci-master = <0>;
		qcom,num-blocks = <4>;

	         
		qcom,page0 = <1 0x0100 2 0x01 1 1>;
		qcom,poll0 = <0 0x0 2 0 1 1>;
		qcom,mem0 = <0 0x0 2 0 1 0>;
		qcom,page1 = <1 0x3d84 2 0xc0 1 1>;
		qcom,poll1 = <0 0x0 2 0 1 1>;
		qcom,mem1 = <0 0x3d00 2 0 1 0>;
		qcom,page2 = <1 0x3d88 2 0x7010 2 1>;
		qcom,poll2 = <0 0x0 2 0 1 1>;
		qcom,mem2 = <0 0x3d00 2 0 1 0>;
		qcom,page3 = <1 0x3d8A 2 0x720f 2 1>;
		qcom,pageen3 = <1 0x3d81 2 0x01 1 10>;
		qcom,poll3 = <0 0x0 2 0 1 1>;
		qcom,mem3 = <512 0x7010 2 0 1 1>;


        cam_vaf-supply = <&pm8916_l10>;
		qcom,cam-vreg-name =  "cam_vaf";
		qcom,cam-vreg-type = < 0 >;
		qcom,cam-vreg-min-voltage = < 2800000 >;
		qcom,cam-vreg-max-voltage = < 2800000 >;
		qcom,cam-vreg-op-mode = < 100000 >;
		pinctrl-names = "cam_default", "cam_suspend";
		pinctrl-0 = <&cam_sensor_mclk0_default
			&cam_sensor_rear_default>;
		pinctrl-1 = <&cam_sensor_mclk0_sleep &cam_sensor_rear_sleep>;
		gpios = <&msm_gpio 26 0>,
			<&msm_gpio 35 0>,
			<&msm_gpio 34 0>,
        	<&msm_gpio 0 0>,
			<&msm_gpio 1 0>,
			<&msm_gpio 16 0>;
      
		qcom,gpio-reset = <1>;
		qcom,gpio-standby = <2>;
		qcom,gpio-vio= <3>;
		qcom,gpio-vdig = <4>;
		qcom,gpio-vana = <5>;
    
		qcom,gpio-req-tbl-num = <0 1 2 3 4 5>;
		qcom,gpio-req-tbl-flags = <1 0 0 0 0 0>;
		qcom,gpio-req-tbl-label = "CAMIF_MCLK0",
			"MCAM_RESET",
			"MCAM_STANDBY",
			"MCAM_IOVDD",
			"MCAM_DVDD",
			"MCAM_AVDD";

		qcom,cam-power-seq-type = "sensor_clk",
			"sensor_gpio", "sensor_gpio",
			"sensor_gpio", "sensor_gpio",
			"sensor_gpio", "sensor_gpio";
		qcom,cam-power-seq-val = "sensor_cam_mclk",
			"sensor_gpio_reset",
			"sensor_gpio_standby",
			"sensor_gpio_vaf",
			"sensor_gpio_vio",
			"sensor_gpio_vdig",
			"sensor_gpio_vana" ;
		qcom,cam-power-seq-cfg-val = <24000000 1 1 1 1 1 1 1>;
		qcom,cam-power-seq-delay = <5 10 10 10 10 10 10 10>;

		clocks = <&clock_gcc clk_mclk0_clk_src>,
			<&clock_gcc clk_gcc_camss_mclk0_clk>;
		clock-names = "cam_src_clk", "cam_clk";
	};

	qcom,camera@0 {
		cell-index = <0>;
		compatible = "qcom,camera";
		reg = <0x2>;
		//qcom,special-support-sensors="ov5670_qc700"; karlanz
		//qcom,special-support-sensors="ov8858_q8v19w";
		qcom,eeprom-src = <&eeprom0>;
		qcom,csiphy-sd-index = <0>;
		qcom,csid-sd-index = <0>;
		qcom,mount-angle = <90>;
		qcom,actuator-src = <&actuator0>;
		qcom,led-flash-src = <&led_flash0>;
		//cam_vana-supply = <&pm8909_l17>;
		//cam_vio-supply = <&pm8909_l6>;
        cam_vaf-supply = <&pm8916_l10>;
		qcom,cam-vreg-name = "cam_vaf";
		qcom,cam-vreg-type = < 0 >;
		qcom,cam-vreg-min-voltage = < 2800000 >;
		qcom,cam-vreg-max-voltage = < 2800000 >;
		qcom,cam-vreg-op-mode = < 100000 >;
		pinctrl-names = "cam_default", "cam_suspend";
		pinctrl-0 = <&cam_sensor_mclk0_default
			&cam_sensor_rear_default>;
		pinctrl-1 = <&cam_sensor_mclk0_sleep &cam_sensor_rear_sleep>;
		gpios = <&msm_gpio 26 0>,
			      <&msm_gpio 35 0>,
			      <&msm_gpio 34 0>,
                  <&msm_gpio 0 0>,
			      <&msm_gpio 1 0>,
			      <&msm_gpio 16 0>;
		qcom,gpio-reset = <1>;
		qcom,gpio-standby = <2>;
		qcom,gpio-vio= <3>;
		qcom,gpio-vdig = <4>;
		qcom,gpio-vana = <5>;
    
		qcom,gpio-req-tbl-num = <0 1 2 3 4 5>;
		qcom,gpio-req-tbl-flags = <1 0 0 0 0 0>;
		qcom,gpio-req-tbl-label = "CAMIF_MCLK0",
			"MCAM_RESET",
			"MCAM_STANDBY",
			"MCAM_IOVDD",
			"MCAM_DVDD",
			"MCAM_AVDD";			
			
		qcom,sensor-position = <0>;
		qcom,sensor-mode = <0>;
		qcom,cci-master = <0>;
		status = "ok";
		clocks = <&clock_gcc clk_mclk0_clk_src>,
			<&clock_gcc clk_gcc_camss_mclk0_clk>;
		clock-names = "cam_src_clk", "cam_clk";
	};

	qcom,camera@1 {
		cell-index = <1>;
		compatible = "qcom,camera";
		reg = <0x1>;
		//qcom,special-support-sensors="ov2680_5987fhq";
		//qcom,special-support-sensors="ov5670_qc700";
		qcom,csiphy-sd-index = <0>;
		qcom,csid-sd-index = <0>;
		qcom,mount-angle = <90>;
        cam_vana-supply = <&pm8916_l17>;
		cam_vio-supply = <&pm8916_l6>;
        qcom,cam-vreg-name = "cam_vana", "cam_vio";
		qcom,cam-vreg-type = < 0 1 >;
		qcom,cam-vreg-min-voltage = <2850000 1800000>;
		qcom,cam-vreg-max-voltage = <2850000 1800000>;
		qcom,cam-vreg-op-mode = <100000 0>;
		pinctrl-names = "cam_default", "cam_suspend";
		pinctrl-0 = <&cam_sensor_mclk1_default &cam_sensor_front_default>;
		pinctrl-1 = <&cam_sensor_mclk1_sleep &cam_sensor_front_sleep>;
		gpios = <&msm_gpio 27 0>,
			<&msm_gpio 28 0>,
            <&msm_gpio 89 0>,
			<&msm_gpio 17 0>;
			
		qcom,gpio-reset = <1>;
		qcom,gpio-standby = <2>;
		qcom,gpio-vdig = <3>;
		
		qcom,gpio-req-tbl-num = <0 1 2 3>;
		qcom,gpio-req-tbl-flags = <1 0 0 0>;
		qcom,gpio-req-tbl-label = "CAMIF_MCLK1",
			"SCAM_RESET",
			"SCAM_STANDBY",
			"SCAM_DVDD";
				
		qcom,cci-master = <0>;
		status = "ok";
		clocks = <&clock_gcc clk_mclk1_clk_src>,
			<&clock_gcc clk_gcc_camss_mclk1_clk>;
		clock-names = "cam_src_clk", "cam_clk";
	};
};
