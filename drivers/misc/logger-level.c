//#pragma GCC diagnostic ignored "-Wunused-function"
//#pragma GCC diagnostic ignored "-Wunused-variable"

#include <linux/slab.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/device.h>
#include <linux/timer.h>
#include <linux/err.h>
#include <linux/ctype.h>

#if defined(CONFIG_LOGGER_LEVEL_CONTROL)
static int logger_level = 6;
#else
static int logger_level = 0;
#endif

#define LOGGER_LEVEL_BUFF_SIZE 10

struct class *logger_level_class;
struct device *logger_level_dev;

static ssize_t logger_level_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	return snprintf(buf, LOGGER_LEVEL_BUFF_SIZE, "%u\n", logger_level);
}

static ssize_t logger_level_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned long state;
	ssize_t ret = -EINVAL;

	ret = kstrtoul(buf, 10, &state);
	if (ret)
		return ret;

	logger_level = state;
	
	return count;
}

static DEVICE_ATTR(control, 0666, logger_level_show, logger_level_store);

static int __init logger_level_init(void)
{
	int ret;

	logger_level_class = class_create(THIS_MODULE, "logger_file");
    	if (IS_ERR(logger_level_class))
        		pr_err("Failed to create class(logger_level_class)!\n");
    	logger_level_dev = device_create(logger_level_class,
                                     NULL, 0, NULL, "logger_level");
    	if (IS_ERR(logger_level_dev))
        	    	pr_err("Failed to create logger_level_dev!\n");
	
	ret = device_create_file(logger_level_dev, &dev_attr_control);
	if(ret<0)
		 pr_err("Failed to create logger_level_dev file !\n");

	printk("logger_level ok\n");
	return ret;
}

static void __exit logger_level_exit(void)
{
	class_destroy(logger_level_class);
	device_unregister(logger_level_dev);
}

subsys_initcall(logger_level_init);
module_exit(logger_level_exit);
MODULE_AUTHOR("SIMCOM, Inc.");
MODULE_DESCRIPTION("SIMCOM LOGGER LEVEL CONTROL");
MODULE_LICENSE("GPL");
