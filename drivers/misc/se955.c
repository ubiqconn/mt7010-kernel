//#pragma GCC diagnostic ignored "-Wunused-function"
//#pragma GCC diagnostic ignored "-Wunused-variable"

#include <linux/types.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <asm/mach/arch.h>
#include <mach/gpiomux.h>
#include <asm/mach-types.h>
#include <linux/scanner_type.h>
#include <linux/of_gpio.h>

#define SE955_NAME "scan_se955"

struct class *se955_scan_class;
struct device *se955_scan_dev;

static struct mutex seX55_mut;

struct se955_platform_data{
	int trigger_gpio;
	uint32_t trigger_gpio_flag;
	int scan_lever_en;
	uint32_t scan_lever_en_flag;
	int scan_rts_gpio;
	uint32_t scan_rts_gpio_flag;
	int scan_cts_gpio;
	uint32_t scan_cts_gpio_flag;
};
struct se955_platform_data *pdata_info;

static ssize_t se955_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	return 0;
}

static ssize_t se955_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct se955_platform_data *pdata = pdata_info;
	
	int ret = 1;
#ifdef CONFIG_HSM_N5600
	if(gpio_is_valid(pdata->scan_rts_gpio))
		gpio_direction_output(pdata->scan_rts_gpio, 1);
	
	if(gpio_is_valid(pdata->scan_cts_gpio))
		gpio_direction_input(pdata->scan_cts_gpio);
#endif
	if(gpio_is_valid(pdata->scan_lever_en))
		gpio_direction_output(pdata->scan_lever_en, 1);
	mutex_lock(&seX55_mut);
	{
		char on = *buf;
		printk("yyw %s on:%c\n", __func__, on);

		if (on == '1'){//work start
			gpio_direction_output(pdata->trigger_gpio, 1);
			msleep(80);
			gpio_direction_output(pdata->trigger_gpio, 0);
			msleep(80);
			ret = 1;
		} else if (on == '0'){//work end
			gpio_direction_output(pdata->trigger_gpio, 1);
			ret = 1;
		} else if (on == '2') {//command start
			int value = 1;
			gpio_direction_output(pdata->scan_rts_gpio, 1);
			msleep(50);
			gpio_direction_output(pdata->scan_rts_gpio, 0);

			while (value) {
				value = gpio_get_value(pdata->scan_cts_gpio);
				udelay(300);
			}
			ret = 1;
		} else if (on == '3') {//command end
			int value = 0;
			gpio_direction_output(pdata->scan_rts_gpio, 1);

			while (!value) {
				value = gpio_get_value(pdata->scan_cts_gpio);
				udelay(300);
			}
			ret = 1;
		} else if (on == '4') {
			gpio_direction_output(pdata->scan_lever_en, 1);
		} else if (on == '5') {
			gpio_direction_output(pdata->scan_lever_en, 0);
		}else {
			printk("yyw trigger error\n");
			ret = -EIO;
		}
	}
	mutex_unlock(&seX55_mut);

	return count;
}

static DEVICE_ATTR(control, 0660, se955_show, se955_store);

static int se955_parse_dt(struct device *dev,
				struct se955_platform_data *pdata)
{
	struct device_node *np = dev->of_node;
	/*SIMTech Warning:
	 *In our design, we use the se955 & n5600 work coexist for demo
	 *so we suggest you to request the GPIOs when using them according to your design
	*/
#ifndef CONFIG_HSM_N5600
	int rc;
#endif

  	pdata->trigger_gpio = of_get_named_gpio_flags(np, "se955,trigger-gpio",
				0, &pdata->trigger_gpio_flag);
    if (pdata->trigger_gpio< 0) {
		dev_err(dev, "Unable to read trigger-gpio\n");
		return pdata->trigger_gpio;
	}
#ifndef CONFIG_HSM_N5600
	else{
		rc = gpio_request(pdata->trigger_gpio, "se955_trigger_gpio");
		if (rc < 0) {
			printk("Failed to request GPIO:pdata->trigger_gpio, ERRNO:%d", rc);
			rc = -ENODEV;
			return rc;
		} 
	}
#endif

  	pdata->scan_lever_en= of_get_named_gpio_flags(np, "se955,scan-lever-en",
				0, &pdata->scan_lever_en_flag);
    if (pdata->scan_lever_en< 0) {
		dev_err(dev, "Unable to read scan_lever_en\n");
		return pdata->scan_lever_en;
	}
#ifndef CONFIG_HSM_N5600
	else{
		rc = gpio_request(pdata->scan_lever_en, "scan_lever_en");
		if (rc < 0) {
			printk("Failed to request GPIO:pdata->scan_lever_en, ERRNO:%d", rc);
			rc = -ENODEV;
			return rc;
		} 
	}
#endif

  	pdata->scan_rts_gpio= of_get_named_gpio_flags(np, "se955,scan-rts-gpio",
				0, &pdata->scan_rts_gpio_flag);
    if (pdata->scan_rts_gpio< 0) {
		dev_err(dev, "Unable to read scan_rts_gpio\n");
		return pdata->scan_rts_gpio;
	}
#ifndef CONFIG_HSM_N5600
	else{
		rc = gpio_request(pdata->scan_rts_gpio, "scan_rts_gpio");
		if (rc < 0) {
			printk("Failed to request GPIO:pdata->scan_rts_gpio, ERRNO:%d", rc);
			rc = -ENODEV;
			return rc;
		} 
	}
#endif

  	pdata->scan_cts_gpio= of_get_named_gpio_flags(np, "se955,scan-cts-gpio",
				0, &pdata->scan_cts_gpio_flag);
    if (pdata->scan_cts_gpio< 0) {
		dev_err(dev, "Unable to read scan_cts_gpio\n");
		return pdata->scan_cts_gpio;
	}
#ifndef CONFIG_HSM_N5600
	else{
		rc = gpio_request(pdata->scan_cts_gpio, "scan_cts_gpio");
		if (rc < 0) {
			printk("Failed to request GPIO:pdata->scan_cts_gpio, ERRNO:%d", rc);
			rc = -ENODEV;
			return rc;
		} 
	}
#endif
	return 0;
}

static int se955_probe(struct platform_device *pdev)
{
	struct se955_platform_data *pdata = pdev->dev.platform_data;
	int ret = 0;

	printk("\n=========%s============\n",__func__);

	if (pdev->dev.of_node) {
		pdata = devm_kzalloc(&pdev->dev, sizeof(*pdata), GFP_KERNEL);
		if (!pdata) {
			dev_err(&pdev->dev, "Failed to allocate memory for pdata\n");
			ret = -ENOMEM;
			goto err_platform_data_null;
		}

		ret = se955_parse_dt(&pdev->dev, pdata);
		}
	platform_set_drvdata(pdev, pdata);
	
	pdata_info = pdata;

#ifndef CONFIG_HSM_N5600
	if(gpio_is_valid(pdata->scan_rts_gpio))
		gpio_direction_output(pdata->scan_rts_gpio, 1);
	
	if(gpio_is_valid(pdata->scan_cts_gpio))
		gpio_direction_input(pdata->scan_cts_gpio);
#endif
   se955_scan_class = class_create(THIS_MODULE, "se955_scan");
    if (IS_ERR(se955_scan_class))
        pr_err("Failed to create class(se955_scan_class)!\n");
    se955_scan_dev = device_create(se955_scan_class,
                                     NULL, 0, NULL, "device");
    if (IS_ERR(se955_scan_dev))
        pr_err("Failed to create device(se955_scan_dev)!\n");
	
	ret = device_create_file(se955_scan_dev, &dev_attr_control);
	if(ret<0)
		 dev_err(&pdev->dev, "Failed to create device file !\n");
	mutex_init(&seX55_mut);

	printk("se955 ok\n");

	return ret;

err_platform_data_null:
	kfree(pdata);
	dev_err(&pdev->dev, "%s:error exit! ret = %d\n", __func__, ret);
	return ret;

}

static int se955_remove(struct platform_device *pdev)
{
	struct se955_platform_data *pdata = platform_get_drvdata(pdev);

	if(gpio_is_valid(pdata->trigger_gpio))
		gpio_direction_output(pdata->trigger_gpio, 0);
	if(gpio_is_valid(pdata->scan_lever_en))
		gpio_direction_output(pdata->scan_lever_en, 0);
	
	return 0;
}
#ifdef CONFIG_PM
//static int se955_suspend(struct platform_device *pdev)
static int se955_suspend(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct se955_platform_data *pdata = platform_get_drvdata(pdev);
	if(gpio_is_valid(pdata->trigger_gpio))
		gpio_direction_output(pdata->trigger_gpio, 0);
	if(gpio_is_valid(pdata->scan_lever_en))
		gpio_direction_output(pdata->scan_lever_en, 0);
	return 0;
}

static int se955_resume(struct device *dev)
{
#if 0
	struct platform_device *pdev = to_platform_device(dev);
	struct se955_platform_data *pdata = platform_get_drvdata(pdev);
	if(gpio_is_valid(pdata->scan_lever_en))
		gpio_direction_output(pdata->scan_lever_en, 1);
	if(gpio_is_valid(pdata->trigger_gpio))
		gpio_direction_output(pdata->trigger_gpio, 1);
#endif
	return 0;
}
static const struct dev_pm_ops se955_dev_pm_ops = {
	.suspend = se955_suspend,
	.resume = se955_resume,
};
#else
static const struct dev_pm_ops se955_dev_pm_ops = {
};
#endif

static struct of_device_id se955_match_table[] = {
	{ .compatible = "mexico,se955",},
	{ },
};

static struct platform_driver se955_driver = {
	.probe = se955_probe,
	.remove = se955_remove,
	.driver = {
		.name = SE955_NAME,
		.owner = THIS_MODULE,
	#ifdef CONFIG_PM
		.pm = &se955_dev_pm_ops,
	#endif
		.of_match_table = se955_match_table,
	},
};

static int __init se955_init(void)
{
	int ret;

	//platform_device_register(&se955_device);
	ret = platform_driver_register(&se955_driver);
	return ret;
}

static void __exit se955_exit(void)
{
	platform_driver_unregister(&se955_driver);
}

module_init(se955_init);
module_exit(se955_exit);
MODULE_AUTHOR("SIMCOM, Inc.");
MODULE_DESCRIPTION("SIMCOM SCAN SE955");
MODULE_LICENSE("GPL");
