/* Includes ------------------------------------------------------------------*/
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/irq.h>
#include <linux/of_gpio.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/workqueue.h>
#include <linux/kobject.h>
#include <linux/platform_device.h>

/* Private typedef & define --------------------------------------------------*/
#define DEBUG
//#define DEBUG_REG_VALUE
//#define DEBUG_ERROR_STATUS

#define REG_SOFT_RESET		    0x09
#define REG_CORE_PLL			0x0A
#define REG_PLL_DIV			    0x0B
#define REG_PLL_EN			    0x0D
#define REG_DSI_CFG			    0x10
#define REG_DSI_EQ			    0x11
#define REG_CHA_DSI_CLK_RNG		0x12
#define REG_LVDS_MODE			0x18
#define REG_LVDS_SIGN			0x19
#define REG_LVDS_TERM			0x1A
#define REG_LVDS_CM_ADJUST	    0x1B
#define REG_CHA_LINE_LEN_LO		0x20
#define REG_CHA_LINE_LEN_HI		0x21
#define REG_CHA_VERT_LINES_LO	0x24
#define REG_CHA_VERT_LINES_HI	0x25
#define REG_CHA_SYNC_DELAY_LO	0x28
#define REG_CHA_SYNC_DELAY_HI	0x29
#define REG_CHA_HSYNC_WIDTH_LO	0x2C
#define REG_CHA_HSYNC_WIDTH_HI	0x2D
#define REG_CHA_VSYNC_WIDTH_LO	0x30
#define REG_CHA_VSYNC_WIDTH_HI	0x31
#define REG_CHA_HORZ_BACKPORCH	0x34
#define REG_CHA_VERT_BACKPORCH	0x36
#define REG_CHA_HORZ_FRONTPORCH	0x38
#define REG_CHA_VERT_FRONTPORCH	0x3A
#define REG_TEST_PATTERN		0x3C

#define SN65DSI83_NAME "sn65dsi83"

struct sn65dsi83_platform_data {
	int framerate;
	int panel_width;
	int panel_height;
	int h_front_porch;
	int h_back_porch;
	int h_pulse_width;
	int sync_delay;
	int v_front_porch;
	int v_back_porch;
	int v_pulse_width;
	int bpp;
	int lvds_clk;
	int dsi_clk;
	bool test_pattern;
};

struct ti_sn65dsi83_info {
	struct i2c_client		*client;
	struct mutex			lock;
	struct sn65dsi83_platform_data	*pdata;
#ifdef DEBUG_ERROR_STATUS
	struct workqueue_struct	*workqueue;
	int						stable_cnt;
#endif
};

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static struct ti_sn65dsi83_info *sn65dsi83_info = NULL;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
#ifdef DEBUG_ERROR_STATUS
static void sn65dsi83_debug_routine(struct work_struct *ws);
static DECLARE_WORK(sn65dsi83_debug_work, sn65dsi83_debug_routine);

static void sn65dsi83_debug_routine(struct work_struct *ws) {
	int tmp;
	struct i2c_client* client = sn65dsi83_info->client;
	tmp = i2c_smbus_read_byte_data(client, 0xe5);
	pr_debug("sn65dsi83[E5]=%02X\n", tmp);
	i2c_smbus_write_byte_data(client, 0xe5, tmp);
	if (tmp)
		sn65dsi83_info->stable_cnt = 3;
	else
		sn65dsi83_info->stable_cnt--;
	if (sn65dsi83_info->stable_cnt > 0) {
		msleep(100);
		queue_work(sn65dsi83_info->workqueue, &sn65dsi83_debug_work);
	}
}

static void sn65dsi83_debug_error(struct ti_sn65dsi83_info *info) {
	if (info->stable_cnt <= 0) {
		info->stable_cnt = 3;
		queue_work(info->workqueue, &sn65dsi83_debug_work);
	}
}
#endif //#ifdef DEBUG_ERROR_STATUS

static int sn65dsi83_parse_dt(struct device *dev,
	struct sn65dsi83_platform_data *pdata) {
	struct device_node *np = dev->of_node;
	int rc;
	u32 tmp;

	rc = of_property_read_u32(np, "sn65dsi83,framerate", &tmp);
	if (rc < 0) {
		dev_err(dev, "sn65dsi83,framerate fail\n");
		return rc;
	} else {
		pdata->framerate = tmp;
	}

	rc = of_property_read_u32(np, "sn65dsi83,panel-width", &tmp);
	if (rc < 0) {
		dev_err(dev, "sn65dsi83,panel-width fail\n");
		return rc;
	} else {
		pdata->panel_width = tmp;
	}

	rc = of_property_read_u32(np, "sn65dsi83,panel-height", &tmp);
	if (rc < 0) {
		dev_err(dev, "sn65dsi83,panel-height fail\n");
		return rc;
	} else {
		pdata->panel_height = tmp;
	}

	rc = of_property_read_u32(np, "sn65dsi83,h-front-porch", &tmp);
	if (rc < 0) {
		dev_err(dev, "sn65dsi83,h-front-porch fail\n");
		return rc;
	} else {
		pdata->h_front_porch = tmp;
	}

	rc = of_property_read_u32(np, "sn65dsi83,h-back-porch", &tmp);
	if (rc < 0) {
		dev_err(dev, "sn65dsi83,h-back-porch fail\n");
		return rc;
	} else {
		pdata->h_back_porch = tmp;
	}

	rc = of_property_read_u32(np, "sn65dsi83,h-pulse-width", &tmp);
	if (rc < 0) {
		dev_err(dev, "sn65dsi83,h-pulse-width fail\n");
		return rc;
	} else {
		pdata->h_pulse_width = tmp;
	}

	rc = of_property_read_u32(np, "sn65dsi83,sync-delay", &tmp);
	if (rc < 0) {
		dev_err(dev, "sn65dsi83,sync-delay fail\n");
		return rc;
	} else {
		pdata->sync_delay = tmp;
	}

	rc = of_property_read_u32(np, "sn65dsi83,v-front-porch", &tmp);
	if (rc < 0) {
		dev_err(dev, "sn65dsi83,v-front-porch fail\n");
		return rc;
	} else {
		pdata->v_front_porch = tmp;
	}

	rc = of_property_read_u32(np, "sn65dsi83,v-back-porch", &tmp);
	if (rc < 0) {
		dev_err(dev, "sn65dsi83,v-back-porch fail\n");
		return rc;
	} else {
		pdata->v_back_porch = tmp;
	}

	rc = of_property_read_u32(np, "sn65dsi83,v-pulse-width", &tmp);
	if (rc < 0) {
		dev_err(dev, "sn65dsi83,v-pulse-width fail\n");
		return rc;
	} else {
		pdata->v_pulse_width = tmp;
	}

	rc = of_property_read_u32(np, "sn65dsi83,bpp", &tmp);
	if (rc < 0) {
		dev_err(dev, "sn65dsi83,bpp fail\n");
		return rc;
	} else {
		pdata->bpp = tmp;
	}

	rc = of_property_read_u32(np, "sn65dsi83,lvds-clk", &tmp);
	if (rc < 0) {
		dev_err(dev, "sn65dsi83,lvds-clk fail\n");
		return rc;
	} else {
		pdata->lvds_clk = tmp;
	}

	rc = of_property_read_u32(np, "sn65dsi83,dsi-clk", &tmp);
	if (rc < 0) {
		dev_err(dev, "sn65dsi83,dsi-clk fail\n");
		return rc;
	} else {
		pdata->dsi_clk = tmp;
	}

	pdata->test_pattern = of_property_read_bool(np, "sn65dsi83,test-pattern");

	dev_dbg(dev, "framerate:%d, width:%d, height:%d, bpp:%d clk:%d dsi=%dMHz\n",
		pdata->framerate, pdata->panel_width, pdata->panel_height, pdata->bpp, pdata->lvds_clk, pdata->dsi_clk);
	dev_dbg(dev, "hfp:%d, hbp:%d, hpw:%d\n", pdata->h_front_porch, pdata->h_back_porch, pdata->h_pulse_width);
	dev_dbg(dev, "vfp:%d, vbp:%d, vpw:%d\n", pdata->v_front_porch, pdata->v_back_porch, pdata->v_pulse_width);

	return 0;
}

static int sn65dsi83_init_script(struct i2c_client *client, struct sn65dsi83_platform_data *pdata) {
	int tmp;

	//i2c_smbus_write_byte_data(client, REG_SOFT_RESET, 0x01);
	i2c_smbus_write_byte_data(client, REG_PLL_EN, 0x00);
	if (pdata->lvds_clk < 25000 || pdata->lvds_clk > 154000) {
		dev_err(&client->dev, "lvds_clk=%d KHz over range!!!\n", pdata->dsi_clk);
		return -EINVAL;
	} else if (pdata->lvds_clk < 37500) {
		tmp = 0;
	} else if (pdata->lvds_clk < 62500) {
		tmp = 1 << 1;
	} else if (pdata->lvds_clk < 87500) {
		tmp = 2 << 1;
	} else if (pdata->lvds_clk < 112500) {
		tmp = 3 << 1;
	} else if (pdata->lvds_clk < 137500) {
		tmp = 4 << 1;
	} else if (pdata->lvds_clk <= 154000) {
		tmp = 5 << 1;
	}
	i2c_smbus_write_byte_data(client, REG_CORE_PLL, (u8)tmp | 0x01);
	tmp = pdata->dsi_clk / pdata->lvds_clk;
	if (tmp < 1 || tmp > 25) {
		dev_err(&client->dev, "PLL over range!!!\n");
		return -EINVAL;
	}
	i2c_smbus_write_byte_data(client, REG_PLL_DIV, (u8)(tmp - 1) << 3);

	i2c_smbus_write_byte_data(client, REG_DSI_CFG, 0x30);
	i2c_smbus_write_byte_data(client, REG_DSI_EQ, 0x00);
	tmp = (pdata->dsi_clk / 5000);
	if (tmp < 8 || tmp > 64) {
		dev_err(&client->dev, "dsi-clk=%d KHz over range!!!\n", pdata->dsi_clk);
		return -EINVAL;
	}
	i2c_smbus_write_byte_data(client, REG_CHA_DSI_CLK_RNG, (u8)tmp);

	i2c_smbus_write_byte_data(client, REG_LVDS_MODE, 0x78);
	i2c_smbus_write_byte_data(client, REG_LVDS_SIGN, 0x04);
	i2c_smbus_write_byte_data(client, REG_LVDS_TERM, 0x01);
	i2c_smbus_write_byte_data(client, REG_LVDS_CM_ADJUST, 0x00);

	i2c_smbus_write_byte_data(client, REG_CHA_LINE_LEN_LO, (u8)(pdata->panel_width));
	i2c_smbus_write_byte_data(client, REG_CHA_LINE_LEN_HI, (u8)(pdata->panel_width >> 8) & 0xF);
	i2c_smbus_write_byte_data(client, REG_CHA_VERT_LINES_LO, (u8)(pdata->panel_height));
	i2c_smbus_write_byte_data(client, REG_CHA_VERT_LINES_HI, (u8)(pdata->panel_height >> 8) & 0xF);
	i2c_smbus_write_byte_data(client, REG_CHA_SYNC_DELAY_LO, (u8)(pdata->sync_delay));
	i2c_smbus_write_byte_data(client, REG_CHA_SYNC_DELAY_HI, (u8)(pdata->sync_delay >> 8) & 0xF);
	i2c_smbus_write_byte_data(client, REG_CHA_HSYNC_WIDTH_LO, (u8)(pdata->h_pulse_width));
	i2c_smbus_write_byte_data(client, REG_CHA_HSYNC_WIDTH_HI, (u8)(pdata->h_pulse_width >> 8) & 0xF);
	i2c_smbus_write_byte_data(client, REG_CHA_HORZ_BACKPORCH, (u8)(pdata->h_back_porch));
	i2c_smbus_write_byte_data(client, REG_CHA_HORZ_FRONTPORCH, (u8)(pdata->h_front_porch));
	i2c_smbus_write_byte_data(client, REG_CHA_VSYNC_WIDTH_LO, (u8)(pdata->v_pulse_width));
	i2c_smbus_write_byte_data(client, REG_CHA_VSYNC_WIDTH_HI, (u8)(pdata->v_pulse_width >> 8) & 0xF);
	i2c_smbus_write_byte_data(client, REG_CHA_VERT_BACKPORCH, (u8)(pdata->v_back_porch));
	i2c_smbus_write_byte_data(client, REG_CHA_VERT_FRONTPORCH, (u8)(pdata->v_front_porch));

	i2c_smbus_write_byte_data(client, REG_TEST_PATTERN, pdata->test_pattern ? 0x10 : 0);
	i2c_smbus_write_byte_data(client, REG_PLL_EN, 0x01);
	mdelay(5);
	i2c_smbus_write_byte_data(client, REG_SOFT_RESET, 0x01);

#ifdef DEBUG_ERROR_STATUS
	sn65dsi83_debug_error(sn65dsi83_info);
#endif //#ifdef DEBUG_ERROR_STATUS
	return 0;
}

static int sn65dsi83_driver_probe(struct i2c_client *client, const struct i2c_device_id *id) {
	int err = 0;
#ifdef DEBUG_REG_VALUE
	int tmp, i;
#endif

	dev_info(&client->dev, "%s\n", __func__);
	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
		dev_err(&client->dev, "need sn65dsi83 I2C_FUNC_I2C\n");
		return -ENODEV;
	}
	if (!(sn65dsi83_info = kzalloc(sizeof(struct ti_sn65dsi83_info), GFP_KERNEL))) {
		err = -ENOMEM;
		goto exit;
	}
	memset(sn65dsi83_info, 0, sizeof(struct ti_sn65dsi83_info));
	sn65dsi83_info->client = client;
	sn65dsi83_info->pdata = devm_kzalloc(&client->dev, sizeof(struct sn65dsi83_platform_data), GFP_KERNEL);
	if (!sn65dsi83_info->pdata) {
		dev_err(&client->dev, "Failed to allocate memory\n");
		err = -ENOMEM;
		goto err_kzalloc;
	}

	err = sn65dsi83_parse_dt(&client->dev, sn65dsi83_info->pdata);
	if (err) {
		goto dt_failed;
	}
	i2c_set_clientdata(client, sn65dsi83_info);
#ifdef DEBUG_ERROR_STATUS
	sn65dsi83_info->workqueue = create_workqueue("sn65dsi83_debug_wq");
	sn65dsi83_info->stable_cnt = 0;
#endif
	mutex_init(&sn65dsi83_info->lock);

	//sn65dsi83_init_script(client, sn65dsi83_info->pdata);

#ifdef DEBUG_REG_VALUE
	for (i = 0; i < 0x40; i++) {
		tmp = i2c_smbus_read_byte_data(client, i);
		pr_err("sn65dsi83[%02X]=%02X\n", i, tmp);
	}
#endif //#ifdef DEBUG_REG_VALUE
#ifdef DEBUG_ERROR_STATUS
	sn65dsi83_debug_error(sn65dsi83_info);
#endif //#ifdef DEBUG_ERROR_STATUS
	return 0;

dt_failed:
	kfree(sn65dsi83_info->pdata);
err_kzalloc:
	kfree(sn65dsi83_info);
	sn65dsi83_info = NULL;
exit:
	return err;
}

int sn65dsi83_panel_enable(int enable) {
	if (sn65dsi83_info == NULL) {
		pr_err("%s: sn65dsi83_info is null\n", __func__);
		return 0;
	}
	if (enable) {
		dev_info(&sn65dsi83_info->client->dev, "%s: on", __func__);
		return sn65dsi83_init_script(sn65dsi83_info->client, sn65dsi83_info->pdata);
	} else {
		dev_info(&sn65dsi83_info->client->dev, "%s: off", __func__);
		return 0;
	}
}

static const struct i2c_device_id sn65dsi83_i2c_id[] = {{SN65DSI83_NAME,0},{}};

static struct of_device_id sn65dsi83_match_table[] = {
	{.compatible = "ti,sn65dsi83"},
	{}
};

static struct i2c_driver sn65dsi83_driver = {
	.driver = {
	.owner = THIS_MODULE,
	.name = SN65DSI83_NAME,
	.of_match_table = sn65dsi83_match_table,
},
.probe = sn65dsi83_driver_probe,
.id_table = sn65dsi83_i2c_id,
//.suspend     = sn65dsi83_suspend,
//.resume      = sn65dsi83_resume,
};

static int __init sn65dsi83_init(void) {
	if (sn65dsi83_info != NULL)
		return 0;
	if (i2c_add_driver(&sn65dsi83_driver) != 0) {
		pr_err("sn65dsi83_init failed to register i2c driver.\n");
	}
	return 0;
}

static void __exit sn65dsi83_exit(void) {
	i2c_del_driver(&sn65dsi83_driver);
}

module_init(sn65dsi83_init);
module_exit(sn65dsi83_exit);

//MODULE_LICENSE("GPL");
//MODULE_DESCRIPTION("I2C sn65dsi83 Driver");

