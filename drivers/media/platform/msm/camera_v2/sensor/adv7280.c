/*
* adv7280.c Analog Devices ADV7280 video decoder driver
* Copyright (c) 2009 Intel Corporation
* Copyright (C) 2013 Cogent Embedded, Inc.
* Copyright (C) 2013 Renesas Solutions Corp.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <linux/module.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <linux/slab.h>
#include <linux/videodev2.h>
#include <media/v4l2-ioctl.h>
#include <media/v4l2-device.h>
#include <media/v4l2-ctrls.h>
#include <linux/mutex.h>
#include <linux/delay.h>
#include <media/soc_camera.h>
#ifdef CONFIG_USE_OF	// Device Tree Machine Description
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/of_i2c.h>
#endif
#include "msm_sensor.h"
#include "cci/msm_cci.h"
#include "io/msm_camera_io_util.h"
#include "io/msm_camera_i2c.h"

#define ADV7280_DEBUG
#ifdef ADV7280_DEBUG
#undef CDBG
#define CDBG(fmt, args...) printk(KERN_ERR "[%s()->Line:%d]: " fmt, __func__, __LINE__, ##args)
#define S_I2C_DBG(fmt, args...) pr_err(fmt, ##args)
#else
#undef CDBG
#define CDBG(fmt, args...) do{;}while(0);
#define S_I2C_DBG(fmt, args...) do { } while (0)
#endif

#define ADV7280_SENSOR_NAME     "adv7280-m" 
static struct msm_sensor_ctrl_t adv7180_sensor_s_ctrl;
static struct msm_camera_i2c_client adv7180_sensor_i2c_client;
static uint16_t regid = 0;

#define ADV7180_STD_AD_PAL_BG_NTSC_J_SECAM              0x0
#define ADV7180_STD_AD_PAL_BG_NTSC_J_SECAM_PED          0x1
#define ADV7180_STD_AD_PAL_N_NTSC_J_SECAM               0x2
#define ADV7180_STD_AD_PAL_N_NTSC_M_SECAM               0x3
#define ADV7180_STD_NTSC_J                              0x4
#define ADV7180_STD_NTSC_M                              0x5
#define ADV7180_STD_PAL60                               0x6
#define ADV7180_STD_NTSC_443                            0x7
#define ADV7180_STD_PAL_BG                              0x8
#define ADV7180_STD_PAL_N                               0x9
#define ADV7180_STD_PAL_M                               0xa
#define ADV7180_STD_PAL_M_PED                           0xb
#define ADV7180_STD_PAL_COMB_N                          0xc
#define ADV7180_STD_PAL_COMB_N_PED                      0xd
#define ADV7180_STD_PAL_SECAM                           0xe
#define ADV7180_STD_PAL_SECAM_PED                       0xf

#define ADV7180_REG_INPUT_CONTROL                       0x0000
#define ADV7180_INPUT_CONTROL_INSEL_MASK                0x0f

#define ADV7182_REG_INPUT_VIDSEL                        0x0002

#define ADV7180_REG_EXTENDED_OUTPUT_CONTROL             0x0004
#define ADV7180_EXTENDED_OUTPUT_CONTROL_NTSCDIS         0xC5

#define ADV7180_REG_AUTODETECT_ENABLE                   0x07
#define ADV7180_AUTODETECT_DEFAULT                      0x7f
/* Contrast */
#define ADV7180_REG_CON         0x0008  /*Unsigned */
#define ADV7180_CON_MIN         0
#define ADV7180_CON_DEF         128
#define ADV7180_CON_MAX         255
/* Brightness*/
#define ADV7180_REG_BRI         0x000a  /*Signed */
#define ADV7180_BRI_MIN         -128
#define ADV7180_BRI_DEF         0
#define ADV7180_BRI_MAX         127
/* Hue */
#define ADV7180_REG_HUE         0x000b  /*Signed, inverted */
#define ADV7180_HUE_MIN         -127
#define ADV7180_HUE_DEF         0
#define ADV7180_HUE_MAX         128

#define ADV7180_REG_CTRL                0x000e
#define ADV7180_CTRL_IRQ_SPACE          0x20

#define ADV7180_REG_PWR_MAN             0x0f
#define ADV7180_PWR_MAN_ON              0x04
#define ADV7180_PWR_MAN_OFF             0x24
#define ADV7180_PWR_MAN_RES             0x80

#define ADV7180_REG_STATUS1             0x0010
#define ADV7180_STATUS1_IN_LOCK         0x01
#define ADV7180_STATUS1_AUTOD_MASK      0x70
#define ADV7180_STATUS1_AUTOD_NTSM_M_J  0x00
#define ADV7180_STATUS1_AUTOD_NTSC_4_43 0x10
#define ADV7180_STATUS1_AUTOD_PAL_M     0x20
#define ADV7180_STATUS1_AUTOD_PAL_60    0x30
#define ADV7180_STATUS1_AUTOD_PAL_B_G   0x40
#define ADV7180_STATUS1_AUTOD_SECAM     0x50
#define ADV7180_STATUS1_AUTOD_PAL_COMB  0x60
#define ADV7180_STATUS1_AUTOD_SECAM_525 0x70

#define ADV7180_REG_IDENT 0x0011
#define ADV7280_VAL_IDENT 0x42

#define ADV7180_REG_ICONF1              0x0040
#define ADV7180_ICONF1_ACTIVE_LOW       0x01
#define ADV7180_ICONF1_PSYNC_ONLY       0x10
#define ADV7180_ICONF1_ACTIVE_TO_CLR    0xC0
/* Saturation */
#define ADV7180_REG_SD_SAT_CB   0x00e3  /*Unsigned */
#define ADV7180_REG_SD_SAT_CR   0x00e4  /*Unsigned */
#define ADV7180_SAT_MIN         0
#define ADV7180_SAT_DEF         128
#define ADV7180_SAT_MAX         255

#define ADV7180_IRQ1_LOCK       0x01
#define ADV7180_IRQ1_UNLOCK     0x02
#define ADV7180_REG_ISR1        0x0042
#define ADV7180_REG_ICR1        0x0043
#define ADV7180_REG_IMR1        0x0044
#define ADV7180_REG_IMR2        0x0048
#define ADV7180_IRQ3_AD_CHANGE  0x08
#define ADV7180_REG_ISR3        0x004A
#define ADV7180_REG_ICR3        0x004B
#define ADV7180_REG_IMR3        0x004C
#define ADV7180_REG_IMR4        0x50

#define ADV7180_REG_NTSC_V_BIT_END      0x00E6
#define ADV7180_NTSC_V_BIT_END_MANUAL_NVEND     0x4F

#define ADV7180_REG_VPP_SLAVE_ADDR      0xFD
#define ADV7180_REG_CSI_SLAVE_ADDR      0xFE

#define ADV7180_REG_FLCONTROL 0x40e0
#define ADV7180_FLCONTROL_FL_ENABLE 0x1

#define ADV7180_CSI_REG_PWRDN   0x00
#define ADV7180_CSI_PWRDN       0x80

#define ADV7180_INPUT_CVBS_AIN1 0x00
#define ADV7180_INPUT_CVBS_AIN2 0x01
#define ADV7180_INPUT_CVBS_AIN3 0x02
#define ADV7180_INPUT_CVBS_AIN4 0x03
#define ADV7180_INPUT_CVBS_AIN5 0x04
#define ADV7180_INPUT_CVBS_AIN6 0x05
#define ADV7180_INPUT_SVIDEO_AIN1_AIN2 0x06
#define ADV7180_INPUT_SVIDEO_AIN3_AIN4 0x07
#define ADV7180_INPUT_SVIDEO_AIN5_AIN6 0x08
#define ADV7180_INPUT_YPRPB_AIN1_AIN2_AIN3 0x09
#define ADV7180_INPUT_YPRPB_AIN4_AIN5_AIN6 0x0a

#define ADV7182_INPUT_CVBS_AIN1 0x00
#define ADV7182_INPUT_CVBS_AIN2 0x01
#define ADV7182_INPUT_CVBS_AIN3 0x02
#define ADV7182_INPUT_CVBS_AIN4 0x03
#define ADV7182_INPUT_CVBS_AIN5 0x04
#define ADV7182_INPUT_CVBS_AIN6 0x05
#define ADV7182_INPUT_CVBS_AIN7 0x06
#define ADV7182_INPUT_CVBS_AIN8 0x07
#define ADV7182_INPUT_SVIDEO_AIN1_AIN2 0x08
#define ADV7182_INPUT_SVIDEO_AIN3_AIN4 0x09
#define ADV7182_INPUT_SVIDEO_AIN5_AIN6 0x0a
#define ADV7182_INPUT_SVIDEO_AIN7_AIN8 0x0b
#define ADV7182_INPUT_YPRPB_AIN1_AIN2_AIN3 0x0c
#define ADV7182_INPUT_YPRPB_AIN4_AIN5_AIN6 0x0d
#define ADV7182_INPUT_DIFF_CVBS_AIN1_AIN2 0x0e
#define ADV7182_INPUT_DIFF_CVBS_AIN3_AIN4 0x0f
#define ADV7182_INPUT_DIFF_CVBS_AIN5_AIN6 0x10
#define ADV7182_INPUT_DIFF_CVBS_AIN7_AIN8 0x11

#define ADV7180_DEFAULT_CSI_I2C_ADDR 0x23
#define ADV7180_DEFAULT_VPP_I2C_ADDR 0x22

#define V4L2_CID_ADV_FAST_SWITCH        (V4L2_CID_USER_ADV7180_BASE + 0x00)

struct adv7180_state;

#define ADV7180_FLAG_RESET_POWERED      BIT(0)
#define ADV7180_FLAG_V2                 BIT(1)
#define ADV7180_FLAG_MIPI_CSI2          BIT(2)
#define ADV7180_FLAG_I2P                BIT(3)

struct adv7180_chip_info {
	unsigned int flags;
	unsigned int valid_input_mask;
	int(*set_std)(struct adv7180_state *st, unsigned int std);
	int(*select_input)(struct adv7180_state *st, unsigned int input);
	int(*init)(struct adv7180_state *state);
};

struct adv7180_state {
	/* V4L stuff */
	struct msm_sensor_ctrl_t *msm_s_ctrl;
	struct v4l2_ctrl_handler ctrl_hdl;
	struct v4l2_mbus_framefmt format;
	v4l2_std_id curr_norm;
	enum v4l2_field field;
	/* I2C clients */
	struct i2c_client *client;		/* Sensor @ 0x20 or 0x21 */
	unsigned int register_page;
	struct i2c_client *csi_client;
	struct i2c_client *vpp_client;

#ifdef CONFIG_MEDIA_CONTROLLER
	struct media_pad pad;
#endif
	struct mutex mutex; /* mutual excl. when accessing chip */

	/* format and configuration */

	/* */
	uint32_t mount_angle;
	int irq;
	bool powered;
	bool autodetect;
	u8 input;

	const struct adv7180_chip_info *chip_info;
};
static struct adv7180_state adv7180_sensor_state;

static int init_device(struct adv7180_state *state);

DEFINE_MSM_MUTEX(adv7180_mut);
static struct msm_sensor_power_setting adv7180_power_setting[] = {
	{
		.seq_type = SENSOR_VREG,
		.seq_val = 0,
		.config_val = 0,
		.delay = 1,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_VANA,
		.config_val = GPIO_OUT_HIGH,
		.delay = 1,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_STANDBY,
		.config_val = GPIO_OUT_HIGH,
		.delay = 5,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_RESET,
		.config_val = GPIO_OUT_LOW,
		.delay = 5,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_RESET,
		.config_val = GPIO_OUT_HIGH,
		.delay = 5,
	},
	{
		.seq_type = SENSOR_I2C_MUX,
		.seq_val = 0,
		.config_val = 0,
		.delay = 0,
	},
};

static struct msm_sensor_power_setting adv7180_power_down_setting[] = {
	//{
	//	.seq_type = SENSOR_VREG,
	//	.seq_val = 0,
	//	.config_val = 0,
	//	.delay = 1,
	//},
	//{
	//	.seq_type = SENSOR_GPIO,
	//	.seq_val = SENSOR_GPIO_VANA,
	//	.config_val = GPIO_OUT_LOW,
	//	.delay = 1,
	//},
	//{
	//	.seq_type = SENSOR_GPIO,
	//	.seq_val = SENSOR_GPIO_STANDBY,
	//	.config_val = GPIO_OUT_LOW,
	//	.delay = 1,
	//},
	{
		.seq_type = SENSOR_I2C_MUX,
		.seq_val = 0,
		.config_val = 0,
		.delay = 0,
	},
};

static struct v4l2_subdev_info adv7180_subdev_info[] = {
	{
		.code = V4L2_MBUS_FMT_YUYV8_2X8,
		.colorspace = V4L2_COLORSPACE_SMPTE170M,
		.fmt = 1,
		.order = 0,
	},
};

static const struct i2c_device_id adv7180_i2c_id[] = {
	{ADV7280_SENSOR_NAME, (kernel_ulong_t)&adv7180_sensor_s_ctrl},
	{}
};

static int32_t adv7180_sensor_power_up(struct msm_sensor_ctrl_t *s_ctrl) {
	int32_t rc = 0;

	CDBG("\n");
	rc = msm_sensor_power_up(s_ctrl);
	adv7180_sensor_state.powered = true;
	rc = init_device(&adv7180_sensor_state);
	return rc;
}

static int32_t adv7180_sensor_power_down(struct msm_sensor_ctrl_t *s_ctrl) {
	int32_t rc = 0;

	CDBG("\n");
	adv7180_sensor_state.powered = false;
	rc = msm_sensor_power_down(s_ctrl);
	return rc;
}

static int32_t adv7180_sensor_match_id(struct msm_sensor_ctrl_t *s_ctrl) {
	int rc = 0;
	struct msm_camera_i2c_client *sensor_i2c_client;
	struct msm_camera_slave_info *slave_info;
	const char *sensor_name;

	if (!s_ctrl) {
		pr_err("%s:%d failed: %p\n", __func__, __LINE__, s_ctrl);
		return -EINVAL;
	}
	CDBG("\n");
	sensor_i2c_client = s_ctrl->sensor_i2c_client;
	slave_info = s_ctrl->sensordata->slave_info;
	sensor_name = s_ctrl->sensordata->sensor_name;

	if (!sensor_i2c_client || !slave_info || !sensor_name) {
		pr_err("%s:%d failed: %p %p %p\n",
			__func__, __LINE__, sensor_i2c_client, slave_info,
			sensor_name);
		return -EINVAL;
	}

	rc = sensor_i2c_client->i2c_func_tbl->i2c_read(
		sensor_i2c_client, slave_info->sensor_id_reg_addr,
		&regid, MSM_CAMERA_I2C_BYTE_DATA);
	//rc = i2c_smbus_read_byte_data(sensor_i2c_client->client, ADV7180_REG_IDENT);
	//regid = rc;
	if (rc < 0) {
		pr_err("%s: %s: read id failed\n", __func__, sensor_name);
		return rc;
	}
	CDBG("read id[0x%x]: 0x%x expected id 0x%x:\n", slave_info->sensor_id_reg_addr, regid, slave_info->sensor_id);

	if (regid != slave_info->sensor_id) {
		pr_err("%s: chipid does not match\n", __func__);
		return -ENODEV;
	}
	return rc;
}

int32_t adv7180_sensor_config(struct msm_sensor_ctrl_t *s_ctrl,
	void __user *argp) {
	struct sensorb_cfg_data *cdata = (struct sensorb_cfg_data *)argp;
	long rc = 0;
	int32_t i = 0;
	mutex_lock(s_ctrl->msm_sensor_mutex);
	CDBG("%s cfgtype = %d\n",
		s_ctrl->sensordata->sensor_name, cdata->cfgtype);
	switch (cdata->cfgtype) {
	case CFG_GET_SENSOR_INFO:
		memcpy(cdata->cfg.sensor_info.sensor_name,
			s_ctrl->sensordata->sensor_name,
			sizeof(cdata->cfg.sensor_info.sensor_name));
		cdata->cfg.sensor_info.session_id =
			s_ctrl->sensordata->sensor_info->session_id;
		for (i = 0; i < SUB_MODULE_MAX; i++)
			cdata->cfg.sensor_info.subdev_id[i] =
			s_ctrl->sensordata->sensor_info->subdev_id[i];
		cdata->cfg.sensor_info.is_mount_angle_valid =
			s_ctrl->sensordata->sensor_info->is_mount_angle_valid;
		cdata->cfg.sensor_info.sensor_mount_angle =
			s_ctrl->sensordata->sensor_info->sensor_mount_angle;
		CDBG("sensor name %s\n",
			cdata->cfg.sensor_info.sensor_name);
		CDBG("session id %d\n",
			cdata->cfg.sensor_info.session_id);
		for (i = 0; i < SUB_MODULE_MAX; i++)
			CDBG("subdev_id[%d] %d\n", i,
				cdata->cfg.sensor_info.subdev_id[i]);
		CDBG("mount angle valid %d value %d\n",
			cdata->cfg.sensor_info.is_mount_angle_valid,
			cdata->cfg.sensor_info.sensor_mount_angle);
		break;
	case CFG_SET_INIT_SETTING:
		CDBG("init setting\n");
		break;
	case CFG_SET_RESOLUTION:
	{
		/*copy from user the desired resoltuion*/
		enum msm_sensor_resolution_t res = MSM_SENSOR_INVALID_RES;
		if (copy_from_user(&res, (void *)cdata->cfg.setting,
			sizeof(enum msm_sensor_resolution_t))) {
			CDBG("failed....\n");
			rc = -EFAULT;
			break;
		}
		break;
	}
	case CFG_SET_STOP_STREAM:
		CDBG("stop stream!!\n");
		break;
	case CFG_SET_START_STREAM:
		CDBG("start stream!!\n");
		break;
	case CFG_GET_SENSOR_INIT_PARAMS:
		cdata->cfg.sensor_init_params.modes_supported =
			s_ctrl->sensordata->sensor_info->modes_supported;
		cdata->cfg.sensor_init_params.position =
			s_ctrl->sensordata->sensor_info->position;
		cdata->cfg.sensor_init_params.sensor_mount_angle =
			s_ctrl->sensordata->sensor_info->sensor_mount_angle;
		CDBG("init params mode %d pos %d mount %d\n",
			cdata->cfg.sensor_init_params.modes_supported,
			cdata->cfg.sensor_init_params.position,
			cdata->cfg.sensor_init_params.sensor_mount_angle);
		break;
	case CFG_SET_SLAVE_INFO:
	{
		struct msm_camera_sensor_slave_info *sensor_slave_info;
		struct msm_camera_power_ctrl_t *p_ctrl;
		uint16_t size;
		int slave_index = 0;
		sensor_slave_info = kmalloc(sizeof(struct msm_camera_sensor_slave_info)
			* 1, GFP_KERNEL);

		if (!sensor_slave_info) {
			CDBG("failed to alloc mem\n");
			rc = -ENOMEM;
			break;
		}
		if (copy_from_user(sensor_slave_info,
			(void *)cdata->cfg.setting,
			sizeof(struct msm_camera_sensor_slave_info))) {
			CDBG("failed!!\n");
			rc = -EFAULT;
			break;
		}
		/* Update sensor slave address */
		if (sensor_slave_info->slave_addr)
			s_ctrl->sensor_i2c_client->cci_client->sid =
			sensor_slave_info->slave_addr >> 1;

		/* Update sensor address type */
		s_ctrl->sensor_i2c_client->addr_type =
			sensor_slave_info->addr_type;

		/* Update power up / down sequence */
		p_ctrl = &s_ctrl->sensordata->power_info;
		size = sensor_slave_info->power_setting_array.size;
		if (p_ctrl->power_setting_size < size) {
			struct msm_sensor_power_setting *tmp;
			tmp = kmalloc(sizeof(struct msm_sensor_power_setting)
				* size, GFP_KERNEL);
			if (!tmp) {
				CDBG("failed to alloc mem\n");
				rc = -ENOMEM;
				break;
			}
			kfree(p_ctrl->power_setting);
			p_ctrl->power_setting = tmp;
		}
		p_ctrl->power_setting_size = size;

		rc = copy_from_user(p_ctrl->power_setting, (void *)
			sensor_slave_info->power_setting_array.power_setting,
			size * sizeof(struct msm_sensor_power_setting));
		if (rc) {
			CDBG("failed\n");
			rc = -EFAULT;
			break;
		}
		for (slave_index = 0; slave_index <
			p_ctrl->power_setting_size; slave_index++) {
			CDBG("i = %d power setting %d %d %ld %d\n",
				slave_index,
				p_ctrl->power_setting[slave_index].seq_type,
				p_ctrl->power_setting[slave_index].seq_val,
				p_ctrl->power_setting[slave_index].config_val,
				p_ctrl->power_setting[slave_index].delay);
		}
		break;
	}
	case CFG_WRITE_I2C_ARRAY:
	{
		struct msm_camera_i2c_reg_setting conf_array;
		struct msm_camera_i2c_reg_array *reg_setting = NULL;

		if (copy_from_user(&conf_array,
			(void *)cdata->cfg.setting,
			sizeof(struct msm_camera_i2c_reg_setting))) {
			CDBG("failed...\n");
			rc = -EFAULT;
			break;
		}

		if (!conf_array.size ||
			conf_array.size > I2C_REG_DATA_MAX) {
			CDBG("failed...\n");
			rc = -EFAULT;
			break;
		}

		reg_setting = kzalloc(conf_array.size *
			(sizeof(struct msm_camera_i2c_reg_array)), GFP_KERNEL);
		if (!reg_setting) {
			CDBG("failed....\n");
			rc = -ENOMEM;
			break;
		}
		if (copy_from_user(reg_setting, (void *)conf_array.reg_setting,
			conf_array.size *
			sizeof(struct msm_camera_i2c_reg_array))) {
			CDBG("failed.....\n");
			kfree(reg_setting);
			rc = -EFAULT;
			break;
		}

		conf_array.reg_setting = reg_setting;
		rc = s_ctrl->sensor_i2c_client->i2c_func_tbl->i2c_write_table(
			s_ctrl->sensor_i2c_client, &conf_array);
		kfree(reg_setting);
		break;
	}
	case CFG_WRITE_I2C_SEQ_ARRAY:
	{
		struct msm_camera_i2c_seq_reg_setting conf_array;
		struct msm_camera_i2c_seq_reg_array *reg_setting = NULL;

		if (copy_from_user(&conf_array,
			(void *)cdata->cfg.setting,
			sizeof(struct msm_camera_i2c_seq_reg_setting))) {
			CDBG("failed...\n");
			rc = -EFAULT;
			break;
		}

		if (!conf_array.size ||
			conf_array.size > I2C_SEQ_REG_DATA_MAX) {
			CDBG("failed...\n");
			rc = -EFAULT;
			break;
		}

		reg_setting = kzalloc(conf_array.size *
			(sizeof(struct msm_camera_i2c_seq_reg_array)),
			GFP_KERNEL);
		if (!reg_setting) {
			CDBG("failed...\n");
			rc = -ENOMEM;
			break;
		}
		if (copy_from_user(reg_setting, (void *)conf_array.reg_setting,
			conf_array.size *
			sizeof(struct msm_camera_i2c_seq_reg_array))) {
			CDBG("failed...\n");
			kfree(reg_setting);
			rc = -EFAULT;
			break;
		}

		conf_array.reg_setting = reg_setting;
		rc = s_ctrl->sensor_i2c_client->i2c_func_tbl->
			i2c_write_seq_table(s_ctrl->sensor_i2c_client,
				&conf_array);
		kfree(reg_setting);
		break;
	}

	case CFG_POWER_UP:
		if (s_ctrl->func_tbl->sensor_power_up)
			rc = s_ctrl->func_tbl->sensor_power_up(s_ctrl);
		else
			rc = -EFAULT;
		break;

	case CFG_POWER_DOWN:
		if (s_ctrl->func_tbl->sensor_power_down)
			rc = s_ctrl->func_tbl->sensor_power_down(s_ctrl);
		else
			rc = -EFAULT;
		break;

	case CFG_SET_STOP_STREAM_SETTING:
	{
		struct msm_camera_i2c_reg_setting *stop_setting =
			&s_ctrl->stop_setting;
		struct msm_camera_i2c_reg_array *reg_setting = NULL;
		if (copy_from_user(stop_setting, (void *)cdata->cfg.setting,
			sizeof(struct msm_camera_i2c_reg_setting))) {
			CDBG("failed...\n");
			rc = -EFAULT;
			break;
		}

		reg_setting = stop_setting->reg_setting;
		stop_setting->reg_setting = kzalloc(stop_setting->size *
			(sizeof(struct msm_camera_i2c_reg_array)), GFP_KERNEL);
		if (!stop_setting->reg_setting) {
			CDBG("failed...\n");
			rc = -ENOMEM;
			break;
		}
		if (copy_from_user(stop_setting->reg_setting,
			(void *)reg_setting, stop_setting->size *
			sizeof(struct msm_camera_i2c_reg_array))) {
			CDBG("failed...\n");
			kfree(stop_setting->reg_setting);
			stop_setting->reg_setting = NULL;
			stop_setting->size = 0;
			rc = -EFAULT;
			break;
		}
		break;
	}
	case CFG_SET_STREAM_TYPE:
	{
		enum msm_camera_stream_type_t stream_type = MSM_CAMERA_STREAM_INVALID;
		if (copy_from_user(&stream_type, (void *)cdata->cfg.setting,
			sizeof(enum msm_camera_stream_type_t))) {
			CDBG("failed...\n");
			rc = -EFAULT;
			break;
		}
		s_ctrl->camera_stream_type = stream_type;
		break;
	}
	case CFG_SET_SATURATION:
		break;
	case CFG_SET_CONTRAST:
		break;
	case CFG_SET_SHARPNESS:
		break;
	case CFG_SET_AUTOFOCUS:
		/* TO-DO: set the Auto Focus */
		CDBG("Setting Auto Focus\n");
		break;
	case CFG_CANCEL_AUTOFOCUS:
		/* TO-DO: Cancel the Auto Focus */
		CDBG("Cancelling Auto Focus\n");
		break;
	case CFG_SET_ISO:
		break;
	case CFG_SET_EXPOSURE_COMPENSATION:
		break;
	case CFG_SET_EFFECT:
		break;
	case CFG_SET_ANTIBANDING:
		break;
	case CFG_SET_BESTSHOT_MODE:
		break;
	case CFG_SET_WHITE_BALANCE:
		break;
	default:
		rc = -EFAULT;
		break;
	}

	mutex_unlock(s_ctrl->msm_sensor_mutex);

	return rc;
}
static struct msm_camera_i2c_client adv7180_sensor_i2c_client = {
	.addr_type = MSM_CAMERA_I2C_BYTE_ADDR,
};

static struct msm_sensor_fn_t adv7180_func_tbl = {
	.sensor_config = adv7180_sensor_config,
	.sensor_power_up = adv7180_sensor_power_up,
	.sensor_power_down = adv7180_sensor_power_down,
	.sensor_match_id = adv7180_sensor_match_id,
};

static struct msm_sensor_ctrl_t adv7180_sensor_s_ctrl = {
	.sensor_i2c_client = &adv7180_sensor_i2c_client,
	.power_setting_array.power_setting = adv7180_power_setting,
	.power_setting_array.size = ARRAY_SIZE(adv7180_power_setting),
	.power_setting_array.power_down_setting = adv7180_power_down_setting,
	.power_setting_array.size_down = ARRAY_SIZE(adv7180_power_down_setting),
	.msm_sensor_mutex = &adv7180_mut,
	.sensor_v4l2_subdev_info = adv7180_subdev_info,
	.sensor_v4l2_subdev_info_size = ARRAY_SIZE(adv7180_subdev_info),
	.func_tbl = &adv7180_func_tbl,
};

static struct adv7180_state adv7180_sensor_state = {
	.msm_s_ctrl = &adv7180_sensor_s_ctrl,
};

static int adv7180_i2c_rxdata(struct i2c_client *client, uint8_t reg) {
	int32_t rc = 0;
	uint16_t saddr = client->addr >> 1;
	uint8_t value;
	struct i2c_msg msgs[] = {
		{
			.addr = saddr,
			.flags = 0,
			.len = 1,
			.buf = &reg,
		},
		{
			.addr = saddr,
			.flags = I2C_M_RD,
			.len = 1,
			.buf = &value,
		},
	};
	rc = i2c_transfer(client->adapter, msgs, 2);
	S_I2C_DBG("adv7180_i2c_rxdata addr=0x%x reg=0x%x val=0x%x ret=%d\n", saddr, reg, value, rc);
	if (rc < 0)
		return rc;
	return (int)value & 0xff;
}

static int32_t adv7180_i2c_txdata(struct i2c_client *client, uint8_t reg, uint8_t value) {
	int32_t rc = 0;
	uint16_t saddr = client->addr >> 1;
	uint8_t txdata[2] = {reg, value};
	struct i2c_msg msgs[] = {
		{
			.addr = saddr,
			.flags = 0,
			.len = 2,
			.buf = txdata,
		},
	};
	rc = i2c_transfer(client->adapter, msgs, 1);
	S_I2C_DBG("adv7180_i2c_txdata addr=0x%x reg=0x%x val=0x%x ret=%d\n", saddr, reg, value, rc);
	return rc;
}

static int adv7180_select_page(struct adv7180_state *state, unsigned int page) {
	int rc = 0;
	if (state->register_page != page) {
		rc = adv7180_i2c_txdata(state->client, ADV7180_REG_CTRL, page);
		state->register_page = page;
	}
	return rc;
}

static int adv7180_write(struct adv7180_state *state, unsigned int reg, unsigned int value) {
	lockdep_assert_held(&state->mutex);
	adv7180_select_page(state, reg >> 8);
	return adv7180_i2c_txdata(state->client, reg & 0xff, value);
}

static int adv7180_read(struct adv7180_state *state, unsigned int reg) {
	lockdep_assert_held(&state->mutex);
	adv7180_select_page(state, reg >> 8);
	return adv7180_i2c_rxdata(state->client, reg & 0xff);
}

static int adv7180_csi_write(struct adv7180_state *state, unsigned int reg,
	unsigned int value) {
	int rc = i2c_smbus_write_byte_data(state->csi_client, reg, value);
	if (rc < 0)
		pr_err("%s: addr=0x%x, reg=0x%x is failed. code=%d\n", __func__, state->csi_client->addr, reg, rc);
	return rc;
}

static int adv7180_set_video_standard(struct adv7180_state *state,
	unsigned int std) {
	return state->chip_info->set_std(state, std);
}

static int adv7180_vpp_write(struct adv7180_state *state, unsigned int reg,
	unsigned int value) {
	int rc = i2c_smbus_write_byte_data(state->vpp_client, reg, value);
	if (rc < 0)
		pr_err("%s: addr=0x%x, reg=0x%x is failed. code=%d\n", __func__, state->csi_client->addr, reg, rc);
	return rc;
}

static v4l2_std_id adv7180_std_to_v4l2(u8 status1) {
	/* in case V4L2_IN_ST_NO_SIGNAL */
	if (!(status1 & ADV7180_STATUS1_IN_LOCK))
		return V4L2_STD_UNKNOWN;

	switch (status1 & ADV7180_STATUS1_AUTOD_MASK) {
	case ADV7180_STATUS1_AUTOD_NTSM_M_J:
		return V4L2_STD_NTSC;
	case ADV7180_STATUS1_AUTOD_NTSC_4_43:
		return V4L2_STD_NTSC_443;
	case ADV7180_STATUS1_AUTOD_PAL_M:
		return V4L2_STD_PAL_M;
	case ADV7180_STATUS1_AUTOD_PAL_60:
		return V4L2_STD_PAL_60;
	case ADV7180_STATUS1_AUTOD_PAL_B_G:
		return V4L2_STD_PAL;
	case ADV7180_STATUS1_AUTOD_SECAM:
		return V4L2_STD_SECAM;
	case ADV7180_STATUS1_AUTOD_PAL_COMB:
		return V4L2_STD_PAL_Nc | V4L2_STD_PAL_N;
	case ADV7180_STATUS1_AUTOD_SECAM_525:
		return V4L2_STD_SECAM;
	default:
		return V4L2_STD_UNKNOWN;
	}
}

static int v4l2_std_to_adv7180(v4l2_std_id std) {
	if (std == V4L2_STD_PAL_60)
		return ADV7180_STD_PAL60;
	if (std == V4L2_STD_NTSC_443)
		return ADV7180_STD_NTSC_443;
	if (std == V4L2_STD_PAL_N)
		return ADV7180_STD_PAL_N;
	if (std == V4L2_STD_PAL_M)
		return ADV7180_STD_PAL_M;
	if (std == V4L2_STD_PAL_Nc)
		return ADV7180_STD_PAL_COMB_N;

	if (std & V4L2_STD_PAL)
		return ADV7180_STD_PAL_BG;
	if (std & V4L2_STD_NTSC)
		return ADV7180_STD_NTSC_M;
	if (std & V4L2_STD_SECAM)
		return ADV7180_STD_PAL_SECAM;

	return -EINVAL;
}

static u32 adv7180_status_to_v4l2(u8 status1) {
	if (!(status1 & ADV7180_STATUS1_IN_LOCK))
		return V4L2_IN_ST_NO_SIGNAL;

	return 0;
}

static int __adv7180_status(struct adv7180_state *state, u32 *status,
	v4l2_std_id *std) {
	int status1 = adv7180_read(state, ADV7180_REG_STATUS1);

	if (status1 < 0)
		return status1;

	if (status)
		*status = adv7180_status_to_v4l2(status1);
	if (std)
		*std = adv7180_std_to_v4l2(status1);

	return 0;
}

//inline struct adv7180_state *to_state(const struct i2c_client *client) {
//	return container_of(
//		container_of(
//			container_of(i2c_get_clientdata(client), struct msm_sd_subdev, sd),
//			struct msm_sensor_ctrl_t, msm_sd),
//		struct adv7180_state, msm_s_ctrl);
//}

/* ---------- functions for register access on i2c ---------- */

/* ---------- functions for user land interface ---------- */

/* ---------- functions for switching camera power ---------- */
static int adv7180_set_power(struct adv7180_state *state, bool on) {
	u8 val;
	int ret;

	if (on)
		val = ADV7180_PWR_MAN_ON;
	else
		val = ADV7180_PWR_MAN_OFF;

	ret = adv7180_write(state, ADV7180_REG_PWR_MAN, val);
	if (ret)
		return ret;

	if (state->chip_info->flags & ADV7180_FLAG_MIPI_CSI2) {
		if (on) {
			adv7180_csi_write(state, 0xDE, 0x02);
			adv7180_csi_write(state, 0xD2, 0xF7);
			adv7180_csi_write(state, 0xD8, 0x65);
			adv7180_csi_write(state, 0xE0, 0x09);
			adv7180_csi_write(state, 0x2C, 0x00);
			if (state->field == V4L2_FIELD_NONE)
				adv7180_csi_write(state, 0x1D, 0x80);
			adv7180_csi_write(state, 0x00, 0x00);
		} else {
			adv7180_csi_write(state, 0x00, 0x80);
		}
	}

	return 0;
}

static int adv7180_s_power(struct v4l2_subdev *sd, int on) {
	//struct i2c_client *client = v4l2_get_subdevdata(sd);
	//struct adv7180_state *state = to_state(client);
	struct adv7180_state *state = &adv7180_sensor_state;
	int ret = mutex_lock_interruptible(&state->mutex);
	if (ret)
		return ret;
	CDBG("on=%d\n", on);

	ret = adv7180_set_power(state, on);
	if (ret == 0)
		state->powered = on;

	mutex_unlock(&state->mutex);
	return ret;
}

//static int adv7180_subscribe_event(struct v4l2_subdev *sd,
//	struct v4l2_fh *fh,
//	struct v4l2_event_subscription *sub) {
//	switch (sub->type) {
//	case V4L2_EVENT_SOURCE_CHANGE:
//		return v4l2_src_change_event_subdev_subscribe(sd, fh, sub);
//	case V4L2_EVENT_CTRL:
//		return v4l2_ctrl_subdev_subscribe_event(sd, fh, sub);
//	default:
//		return -EINVAL;
//	}
//}

/* ---------- V4L format handling ---------- */

/* SoC camera ops (struct v4l2_subdev_ops/struct v4l2_subdev_video_ops) */

static int adv7180_program_std(struct adv7180_state *state) {
	int ret;

	if (state->autodetect) {
		ret = adv7180_set_video_standard(state,
			ADV7180_STD_AD_PAL_BG_NTSC_J_SECAM);
		if (ret < 0)
			return ret;

		__adv7180_status(state, NULL, &state->curr_norm);
	} else {
		ret = v4l2_std_to_adv7180(state->curr_norm);
		if (ret < 0)
			return ret;

		ret = adv7180_set_video_standard(state, ret);
		if (ret < 0)
			return ret;
	}
	return 0;
}

static int adv7180_querystd(struct v4l2_subdev *sd, v4l2_std_id *std) {
	//struct i2c_client *client = v4l2_get_subdevdata(sd);
	//struct adv7180_state *state = to_state(client);
	struct adv7180_state *state = &adv7180_sensor_state;
	int err = mutex_lock_interruptible(&state->mutex);
	if (err)
		return err;
	CDBG("\n");

	/* when we are interrupt driven we know the state */
	if (!state->autodetect || state->irq > 0)
		*std = state->curr_norm;
	else
		err = __adv7180_status(state, NULL, std);

	mutex_unlock(&state->mutex);
	return err;
}

static int adv7180_s_routing(struct v4l2_subdev *sd, u32 input,
	u32 output, u32 config) {
	//struct i2c_client *client = v4l2_get_subdevdata(sd);
	//struct adv7180_state *state = to_state(client);
	struct adv7180_state *state = &adv7180_sensor_state;
	int ret = mutex_lock_interruptible(&state->mutex);
	if (ret)
		return ret;
	CDBG("\n");

	if (input > 31 || !(BIT(input) & state->chip_info->valid_input_mask)) {
		ret = -EINVAL;
		goto out;
	}

	ret = state->chip_info->select_input(state, input);

	if (ret == 0)
		state->input = input;
out:
	mutex_unlock(&state->mutex);
	return ret;
}

static int adv7180_g_input_status(struct v4l2_subdev *sd, u32 *status) {
	//struct i2c_client *client = v4l2_get_subdevdata(sd);
	//struct adv7180_state *state = to_state(client);
	struct adv7180_state *state = &adv7180_sensor_state;
	int ret = mutex_lock_interruptible(&state->mutex);
	if (ret)
		return ret;
	CDBG("\n");

	ret = __adv7180_status(state, status, NULL);
	mutex_unlock(&state->mutex);
	return ret;
}

static int adv7180_s_std(struct v4l2_subdev *sd, v4l2_std_id std) {
	//struct i2c_client *client = v4l2_get_subdevdata(sd);
	//struct adv7180_state *state = to_state(client);
	struct adv7180_state *state = &adv7180_sensor_state;
	int ret = mutex_lock_interruptible(&state->mutex);
	if (ret)
		return ret;
	CDBG("\n");

	/* all standards -> autodetect */
	if (std == V4L2_STD_ALL) {
		state->autodetect = true;
	} else {
		/* Make sure we can support this std */
		ret = v4l2_std_to_adv7180(std);
		if (ret < 0)
			goto out;

		state->curr_norm = std;
		state->autodetect = false;
	}
	ret = adv7180_program_std(state);
out:
	mutex_unlock(&state->mutex);
	return ret;
}

//static int adv7180_g_std(struct v4l2_subdev *sd, v4l2_std_id *norm) {
//	struct i2c_client *client = v4l2_get_subdevdata(sd);
//	struct adv7180_state *state = to_state(client);
//
//	*norm = state->curr_norm;
//
//	return 0;
//}

static int adv7180_g_mbus_config(struct v4l2_subdev *sd,
	struct v4l2_mbus_config *cfg) {
	//struct i2c_client *client = v4l2_get_subdevdata(sd);
	//struct adv7180_state *state = to_state(client);
	struct adv7180_state *state = &adv7180_sensor_state;
	CDBG("\n");

	if (state->chip_info->flags & ADV7180_FLAG_MIPI_CSI2) {
		cfg->type = V4L2_MBUS_CSI2;
		cfg->flags = V4L2_MBUS_CSI2_1_LANE |
			V4L2_MBUS_CSI2_CHANNEL_0 |
			V4L2_MBUS_CSI2_CONTINUOUS_CLOCK;
	} else {
		/*
		* The ADV7180 sensor supports BT.601/656 output modes.
		* The BT.656 is default and not yet configurable by s/w.
		*/
		cfg->flags = V4L2_MBUS_MASTER | V4L2_MBUS_PCLK_SAMPLE_RISING |
			V4L2_MBUS_DATA_ACTIVE_HIGH;
		cfg->type = V4L2_MBUS_BT656;
	}

	return 0;
}

//static int adv7180_cropcap(struct v4l2_subdev *sd, struct v4l2_cropcap *cropcap) {
//	struct i2c_client *client = v4l2_get_subdevdata(sd);
//	struct adv7180_state *state = to_state(client);
//
//	if (state->curr_norm & V4L2_STD_525_60) {
//		cropcap->pixelaspect.numerator = 11;
//		cropcap->pixelaspect.denominator = 10;
//	} else {
//		cropcap->pixelaspect.numerator = 54;
//		cropcap->pixelaspect.denominator = 59;
//	}
//
//	return 0;
//}
//
//static int adv7180_g_tvnorms(struct v4l2_subdev *sd, v4l2_std_id *norm) {
//	*norm = V4L2_STD_ALL;
//	return 0;
//}
//
//static int adv7180_s_stream(struct v4l2_subdev *sd, int enable) {
//	struct i2c_client *client = v4l2_get_subdevdata(sd);
//	struct adv7180_state *state = to_state(client);
//	int ret;
//
//	/* It's always safe to stop streaming, no need to take the lock */
//	if (!enable) {
//		state->streaming = enable;
//		return 0;
//	}
//
//	/* Must wait until querystd released the lock */
//	ret = mutex_lock_interruptible(&state->mutex);
//	if (ret)
//		return ret;
//	state->streaming = enable;
//	mutex_unlock(&state->mutex);
//	return 0;
//}

inline struct v4l2_subdev *adv7180_get_subdev(struct adv7180_state *dev) {
	return &dev->msm_s_ctrl->msm_sd.sd;
}

static int adv7180_mbus_fmt(struct v4l2_subdev *sd,
	struct v4l2_mbus_framefmt *fmt) {
	//struct i2c_client *client = v4l2_get_subdevdata(sd);
	//struct adv7180_state *state = to_state(client);
	struct adv7180_state *state = &adv7180_sensor_state;
	CDBG("\n");

	fmt->code = V4L2_MBUS_FMT_YUYV8_2X8;
	fmt->colorspace = V4L2_COLORSPACE_SMPTE170M;
	fmt->width = 720;
	fmt->height = state->curr_norm & V4L2_STD_525_60 ? 480 : 576;

	return 0;
}

#ifdef CONFIG_MEDIA_CONTROLLER
/* --------------------------------------------------------------------------
* V4L2 subdev pad operations
*/
static int adv7180_set_field_mode(struct adv7180_state *state) {
	if (!(state->chip_info->flags & ADV7180_FLAG_I2P))
		return 0;

	if (state->field == V4L2_FIELD_NONE) {
		if (state->chip_info->flags & ADV7180_FLAG_MIPI_CSI2) {
			adv7180_csi_write(state, 0x01, 0x20);
			adv7180_csi_write(state, 0x02, 0x28);
			adv7180_csi_write(state, 0x03, 0x38);
			adv7180_csi_write(state, 0x04, 0x30);
			adv7180_csi_write(state, 0x05, 0x30);
			adv7180_csi_write(state, 0x06, 0x80);
			adv7180_csi_write(state, 0x07, 0x70);
			adv7180_csi_write(state, 0x08, 0x50);
		}
		adv7180_vpp_write(state, 0xa3, 0x00);
		adv7180_vpp_write(state, 0x5b, 0x00);
		adv7180_vpp_write(state, 0x55, 0x80);
	} else {
		if (state->chip_info->flags & ADV7180_FLAG_MIPI_CSI2) {
			adv7180_csi_write(state, 0x01, 0x18);
			adv7180_csi_write(state, 0x02, 0x18);
			adv7180_csi_write(state, 0x03, 0x30);
			adv7180_csi_write(state, 0x04, 0x20);
			adv7180_csi_write(state, 0x05, 0x28);
			adv7180_csi_write(state, 0x06, 0x40);
			adv7180_csi_write(state, 0x07, 0x58);
			adv7180_csi_write(state, 0x08, 0x30);
		}
		adv7180_vpp_write(state, 0xa3, 0x70);
		adv7180_vpp_write(state, 0x5b, 0x80);
		adv7180_vpp_write(state, 0x55, 0x00);
	}

	return 0;
}

/*
* adv7180_enum_mbus_code - V4L2 sensor interface handler for pad_ops
* @subdev: pointer to standard V4L2 sub-device structure
* @qctrl: standard V4L2 VIDIOC_QUERYCTRL ioctl structure
*
* If the requested control is supported, returns the control information
* from the video_control[] array.  Otherwise, returns -EINVAL if the
* control is not supported.
*/
static int adv7180_enum_mbus_code(struct v4l2_subdev *sd,
	struct v4l2_subdev_fh *fh,
	struct v4l2_subdev_mbus_code_enum *code) {
	CDBG("\n");
	if (code->index != 0)
		return -EINVAL;
	code->code = V4L2_MBUS_FMT_YUYV8_2X8;
	return 0;
}

static int adv7180_set_pad_format(struct v4l2_subdev *sd, struct v4l2_subdev_fh *fh, struct v4l2_subdev_format *format) {
	//struct i2c_client *client = v4l2_get_subdevdata(sd);
	//struct adv7180_state *state = to_state(client);
	struct adv7180_state *state = &adv7180_sensor_state;
	struct v4l2_mbus_framefmt *framefmt;
	CDBG("\n");

	switch (format->format.field) {
	case V4L2_FIELD_NONE:
		if (!(state->chip_info->flags & ADV7180_FLAG_I2P))
			format->format.field = V4L2_FIELD_INTERLACED;
		break;
	default:
		format->format.field = V4L2_FIELD_INTERLACED;
		break;
	}

	if (format->which == V4L2_SUBDEV_FORMAT_ACTIVE) {
		framefmt = &format->format;
		if (state->field != format->format.field) {
			state->field = format->format.field;
			adv7180_set_power(state, false);
			adv7180_set_field_mode(state);
			adv7180_set_power(state, true);
		}
	} else {
		framefmt = v4l2_subdev_get_try_format(fh, 0);
		*framefmt = format->format;
	}

	return adv7180_mbus_fmt(sd, framefmt);
}

static int adv7180_get_pad_format(struct v4l2_subdev *sd, struct v4l2_subdev_fh *fh, struct v4l2_subdev_format *format) {
	//struct i2c_client *client = v4l2_get_subdevdata(sd);
	//struct adv7180_state *state = to_state(client);
	struct adv7180_state *state = &adv7180_sensor_state;
	CDBG("\n");

	if (format->which == V4L2_SUBDEV_FORMAT_TRY) {
		format->format = *v4l2_subdev_get_try_format(fh, 0);
	} else {
		adv7180_mbus_fmt(sd, &format->format);
		format->format.field = state->field;
	}

	return 0;
}
#endif /* CONFIG_MEDIA_CONTROLLER */

/* ---------- V4L operations ---------- */
static struct v4l2_subdev_core_ops adv7180_subdev_core_ops = {
	.s_power = adv7180_s_power,
	//.subscribe_event = adv7180_subscribe_event,
	//.unsubscribe_event = v4l2_event_subdev_unsubscribe,
	//.s_power = hsm_s_power,
	//.ioctl = hsm_ioctl,
	//.init = hsm_init,
	//.g_ctrl = hsm_subdev_g_ctrl,
	//.s_ctrl = hsm_subdev_s_ctrl,
};

static struct v4l2_subdev_video_ops adv7180_subdev_video_ops = {
	.s_std_output = adv7180_s_std,
	//.g_std_output = adv7180_g_std,
	.querystd = adv7180_querystd,
	.g_input_status = adv7180_g_input_status,
	.s_routing = adv7180_s_routing,
	.g_mbus_config = adv7180_g_mbus_config,
	//.cropcap = adv7180_cropcap,
	//.g_tvnorms_output = adv7180_g_tvnorms,
	//.s_stream = adv7180_s_stream,
	//.enum_mbus_fmt = hsm_enum_fmt,  /* enum_mbus_fmt: enumerate pixel formats, provided by a video data source */
	//.try_mbus_fmt = hsm_try_fmt,    /* try_mbus_fmt: try to set a pixel format on a video data source */
	//.g_mbus_fmt = hsm_g_fmt,        /* g_mbus_fmt: get the current pixel format, provided by a video data source */
	//.s_mbus_fmt = hsm_s_fmt,        /* s_mbus_fmt: set a pixel format on a video data source */
	//.enum_framesizes = hsm_enum_framesizes,
	//.cropcap = hsm_cropcap,
	//.g_crop = hsm_g_crop,
	//.s_stream = hsm_s_stream,
};

#ifdef CONFIG_MEDIA_CONTROLLER
static const struct v4l2_subdev_pad_ops adv7180_subdev_pad_ops = {
	.enum_mbus_code = adv7180_enum_mbus_code,
	.set_fmt = adv7180_set_pad_format,
	.get_fmt = adv7180_get_pad_format,
	//.enum_mbus_code = hsm_enum_mbus_code,
	//.enum_frame_size = hsm_enum_frame_size,
	//.get_fmt = hsm_get_pad_format,
	//.set_fmt = hsm_set_pad_format,
};
#endif

static struct v4l2_subdev_ops adv7180_subdev_ops = {
	.core = &adv7180_subdev_core_ops,
	.video = &adv7180_subdev_video_ops,
#ifdef CONFIG_MEDIA_CONTROLLER
	.pad = &adv7180_subdev_pad_ops,
#endif
};

#ifdef CONFIG_MEDIA_CONTROLLER
#ifndef MEDIA_PAD_FLAG_OUTPUT
#define MEDIA_PAD_FLAG_OUTPUT	MEDIA_PAD_FL_SOURCE
#endif
#endif

static irqreturn_t adv7180_irq(int irq, void *devid) {
	struct adv7180_state *state = devid;
	u8 isr3;

	mutex_lock(&state->mutex);
	isr3 = adv7180_read(state, ADV7180_REG_ISR3);
	/* clear */
	adv7180_write(state, ADV7180_REG_ICR3, isr3);

	if (isr3 & ADV7180_IRQ3_AD_CHANGE && state->autodetect)
		__adv7180_status(state, NULL, &state->curr_norm);
	mutex_unlock(&state->mutex);

	return IRQ_HANDLED;
}

static int adv7182_init(struct adv7180_state *state) {
	CDBG("\n");
	if (state->chip_info->flags & ADV7180_FLAG_MIPI_CSI2)
		adv7180_write(state, ADV7180_REG_CSI_SLAVE_ADDR,
			ADV7180_DEFAULT_CSI_I2C_ADDR << 1);

	if (state->chip_info->flags & ADV7180_FLAG_I2P)
		adv7180_write(state, ADV7180_REG_VPP_SLAVE_ADDR,
			ADV7180_DEFAULT_VPP_I2C_ADDR << 1);

	if (state->chip_info->flags & ADV7180_FLAG_V2) {
		/* ADI recommended writes for improved video quality */
		adv7180_write(state, 0x0080, 0x51);
		adv7180_write(state, 0x0081, 0x51);
		adv7180_write(state, 0x0082, 0x68);
	}

	/* ADI required writes */
	if (state->chip_info->flags & ADV7180_FLAG_MIPI_CSI2) {
		adv7180_write(state, 0x0003, 0x4e);
		adv7180_write(state, 0x0004, 0x57);
		adv7180_write(state, 0x001d, 0xc0);
	} else {
		if (state->chip_info->flags & ADV7180_FLAG_V2)
			adv7180_write(state, 0x0004, 0x17);
		else
			adv7180_write(state, 0x0004, 0x07);
		adv7180_write(state, 0x0003, 0x0c);
		adv7180_write(state, 0x001d, 0x40);
	}

	adv7180_write(state, 0x0013, 0x00);

	return 0;
}

//#define to_adv7180_sd(_ctrl) (&container_of(_ctrl->handler, struct adv7180_state, ctrl_hdl)->sd)

static int adv7180_s_ctrl(struct v4l2_ctrl *ctrl) {
	//struct v4l2_subdev *sd = to_adv7180_sd(ctrl);
	//struct adv7180_state *state = container_of(sd, struct adv7180_state, sd);
	struct adv7180_state *state = &adv7180_sensor_state;
	int ret = mutex_lock_interruptible(&state->mutex);
	int val;

	if (ret)
		return ret;
	val = ctrl->val;
	switch (ctrl->id) {
	case V4L2_CID_BRIGHTNESS:
		ret = adv7180_write(state, ADV7180_REG_BRI, val);
		break;
	case V4L2_CID_HUE:
		/*Hue is inverted according to HSL chart */
		ret = adv7180_write(state, ADV7180_REG_HUE, -val);
		break;
	case V4L2_CID_CONTRAST:
		ret = adv7180_write(state, ADV7180_REG_CON, val);
		break;
	case V4L2_CID_SATURATION:
		/*
		*This could be V4L2_CID_BLUE_BALANCE/V4L2_CID_RED_BALANCE
		*Let's not confuse the user, everybody understands saturation
		*/
		ret = adv7180_write(state, ADV7180_REG_SD_SAT_CB, val);
		if (ret < 0)
			break;
		ret = adv7180_write(state, ADV7180_REG_SD_SAT_CR, val);
		break;
	case V4L2_CID_ADV_FAST_SWITCH:
		if (ctrl->val) {
			/* ADI required write */
			adv7180_write(state, 0x80d9, 0x44);
			adv7180_write(state, ADV7180_REG_FLCONTROL,
				ADV7180_FLCONTROL_FL_ENABLE);
		} else {
			/* ADI required write */
			adv7180_write(state, 0x80d9, 0xc4);
			adv7180_write(state, ADV7180_REG_FLCONTROL, 0x00);
		}
		break;
	default:
		ret = -EINVAL;
	}

	mutex_unlock(&state->mutex);
	return ret;
}

static const struct v4l2_ctrl_ops adv7180_ctrl_ops = {
	.s_ctrl = adv7180_s_ctrl,
};

static const struct v4l2_ctrl_config adv7180_ctrl_fast_switch = {
	.ops = &adv7180_ctrl_ops,
	.id = V4L2_CID_ADV_FAST_SWITCH,
	.name = "Fast Switching",
	.type = V4L2_CTRL_TYPE_BOOLEAN,
	.min = 0,
	.max = 1,
	.step = 1,
};

static int adv7180_init_controls(struct adv7180_state *state) {
	/*
	v4l2_ctrl_handler_init(&state->ctrl_hdl, 4);

	v4l2_ctrl_new_std(&state->ctrl_hdl, &adv7180_ctrl_ops,
		V4L2_CID_BRIGHTNESS, ADV7180_BRI_MIN,
		ADV7180_BRI_MAX, 1, ADV7180_BRI_DEF);
	v4l2_ctrl_new_std(&state->ctrl_hdl, &adv7180_ctrl_ops,
		V4L2_CID_CONTRAST, ADV7180_CON_MIN,
		ADV7180_CON_MAX, 1, ADV7180_CON_DEF);
	v4l2_ctrl_new_std(&state->ctrl_hdl, &adv7180_ctrl_ops,
		V4L2_CID_SATURATION, ADV7180_SAT_MIN,
		ADV7180_SAT_MAX, 1, ADV7180_SAT_DEF);
	v4l2_ctrl_new_std(&state->ctrl_hdl, &adv7180_ctrl_ops,
		V4L2_CID_HUE, ADV7180_HUE_MIN,
		ADV7180_HUE_MAX, 1, ADV7180_HUE_DEF);
	v4l2_ctrl_new_custom(&state->ctrl_hdl, &adv7180_ctrl_fast_switch, NULL);
	
	adv7180_get_subdev(state)->ctrl_handler = &state->ctrl_hdl;
	if (state->ctrl_hdl.error) {
		int err = state->ctrl_hdl.error;

		v4l2_ctrl_handler_free(&state->ctrl_hdl);
		return err;
	}
	v4l2_ctrl_handler_setup(&state->ctrl_hdl);
	*/
	return 0;
}

static void adv7180_exit_controls(struct adv7180_state *state) {
	v4l2_ctrl_handler_free(&state->ctrl_hdl);
}

static int adv7182_set_std(struct adv7180_state *state, unsigned int std) {
	return adv7180_write(state, ADV7182_REG_INPUT_VIDSEL, std << 4);
}

enum adv7182_input_type {
	ADV7182_INPUT_TYPE_CVBS,
	ADV7182_INPUT_TYPE_DIFF_CVBS,
	ADV7182_INPUT_TYPE_SVIDEO,
	ADV7182_INPUT_TYPE_YPBPR,
};

static enum adv7182_input_type adv7182_get_input_type(unsigned int input) {
	switch (input) {
	case ADV7182_INPUT_CVBS_AIN1:
	case ADV7182_INPUT_CVBS_AIN2:
	case ADV7182_INPUT_CVBS_AIN3:
	case ADV7182_INPUT_CVBS_AIN4:
	case ADV7182_INPUT_CVBS_AIN5:
	case ADV7182_INPUT_CVBS_AIN6:
	case ADV7182_INPUT_CVBS_AIN7:
	case ADV7182_INPUT_CVBS_AIN8:
		return ADV7182_INPUT_TYPE_CVBS;
	case ADV7182_INPUT_SVIDEO_AIN1_AIN2:
	case ADV7182_INPUT_SVIDEO_AIN3_AIN4:
	case ADV7182_INPUT_SVIDEO_AIN5_AIN6:
	case ADV7182_INPUT_SVIDEO_AIN7_AIN8:
		return ADV7182_INPUT_TYPE_SVIDEO;
	case ADV7182_INPUT_YPRPB_AIN1_AIN2_AIN3:
	case ADV7182_INPUT_YPRPB_AIN4_AIN5_AIN6:
		return ADV7182_INPUT_TYPE_YPBPR;
	case ADV7182_INPUT_DIFF_CVBS_AIN1_AIN2:
	case ADV7182_INPUT_DIFF_CVBS_AIN3_AIN4:
	case ADV7182_INPUT_DIFF_CVBS_AIN5_AIN6:
	case ADV7182_INPUT_DIFF_CVBS_AIN7_AIN8:
		return ADV7182_INPUT_TYPE_DIFF_CVBS;
	default: /* Will never happen */
		return 0;
	}
}

/* ADI recommended writes to registers 0x52, 0x53, 0x54 */
static unsigned int adv7182_lbias_settings[][3] = {
	[ADV7182_INPUT_TYPE_CVBS] = {0xCB, 0x4E, 0x80},
	[ADV7182_INPUT_TYPE_DIFF_CVBS] = {0xC0, 0x4E, 0x80},
	[ADV7182_INPUT_TYPE_SVIDEO] = {0x0B, 0xCE, 0x80},
	[ADV7182_INPUT_TYPE_YPBPR] = {0x0B, 0x4E, 0xC0},
};

static unsigned int adv7280_lbias_settings[][3] = {
	[ADV7182_INPUT_TYPE_CVBS] = {0xCD, 0x4E, 0x80},
	[ADV7182_INPUT_TYPE_DIFF_CVBS] = {0xC0, 0x4E, 0x80},
	[ADV7182_INPUT_TYPE_SVIDEO] = {0x0B, 0xCE, 0x80},
	[ADV7182_INPUT_TYPE_YPBPR] = {0x0B, 0x4E, 0xC0},
};

static int adv7182_select_input(struct adv7180_state *state, unsigned int input) {
	enum adv7182_input_type input_type;
	unsigned int *lbias;
	unsigned int i;
	int ret;

	ret = adv7180_write(state, ADV7180_REG_INPUT_CONTROL, input);
	if (ret)
		return ret;

	/* Reset clamp circuitry - ADI recommended writes */
	adv7180_write(state, 0x809c, 0x00);
	adv7180_write(state, 0x809c, 0xff);

	input_type = adv7182_get_input_type(input);

	switch (input_type) {
	case ADV7182_INPUT_TYPE_CVBS:
	case ADV7182_INPUT_TYPE_DIFF_CVBS:
		/* ADI recommends to use the SH1 filter */
		adv7180_write(state, 0x0017, 0x41);
		break;
	default:
		adv7180_write(state, 0x0017, 0x01);
		break;
	}

	if (state->chip_info->flags & ADV7180_FLAG_V2)
		lbias = adv7280_lbias_settings[input_type];
	else
		lbias = adv7182_lbias_settings[input_type];

	for (i = 0; i < ARRAY_SIZE(adv7182_lbias_settings[0]); i++)
		adv7180_write(state, 0x0052 + i, lbias[i]);

	if (input_type == ADV7182_INPUT_TYPE_DIFF_CVBS) {
		/* ADI required writes to make differential CVBS work */
		adv7180_write(state, 0x005f, 0xa8);
		adv7180_write(state, 0x005a, 0x90);
		adv7180_write(state, 0x0060, 0xb0);
		adv7180_write(state, 0x80b6, 0x08);
		adv7180_write(state, 0x80c0, 0xa0);
	} else {
		adv7180_write(state, 0x005f, 0xf0);
		adv7180_write(state, 0x005a, 0xd0);
		adv7180_write(state, 0x0060, 0x10);
		adv7180_write(state, 0x80b6, 0x9c);
		adv7180_write(state, 0x80c0, 0x00);
	}

	return 0;
}

//static const struct adv7180_chip_info adv7280_info = {
//	.flags = ADV7180_FLAG_V2 | ADV7180_FLAG_I2P,
//	.valid_input_mask = BIT(ADV7182_INPUT_CVBS_AIN1) |
//		BIT(ADV7182_INPUT_CVBS_AIN2) |
//		BIT(ADV7182_INPUT_CVBS_AIN3) |
//		BIT(ADV7182_INPUT_CVBS_AIN4) |
//		BIT(ADV7182_INPUT_SVIDEO_AIN1_AIN2) |
//		BIT(ADV7182_INPUT_SVIDEO_AIN3_AIN4) |
//		BIT(ADV7182_INPUT_YPRPB_AIN1_AIN2_AIN3),
//	.init = adv7182_init,
//	.set_std = adv7182_set_std,
//	.select_input = adv7182_select_input,
//};

static const struct adv7180_chip_info adv7280_m_info = {
	.flags = ADV7180_FLAG_V2 | ADV7180_FLAG_MIPI_CSI2 | ADV7180_FLAG_I2P,
	.valid_input_mask = BIT(ADV7182_INPUT_CVBS_AIN1) |
		BIT(ADV7182_INPUT_CVBS_AIN2) |
		BIT(ADV7182_INPUT_CVBS_AIN3) |
		BIT(ADV7182_INPUT_CVBS_AIN4) |
		BIT(ADV7182_INPUT_CVBS_AIN5) |
		BIT(ADV7182_INPUT_CVBS_AIN6) |
		BIT(ADV7182_INPUT_CVBS_AIN7) |
		BIT(ADV7182_INPUT_CVBS_AIN8) |
		BIT(ADV7182_INPUT_SVIDEO_AIN1_AIN2) |
		BIT(ADV7182_INPUT_SVIDEO_AIN3_AIN4) |
		BIT(ADV7182_INPUT_SVIDEO_AIN5_AIN6) |
		BIT(ADV7182_INPUT_SVIDEO_AIN7_AIN8) |
		BIT(ADV7182_INPUT_YPRPB_AIN1_AIN2_AIN3) |
		BIT(ADV7182_INPUT_YPRPB_AIN4_AIN5_AIN6),
	.init = adv7182_init,
	.set_std = adv7182_set_std,
	.select_input = adv7182_select_input,
};

static int init_device(struct adv7180_state *state) {
	int ret;

	CDBG("\n");
	mutex_lock(&state->mutex);
	
	ret = adv7180_read(state, ADV7180_REG_IDENT);
	if (ret < 0) {
		dev_err(&state->client->dev, "%s failed=%d\n", __func__, ret);
		goto out_unlock;
	}
	adv7180_write(state, ADV7180_REG_PWR_MAN, ADV7180_PWR_MAN_RES);
	usleep_range(5000, 10000);

	ret = state->chip_info->init(state);
	if (ret)
		goto out_unlock;

	ret = adv7180_program_std(state);
	if (ret)
		goto out_unlock;

	adv7180_set_field_mode(state);

	/* register for interrupts */
	if (state->irq > 0) {
		/* config the Interrupt pin to be active low */
		ret = adv7180_write(state, ADV7180_REG_ICONF1,
			ADV7180_ICONF1_ACTIVE_LOW |
			ADV7180_ICONF1_PSYNC_ONLY);
		if (ret < 0)
			goto out_unlock;

		ret = adv7180_write(state, ADV7180_REG_IMR1, 0);
		if (ret < 0)
			goto out_unlock;

		ret = adv7180_write(state, ADV7180_REG_IMR2, 0);
		if (ret < 0)
			goto out_unlock;

		/* enable AD change interrupts interrupts */
		ret = adv7180_write(state, ADV7180_REG_IMR3,
			ADV7180_IRQ3_AD_CHANGE);
		if (ret < 0)
			goto out_unlock;

		ret = adv7180_write(state, ADV7180_REG_IMR4, 0);
		if (ret < 0)
			goto out_unlock;
	}

out_unlock:
	mutex_unlock(&state->mutex);

	return ret;
}

static int adv7180_get_dt_data(struct device_node *of_node, struct adv7180_state * dev) {
	int	ret;
	u32	val;

	ret = of_property_read_u32(of_node, "qcom,mount-angle", &val);
	if (ret >= 0)
		dev->mount_angle = val % 360;
	return 0;
}

static int32_t adv7180_i2c_probe(struct i2c_client *client,
	const struct i2c_device_id *id) {
	int rc = -1;
	struct adv7180_state *state = &adv7180_sensor_state;
	struct i2c_adapter *adapter = client->adapter;

	if (!i2c_check_functionality(adapter, I2C_FUNC_SMBUS_BYTE_DATA)) {
		dev_err(&adapter->dev, "%s: I2C adapter doesn't support SMBus.\n",
			__FUNCTION__);
		return -EIO;
	}
	CDBG("client addr=%x\n", client->addr);
	//state = devm_kzalloc(&client->dev, sizeof(*state), GFP_KERNEL);
	//if (!state) {
	//	dev_err(&client->dev, "Failed to allocate memory for private data.\n");
	//	return -ENOMEM;
	//}

#ifdef CONFIG_USE_OF
	if (client->dev.of_node) {
		rc = adv7180_get_dt_data(client->dev.of_node, state);
		if (rc < 0) {
			dev_err(&client->dev, "Failed to get platform data from dt, err=%d\n", rc);
			return rc;
		}
	}
#endif
	state->client = client;
	state->register_page = 0;
	mutex_init(&state->mutex);
	state->field = V4L2_FIELD_INTERLACED;
	state->chip_info = &adv7280_m_info;
	if (state->chip_info->flags & ADV7180_FLAG_MIPI_CSI2) {
		state->csi_client = i2c_new_dummy(client->adapter, ADV7180_DEFAULT_CSI_I2C_ADDR);
		if (!state->csi_client) {
			rc = -ENOMEM;
			dev_err(&client->dev, "i2c_new_dummy CSI failed, err=%d\n", rc);
			goto err_destroy_mutex;
		}
	}

	if (state->chip_info->flags & ADV7180_FLAG_I2P) {
		state->vpp_client = i2c_new_dummy(client->adapter, ADV7180_DEFAULT_VPP_I2C_ADDR);
		if (!state->vpp_client) {
			rc = -ENOMEM;
			dev_err(&client->dev, "i2c_new_dummy VPP failed, err=%d\n", rc);
			goto err_unregister_csi_client;
		}
	}
	state->autodetect = true;
	if (state->chip_info->flags & ADV7180_FLAG_RESET_POWERED)
		state->powered = true;
	else
		state->powered = false;
	//v4l2_i2c_subdev_init(adv7180_get_subdev(state), client, &adv7180_subdev_ops);
	adv7180_get_subdev(state)->flags = V4L2_SUBDEV_FL_HAS_DEVNODE;

	rc = adv7180_init_controls(state);
	if (rc)
		goto err_unregister_vpp_client;

	if (state->irq) {
		rc = request_threaded_irq(client->irq, NULL, adv7180_irq,
			IRQF_ONESHOT | IRQF_TRIGGER_FALLING,
			ADV7280_SENSOR_NAME, state);
		if (rc < 0) {
			dev_err(&client->dev, "request_threaded_irq failed, error code: %d\n", rc);
			goto err_free_ctrl;
		}
	}

#ifdef CONFIG_MEDIA_CONTROLLER
	adv7180_get_subdev(state)->flags |= V4L2_SUBDEV_FL_HAS_DEVNODE;
	state->pad.flags = MEDIA_PAD_FLAG_OUTPUT;
	rc = media_entity_init(&adv7180_get_subdev(state)->entity, 1, &state->pad, 0);
	if (rc < 0) {
		dev_err(&client->dev, "media_entity_init failed, error code: %d\n", rc);
		goto err_disable_irq;
	}
#endif
	state->msm_s_ctrl->sensor_v4l2_subdev_ops = &adv7180_subdev_ops;
	rc = msm_sensor_i2c_probe(client, id, state->msm_s_ctrl);
	if (rc < 0) {
		dev_err(&client->dev, "msm_sensor_i2c_probe failed, error code: %d\n", rc);
		goto err_media_entity_cleanup;
	}

	if ((regid == ADV7280_VAL_IDENT)) {
		CDBG("found %s, ID register 0x%x", client->name, regid);
	} else {
		CDBG("No found, ID register 0x%x", regid);
	}
	return 0;

err_media_entity_cleanup:
	media_entity_cleanup(&adv7180_get_subdev(state)->entity);
err_disable_irq:
	disable_irq(state->irq);
err_free_ctrl:
	adv7180_exit_controls(state);
err_unregister_vpp_client:
	if (state->chip_info->flags & ADV7180_FLAG_I2P)
		i2c_unregister_device(state->vpp_client);
err_unregister_csi_client:
	if (state->chip_info->flags & ADV7180_FLAG_MIPI_CSI2)
		i2c_unregister_device(state->csi_client);
err_destroy_mutex:
	mutex_destroy(&state->mutex);
	//devm_kfree(&client->dev, state);
	return rc;
}

static struct i2c_driver adv7180_i2c_driver = {
	.id_table = adv7180_i2c_id,
	.probe = adv7180_i2c_probe,
	.driver = {
		.name = ADV7280_SENSOR_NAME,
		.owner = THIS_MODULE,
},
};

static const struct of_device_id adv7180_dt_match[] = {
	{.compatible = "analog,adv7280-m",.data = &adv7180_sensor_s_ctrl},
	{}
};
MODULE_DEVICE_TABLE(of, adv7180_dt_match);

static int32_t adv7180_platform_probe(struct platform_device *pdev) {
	int32_t rc = 0;
	const struct of_device_id *match;
	match = of_match_device(adv7180_dt_match, &pdev->dev);
	rc = msm_sensor_platform_probe(pdev, match->data);
	return rc;
}

static struct platform_driver adv7180_platform_driver = {
	.driver = {
		.name = ADV7280_SENSOR_NAME,
		.owner = THIS_MODULE,
		.of_match_table = adv7180_dt_match,
	},
	.probe = adv7180_platform_probe,
};

static int __init adv7180_module_init(void) {
	int32_t rc = 0;
	rc = i2c_add_driver(&adv7180_i2c_driver);

	CDBG("rc = %d\n", rc);
	if (!rc)
		return rc;
	return platform_driver_register(&adv7180_platform_driver);
}

static void __exit adv7180_module_exit(void) {
	if (adv7180_sensor_s_ctrl.pdev) {
		msm_sensor_free_sensor_data(&adv7180_sensor_s_ctrl);
		platform_driver_unregister(&adv7180_platform_driver);
	} else
		i2c_del_driver(&adv7180_i2c_driver);
	return;
}
module_init(adv7180_module_init);
module_exit(adv7180_module_exit);

MODULE_DESCRIPTION("Analog Devices ADV7180 video decoder driver");
MODULE_AUTHOR("Mocean Laboratories");
MODULE_LICENSE("GPL v2");
