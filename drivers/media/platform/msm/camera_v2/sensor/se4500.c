/*
 * drivers/media/platform/msm/camera_v2/sensor/se4500.c
 *
 * Motorola SE4500 sensor driver
 *
 * Copyright (C) 2011 Motorola Solutions, Inc
 * Author: Motorola
 *
 * This file is licensed under the terms of the GNU General Public License
 * version 2. This program is licensed "as is" without any warranty of any
 * kind, whether express or implied.
 */
#include <media/se4500.h>
#include <linux/i2c-dev.h>
#include "msm_sensor.h"
#include "msm_cci.h"
#include "msm_camera_io_util.h"

//define for debug
#define SE4500_VERBOSE_DGB

#ifdef SE4500_VERBOSE_DGB
static int debug_flag = 1;
#define SEDBG(fmt, args...) \
do { \
	if (debug_flag) \
		printk(KERN_DEBUG "[%s()->Line:%d] " fmt, __func__, __LINE__, ##args); \
} while(0)
#define SEDBG_HIGH(fmt, args...) printk(KERN_DEBUG "[%s()->Line:%d] " fmt, __func__, __LINE__, ##args)
#else
#define SEDBG(fmt, args...) do { } while (0)
#define SEDBG_HIGH(fmt, args...) printk(fmt, ##args)
#endif

DEFINE_MSM_MUTEX(moto_se4500_mut);
static struct msm_sensor_ctrl_t moto_se4500_s_ctrl;

#define SE4500_USER_I2C_DEV 1
#if SE4500_USER_I2C_DEV
struct se4500_dev {
	atomic_t  open_excl;
	int       ver;
	char      model[SE45PARAM_MODELNO_LEN + 1];
	char      sn[SE45PARAM_SERIALNO_LEN + 6];
};

struct se4500_dev *pSE4500Dev;
struct i2c_client *se4500_client;
#endif

#ifndef CONFIG_MOTO_MIPI
#ifdef CONFIG_TOSHIBA_TC358746
extern int tc358746_reset(int on);
extern int tc358746_config_registers(int which);
#endif
#endif

static bool first_probe = true;
#ifdef SE4500_VERBOSE_DGB
/* sysfs interface */
static ssize_t moto_se4500_show(struct device *dev,struct device_attribute *attr, char *buf)
{
	return sprintf(buf,"%d\n", debug_flag);
}

/* debug fs, "echo @1 > /sys/bus/i2c/devices/xxx/se4500_debug" @1:debug_flag  */
static ssize_t moto_se4500_store(struct device *dev,
        struct device_attribute *attr, const char *buf,size_t count)
{
	sscanf(buf, "%d", &debug_flag);
	return count;
}

static struct device_attribute moto_se4500_dev_attr =
	__ATTR(se4500_debug, S_IRUGO | S_IWUGO, moto_se4500_show, moto_se4500_store);
#endif

#if SE4500_USER_I2C_DEV
static int moto_se4500_misc_open(struct inode *inode, struct file *filp)
{
	SEDBG("======== In ========\n");
	if ( atomic_inc_return(&pSE4500Dev->open_excl) != 1 ){
    	atomic_dec(&pSE4500Dev->open_excl);
        return -EBUSY;
    }
    filp->private_data = pSE4500Dev;

    return 0;
}

static long moto_se4500_misc_ioctl(struct file* pFile, unsigned int uiCmd, unsigned long ulArg)
{
    struct i2c_rdwr_ioctl_data              I2CData;
    struct i2c_msg                          I2CMsg;
    u8 __user*                              pData;
    long                                    lRetVal;

	SEDBG("======== In ========\n");

    if ((uiCmd != I2C_RDWR) || !ulArg ){
        return(-EINVAL);
    }

    // Copy data structure argument from user-space
    if ( copy_from_user(&I2CData, (struct i2c_rdwr_ioctl_data __user*) ulArg, sizeof(I2CData)) ){
        return(-EFAULT);
    }

    // Only allow one message at a time
    if ( I2CData.nmsgs != 1 ){
        return(-EINVAL);
    }

    // Copy the message structure from user-space
    if ( copy_from_user(&I2CMsg, I2CData.msgs, sizeof(struct i2c_msg)) ){
        return(-EFAULT);
    }

    lRetVal = 0;
    // Only allow transfers to the SE4500, limit the size of the message and don't allow received length changes
    if ( (I2CMsg.addr != SE4500_I2C_ADDR) || (I2CMsg.len > 256) || (I2CMsg.flags & I2C_M_RECV_LEN) ){
        return(-EINVAL);
    }

    // Map the data buffer from user-space
    pData = (u8 __user*) I2CMsg.buf;
    I2CMsg.buf = memdup_user(pData, I2CMsg.len);
    if ( IS_ERR(I2CMsg.buf) ){
        return(PTR_ERR(I2CMsg.buf));
    }

    // Perform the I2C transfer
    lRetVal = i2c_transfer(se4500_client->adapter, &I2CMsg, 1);
    if ( (lRetVal >= 0) && (I2CMsg.flags & I2C_M_RD) ){
        // Successful read, copy data to user-space
        SEDBG("I2CMsg.buf = %s\n", I2CMsg.buf);
        if ( copy_to_user(pData, I2CMsg.buf, I2CMsg.len) ){
            lRetVal = -EFAULT;
        }
    }
	
    kfree(I2CMsg.buf);
	SEDBG("lRetVal = %ld\n", lRetVal);
    return(lRetVal);
}

static int moto_se4500_misc_release(struct inode* pINode, struct file* pFile)
{
	SEDBG("======== In ========\n");
	atomic_dec(&pSE4500Dev->open_excl);
    //kfree(pSE4500Dev);
    return 0;
}

static const struct file_operations moto_se4500_misc_fops = {
        .owner			= THIS_MODULE,
        .unlocked_ioctl = moto_se4500_misc_ioctl,
        .open			= moto_se4500_misc_open,
        .release		= moto_se4500_misc_release,
};

static struct miscdevice moto_se4500_misc_device = {
        .minor			= MISC_DYNAMIC_MINOR,
        .name			= SE4500_MISC_NAME,
        .fops			= &moto_se4500_misc_fops,
};
#endif

static struct msm_sensor_power_setting moto_se4500_power_setting[] = {
	{
		.seq_type = SENSOR_VREG,
		.seq_val = 0,
		.config_val = 0,
		.delay = 1,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_VANA,
		.config_val = GPIO_OUT_HIGH,
		.delay = 1,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_VIO,
		.config_val = GPIO_OUT_HIGH,
		.delay = 10,//must be
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_VAF,
		.config_val = GPIO_OUT_HIGH,
		.delay = 1,
	},
	{
		.seq_type = SENSOR_CLK,
		.seq_val = SENSOR_CAM_MCLK,
		.config_val = 24000000,
		.delay = 5,
	},
	{
		.seq_type = SENSOR_I2C_MUX,
		.seq_val = 0,
		.config_val = 0,
		.delay = 0,
	},
};

static struct msm_sensor_power_setting moto_se4500_power_down_setting[] = {
	{
		.seq_type = SENSOR_VREG,
		.seq_val = 0,
		.config_val = 0,
		.delay = 1,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_VANA,
		.config_val = GPIO_OUT_LOW,
		.delay = 1,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_VIO,
		.config_val = GPIO_OUT_LOW,
		.delay = 1,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_VAF,
		.config_val = GPIO_OUT_LOW,
		.delay = 1,
	},
	{
		.seq_type = SENSOR_CLK,
		.seq_val = SENSOR_CAM_MCLK,
		.config_val = 24000000,
		.delay = 5,
	},
	{
		.seq_type = SENSOR_I2C_MUX,
		.seq_val = 0,
		.config_val = 0,
		.delay = 0,
	},
};

static struct v4l2_subdev_info moto_se4500_subdev_info[] = {
	{
		.code   = V4L2_MBUS_FMT_YUYV8_2X8,
		.colorspace = V4L2_COLORSPACE_JPEG,
		.fmt    = 1,
		.order    = 0,
	},
};

/*
 * moto_se4500_read - Read a command to the SE4500 device
 * @pClient:		i2c driver client structure
 * @data:			pointer to data to write
 * @data_length:	length of data to write
 *
 * Write a command to the SE4500 device.
 * Returns zero if successful, or non-zero otherwise.
 */
static int moto_se4500_read(struct i2c_client* pClient, u8* data, int data_length)
{
	struct i2c_msg	msg[1];
	int				err;

	msg->addr	= pClient->addr;
	msg->flags	= I2C_M_RD;
	msg->len	= data_length;
	msg->buf	= data;
	err			= i2c_transfer(pClient->adapter, msg, 1);
	if ( err >= 0 )
	{
		err = 0;	// Success
	}
	return(err);
}

/*
 * moto_se4500_write - Write a command to the SE4500 device
 * @pClient:		i2c driver client structure
 * @data:			pointer to data to write
 * @data_length:	length of data to write
 *
 * Write a command to the SE4500 device.
 * Returns zero if successful, or non-zero otherwise.
 */
static int moto_se4500_write(struct i2c_client* pClient, u8* data, int data_length)
{
	struct i2c_msg	msg[1];
	int				err;

	printk("se4500==========CMD_LEN = %d, CMD = %02x:%02x:%02x:%02x\n", 
			data_length, data[0], data[1], data[2], data[3]);

	msg->addr	= pClient->addr;
	msg->flags	= 0;
	msg->len	= data_length;
	msg->buf	= data;
	err			= i2c_transfer(pClient->adapter, msg, 1);
	if ( err >= 0 )
	{
		err = 0;	// Non-negative indicates success
	}

	return(err);
}

/*
 * moto_se4500_command - Send a command to the SE4500 device and wait for a response
 * @bCmd:			SE4500 command byte
 * @pParam:			buffer with command parameters
 * @num_param:		number of parameter bytes
 * @pResp:			buffer for response
 * @resp_len:		expected response length
 *
 * Sends a command and optional parameters to the SE4500 via I2C and waits
 * for a response.
 * Returns the number of response bytes receive or a negative error code
 */
static int moto_se4500_command(u8 bCmd, u8* pParam, int num_param, u8* pResp, int resp_len)
{
	int	retVal;
	int	iIdx;
	int	iCmdLen;
	u8	abCmd[SE45_MAX_CMD_LEN];
	u8	bCkSum;
	u8	bTmp;

	// Make sure command, params and checksum will fit in buffer
	if ( (num_param >= (sizeof(abCmd) - 2)) || (num_param < 0) )
	{
		SEDBG_HIGH("se4500_command: invalid param count: %d\n", num_param);
		return(-EINVAL);
	}
	// Build command and calculate checksum
	abCmd[0] = bCkSum = bCmd;
	for ( iIdx = 0; iIdx < num_param; )
	{
		bTmp = pParam[iIdx++];
		abCmd[iIdx] = bTmp;
		bCkSum += bTmp;
	}
	abCmd[++iIdx] = -bCkSum;	// Store checksum

	iCmdLen = num_param + 2;
	retVal = -EIO;

	SEDBG("se4500==========CMD_LEN = %d, CMD = %02x:%02x:%02x:%02x\n", 
			iCmdLen, abCmd[0], abCmd[1], abCmd[2], abCmd[3]);
	// Try up to 3 times to send the command
	for ( iIdx = 0; iIdx < 3; ++iIdx )
	{
		retVal = moto_se4500_write(se4500_client, abCmd, iCmdLen);
		if ( 0 == retVal )
		{
			// Command successfully sent
			// Try up to 3 times to read the response
			for ( iIdx = 0; iIdx < 3; ++iIdx )
			{
				msleep(5);
				retVal = moto_se4500_read(se4500_client, pResp, resp_len);
				if ( 0 == retVal )
				{
					//TODO: Should we check for ACK?
					SEDBG("Read_res = %d, Resp = %02x:%02x\n", 
							retVal, pResp[0], pResp[1]);
					return(resp_len);
				}
			}
			SEDBG("Read %02X response failed, err=%d\n", bCmd, retVal);
			return(retVal);
		}
	}
	SEDBG("Write %02X failed, err=%d\n", bCmd, retVal);
	return(retVal);
}

/*
 * moto_se4500_detect - Detect if an SE4500 is present, and if so get the model number
 * 
 * Detect if an SE4500 is present
 * Returns a negative error number if no device is detected, or 0x99
 * if the model number is successfully read.
 */
 
static int moto_se4500_detect(void)
{
	int	retVal;
	int	numParam;
	u8	abParam[2];
	u8	abResp[SE45PARAM_MODELNO_LEN + 4];
	
    SEDBG("=====begin=====\n");
	// Try to get the model number from the sensor
	numParam = 2;
	abParam[0] = (SE45PARAM_MODELNO & 0x00FF) >> 0;
	abParam[1] = (SE45PARAM_MODELNO & 0xFF00) >> 8;
	retVal = moto_se4500_command(SE45OP_GETPARAM, abParam, numParam, abResp, sizeof(abResp));
	if ( retVal > 0 )
	{
		if (first_probe){
			u8 model_temp[SE45PARAM_MODELNO_LEN + 1];
		
			snprintf(model_temp, sizeof(model_temp) - 1, "%s", abResp + 4);
			SEDBG_HIGH("Model Name: %s\n", model_temp);
			if(strstr(model_temp, SE4500) != NULL){
				strlcpy(pSE4500Dev->model, SE4500_MODEL, 12);
				pSE4500Dev->ver = 0;
			}
			#ifdef CONFIG_MOTO_MIPI
			else if(strstr(model_temp, SE4710) != NULL){
				strlcpy(pSE4500Dev->model, SE4710_MIPI_MODEL, 12);
				pSE4500Dev->ver = 1;
			}
			else if(strstr(model_temp, SE4750) != NULL){
				strlcpy(pSE4500Dev->model, SE4750_MIPI_MODEL, 12);
				pSE4500Dev->ver = 2;
			}
			#else
			else if(strstr(model_temp, SE4710) != NULL){
				strlcpy(pSE4500Dev->model, SE4710_MODEL, 12);
				pSE4500Dev->ver = 1;
			}
			else if(strstr(model_temp, SE4750) != NULL){
				strlcpy(pSE4500Dev->model, SE4750_MODEL, 12);
				pSE4500Dev->ver = 2;
			}
			#endif
		} else
			SEDBG_HIGH("Model Name: %s\n", abResp + 4);
	}
	else
	{
		u8	abSN[SE45PARAM_SERIALNO_LEN + 5];

		abParam[0] = (SE45PARAM_SERIALNO & 0x00FF) >> 0;
		abParam[1] = (SE45PARAM_SERIALNO & 0xFF00) >> 8;
		memset(abSN, 0, sizeof(abSN));
		retVal = moto_se4500_command(SE45OP_GETPARAM, abParam, numParam, abSN, sizeof(abSN) - 1);
		if ( retVal > 0 )
		{
			SEDBG("SE4500 S/N=%s\n", (char*) abSN);
		}
	}

	return retVal;
}

static void se4500_stream_on(void)
{
	int	retVal;
	int	numParam;
	u8	abResp[2];
	u8	abParam[1];
	/* If the 'misc' device is open, do not attempt to enable or disable acquisition,
	 * We return here. If not when turn on the scanner, the led of se4500 will
	 * flickering. Because at first the scanner will open camera, the camera will
	 * do startPreview, so this function will invoked, and the led is turned on
	 * and than scanner libs will send i2c command to do some init, then the led
	 * is turned off, so we get a led flicker. But we returned here will case the
	 * camera can not work will this se4500 module.
	 */
	if(!debug_flag)
		return;

	SEDBG("=========in=========\n");

	numParam = 1;
#ifdef CONFIG_MOTO_MIPI
	abParam[0] = 0x03;
#else
	abParam[0] = 0x04;
#endif
	retVal = moto_se4500_command(SE45OP_INTERFACETYPE, abParam, numParam, abResp, sizeof(abResp));

	// Turn acquisition on or off
	abParam[0] = 1;
	retVal = moto_se4500_command(SE45OP_ARMACQUISITION, abParam, numParam, abResp, sizeof(abResp));
}

static void se4500_stream_off(void)
{
	int	retVal;
	int	numParam;
	u8	abResp[2];
	u8	abParam[1];

	/* If the 'misc' device is open, do not attempt to enable or disable acquisition,
	 * We return here. If not when turn on the scanner, the led of se4500 will
	 * flickering. Because at first the scanner will open camera, the camera will
	 * do startPreview, so this function will invoked, and the led is turned on
	 * and than scanner libs will send i2c command to do some init, then the led
	 * is turned off, so we get a led flicker. But we returned here will case the
	 * camera can not work will this se4500 module.
	 */
	if(!debug_flag)
		return;

	SEDBG("=========in=========\n");

	numParam = 1;
	abParam[0] = 0;
	retVal = moto_se4500_command(SE45OP_ARMACQUISITION, abParam, numParam, abResp, sizeof(abResp));
}

static int32_t moto_se4500_sensor_match_id(struct msm_sensor_ctrl_t *s_ctrl)
{
	int rc = 0;

	SEDBG("We will detect the device!\n");
	mdelay(2);// power need time,need
	rc = moto_se4500_detect();
	s_ctrl->sensordata->sensor_name = pSE4500Dev->model;
	
	SEDBG("Scan Sensor Name = %s\n", s_ctrl->sensordata->sensor_name);

	return rc;
}

static int32_t moto_se4500_sensor_power_up(struct msm_sensor_ctrl_t *s_ctrl)
{
	int32_t rc = 0;
	
	SEDBG("======power up======\n");	
	
	rc = msm_sensor_power_up(s_ctrl);

#ifndef CONFIG_MOTO_MIPI
#ifdef CONFIG_TOSHIBA_TC358746
	if(!first_probe){
		rc = tc358746_reset(1);
		if(rc){
			SEDBG_HIGH("tc358746_reset failed: %d !\n", rc);
		}
		rc = tc358746_config_registers(pSE4500Dev->ver);
		SEDBG("We config tc358746xbg first rc = %d\n", rc);	
		if(rc){
			SEDBG_HIGH("config tc358746xbg failed: %d !\n", rc);
		}
	}
#endif
#endif

	return rc;
}

static int32_t moto_se4500_sensor_power_down(struct msm_sensor_ctrl_t *s_ctrl)
{
	int32_t rc = 0;
	
	SEDBG("======power down======\n");

	rc = msm_sensor_power_down(s_ctrl);

#ifndef CONFIG_MOTO_MIPI
#ifdef CONFIG_TOSHIBA_TC358746
	if(!first_probe){
		tc358746_reset(0);
	}
#endif
#endif

	first_probe = false;

	return rc;
}

static const struct i2c_device_id moto_se4500_i2c_id[] = {
	{MOTO_SE4500_SENSOR_NAME, (kernel_ulong_t)&moto_se4500_s_ctrl},
	{ }
};

static int32_t moto_se4500_i2c_probe(struct i2c_client *client,
	const struct i2c_device_id *id)
{
	int rc = -1;
	
	SEDBG(" ======IN===== !\n");
	se4500_client = client;//save for using later
	
#ifdef SE4500_VERBOSE_DGB
	rc = device_create_file(&client->dev, &moto_se4500_dev_attr);
	if (rc) {
		SEDBG_HIGH("sysfs registration failed, error %d \n", rc);
		return -ENOMEM;
	}
#endif

#if SE4500_USER_I2C_DEV
	/* new struct buf for pSE4500Dev */
	pSE4500Dev = kzalloc(sizeof(struct se4500_dev), GFP_KERNEL);
	if (!pSE4500Dev) {
		SEDBG_HIGH("ERROR: kzalloc for pSE4500Dev failed!\n");
		rc = -ENOMEM;
		goto probe_failure_1;
	} else {
		memset(pSE4500Dev, 0, sizeof(struct se4500_dev));
	}

	// Initialize exclusive open lock
	atomic_set(&pSE4500Dev->open_excl, 0);

	// Register as a 'misc' device
	// If it fails, we just won't have I2C access from VM's but we will still work as a camera
	misc_register(&moto_se4500_misc_device);
#endif

	rc = msm_sensor_i2c_probe(client, id, &moto_se4500_s_ctrl);
	if(rc){
		SEDBG_HIGH("msm_sensor_i2c_probe failed!\n");
		rc = -ENODEV;
		goto i2c_probe_failed;
	}
	
	SEDBG(" ======OUT===== !\n");
	return 0;

#if SE4500_USER_I2C_DEV	
i2c_probe_failed:
	misc_deregister(&moto_se4500_misc_device);
	kfree(pSE4500Dev);
	pSE4500Dev = NULL;
probe_failure_1:
	#ifdef SE4500_VERBOSE_DGB
	device_remove_file(&client->dev, &moto_se4500_dev_attr);
	#endif
	return rc;
#else
i2c_probe_failed:
	#ifdef SE4500_VERBOSE_DGB
	device_remove_file(&client->dev, &moto_se4500_dev_attr);
	#endif
	return rc;
#endif
}

static struct i2c_driver moto_se4500_i2c_driver = {
	.id_table = moto_se4500_i2c_id,
	.probe  = moto_se4500_i2c_probe,
	.driver = {
		.name	= MOTO_SE4500_SENSOR_NAME,
	},
};

static struct msm_camera_i2c_client moto_se4500_sensor_i2c_client = {
	.addr_type = MSM_CAMERA_I2C_BYTE_ADDR,
};
static const struct of_device_id moto_se4500_dt_match[] = {
	{.compatible = "motorola,se4500_moto", .data = &moto_se4500_s_ctrl},
	{}
};

MODULE_DEVICE_TABLE(of, moto_se4500_dt_match);

static int32_t moto_se4500_platform_probe(struct platform_device *pdev)
{
	int32_t rc = 0;
	const struct of_device_id *match;

	match = of_match_device(moto_se4500_dt_match, &pdev->dev);
	if (!match)
		return -EFAULT;
	rc = msm_sensor_platform_probe(pdev, match->data);
	return rc;
}

static struct platform_driver moto_se4500_platform_driver = {
	.driver = {
		.name = MOTO_SE4500_SENSOR_NAME,
		.owner = THIS_MODULE,
		.of_match_table = moto_se4500_dt_match,
	},
	.probe = moto_se4500_platform_probe,
};

static int __init moto_se4500_init_module(void)
{
	int32_t rc = 0;

	SEDBG("\n==========se4500 init module==========\n");
	rc = i2c_add_driver(&moto_se4500_i2c_driver);

	if (!rc)
		return rc;
	SEDBG_HIGH("rc = %d\n", rc);
	return platform_driver_register(&moto_se4500_platform_driver);
}

static void __exit moto_se4500_exit_module(void)
{
	SEDBG("=========se4500 exit module============\n");
	if (moto_se4500_s_ctrl.pdev) {
		msm_sensor_free_sensor_data(&moto_se4500_s_ctrl);
		platform_driver_unregister(&moto_se4500_platform_driver);
	} else
		i2c_del_driver(&moto_se4500_i2c_driver);
	return;
}

int32_t moto_se4500_sensor_config(struct msm_sensor_ctrl_t *s_ctrl,
	void __user *argp)
{
	struct sensorb_cfg_data *cdata = (struct sensorb_cfg_data *)argp;
	long rc = 0;
	int32_t i = 0;
	
	mutex_lock(s_ctrl->msm_sensor_mutex);
	SEDBG_HIGH("%s cfgtype = %d\n",
		s_ctrl->sensordata->sensor_name, cdata->cfgtype);
	switch (cdata->cfgtype) {
	case CFG_GET_SENSOR_INFO:
		memcpy(cdata->cfg.sensor_info.sensor_name,
			s_ctrl->sensordata->sensor_name,
			sizeof(cdata->cfg.sensor_info.sensor_name));
		cdata->cfg.sensor_info.session_id =
			s_ctrl->sensordata->sensor_info->session_id;
		for (i = 0; i < SUB_MODULE_MAX; i++)
			cdata->cfg.sensor_info.subdev_id[i] =
				s_ctrl->sensordata->sensor_info->subdev_id[i];
		cdata->cfg.sensor_info.is_mount_angle_valid =
			s_ctrl->sensordata->sensor_info->is_mount_angle_valid;
		cdata->cfg.sensor_info.sensor_mount_angle =
			s_ctrl->sensordata->sensor_info->sensor_mount_angle;
		SEDBG("sensor name %s\n",
			cdata->cfg.sensor_info.sensor_name);
		SEDBG("session id %d\n",
			cdata->cfg.sensor_info.session_id);
		for (i = 0; i < SUB_MODULE_MAX; i++)
			SEDBG("subdev_id[%d] %d\n", i,
				cdata->cfg.sensor_info.subdev_id[i]);
		SEDBG("mount angle valid %d value %d\n", 
			cdata->cfg.sensor_info.is_mount_angle_valid,
			cdata->cfg.sensor_info.sensor_mount_angle);
		break;
	case CFG_SET_INIT_SETTING:
		SEDBG("init setting\n");
		break;
	case CFG_SET_RESOLUTION: {
		/*copy from user the desired resoltuion*/
		enum msm_sensor_resolution_t res = MSM_SENSOR_INVALID_RES;
		if (copy_from_user(&res, (void *)cdata->cfg.setting,
			sizeof(enum msm_sensor_resolution_t))) {
			SEDBG_HIGH("failed....\n");
			rc = -EFAULT;
			break;
		}
		break;
	}
	case CFG_SET_STOP_STREAM:
		SEDBG("stop stream!!\n");
		se4500_stream_off();
		break;
	case CFG_SET_START_STREAM:
		SEDBG("start stream!!\n");
		se4500_stream_on();
		break;
	case CFG_GET_SENSOR_INIT_PARAMS:
		cdata->cfg.sensor_init_params.modes_supported =
			s_ctrl->sensordata->sensor_info->modes_supported;
		cdata->cfg.sensor_init_params.position =
			s_ctrl->sensordata->sensor_info->position;
		cdata->cfg.sensor_init_params.sensor_mount_angle =
			s_ctrl->sensordata->sensor_info->sensor_mount_angle;
		SEDBG("init params mode %d pos %d mount %d\n",
			cdata->cfg.sensor_init_params.modes_supported,
			cdata->cfg.sensor_init_params.position,
			cdata->cfg.sensor_init_params.sensor_mount_angle);
		break;
	case CFG_SET_SLAVE_INFO: {
		struct msm_camera_sensor_slave_info *sensor_slave_info;
		struct msm_camera_power_ctrl_t *p_ctrl;
		uint16_t size;
		int slave_index = 0;
		sensor_slave_info = kmalloc(sizeof(struct msm_camera_sensor_slave_info)
				      * 1, GFP_KERNEL);

		if (!sensor_slave_info) {
			SEDBG_HIGH("failed to alloc mem\n");
			rc = -ENOMEM;
			break;
		}
		if (copy_from_user(sensor_slave_info,
			(void *)cdata->cfg.setting,
			sizeof(struct msm_camera_sensor_slave_info))) {
			SEDBG_HIGH("failed!!\n");
			rc = -EFAULT;
			break;
		}
		/* Update sensor slave address */
		if (sensor_slave_info->slave_addr)
			s_ctrl->sensor_i2c_client->cci_client->sid =
				sensor_slave_info->slave_addr;

		/* Update sensor address type */
		s_ctrl->sensor_i2c_client->addr_type =
			sensor_slave_info->addr_type;

		/* Update power up / down sequence */
		p_ctrl = &s_ctrl->sensordata->power_info;
		size = sensor_slave_info->power_setting_array.size;
		if (p_ctrl->power_setting_size < size) {
			struct msm_sensor_power_setting *tmp;
			tmp = kmalloc(sizeof(struct msm_sensor_power_setting)
				      * size, GFP_KERNEL);
			if (!tmp) {
				SEDBG_HIGH("failed to alloc mem\n");
				rc = -ENOMEM;
				break;
			}
			kfree(p_ctrl->power_setting);
			p_ctrl->power_setting = tmp;
		}
		p_ctrl->power_setting_size = size;

		rc = copy_from_user(p_ctrl->power_setting, (void *)
			sensor_slave_info->power_setting_array.power_setting,
			size * sizeof(struct msm_sensor_power_setting));
		if (rc) {
			SEDBG_HIGH("failed\n");
			rc = -EFAULT;
			break;
		}
		for (slave_index = 0; slave_index <
			p_ctrl->power_setting_size; slave_index++) {
			SEDBG("i = %d power setting %d %d %ld %d\n",
				slave_index,
				p_ctrl->power_setting[slave_index].seq_type,
				p_ctrl->power_setting[slave_index].seq_val,
				p_ctrl->power_setting[slave_index].config_val,
				p_ctrl->power_setting[slave_index].delay);
		}
		break;
	}
	case CFG_WRITE_I2C_ARRAY: {
		struct msm_camera_i2c_reg_setting conf_array;
		struct msm_camera_i2c_reg_array *reg_setting = NULL;
		int	numParam;
		u8	abResp[2];
		u8	abParam[1];
		
		if (copy_from_user(&conf_array,
			(void *)cdata->cfg.setting,
			sizeof(struct msm_camera_i2c_reg_setting))) {
			SEDBG_HIGH("failed...\n");
			rc = -EFAULT;
			break;
		}

		if (!conf_array.size ||
			conf_array.size > I2C_REG_DATA_MAX) {
			SEDBG_HIGH("failed...\n");
			rc = -EFAULT;
			break;
		}

		reg_setting = kzalloc(conf_array.size *
			(sizeof(struct msm_camera_i2c_reg_array)), GFP_KERNEL);
		if (!reg_setting) {
			SEDBG_HIGH("failed....\n");
			rc = -ENOMEM;
			break;
		}
		if (copy_from_user(reg_setting, (void *)conf_array.reg_setting,
			conf_array.size *
			sizeof(struct msm_camera_i2c_reg_array))) {
			SEDBG_HIGH("failed.....\n");
			kfree(reg_setting);
			rc = -EFAULT;
			break;
		}

		conf_array.reg_setting = reg_setting;
		for(i = 0; i < conf_array.size; i++){
			if(reg_setting->reg_addr == 0){
				rc = 0;
				break;
			}
				
			SEDBG("addr = 0x%x, data = 0x%x\n", reg_setting[i].reg_addr, reg_setting[i].reg_data);
			numParam = 1;
			abParam[0] = reg_setting[i].reg_data;
			rc = moto_se4500_command(reg_setting[i].reg_addr, abParam, numParam, abResp, sizeof(abResp));
		}
		kfree(reg_setting);
		break;
	}
	case CFG_WRITE_I2C_SEQ_ARRAY: {
		struct msm_camera_i2c_seq_reg_setting conf_array;
		struct msm_camera_i2c_seq_reg_array *reg_setting = NULL;

		if (copy_from_user(&conf_array,
			(void *)cdata->cfg.setting,
			sizeof(struct msm_camera_i2c_seq_reg_setting))) {
			SEDBG_HIGH("failed...\n");
			rc = -EFAULT;
			break;
		}

		if (!conf_array.size ||
			conf_array.size > I2C_SEQ_REG_DATA_MAX) {
			SEDBG_HIGH("failed...\n");
			rc = -EFAULT;
			break;
		}

		reg_setting = kzalloc(conf_array.size *
			(sizeof(struct msm_camera_i2c_seq_reg_array)),
			GFP_KERNEL);
		if (!reg_setting) {
			SEDBG_HIGH("failed...\n");
			rc = -ENOMEM;
			break;
		}
		if (copy_from_user(reg_setting, (void *)conf_array.reg_setting,
			conf_array.size *
			sizeof(struct msm_camera_i2c_seq_reg_array))) {
			SEDBG_HIGH("failed...\n");
			kfree(reg_setting);
			rc = -EFAULT;
			break;
		}

		conf_array.reg_setting = reg_setting;
		rc = s_ctrl->sensor_i2c_client->i2c_func_tbl->
			i2c_write_seq_table(s_ctrl->sensor_i2c_client,
			&conf_array);
		kfree(reg_setting);
		break;
	}

	case CFG_POWER_UP:
		if (s_ctrl->func_tbl->sensor_power_up)
			rc = s_ctrl->func_tbl->sensor_power_up(s_ctrl);
		else
			rc = -EFAULT;
		break;

	case CFG_POWER_DOWN:
		if (s_ctrl->func_tbl->sensor_power_down)
			rc = s_ctrl->func_tbl->sensor_power_down(s_ctrl);
		else
			rc = -EFAULT;
		break;

	case CFG_SET_STOP_STREAM_SETTING: {
		struct msm_camera_i2c_reg_setting *stop_setting =
			&s_ctrl->stop_setting;
		struct msm_camera_i2c_reg_array *reg_setting = NULL;
		if (copy_from_user(stop_setting, (void *)cdata->cfg.setting,
			sizeof(struct msm_camera_i2c_reg_setting))) {
			SEDBG_HIGH("failed...\n");
			rc = -EFAULT;
			break;
		}

		reg_setting = stop_setting->reg_setting;
		stop_setting->reg_setting = kzalloc(stop_setting->size *
			(sizeof(struct msm_camera_i2c_reg_array)), GFP_KERNEL);
		if (!stop_setting->reg_setting) {
			SEDBG_HIGH("failed...\n");
			rc = -ENOMEM;
			break;
		}
		if (copy_from_user(stop_setting->reg_setting,
			(void *)reg_setting, stop_setting->size *
			sizeof(struct msm_camera_i2c_reg_array))) {
			SEDBG_HIGH("failed...\n");
			kfree(stop_setting->reg_setting);
			stop_setting->reg_setting = NULL;
			stop_setting->size = 0;
			rc = -EFAULT;
			break;
		}
		break;
		}
	case CFG_SET_STREAM_TYPE: {
		enum msm_camera_stream_type_t stream_type = MSM_CAMERA_STREAM_INVALID;
		if (copy_from_user(&stream_type, (void *)cdata->cfg.setting,
			sizeof(enum msm_camera_stream_type_t))) {
			SEDBG_HIGH("failed...\n");
			rc = -EFAULT;
			break;
		}
		s_ctrl->camera_stream_type = stream_type;
		break;
	}
	case CFG_SET_SATURATION:
		break;
	case CFG_SET_CONTRAST:
		break;
	case CFG_SET_SHARPNESS:
		break;
	case CFG_SET_AUTOFOCUS:
		/* TO-DO: set the Auto Focus */
		SEDBG_HIGH("Setting Auto Focus\n");
		break;
	case CFG_CANCEL_AUTOFOCUS:
		/* TO-DO: Cancel the Auto Focus */
		SEDBG_HIGH("Cancelling Auto Focus\n");
		break;
	case CFG_SET_ISO:
		break;
	case CFG_SET_EXPOSURE_COMPENSATION:
		break;
	case CFG_SET_EFFECT:
		break;
	case CFG_SET_ANTIBANDING:
		break;
	case CFG_SET_BESTSHOT_MODE:
		break;
	case CFG_SET_WHITE_BALANCE:
		break;
	default:
		rc = -EFAULT;
		break;
	}

	mutex_unlock(s_ctrl->msm_sensor_mutex);

	return rc;
}

static struct msm_sensor_fn_t moto_se4500_func_tbl = {
	.sensor_config = moto_se4500_sensor_config,
	.sensor_power_up = moto_se4500_sensor_power_up,
	.sensor_power_down = moto_se4500_sensor_power_down,
	.sensor_match_id = moto_se4500_sensor_match_id,
};

static struct msm_sensor_ctrl_t moto_se4500_s_ctrl = {
	.sensor_i2c_client = &moto_se4500_sensor_i2c_client,
	.power_setting_array.power_setting = moto_se4500_power_setting,
	.power_setting_array.size = ARRAY_SIZE(moto_se4500_power_setting),
	.power_setting_array.power_down_setting = moto_se4500_power_down_setting,
	.power_setting_array.size_down = ARRAY_SIZE(moto_se4500_power_down_setting),
	.msm_sensor_mutex = &moto_se4500_mut,
	.sensor_v4l2_subdev_info = moto_se4500_subdev_info,
	.sensor_v4l2_subdev_info_size = ARRAY_SIZE(moto_se4500_subdev_info),
	.func_tbl = &moto_se4500_func_tbl,
};

module_init(moto_se4500_init_module);
module_exit(moto_se4500_exit_module);
MODULE_DESCRIPTION("MOTO SE4500 Driver");
MODULE_LICENSE("GPL v2");
