/*
 * Driver for the TC358746AXBG chip.
 *
 * Copyright (C) SIMcom Corporation
 *
 * Author: Joco
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <linux/kernel.h>
#include <linux/err.h>
#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/of_gpio.h>
#include <mach/gpio.h>
#include <linux/clk.h>
#include <mach/gpiomux.h>
#include <linux/slab.h>
//#include <linux/scanner_type.h>

#define TC358746_DRIVER_NAME "tc358746axbg"
#define PINCTRL_ACTIVE	"clk_active"
#define PINCTRL_SUSPEND	"clk_suspend"

//registers
//16 bit
#define TC358746_CHIP           0x0000 //for chip id and revison id
#define TC358746_SYSCTL         0x0002 //for soft reset
#define TC358746_PLLCTL0        0x0016 //for Program CSI Tx PLL
#define TC358746_PLLCTL1        0x0018
#define TC358746_FIFOCTL        0x0006 //for DPI Input Control
#define TC358746_DATAFMT        0x0008
#define TC358746_WORDCNT        0x0022
#define TC358746_CONFCTL        0x0004
//32 bit
#define TC358746_CLW_CNTRL      0x0140 //for CSI Tx PHY
#define TC358746_D0W_CNTRL      0x0144
#define TC358746_D1W_CNTRL      0x0148
#define TC358746_D2W_CNTRL      0x014C
#define TC358746_D3W_CNTRL      0x0150
#define TC358746_LINE_INIT_CNT  0x0210 //for CSI Tx PPI
#define TC358746_LP_TIME_CNT    0x0214
#define TC358746_TCLK_H_CNT     0x0218
#define TC358746_TCLK_TRAILCNT  0x021C
#define TC358746_THS_H_CNT      0x0220
#define TC358746_T_WAKEUP       0x0224
#define TC358746_TX_CLK_POSTCTL 0x0228
#define TC358746_THS_TRAILCNT   0x022C
#define TC358746_VREG_EN        0x0234
#define TC358746_TX_CONTICTL    0x0238
#define TC358746_STARTCNTRL     0x0204
#define TC358746_CSI_START      0x0518
#define TC358746_CSI_CONFW      0x0500

//values
//16 BIT
#define TC_CHIP               0x4401
#define TC_SYSCTL_RESET_ASS   0x0001 //Assert Reset
#define TC_SYSCTL_RESET_REL   0x0000 //Release Reset, Exit Sleep
#define TC_PLLCTL0            0x2081
#define TC_PLLCTL1            0x0213
#define TC_FIFOCTL            0x00C8
#define TC_DATAFMT            0x0060
#define TC_WORDCNT        	  0x05E0
#define TC_CONFCTL        	  0x0240
//32 BIT
#define TC_CLW_CNTRL_H     	  0x0000
#define TC_CLW_CNTRL_L     	  0x0000
#define TC_D0W_CNTRL_H        0x0000
#define TC_D0W_CNTRL_L     	  0x0000
#define TC_D1W_CNTRL_H        0x0000
#define TC_D1W_CNTRL_L        0x0001
#define TC_D2W_CNTRL_H        0x0000
#define TC_D2W_CNTRL_L        0x0001
#define TC_D3W_CNTRL_H        0x0000
#define TC_D3W_CNTRL_L        0x0001
#define TC_LINE_INIT_CNT_H    0x0000
#define TC_LINE_INIT_CNT_L    0x3000
#define TC_LP_TIME_CNT_H      0x0000
#define TC_LP_TIME_CNT_L      0x0005
#define TC_TCLK_H_CNT_H       0x0000
#define TC_TCLK_H_CNT_L       0x2004
#define TC_TCLK_TRAILCNT_H    0x0000
#define TC_TCLK_TRAILCNT_L    0x0003
#define TC_THS_H_CNT_H        0x0000
#define TC_THS_H_CNT_L        0x1005
#define TC_T_WAKEUP_H         0x0000
#define TC_T_WAKEUP_L         0x5000
#define TC_TX_CLK_POSTCTL_H   0x0000
#define TC_TX_CLK_POSTCTL_L   0x000A
#define TC_THS_TRAILCNT_H     0x0000
#define TC_THS_TRAILCNT_L     0x0004
#define TC_VREG_EN_H          0x0000
#define TC_VREG_EN_L          0x0003
#define TC_TX_CONTICTL_H      0x0000
#define TC_TX_CONTICTL_L      0x0000
#define TC_STARTCNTRL_H       0x0000
#define TC_STARTCNTRL_L       0x0001
#define TC_CSI_START_H        0x0000
#define TC_CSI_START_L        0x0001
#define TC_CSI_CONFW_H        0xA300
#define TC_CSI_CONFW_L        0x80A0

struct tc_register_t {
	const u16 reg_addr;
	const u16 value_h;//msb(16) value
	const u16 value_l;//lsb(16) value
};

static struct tc_register_t sTCRegisterDB[]={
	{TC358746_SYSCTL, 0xFFFF, TC_SYSCTL_RESET_ASS},
	{TC358746_SYSCTL, 0xFFFF, TC_SYSCTL_RESET_REL},
	{TC358746_PLLCTL0, 0xFFFF, TC_PLLCTL0},
	{TC358746_PLLCTL1, 0xFFFF, TC_PLLCTL1},
	{TC358746_FIFOCTL, 0xFFFF, TC_FIFOCTL},
	{TC358746_DATAFMT, 0xFFFF, TC_DATAFMT},
	{TC358746_WORDCNT, 0xFFFF, TC_WORDCNT},
	{TC358746_CLW_CNTRL, TC_CLW_CNTRL_H, TC_CLW_CNTRL_L},
	{TC358746_D0W_CNTRL, TC_D0W_CNTRL_H, TC_D0W_CNTRL_L},
	{TC358746_D1W_CNTRL, TC_D1W_CNTRL_H, TC_D1W_CNTRL_L},
	{TC358746_D2W_CNTRL, TC_D2W_CNTRL_H, TC_D2W_CNTRL_L},
	{TC358746_D3W_CNTRL, TC_D3W_CNTRL_H, TC_D3W_CNTRL_L},
	{TC358746_LINE_INIT_CNT, TC_LINE_INIT_CNT_H, TC_LINE_INIT_CNT_L},
	{TC358746_LP_TIME_CNT, TC_LP_TIME_CNT_H, TC_LP_TIME_CNT_L},
	{TC358746_TCLK_H_CNT, TC_TCLK_H_CNT_H, TC_TCLK_H_CNT_L},
	{TC358746_TCLK_TRAILCNT, TC_TCLK_TRAILCNT_H, TC_TCLK_TRAILCNT_L},
	{TC358746_THS_H_CNT, TC_THS_H_CNT_H, TC_THS_H_CNT_L},
	{TC358746_T_WAKEUP, TC_T_WAKEUP_H, TC_T_WAKEUP_L},
	{TC358746_TX_CLK_POSTCTL, TC_TX_CLK_POSTCTL_H, TC_TX_CLK_POSTCTL_L},
	{TC358746_THS_TRAILCNT, TC_THS_TRAILCNT_H, TC_THS_TRAILCNT_L},
	{TC358746_VREG_EN, TC_VREG_EN_H, TC_VREG_EN_L},
	{TC358746_TX_CONTICTL, TC_TX_CONTICTL_H, TC_TX_CONTICTL_L},
	{TC358746_STARTCNTRL, TC_STARTCNTRL_H, TC_STARTCNTRL_L},
	{TC358746_CSI_START, TC_CSI_START_H, TC_CSI_START_L},
	{TC358746_CSI_CONFW, TC_CSI_CONFW_H, TC_CSI_CONFW_L},
	{TC358746_CONFCTL, 0xFFFF, TC_CONFCTL},	
};

struct tc358746_platform_data {
	int power_gpio;
	int reset_gpio;
	int clk_gpio;
};

struct tc358746_info {
	int power_gpio;
	int reset_gpio;
	int clk_gpio;
	bool clk_run;
	int clk_rate;
	struct i2c_client *client;
	struct clk *refclk;
	struct pinctrl *tc_pinctrl;
	struct pinctrl_state *pinctrl_active;
	struct pinctrl_state *pinctrl_suspend;
};

static struct i2c_client *tc358746_client;
#define TC358746_CLK_FREQ (19200000)

#define TC358746_DEBUG
#if defined(TC358746_DEBUG)
static int debug_flag = 1;

#define TC358746_DEB(fmt, args...) \
do { \
	if (debug_flag) \
		printk(KERN_DEBUG "[%s()->Line:%d] " fmt, __func__, __LINE__, ##args); \
} while(0)
#else
#define TC358746_DEB(fmt, args...)
#endif

#define TC358746_INFO(fmt, args...) \
	pr_info("[%s()->Line:%d] " fmt, __func__, __LINE__, ##args)

#define TC358746_ERR(fmt, args...) \
	pr_err("[%s()->Line:%d] " fmt, __func__, __LINE__, ##args)

#if defined(TC358746_DEBUG)
/* sysfs interface */
static ssize_t tc358746_show(struct device *dev,struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", debug_flag);
}

/* debug fs, "echo @1 > /sys/bus/i2c/devices/xxx/tc358746_debug" @1:debug_flag  */
static ssize_t tc358746_store(struct device *dev,
        struct device_attribute *attr, const char *buf,size_t count)
{
	sscanf(buf, "%d", &debug_flag);
	return count;
}

static struct device_attribute tc358746_dev_attr =
	__ATTR(tc358746_debug, S_IRUGO | S_IWUGO, tc358746_show, tc358746_store);

#endif

static int tc358746_write(struct i2c_client* pClient, u8* data, int data_length)
{
	struct i2c_msg	msg[1];
	int				err;

	msg->addr	= pClient->addr;
	msg->flags	= 0;
	msg->len	= data_length;
	msg->buf	= data;
	err			= i2c_transfer(pClient->adapter, msg, 1);
	if ( err >= 0 )
	{
		err = 0;	// Non-negative indicates success
	}

	return(err);
}

static int tc358746_write_word(struct i2c_client *client, u16 reg, u16 data)
{
	u8 cmd[4];

	cmd[0] = reg >> 8;
	cmd[1] = reg & 0xFF;
	cmd[2] = data >> 8;
	cmd[3] = data & 0xFF;

	TC358746_DEB("Word CMD 0x%02x%02x, 0x%02x%02x\n", cmd[0], cmd[1], cmd[2], cmd[3]);
	
	return tc358746_write(client, cmd, 4);
}

static int tc358746_write_word_word(struct i2c_client *client, u16 reg, u16 data_l, u16 data_h)
{
	u8 cmd[6];

	cmd[0] = reg >> 8;
	cmd[1] = reg & 0xFF;
	cmd[2] = data_l >> 8;
	cmd[3] = data_l & 0xFF;
	cmd[4] = data_h >> 8;
	cmd[5] = data_h & 0xFF;
	
	TC358746_DEB("Word-Word CMD 0x%02x%02x, 0x%02x%02x, 0x%02x%02x\n", cmd[0], 
					cmd[1], cmd[2], cmd[3], cmd[4], cmd[5]);
	
	return tc358746_write(client, cmd, 6);
}

int tc358746_config_registers(int which)
{
	int ret = 0;
	int index;

	//write reg
	for(index = 0; index < ARRAY_SIZE(sTCRegisterDB); index ++){
		if(sTCRegisterDB[index].value_h == 0xFFFF){
			switch(sTCRegisterDB[index].reg_addr){
				case TC358746_PLLCTL0:{
					if(which == 2)//4750
						ret = tc358746_write_word(tc358746_client, TC358746_PLLCTL0, 0x20C4);
					else
						ret = tc358746_write_word(tc358746_client, TC358746_PLLCTL0, 0x2081);
					break;
				}
				case TC358746_FIFOCTL:{
					if(which == 2)//4750
						ret = tc358746_write_word(tc358746_client, TC358746_FIFOCTL, 0x007A);
					else
						ret = tc358746_write_word(tc358746_client, TC358746_FIFOCTL, 0x00C8);
					break;
				}
				case TC358746_WORDCNT:
				{
					if(which == 0)//4500
						ret = tc358746_write_word(tc358746_client, TC358746_WORDCNT, 0x05E0);
					else
						ret = tc358746_write_word(tc358746_client, TC358746_WORDCNT, 0x0AA0);
					break;
				}
				default:
					ret = tc358746_write_word(tc358746_client, sTCRegisterDB[index].reg_addr, sTCRegisterDB[index].value_l);
			}
		}else{
			ret = tc358746_write_word_word(tc358746_client, sTCRegisterDB[index].reg_addr, 
								sTCRegisterDB[index].value_l, sTCRegisterDB[index].value_h);
		}
		if(index == 0){
			mdelay(1);//soft reset need 1000 us
		}
	}

	return ret;
}
EXPORT_SYMBOL(tc358746_config_registers);

static int tc358746_pinctrl_init(struct i2c_client *client,
									struct tc358746_info *info)
{
	int ret;

	/* Get pinctrl if target uses pinctrl */
	info->tc_pinctrl = devm_pinctrl_get(&(client->dev));
	if (IS_ERR_OR_NULL(info->tc_pinctrl)) {
		ret = PTR_ERR(info->tc_pinctrl);
		TC358746_ERR("Target does not use pinctrl %d\n", ret);
		goto err_pinctrl_get;
	}

	info->pinctrl_active
		= pinctrl_lookup_state(info->tc_pinctrl, PINCTRL_ACTIVE);
	if (IS_ERR_OR_NULL(info->pinctrl_active)) {
		ret = PTR_ERR(info->pinctrl_active);
		TC358746_ERR("Can not lookup %s pinstate %d\n",
			PINCTRL_ACTIVE, ret);
		goto err_pinctrl_lookup;
	}

	info->pinctrl_suspend
		= pinctrl_lookup_state(info->tc_pinctrl, PINCTRL_SUSPEND);
	if (IS_ERR_OR_NULL(info->pinctrl_suspend)) {
		ret = PTR_ERR(info->pinctrl_suspend);
		TC358746_ERR("Can not lookup %s pinstate %d\n",
			PINCTRL_SUSPEND, ret);
		goto err_pinctrl_lookup;
	}

	return 0;

err_pinctrl_lookup:
	devm_pinctrl_put(info->tc_pinctrl);
err_pinctrl_get:
	info->tc_pinctrl = NULL;
	return ret;
}

static int tc358746_clk_enable(struct tc358746_info *info, int on)
{
	int rc = -1;
	TC358746_INFO("=======on = %d , clk_run = %d========\n", on, info->clk_run);
	
	if(IS_ERR(info->refclk)){
		TC358746_ERR("clock is NULL.\n");
		return rc;
	}

	if (on) {
		if (info->clk_run == false) {
			pinctrl_select_state(info->tc_pinctrl,
					info->pinctrl_active);
			/*enable the clock*/
			rc = clk_prepare_enable(info->refclk);
			if(rc){
				TC358746_ERR("enable refclk failed.\n");
			}
			info->clk_run = true;
		}
	} else{
		if (info->clk_run == true) {
			pinctrl_select_state(info->tc_pinctrl,
					info->pinctrl_suspend);
			/*disable the clock*/
			clk_disable_unprepare(info->refclk);
			info->clk_run = false;
		}
	}

	return rc;
}

static int tc358746_get_gp_clk(struct i2c_client *client,
	 								struct tc358746_info *info)
{
	int rc = -1;

	info->refclk = clk_get(&client->dev, "tc358746_refclk");
	if(IS_ERR(info->refclk)){
		TC358746_ERR("clock get failed.\n");
		info->refclk = NULL;
		return rc;
	}

	info->clk_rate = TC358746_CLK_FREQ;

	rc = clk_set_rate(info->refclk, info->clk_rate);
	if(rc){
		TC358746_ERR("clock set rate failed rc = %d.\n", rc);
		return rc;
	}
	return 0;
}

int tc358746_reset(int on)
{
	struct tc358746_info *info = i2c_get_clientdata(tc358746_client);
 
 	if(info == NULL)
		return -ENODEV;

	TC358746_INFO("=====reset = %d=====\n", on);
	if(on)
	{
		gpio_direction_output(info->reset_gpio, 1);
	}else{
		gpio_direction_output(info->reset_gpio, 0);
	}
	return tc358746_clk_enable(info, on);
}
EXPORT_SYMBOL(tc358746_reset);

static int tc358746_power_internal(struct tc358746_info *info, int on)
{
	TC358746_INFO("======power = %d======\n", on);
	if(on)
	{		
		gpio_direction_output(info->power_gpio, 1);
		mdelay(10);//must need
		gpio_direction_output(info->reset_gpio, 1);
		mdelay(2);//must need
		tc358746_clk_enable(info, on);
		mdelay(2);
	}else{
		tc358746_clk_enable(info, on);
		gpio_direction_output(info->reset_gpio, 0);
		gpio_direction_output(info->power_gpio, 0);
	}
	
	return 0;
}

static int tc358746_parse_dt(struct device *dev,
			   struct tc358746_platform_data *pdata)
{
	struct device_node *np = dev->of_node;
	int rc;

  	pdata->power_gpio = of_get_named_gpio_flags(np, "tc358746,power", 0, NULL);
    if (pdata->power_gpio < 0) {
		TC358746_ERR("Unable to read tc358746,power\n");
		return pdata->power_gpio;
	}else{
		rc = gpio_request(pdata->power_gpio, "tc358746_power");
		if (rc < 0) {
			TC358746_ERR("Failed to request power_gpio, ERRNO:%d", rc);
			rc = -ENODEV;
			return rc;
		} 
		gpio_direction_output(pdata->power_gpio, 0);
	}

  	pdata->reset_gpio = of_get_named_gpio_flags(np, "tc358746,reset", 0, NULL);
    if (pdata->reset_gpio < 0) {
		TC358746_ERR("Unable to read tc358746,reset\n");
		gpio_free(pdata->power_gpio);
		return pdata->reset_gpio;
	}else{
		rc = gpio_request(pdata->reset_gpio, "tc358746_reset");
		if (rc < 0) {
			TC358746_ERR("Failed to request reset_gpio, ERRNO:%d", rc);
			gpio_free(pdata->power_gpio);
			rc = -ENODEV;
			return rc;
		} 
		gpio_direction_output(pdata->reset_gpio, 0);
	}

	rc = of_get_named_gpio_flags(np, "tc358746,gpclk_gpio", 0, NULL);
	if (rc < 0) {
		TC358746_ERR("Unable to read clk gpio\n");
		gpio_free(pdata->power_gpio);
		gpio_free(pdata->reset_gpio);
		return rc;
	} else {
		pdata->clk_gpio = rc;
	}

	return 0;
}

static int tc358746_probe(struct i2c_client *client,
		const struct i2c_device_id *id)
{
	int ret;
	u16 chip_version;
	struct tc358746_platform_data *pdata;
	struct tc358746_info *tci;
	
	TC358746_INFO("IN !\n");
	tci = kzalloc(sizeof(struct tc358746_info), GFP_KERNEL);
	if (!tci)
		return -ENOMEM;
	
	tc358746_client = client;

	if (client->dev.of_node) {
		pdata = devm_kzalloc(&client->dev,
				     sizeof(struct tc358746_platform_data),
				     GFP_KERNEL);
		if (!pdata) {
			TC358746_ERR("Failed to allocate memory for pdata\n");
			ret = -ENOMEM;
			goto err_kzalloc;
		}

		ret = tc358746_parse_dt(&client->dev, pdata);
		if (ret){
			goto dt_failed;
		}
	} else
		pdata = client->dev.platform_data;

	tci->clk_run = false;
	tci->client = client;
	tci->power_gpio = pdata->power_gpio;
	tci->reset_gpio = pdata->reset_gpio;
	tci->clk_gpio = pdata->clk_gpio;
	
	ret = tc358746_pinctrl_init(client, tci);
	if(ret)
		TC358746_ERR("pinctrl init failed.\n");

	ret = tc358746_get_gp_clk(client, tci);
	if(ret){
		TC358746_ERR("get the gp_clk failed.\n");
		goto get_clk_failed;
	}

	i2c_set_clientdata(client, tci);
	ret = tc358746_power_internal(tci, 1);
	if(ret){
		TC358746_ERR("power failed.\n");
		goto probe_failed;
	}

	chip_version = i2c_smbus_read_word_data(client, TC358746_CHIP);
	TC358746_INFO("chip_version: 0x%04x\n", swab16(chip_version));
	if(swab16(chip_version) != TC_CHIP){
		TC358746_ERR("match chip id and version failed.\n");
		ret = -ENODEV;
		goto probe_failed;
	}
	
#ifdef TC358746_DEBUG
	ret = device_create_file(&client->dev, &tc358746_dev_attr);
	if (ret) {
		TC358746_ERR("sysfs registration failed, error %d \n", ret);
		goto probe_failed;
	}
#endif

	tc358746_power_internal(tci, 0);
	gpio_free(tci->power_gpio);
	return 0;

probe_failed:
	tc358746_power_internal(tci, 0);
	clk_put(tci->refclk);
	tci->refclk = NULL;
get_clk_failed:
	gpio_free(pdata->power_gpio);
	gpio_free(pdata->reset_gpio);
dt_failed:
	devm_kfree(&client->dev, pdata);
err_kzalloc:
	kfree(tci);
	TC358746_ERR("error exit..\n");
	return ret;
}

static int tc358746_remove(struct i2c_client *client)
{
	struct tc358746_info *info = i2c_get_clientdata(client);
#ifdef TC358746_DEBUG
	device_remove_file(&client->dev, &tc358746_dev_attr);
#endif
	if(info->refclk != NULL){
		if(info->clk_run)
			clk_disable_unprepare(info->refclk);
		clk_put(info->refclk);
		info->refclk = NULL;
	}
	return 0;
}

static int tc358746_suspend(struct i2c_client *client, pm_message_t state)
{
	struct tc358746_info *info = i2c_get_clientdata(client);
	
	if(info->refclk != NULL)
		clk_disable_unprepare(info->refclk);
	
	return 0;
}

static int tc358746_resume(struct i2c_client *client)
{
	struct tc358746_info *info = i2c_get_clientdata(client);

	if(info->refclk != NULL)
		clk_prepare_enable(info->refclk);

	return 0;
}

static const struct i2c_device_id tc358746_id[] = {
	{ TC358746_DRIVER_NAME, 0 },
	{ }
};

static struct of_device_id tc358746_match_table[] = {
	{ .compatible = "tosiba,tc358746axbg",},
	{ },
};

static struct i2c_driver tc358746_driver = {
	.id_table	= tc358746_id,
	.probe		= tc358746_probe,
	.remove		= tc358746_remove,
	.driver		= {
		.owner	= THIS_MODULE,
		.name	= TC358746_DRIVER_NAME,
		.of_match_table = tc358746_match_table,
	},
	.suspend = tc358746_suspend,
	.resume = tc358746_resume,
};

/*
 * module load/unload record keeping
 */

static int __init tc358746_dev_init(void)
{
	//if(scanner_type != SCANNER_2D){
	//	TC358746_INFO("Don't use 2D Scanner\n");
	//	return -ENODEV;
	//}
	
	TC358746_INFO("Loading TC358746 driver\n");
	return i2c_add_driver(&tc358746_driver);
}

static void __exit tc358746_dev_exit(void)
{
	TC358746_INFO("Unloading TC358746 driver\n");
	i2c_del_driver(&tc358746_driver);
}

module_init(tc358746_dev_init);
module_exit(tc358746_dev_exit);

MODULE_AUTHOR("SIMcom CDMA-Dept");
MODULE_DESCRIPTION("TC358746AXBG driver");
MODULE_LICENSE("GPL");
