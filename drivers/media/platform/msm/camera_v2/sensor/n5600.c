/*
 * drivers/media/platform/msm/camera_v2/sensor/n5600.c
 *
 * Honeywell N5600 sensor driver
 *
 * Copyright (C) 2011 SIMTech Solutions, Inc
 * Author: pingsheng.zhang@sim.com
 *
 * This file is licensed under the terms of the GNU General Public License
 * version 2. This program is licensed "as is" without any warranty of any
 * kind, whether express or implied.
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/videodev2.h>
#include <linux/i2c-dev.h>
#include "msm_sensor.h"
#include "msm_cci.h"
#include "msm_camera_io_util.h"
#include <media/soc_camera.h>
#include <media/v4l2-chip-ident.h>
#include <media/v4l2-subdev.h>
#include <media/v4l2-chip-ident.h>
#include <asm/gpio.h>
#include <mach/hardware.h>
#include <linux/io.h>
#include <asm/io.h>
#include <linux/i2c-dev.h>
#include <mach/gpiomux.h>
#include <media/hsm_imager.h>
#include <linux/version.h>
#ifdef CONFIG_USE_OF	// Device Tree Machine Description
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/of_i2c.h>
#endif
#ifdef CONFIG_ARCH_MSM
#include "msm_sensor.h"
#endif
#include "camera.h"

//#define N5600_DEBUG
#ifdef N5600_DEBUG
#undef CDBG
#define CDBG(fmt, args...) printk(KERN_DEBUG "[%s()->Line:%d]: " fmt, __func__, __LINE__, ##args)
#else
#undef CDBG
#define CDBG(fmt, args...) do{;}while(0);
#endif

#define MAX_TMP_BUF_SIZE	256
#define I2C_RETRY_COUNT		1

#define JADE_REVISION       0x0700
#define JADE_REVISION_2     0x0701

#define N5600_SENSOR_NAME     "n5600" 
#ifdef CONFIG_TOSHIBA_TC358746
extern int tc358746_reset(int on);
static bool first_probe = true;
#endif
static struct msm_sensor_ctrl_t n5600_s_ctrl;
static struct msm_camera_i2c_client n5600_sensor_i2c_client;
static uint16_t jadid = 0;
#define IS_MIPI_I2C_ADDR(a)	(((a)==TC358748_I2C_ADDR) || ((a)==TC358746_I2C_ADDR))

const char *dbg_gpio_name(unsigned int gpio)
{
	const char	*GPIOName[] = {"PwrEn", "Aimer", "Illum", "Reset", "VSync", "MIPI-Rst", "PwrSupply"};

	return (gpio < ARRAY_SIZE(GPIOName))? GPIOName[gpio] : "Unknown";
}

struct hsm_dev {
	/* V4L stuff */
	struct v4l2_subdev subdev;
	struct v4l2_ctrl_handler ctrl_handler;
	/* I2C clients */
	struct i2c_client * client;		/* Sensor @ 0x18 or 0x48 */

#ifdef CONFIG_MEDIA_CONTROLLER
	struct media_pad pad;
#endif

	/* platform specifics (mounting, gpio, ..) */
	struct hsm_platform_data platform;

	/* format and configuration */
	struct v4l2_mbus_framefmt format;

	/* */
	u8 i2c_addr_sensor;		//!< Address of camera sensor in 7bit format
	u8 i2c_addr_psoc;		//!< Address of PSOC in 7bit format
	u8 i2c_addr_clock;		//!< Address of clock chip in 7bit format
	u8 i2c_addr_mipi;		//!< Address of mipi in 7bit formatq
	s32 SensorRevision;
	uint32_t num_clk;
	struct i2c_adapter * i2c_adapter_mipi;
	int width;
	int height;
	u8 supplied;
	u8 need_reinit;
	u8 gpio_requested;
#ifdef CONFIG_ARCH_MSM
	struct msm_sensor_ctrl_t	msm_s_ctrl;
	struct msm_camera_i2c_client	msm_i2c_client;
#endif
};

DEFINE_MSM_MUTEX(n5600_mut);
static struct msm_sensor_power_setting n5600_power_setting[] = {
	{
		.seq_type = SENSOR_VREG,
		.seq_val = 0,
		.config_val = 0,
		.delay = 1,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_VIO,
		.config_val = GPIO_OUT_HIGH,
		.delay = 1,
	},//mipi select must be high
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_VANA,
		.config_val = GPIO_OUT_HIGH,
		.delay = 0,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_STANDBY,
		.config_val = GPIO_OUT_HIGH,
		.delay = 0,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_RESET,
		.config_val = GPIO_OUT_HIGH,
		.delay = 5,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_RESET,
		.config_val = GPIO_OUT_LOW,
		.delay = 10,
	},
	{
		.seq_type = SENSOR_CLK,
		.seq_val = SENSOR_CAM_MCLK,
		.config_val = 24000000,
		.delay = 5,
	},
	{
		.seq_type = SENSOR_I2C_MUX,
		.seq_val = 0,
		.config_val = 0,
		.delay = 0,
	},
};

static struct msm_sensor_power_setting n5600_power_down_setting[] = {
	{
		.seq_type = SENSOR_VREG,
		.seq_val = 0,
		.config_val = 0,
		.delay = 1,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_VIO,
		.config_val = GPIO_OUT_LOW,
		.delay = 1,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_VANA,
		.config_val = GPIO_OUT_LOW,
		.delay = 2,
	},
	{
		.seq_type = SENSOR_GPIO,
		.seq_val = SENSOR_GPIO_STANDBY,
		.config_val = GPIO_OUT_LOW,
		.delay = 2,
	},
	{
		.seq_type = SENSOR_CLK,
		.seq_val = SENSOR_CAM_MCLK,
		.config_val = 24000000,
		.delay = 5,
	},
	{
		.seq_type = SENSOR_I2C_MUX,
		.seq_val = 0,
		.config_val = 0,
		.delay = 0,
	},
};

static struct v4l2_subdev_info n5600_subdev_info[] = {
	{
		.code   = V4L2_MBUS_FMT_SGRBG8_1X8,
		.colorspace = V4L2_COLORSPACE_JPEG,
		.fmt    = 1,
		.order    = 0,
	},
};

static const struct i2c_device_id n5600_i2c_id[] = {
	{N5600_SENSOR_NAME, (kernel_ulong_t)&n5600_s_ctrl},
	{ }
};

static int32_t n5600_sensor_power_up(struct msm_sensor_ctrl_t *s_ctrl)
{
	int32_t rc = 0;
	
	CDBG("======power up======\n");
	rc = msm_sensor_power_up(s_ctrl);
#ifdef CONFIG_TOSHIBA_TC358746
	if(!first_probe){
		rc = tc358746_reset(1);
		if(rc){
			CDBG("tc358746_reset failed: %d !\n", rc);
		}
	}
#endif

	return rc;
}

static int32_t n5600_sensor_power_down(struct msm_sensor_ctrl_t *s_ctrl)
{
	int32_t rc = 0;
	
	CDBG("======power down======\n");

	rc = msm_sensor_power_down(s_ctrl);
#ifdef CONFIG_TOSHIBA_TC358746
	if(!first_probe){
		tc358746_reset(0);
	}

	first_probe = false;
#endif

	return rc;
}

static int32_t n5600_sensor_match_id(struct msm_sensor_ctrl_t *s_ctrl)
{
	int rc = 0;
	struct msm_camera_i2c_client *sensor_i2c_client;
	struct msm_camera_slave_info *slave_info;
	const char *sensor_name;

	if (!s_ctrl) {
		pr_err("%s:%d failed: %p\n",
			__func__, __LINE__, s_ctrl);
		return -EINVAL;
	}
	sensor_i2c_client = s_ctrl->sensor_i2c_client;
	slave_info = s_ctrl->sensordata->slave_info;
	sensor_name = s_ctrl->sensordata->sensor_name;

	if (!sensor_i2c_client || !slave_info || !sensor_name) {
		pr_err("%s:%d failed: %p %p %p\n",
			__func__, __LINE__, sensor_i2c_client, slave_info,
			sensor_name);
		return -EINVAL;
	}

	rc = sensor_i2c_client->i2c_func_tbl->i2c_read(
		sensor_i2c_client, slave_info->sensor_id_reg_addr,
		&jadid, MSM_CAMERA_I2C_WORD_DATA);
	if (rc < 0) {
		pr_err("%s: %s: read id failed\n", __func__, sensor_name);
		return rc;
	}

	CDBG("read id: 0x%x expected id 0x%x:\n", jadid,
		slave_info->sensor_id);

	if ((jadid != slave_info->sensor_id) && (jadid != JADE_REVISION_2) ){
		pr_err("%s: chipid does not match\n", __func__);
		return -ENODEV;
	}
	return rc;
}

int32_t n5600_sensor_config(struct msm_sensor_ctrl_t *s_ctrl,
	void __user *argp)
{
	struct sensorb_cfg_data *cdata = (struct sensorb_cfg_data *)argp;
	long rc = 0;
	int32_t i = 0;
	mutex_lock(s_ctrl->msm_sensor_mutex);
	CDBG("%s cfgtype = %d\n",
		s_ctrl->sensordata->sensor_name, cdata->cfgtype);
	switch (cdata->cfgtype) {
	case CFG_GET_SENSOR_INFO:
		memcpy(cdata->cfg.sensor_info.sensor_name,
			s_ctrl->sensordata->sensor_name,
			sizeof(cdata->cfg.sensor_info.sensor_name));
		cdata->cfg.sensor_info.session_id =
			s_ctrl->sensordata->sensor_info->session_id;
		for (i = 0; i < SUB_MODULE_MAX; i++)
			cdata->cfg.sensor_info.subdev_id[i] =
				s_ctrl->sensordata->sensor_info->subdev_id[i];
		cdata->cfg.sensor_info.is_mount_angle_valid =
			s_ctrl->sensordata->sensor_info->is_mount_angle_valid;
		cdata->cfg.sensor_info.sensor_mount_angle =
			s_ctrl->sensordata->sensor_info->sensor_mount_angle;
		CDBG("sensor name %s\n",
			cdata->cfg.sensor_info.sensor_name);
		CDBG("session id %d\n",
			cdata->cfg.sensor_info.session_id);
		for (i = 0; i < SUB_MODULE_MAX; i++)
			CDBG("subdev_id[%d] %d\n", i,
				cdata->cfg.sensor_info.subdev_id[i]);
		CDBG("mount angle valid %d value %d\n", 
			cdata->cfg.sensor_info.is_mount_angle_valid,
			cdata->cfg.sensor_info.sensor_mount_angle);
		break;
	case CFG_SET_INIT_SETTING:
		CDBG("init setting\n");
		break;
	case CFG_SET_RESOLUTION: {
		/*copy from user the desired resoltuion*/
		enum msm_sensor_resolution_t res = MSM_SENSOR_INVALID_RES;
		if (copy_from_user(&res, (void *)cdata->cfg.setting,
			sizeof(enum msm_sensor_resolution_t))) {
			CDBG("failed....\n");
			rc = -EFAULT;
			break;
		}
		break;
	}
	case CFG_SET_STOP_STREAM:
		CDBG("stop stream!!\n");
		break;
	case CFG_SET_START_STREAM:
		CDBG("start stream!!\n");
		break;
	case CFG_GET_SENSOR_INIT_PARAMS:
		cdata->cfg.sensor_init_params.modes_supported =
			s_ctrl->sensordata->sensor_info->modes_supported;
		cdata->cfg.sensor_init_params.position =
			s_ctrl->sensordata->sensor_info->position;
		cdata->cfg.sensor_init_params.sensor_mount_angle =
			s_ctrl->sensordata->sensor_info->sensor_mount_angle;
		CDBG("init params mode %d pos %d mount %d\n",
			cdata->cfg.sensor_init_params.modes_supported,
			cdata->cfg.sensor_init_params.position,
			cdata->cfg.sensor_init_params.sensor_mount_angle);
		break;
	case CFG_SET_SLAVE_INFO: {
		struct msm_camera_sensor_slave_info *sensor_slave_info;
		struct msm_camera_power_ctrl_t *p_ctrl;
		uint16_t size;
		int slave_index = 0;
		sensor_slave_info = kmalloc(sizeof(struct msm_camera_sensor_slave_info)
				      * 1, GFP_KERNEL);

		if (!sensor_slave_info) {
			CDBG("failed to alloc mem\n");
			rc = -ENOMEM;
			break;
		}
		if (copy_from_user(sensor_slave_info,
			(void *)cdata->cfg.setting,
			sizeof(struct msm_camera_sensor_slave_info))) {
			CDBG("failed!!\n");
			rc = -EFAULT;
			break;
		}
		/* Update sensor slave address */
		if (sensor_slave_info->slave_addr)
			s_ctrl->sensor_i2c_client->cci_client->sid =
				sensor_slave_info->slave_addr >> 1;

		/* Update sensor address type */
		s_ctrl->sensor_i2c_client->addr_type =
			sensor_slave_info->addr_type;

		/* Update power up / down sequence */
		p_ctrl = &s_ctrl->sensordata->power_info;
		size = sensor_slave_info->power_setting_array.size;
		if (p_ctrl->power_setting_size < size) {
			struct msm_sensor_power_setting *tmp;
			tmp = kmalloc(sizeof(struct msm_sensor_power_setting)
				      * size, GFP_KERNEL);
			if (!tmp) {
				CDBG("failed to alloc mem\n");
				rc = -ENOMEM;
				break;
			}
			kfree(p_ctrl->power_setting);
			p_ctrl->power_setting = tmp;
		}
		p_ctrl->power_setting_size = size;

		rc = copy_from_user(p_ctrl->power_setting, (void *)
			sensor_slave_info->power_setting_array.power_setting,
			size * sizeof(struct msm_sensor_power_setting));
		if (rc) {
			CDBG("failed\n");
			rc = -EFAULT;
			break;
		}
		for (slave_index = 0; slave_index <
			p_ctrl->power_setting_size; slave_index++) {
			CDBG("i = %d power setting %d %d %ld %d\n",
				slave_index,
				p_ctrl->power_setting[slave_index].seq_type,
				p_ctrl->power_setting[slave_index].seq_val,
				p_ctrl->power_setting[slave_index].config_val,
				p_ctrl->power_setting[slave_index].delay);
		}
		break;
	}
	case CFG_WRITE_I2C_ARRAY: {
		struct msm_camera_i2c_reg_setting conf_array;
		struct msm_camera_i2c_reg_array *reg_setting = NULL;

		if (copy_from_user(&conf_array,
			(void *)cdata->cfg.setting,
			sizeof(struct msm_camera_i2c_reg_setting))) {
			CDBG("failed...\n");
			rc = -EFAULT;
			break;
		}

		if (!conf_array.size ||
			conf_array.size > I2C_REG_DATA_MAX) {
			CDBG("failed...\n");
			rc = -EFAULT;
			break;
		}

		reg_setting = kzalloc(conf_array.size *
			(sizeof(struct msm_camera_i2c_reg_array)), GFP_KERNEL);
		if (!reg_setting) {
			CDBG("failed....\n");
			rc = -ENOMEM;
			break;
		}
		if (copy_from_user(reg_setting, (void *)conf_array.reg_setting,
			conf_array.size *
			sizeof(struct msm_camera_i2c_reg_array))) {
			CDBG("failed.....\n");
			kfree(reg_setting);
			rc = -EFAULT;
			break;
		}

		conf_array.reg_setting = reg_setting;
		rc = s_ctrl->sensor_i2c_client->i2c_func_tbl->i2c_write_table(
			s_ctrl->sensor_i2c_client, &conf_array);
		kfree(reg_setting);
		break;
	}
	case CFG_WRITE_I2C_SEQ_ARRAY: {
		struct msm_camera_i2c_seq_reg_setting conf_array;
		struct msm_camera_i2c_seq_reg_array *reg_setting = NULL;

		if (copy_from_user(&conf_array,
			(void *)cdata->cfg.setting,
			sizeof(struct msm_camera_i2c_seq_reg_setting))) {
			CDBG("failed...\n");
			rc = -EFAULT;
			break;
		}

		if (!conf_array.size ||
			conf_array.size > I2C_SEQ_REG_DATA_MAX) {
			CDBG("failed...\n");
			rc = -EFAULT;
			break;
		}

		reg_setting = kzalloc(conf_array.size *
			(sizeof(struct msm_camera_i2c_seq_reg_array)),
			GFP_KERNEL);
		if (!reg_setting) {
			CDBG("failed...\n");
			rc = -ENOMEM;
			break;
		}
		if (copy_from_user(reg_setting, (void *)conf_array.reg_setting,
			conf_array.size *
			sizeof(struct msm_camera_i2c_seq_reg_array))) {
			CDBG("failed...\n");
			kfree(reg_setting);
			rc = -EFAULT;
			break;
		}

		conf_array.reg_setting = reg_setting;
		rc = s_ctrl->sensor_i2c_client->i2c_func_tbl->
			i2c_write_seq_table(s_ctrl->sensor_i2c_client,
			&conf_array);
		kfree(reg_setting);
		break;
	}

	case CFG_POWER_UP:
		if (s_ctrl->func_tbl->sensor_power_up)
			rc = s_ctrl->func_tbl->sensor_power_up(s_ctrl);
		else
			rc = -EFAULT;
		break;

	case CFG_POWER_DOWN:
		if (s_ctrl->func_tbl->sensor_power_down)
			rc = s_ctrl->func_tbl->sensor_power_down(s_ctrl);
		else
			rc = -EFAULT;
		break;

	case CFG_SET_STOP_STREAM_SETTING: {
		struct msm_camera_i2c_reg_setting *stop_setting =
			&s_ctrl->stop_setting;
		struct msm_camera_i2c_reg_array *reg_setting = NULL;
		if (copy_from_user(stop_setting, (void *)cdata->cfg.setting,
			sizeof(struct msm_camera_i2c_reg_setting))) {
			CDBG("failed...\n");
			rc = -EFAULT;
			break;
		}

		reg_setting = stop_setting->reg_setting;
		stop_setting->reg_setting = kzalloc(stop_setting->size *
			(sizeof(struct msm_camera_i2c_reg_array)), GFP_KERNEL);
		if (!stop_setting->reg_setting) {
			CDBG("failed...\n");
			rc = -ENOMEM;
			break;
		}
		if (copy_from_user(stop_setting->reg_setting,
			(void *)reg_setting, stop_setting->size *
			sizeof(struct msm_camera_i2c_reg_array))) {
			CDBG("failed...\n");
			kfree(stop_setting->reg_setting);
			stop_setting->reg_setting = NULL;
			stop_setting->size = 0;
			rc = -EFAULT;
			break;
		}
		break;
		}
	case CFG_SET_STREAM_TYPE: {
		enum msm_camera_stream_type_t stream_type = MSM_CAMERA_STREAM_INVALID;
		if (copy_from_user(&stream_type, (void *)cdata->cfg.setting,
			sizeof(enum msm_camera_stream_type_t))) {
			CDBG("failed...\n");
			rc = -EFAULT;
			break;
		}
		s_ctrl->camera_stream_type = stream_type;
		break;
	}
	case CFG_SET_SATURATION:
		break;
	case CFG_SET_CONTRAST:
		break;
	case CFG_SET_SHARPNESS:
		break;
	case CFG_SET_AUTOFOCUS:
		/* TO-DO: set the Auto Focus */
		CDBG("Setting Auto Focus\n");
		break;
	case CFG_CANCEL_AUTOFOCUS:
		/* TO-DO: Cancel the Auto Focus */
		CDBG("Cancelling Auto Focus\n");
		break;
	case CFG_SET_ISO:
		break;
	case CFG_SET_EXPOSURE_COMPENSATION:
		break;
	case CFG_SET_EFFECT:
		break;
	case CFG_SET_ANTIBANDING:
		break;
	case CFG_SET_BESTSHOT_MODE:
		break;
	case CFG_SET_WHITE_BALANCE:
		break;
	default:
		rc = -EFAULT;
		break;
	}

	mutex_unlock(s_ctrl->msm_sensor_mutex);

	return rc;
}

static struct msm_sensor_fn_t n5600_func_tbl = {
	.sensor_config = n5600_sensor_config,
	.sensor_power_up = n5600_sensor_power_up,
	.sensor_power_down = n5600_sensor_power_down,
	.sensor_match_id = n5600_sensor_match_id,
};

static struct msm_sensor_ctrl_t n5600_s_ctrl = {
	.sensor_i2c_client = &n5600_sensor_i2c_client,
	.power_setting_array.power_setting = n5600_power_setting,
	.power_setting_array.size = ARRAY_SIZE(n5600_power_setting),
	.power_setting_array.power_down_setting = n5600_power_down_setting,
	.power_setting_array.size_down = ARRAY_SIZE(n5600_power_down_setting),
	.msm_sensor_mutex = &n5600_mut,
	.sensor_v4l2_subdev_info = n5600_subdev_info,
	.sensor_v4l2_subdev_info_size = ARRAY_SIZE(n5600_subdev_info),
	.func_tbl = &n5600_func_tbl,
};

inline struct hsm_dev *to_hsm(const struct i2c_client * client)
{
#ifdef CONFIG_ARCH_MSM
	return container_of(
		container_of(
			container_of(i2c_get_clientdata(client), struct msm_sd_subdev, sd),
			struct msm_sensor_ctrl_t,
			msm_sd),
		struct hsm_dev,
		msm_s_ctrl);
#else
	return container_of(i2c_get_clientdata(client), struct hsm_dev, subdev);
#endif
}

/* ---------- functions for register access on i2c ---------- */
/**
 * hsm_read_reg - Read I2C register
 * @adapter: i2c_adapter object
 * @i2c_addr: I2C address
 * @reg: First register number
 * @buf: Buffer to be filled
 * @len: Length of buffer
 * Returns zero on success, else negative errno.
 */
static int hsm_read_reg(struct i2c_adapter *adapter, u8 i2c_addr, u16 reg,
		u8 *buf, u8 len) {
	int err = 0;
	u8 regbytes[2];
	struct i2c_msg msg[2];

	CDBG("addr[0x%02x], reg[0x%x] ,len[%d]\n", i2c_addr, reg, len);
	if (!adapter) {
		err = -ENODEV;
	} else {
		if (IS_MIPI_I2C_ADDR(i2c_addr)) {
          	msg[0].len = 2;
			regbytes[0] = (u8) (reg >> 8); // MSB first
			regbytes[1] = (u8) reg;
			//return 0;
		} else {
			msg[0].len = 1;
			regbytes[0] = (u8) reg;
		}

		msg[0].addr = i2c_addr;
		msg[0].flags = 0;
		msg[0].buf = regbytes;

		msg[1].addr = i2c_addr;
		msg[1].flags = I2C_M_RD;
		msg[1].len = len;
		msg[1].buf = buf;
		err = i2c_transfer(adapter, msg, 2);
	}

	{
		int i;
		for(i = 0; i < len; i++)
		CDBG("buf[%d]= 0x%x\n", i, buf[i]);
	}
    CDBG("i2c_transfer result = %d\n", err);
	if(err == 2) {
		err = 0;
	} else if (err >= 0) {
		err = -EIO;
	} else if (err < 0) {
		if (err != -ENXIO) {
			CDBG("failed err = %d, i2c:%02X, reg:%i\n", err, i2c_addr, reg);
		}
	}
	return err;
}

/**
 * hsm_write_reg - Write I2C register
 * @adapter: i2c_adapter object
 * @i2c_addr: I2C address
 * @reg: First register number
 * @buf: Buffer to be written
 * @len: Length of buffer
 * Returns zero on success, else negative errno.
 */
static int hsm_write_reg(struct i2c_adapter *adapter, u8 i2c_addr, u16 reg,
			const u8 *buf, u8 len) {
	int err = -1;
	int trycnt = 0;
	struct i2c_msg msg[2];
	unsigned char tmp[MAX_TMP_BUF_SIZE];
	
	if (!adapter) {
		err = -ENODEV;
		return err;
	}
	
	CDBG("addr[0x%02x], reg[0x%x] ,len[%d]\n", i2c_addr, reg, len);
	{
		int i;
		for(i = 0 ; i < len;i++)
			CDBG("buf[%d]= 0x%x\n", i, buf[i]);
    }

	if (IS_MIPI_I2C_ADDR(i2c_addr)) {
		msg[0].len = 2;
		tmp[0] = (u8) (reg >> 8); // MSB first
		tmp[1] = (u8) reg;
		//return 0;
	} else {
		msg[0].len = 1;
		tmp[0] = (u8) reg;
	}

	memcpy(&tmp[msg[0].len], buf, len);
	msg[0].addr = i2c_addr;
	msg[0].flags = 0;
	msg[0].len += len;
	msg[0].buf = tmp;

	while ((err < 0) && (trycnt < I2C_RETRY_COUNT)) {
		trycnt++;
		err = i2c_transfer(adapter, msg, 1);
	}

    CDBG("i2c_transfer result = %d\n", err);
	if(err == 1) {
		err = 0;
	} else if (err >= 0) {
		err = -EIO;
	} else if (err < 0) {
		if (err != -ENXIO) {
			CDBG("failed err = %d, i2c:%02X, reg:%i\n", -err, i2c_addr, reg);
		}
	}
	return err;
}

/* taken from i2c-dev.c */
/* Returns negative errno, else the number of messages executed. */
static noinline int hsm_ioctl_rdrw(struct i2c_adapter *adapter,
		void *arg)
{
	struct i2c_rdwr_ioctl_data *p_rdwr_arg =
					(struct i2c_rdwr_ioctl_data*) arg;
	struct i2c_msg *rdwr_pa;
	u8 __user **data_ptrs;
	int i, res;

	/* *arg is is already in kernel space. Was handled by the V4L2 layer */

	/* Put an arbitrary limit on the number of messages that can
	 * be sent at once */
	if (p_rdwr_arg->nmsgs > I2C_RDRW_IOCTL_MAX_MSGS)
		return -EINVAL;

	rdwr_pa = kmalloc(p_rdwr_arg->nmsgs * sizeof(struct i2c_msg),
			  GFP_KERNEL);
	if (!rdwr_pa)
		return -ENOMEM;

	if (copy_from_user(rdwr_pa, p_rdwr_arg->msgs,
			   p_rdwr_arg->nmsgs * sizeof(struct i2c_msg))) {
		kfree(rdwr_pa);
		return -EFAULT;
	}

	data_ptrs = kmalloc(p_rdwr_arg->nmsgs * sizeof(u8 __user *), GFP_KERNEL);
	if (data_ptrs == NULL) {
		kfree(rdwr_pa);
		return -ENOMEM;
	}

	res = 0;
	for (i = 0; i < p_rdwr_arg->nmsgs; i++) {
		/* Limit the size of the message to a sane amount;
		 * and don't let length change either. */
		if ((rdwr_pa[i].len > 8192) ||
		    (rdwr_pa[i].flags & I2C_M_RECV_LEN)) {
			res = -EINVAL;
			break;
		}
		data_ptrs[i] = (u8 __user *)rdwr_pa[i].buf;
		rdwr_pa[i].buf = memdup_user(data_ptrs[i], rdwr_pa[i].len);
		if (IS_ERR(rdwr_pa[i].buf)) {
			res = PTR_ERR(rdwr_pa[i].buf);
			break;
		}
	}
	if (res < 0) {
		int j;
		for (j = 0; j < i; ++j)
			kfree(rdwr_pa[j].buf);
		kfree(data_ptrs);
		kfree(rdwr_pa);
		return res;
	}

	res = i2c_transfer(adapter, rdwr_pa, p_rdwr_arg->nmsgs);
	while (i-- > 0) {
		if (res >= 0 && (rdwr_pa[i].flags & I2C_M_RD)) {
			if (copy_to_user(data_ptrs[i], rdwr_pa[i].buf,
					 rdwr_pa[i].len))
				res = -EFAULT;
		}
		kfree(rdwr_pa[i].buf);
	}
	kfree(data_ptrs);
	kfree(rdwr_pa);
	return res;
}

/* ---------- functions for user land interface ---------- */

/**
 * hsm_ioctl - IOCTL handler
 * @sd: device object
 * @cmd: Command
 * @arg: arguments
 */

#ifdef CONFIG_VIDEO_FIMC
/* Samsung needs adjacent frame buffers
 * and probes only after VIDIOC_S_INPUT */
#define PLATFORM_DETAILS \
	HSM_PLATFORM_HAS_STATUS| \
	HSM_PLATFORM_PROBE_AFTER_S_INPUT| \
	HSM_PLATFORM_SET_SKIP_IMAGES(1)
#else
/* Others want frame buffers be aligned */
#define PLATFORM_DETAILS \
	HSM_PLATFORM_HAS_STATUS| \
	HSM_PLATFORM_BUFFER_ALLIGN
#endif

static int hsm_gpio_set(struct hsm_dev * dev, int id, unsigned char value)
{
	struct msm_sensor_ctrl_t *s_ctrl = &dev->msm_s_ctrl;
	struct msm_camera_power_ctrl_t *ctrl;
	int gpio_index = HSM_NUM_GPIO;
	ctrl = &s_ctrl->sensordata->power_info;
	if(ctrl == NULL)
	{
	  pr_err("%s:%d power_info NULL\n", __func__, __LINE__);
	  return -ENXIO;
	}
	
	CDBG("%s: %s = %d\n", __func__, dbg_gpio_name(id), value);
	if(id == HSM_GPIO_POWER_ENABLE)
		   gpio_index = SENSOR_GPIO_STANDBY;
	else if(id == HSM_GPIO_AIMER)
		   gpio_index = SENSOR_GPIO_VAF;   
	else if(id == HSM_GPIO_ILLUMINATOR)
		   gpio_index = SENSOR_GPIO_VDIG;
	else if(id == HSM_GPIO_ENGINE_RESET)
		   gpio_index = SENSOR_GPIO_RESET;
	else if(id == HSM_GPIO_POWER_SUPPLY)
		   gpio_index = SENSOR_GPIO_VIO;
	else if(id == HSM_GPIO_MIPI_RESET){
#ifdef CONFIG_TOSHIBA_TC358746
		tc358746_reset((int)value);
#endif
		return 0;
	}

	if(gpio_index == HSM_NUM_GPIO)
		   return -ENXIO;
	if (!ctrl->gpio_conf->gpio_num_info->valid[gpio_index]){
		CDBG("invalid gpio\n");
	} else{
		gpio_set_value_cansleep(ctrl->gpio_conf->gpio_num_info->gpio_num[gpio_index], (int)value);
	}
	
    return 0;
}

static int hsm_gpio_get(struct hsm_dev * dev, int id,
		unsigned char *pvalue)
{
	struct msm_sensor_ctrl_t *s_ctrl = &dev->msm_s_ctrl;
	struct msm_camera_power_ctrl_t *ctrl;
	int gpio_index = HSM_NUM_GPIO;
	ctrl = &s_ctrl->sensordata->power_info;
	if(ctrl == NULL)
	{
		pr_err("%s:%d power_info NULL\n", __func__, __LINE__);
		return -ENXIO;
	}
	
	if(id == HSM_GPIO_POWER_ENABLE)
		gpio_index = SENSOR_GPIO_STANDBY;
	else if(id == HSM_GPIO_AIMER)
		gpio_index = SENSOR_GPIO_VAF;	
	else if(id == HSM_GPIO_ILLUMINATOR)
		gpio_index = SENSOR_GPIO_VDIG;
	else if(id == HSM_GPIO_ENGINE_RESET)
		gpio_index = SENSOR_GPIO_RESET;
	else if(id == HSM_GPIO_POWER_SUPPLY)
		gpio_index = SENSOR_GPIO_VIO;

	if(gpio_index == HSM_NUM_GPIO)
	   return -ENXIO;

	if (!ctrl->gpio_conf->gpio_num_info->valid[gpio_index]){
	   	CDBG("invalid gpio\n");
	} else{
		*pvalue = gpio_get_value(ctrl->gpio_conf->gpio_num_info->gpio_num[gpio_index]) ? true : false;
	}
	
	CDBG("%s: %s = %d\n", __func__, dbg_gpio_name(id), *pvalue);
	return 0;
}

static long hsm_ioctl(struct v4l2_subdev *sd, unsigned int cmd, void *arg) {
	long ret = 0;
	struct i2c_client *client = v4l2_get_subdevdata(sd);
	struct hsm_dev * dev = to_hsm(client);

//	CDBG("(cmd=%X)", cmd);
	switch (cmd) {
	case HSM_GET_PROPERTIES: {
		struct hsm_engine_properties *prop = arg;
		if (prop->version == HSM_PROPERTIES_V1) {
			prop->width = dev->width;
			prop->height = dev->height;
			prop->mount = dev->platform.mount;
			prop->spreadspectrum = dev->platform.spreadspectrum;
			prop->i2c_addr_sensor = dev->i2c_addr_sensor;
			prop->i2c_addr_psoc = dev->i2c_addr_psoc;
			prop->i2c_addr_clock = dev->i2c_addr_clock;
			prop->i2c_addr_mipi = dev->i2c_addr_mipi;
			prop->platform_details = PLATFORM_DETAILS;
			if ((dev->platform.hsm_supply_en) || (dev->platform.gpio[HSM_GPIO_POWER_SUPPLY].name))
				prop->platform_details |= HSM_PLATFORM_SUPPORTS_SUPPLY;
			dev->need_reinit = false;
		} else {
			ret = -EINVAL;
		}
	}
		break;
	case HSM_IIC_READ: {
		struct hsm_iic_data *iic = arg;
		struct i2c_adapter *adapter = IS_MIPI_I2C_ADDR(iic->i2c_addr)? dev->i2c_adapter_mipi : client->adapter;
		u8 buf[MAX_TMP_BUF_SIZE];
		memset(buf,MAX_TMP_BUF_SIZE,0);
		if (iic->len <= MAX_TMP_BUF_SIZE) {
			ret = hsm_read_reg(adapter, iic->i2c_addr, iic->reg, buf, iic->len);
			if (!ret) {
				if (copy_to_user(iic->buf, buf, iic->len)) {
					ret = -EFAULT;
					break;
				}
			}
		} else {
			ret = -ENOMEM;
		}
	}
		break;
	case HSM_IIC_WRITE: {
		struct hsm_iic_data *iic = arg;
		struct i2c_adapter *adapter = 
			IS_MIPI_I2C_ADDR(iic->i2c_addr)? dev->i2c_adapter_mipi : client->adapter;
		unsigned char buf[MAX_TMP_BUF_SIZE - 1];
		if (iic->len <= MAX_TMP_BUF_SIZE - 1) {
			if (copy_from_user(buf, iic->buf, iic->len)) {
				ret = -EFAULT;
				break;
			}
			ret = hsm_write_reg(adapter, iic->i2c_addr, iic->reg, buf, iic->len);
		} else {
			ret = -ENOMEM;
		}
	}
		break;
	case HSM_IIC_TRANSFER:
		ret = hsm_ioctl_rdrw(client->adapter, arg);
		break;
	case HSM_GPIO_WRITE: {
		struct hsm_gpio_data *gpio_data = arg;
		ret = hsm_gpio_set(dev, gpio_data->pin, gpio_data->value);
	}
		break;
	case HSM_GPIO_READ: {
		struct hsm_gpio_data *gpio_data = arg;
		ret = hsm_gpio_get(dev, gpio_data->pin, &gpio_data->value);
	}
		break;
	case HSM_SUPPLY_WRITE: {
		struct hsm_gpio_data *gpio_data = arg;
		ret = hsm_gpio_set(dev, HSM_GPIO_POWER_SUPPLY, gpio_data->value);
		dev->supplied = gpio_data->value;
	}
		break;
	case HSM_SUPPLY_READ: {
		struct hsm_gpio_data *gpio_data = arg;
		gpio_data->value = dev->supplied;
	}
		break;
	case HSM_STATUS_READ: {
		struct hsm_status_data *result = arg;
		if (result->version == HSM_STATUS_V1) {
			result->status = dev->need_reinit ? HSM_STATUS_NEED_REINIT : 0;
			dev->need_reinit = false;
		} else {
			ret = -EINVAL;
		}
	}
		break;
		
#ifdef CONFIG_ARCH_MSM
	case VIDIOC_MSM_SENSOR_CFG:
		ret = msm_sensor_config(&dev->msm_s_ctrl, (void __user *)arg); 
		break;
#endif

	default:
		CDBG("Unknown IOCTL cmd = %08X", cmd);
		ret = -EINVAL;
		break;
	}
	return ret;
}

/* ---------- functions for switching camera power ---------- */
static int hsm_s_power(struct v4l2_subdev * sd, int switch_on)
{
	int ret=0;
	struct i2c_client * client = v4l2_get_subdevdata(sd);
	struct hsm_dev * dev = to_hsm(client);
	CDBG("switch power %s",(switch_on ? "ON" : "OFF"));

	/* Enable NSTANDBY. VCC should be handled in other places. */
	hsm_gpio_set(dev, HSM_GPIO_POWER_ENABLE, switch_on);
	if (switch_on)
		msleep(31);	//give the sensor time to wake up

	return ret;
}

/* ---------- V4L format handling ---------- */

/* SoC camera ops (struct v4l2_subdev_ops/struct v4l2_subdev_video_ops) */

static int hsm_enum_framesizes(struct v4l2_subdev *sd,
				struct v4l2_frmsizeenum *fsize)
{
	struct i2c_client *client = v4l2_get_subdevdata(sd);
	struct hsm_dev * dev = to_hsm(client);
	CDBG("%dx%d", dev->format.width, dev->format.height);

	/*
	 * The camera interface should read this value, this is the resolution
	 * at which the sensor would provide framedata to the camera i/f
	 */
	fsize->type = V4L2_FRMSIZE_TYPE_DISCRETE;
	fsize->discrete.width = dev->format.width;
	fsize->discrete.height = dev->format.height;
	return 0;
}

static int hsm_enum_fmt(struct v4l2_subdev * sd, unsigned int index,
			enum v4l2_mbus_pixelcode * code)
{
	struct i2c_client * client = v4l2_get_subdevdata(sd);
	struct hsm_dev * dev = to_hsm(client);
	CDBG("\n");

	/* Currently we have only one valid format. */
	if (index != 0) {
		return -EINVAL;
	}

	*code = dev->platform.pixelformat;
	return 0;
}

static int hsm_try_fmt(struct v4l2_subdev * sd, struct v4l2_mbus_framefmt * fmt)
{
	struct i2c_client * client = v4l2_get_subdevdata(sd);
	struct hsm_dev * dev = to_hsm(client);

	fmt->field = V4L2_FIELD_NONE;
	fmt->code = dev->platform.pixelformat;
	fmt->colorspace = V4L2_COLORSPACE_JPEG;

	if (!fmt->width || (fmt->width > dev->width)) {
		fmt->width = dev->width;
	}
	if (!fmt->height || (fmt->height > dev->height)) {
		fmt->height = dev->height;
	}

	CDBG("fmt: %d, width: %d, height: %d\n", fmt->code,
		fmt->width, fmt->height);
	return 0;
}

inline struct v4l2_subdev *hsm_get_subdev(struct hsm_dev * dev)
{
#ifdef CONFIG_ARCH_MSM
	return &dev->msm_s_ctrl.msm_sd.sd;
#else
	return &dev->subdev;
#endif
}

static int hsm_init_format(struct hsm_dev * dev)
{
	struct v4l2_mbus_framefmt fmt;
	memset(&fmt, 0, sizeof(fmt));
	CDBG("\n");

	hsm_try_fmt(hsm_get_subdev(dev), &fmt);

	dev->format = fmt;

	return 0;
}

static int hsm_g_fmt(struct v4l2_subdev * sd, struct v4l2_mbus_framefmt * fmt)
{
	struct i2c_client * client = v4l2_get_subdevdata(sd);
	struct hsm_dev * dev = to_hsm(client);
	int ret;

	*fmt = dev->format;

	ret = hsm_try_fmt(sd, fmt);

	CDBG("current fmt: %d, width: %d, height: %d", fmt->code,
		fmt->width, fmt->height);

	if (ret < 0)
		return ret;

	return 0;
}

static int hsm_s_fmt(struct v4l2_subdev * sd, struct v4l2_mbus_framefmt * fmt)
{
	struct i2c_client * client = v4l2_get_subdevdata(sd);
	struct hsm_dev * dev = to_hsm(client);
	int ret;

	CDBG("fmt: %d, width: %d, height: %d", fmt->code,
		fmt->width, fmt->height);

	ret = hsm_try_fmt(sd, fmt);
	if (ret < 0) {
		return ret;
	}

	dev->format = *fmt;

	return 0;
}

static int hsm_g_crop(struct v4l2_subdev * sd, struct v4l2_crop * crop)
{
	struct i2c_client * client = v4l2_get_subdevdata(sd);
	struct hsm_dev * dev = to_hsm(client);
	CDBG("");

	crop->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	crop->c.left = 0;
	crop->c.top = 0;
	crop->c.width = dev->width;
	crop->c.height  = dev->height;

	return 0;
}

static int hsm_cropcap(struct v4l2_subdev * sd, struct v4l2_cropcap * cc)
{
	struct i2c_client * client = v4l2_get_subdevdata(sd);
	struct hsm_dev * dev = to_hsm(client);
	CDBG("");

	cc->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	cc->bounds.left = 0;
	cc->bounds.top = 0;
	cc->bounds.width = dev->width;
	cc->bounds.height   = dev->height;

	cc->defrect = cc->bounds;

	cc->pixelaspect.numerator = 1;
	cc->pixelaspect.denominator = 1;

	return 0;
}

static int hsm_subdev_g_ctrl(struct v4l2_subdev *sd,
			     struct v4l2_control *control)
{
	int ret = 0;
	CDBG("(cmd=%X)", control->id);

	switch (control->id) {
#ifdef V4L2_CID_CAM_JPEG_MEMSIZE	/* Samsung specific */
	struct i2c_client *client = v4l2_get_subdevdata(sd);
	struct hsm_dev * dev = to_hsm(client);
	case V4L2_CID_CAM_JPEG_MEMSIZE:
		control->value = dev->width * dev->height;
		CDBG("- V4L2_CID_CAM_JPEG_MEMSIZE cam_ctrl.value=%X (%i)",
			control->value, control->value);
		break;
#endif
	default:
		ret = -EINVAL;
		break;
	}
	return ret;
}

int hsm_init(struct v4l2_subdev *sd, u32 val)
{
	return 0;	/* just a dummy to make Samsung code happy.*/
}

int hsm_subdev_s_ctrl(struct v4l2_subdev *sd, struct v4l2_control *control)
{
	return 0;	/* just a dummy to make Samsung code happy. */
}

static int hsm_s_stream(struct v4l2_subdev *sd, int enable)
{
	int ret = 0;
	CDBG("\n");
	return ret;
}

#ifdef CONFIG_MEDIA_CONTROLLER
/* --------------------------------------------------------------------------
 * V4L2 subdev pad operations
 */
/*
 * hsm_enum_mbus_code - V4L2 sensor interface handler for pad_ops
 * @subdev: pointer to standard V4L2 sub-device structure
 * @qctrl: standard V4L2 VIDIOC_QUERYCTRL ioctl structure
 *
 * If the requested control is supported, returns the control information
 * from the video_control[] array.  Otherwise, returns -EINVAL if the
 * control is not supported.
 */
static int hsm_enum_mbus_code(struct v4l2_subdev *sd,
                              struct v4l2_subdev_fh *fh,
                              struct v4l2_subdev_mbus_code_enum *code)
{
	int ret;
	enum v4l2_mbus_pixelcode pixelcode;
	
	ret = hsm_enum_fmt(sd, code->index, &pixelcode);
	if( !ret ) code->code = pixelcode;
	return ret;
}

static int hsm_enum_frame_size(struct v4l2_subdev *sd,
                               struct v4l2_subdev_fh *fh,
                               struct v4l2_subdev_frame_size_enum *fse)
{
	struct i2c_client * client = v4l2_get_subdevdata(sd);
	struct hsm_dev * dev = to_hsm(client);

	/* Is requested media-bus format/pixelformat not found on sensor? */
	if( fse->code != dev->platform.pixelformat ) {
		return -EINVAL;
	}

	/*
	 * Currently only supports default resolution
	 */
	fse->min_width = fse->max_width = dev->width;
	fse->min_height = fse->max_height = dev->height;
	return 0;
}

static int hsm_get_pad_format(struct v4l2_subdev *sd, struct v4l2_subdev_fh *fh, struct v4l2_subdev_format *fmt)
{
	return hsm_g_fmt(sd, &fmt->format);
}

static int hsm_set_pad_format(struct v4l2_subdev *sd, struct v4l2_subdev_fh *fh, struct v4l2_subdev_format *fmt)
{
	return hsm_s_fmt(sd, &fmt->format);
}
#endif /* CONFIG_MEDIA_CONTROLLER */

/* ---------- V4L operations ---------- */
static struct v4l2_subdev_core_ops hsm_subdev_core_ops = {
	.s_power = hsm_s_power,
	.ioctl = hsm_ioctl,
	.init = hsm_init,
	.g_ctrl = hsm_subdev_g_ctrl,
	.s_ctrl = hsm_subdev_s_ctrl,
};

static struct v4l2_subdev_video_ops hsm_subdev_video_ops = {
	.enum_mbus_fmt = hsm_enum_fmt,  /* enum_mbus_fmt: enumerate pixel formats, provided by a video data source */
	.try_mbus_fmt = hsm_try_fmt,    /* try_mbus_fmt: try to set a pixel format on a video data source */
	.g_mbus_fmt = hsm_g_fmt,        /* g_mbus_fmt: get the current pixel format, provided by a video data source */
	.s_mbus_fmt = hsm_s_fmt,        /* s_mbus_fmt: set a pixel format on a video data source */
	.enum_framesizes = hsm_enum_framesizes,
	.cropcap = hsm_cropcap,
	.g_crop = hsm_g_crop,
	.s_stream = hsm_s_stream,
};

#ifdef CONFIG_MEDIA_CONTROLLER
static const struct v4l2_subdev_pad_ops hsm_pad_ops = {
	.enum_mbus_code	= hsm_enum_mbus_code,
	.enum_frame_size = hsm_enum_frame_size,
	.get_fmt	= hsm_get_pad_format,
	.set_fmt	= hsm_set_pad_format,
};
#endif

static struct v4l2_subdev_ops hsm_subdev_ops = {
	.core = &hsm_subdev_core_ops,
	.video = &hsm_subdev_video_ops,
#ifdef CONFIG_MEDIA_CONTROLLER
	.pad = &hsm_pad_ops,
#endif
};

#ifdef CONFIG_MEDIA_CONTROLLER
#ifndef MEDIA_PAD_FLAG_OUTPUT
#define MEDIA_PAD_FLAG_OUTPUT	MEDIA_PAD_FL_SOURCE
#endif
#endif

static int n5600_get_dt_data(struct device_node *of_node, struct hsm_dev * dev)
{
  	int	ret;
	u32	val;

	ret = of_property_read_u32(of_node, "qcom,mount-angle", &val);
	if (ret >= 0) 
		dev->platform.mount = (val % 360) / 90;
	
	of_node = of_parse_phandle(of_node, "n5600,mipic", 0);
	if (of_node) {
		struct i2c_client *i2c_client_mipi;

		i2c_client_mipi = of_find_i2c_device_by_node(of_node);
		if(i2c_client_mipi) {
			dev->i2c_adapter_mipi = i2c_client_mipi->adapter;
			put_device(&i2c_client_mipi->dev);
		} else {
			dev->i2c_adapter_mipi = NULL;
			CDBG("Failed to get MIPI adapter %s", of_node->parent? of_node->parent->name : "???");			
		}

		ret = of_property_read_u32(of_node, "reg", &val);
		if ((ret >= 0) && (val < 255)) {
			dev->platform.i2c_addr_mipi = (u8)val;
		} else {
			CDBG("Wrong MIPI address\n");
		}
	}
	return 0;
}

static int32_t n5600_i2c_probe(struct i2c_client *client,
	const struct i2c_device_id *id)
{
	int rc = -1;
	struct hsm_dev * dev;
	struct i2c_adapter * adapter = client->adapter;

	if (!i2c_check_functionality(adapter, I2C_FUNC_SMBUS_BYTE_DATA)) {
		dev_err(&adapter->dev, "%s: I2C adapter doesn't support SMBus.\n",
			__FUNCTION__);
		return -EIO;
	}

	dev = kzalloc(sizeof(struct hsm_dev), GFP_KERNEL);
	if (!dev) {
		dev_err(&client->dev, "Failed to allocate memory for private data.\n");
		return -ENOMEM;
	}
	
#ifdef CONFIG_USE_OF
	if (client->dev.of_node) {
		rc = n5600_get_dt_data(client->dev.of_node, dev);
		if (rc < 0) {
			dev_err(&client->dev, "Failed to get platform data from dt, err=%d\n", rc);
		}
	}
#endif

	dev->client = client;
	dev->supplied = false;
	dev->SensorRevision = 0;
	dev->i2c_addr_psoc = PSOC_I2C_ADDR;
	dev->i2c_addr_mipi = dev->platform.i2c_addr_mipi;
	
	v4l2_i2c_subdev_init(hsm_get_subdev(dev), client, &hsm_subdev_ops);
	hsm_init_format(dev);
#ifdef CONFIG_MEDIA_CONTROLLER
	hsm_get_subdev(dev)->flags |= V4L2_SUBDEV_FL_HAS_DEVNODE;
	dev->pad.flags = MEDIA_PAD_FLAG_OUTPUT;
	rc = media_entity_init(&hsm_get_subdev(dev)->entity, 1, &dev->pad, 0);
	if( rc < 0 ){
		dev_err(&client->dev, "Could not init media entity, error code: %d\n", rc);
	}
#endif

#ifdef CONFIG_ARCH_MSM
	dev->msm_i2c_client.addr_type = MSM_CAMERA_I2C_BYTE_ADDR;
	dev->msm_s_ctrl = n5600_s_ctrl ;
	dev->msm_s_ctrl.sensor_i2c_client = &dev->msm_i2c_client;
	dev->msm_s_ctrl.sensor_v4l2_subdev_ops = &hsm_subdev_ops;
#endif

	rc = msm_sensor_i2c_probe(client, id, &dev->msm_s_ctrl);
	if(rc){
		CDBG("msm_sensor_i2c_probe failed!\n");
		rc = -ENODEV;
	}

	if ((jadid == JADE_REVISION || jadid == JADE_REVISION_2)) {
		CDBG("found n5600");
		dev->SensorRevision = jadid;
		dev->i2c_addr_sensor = EV76C454_I2C_ADDR;
		dev->i2c_addr_clock = IDT6P50016_I2C_ADDR;
		dev->width = N5600_WIDTH;
		dev->height = N5600_HEIGHT;
	} else {
		CDBG("No n5600 found, ID register 0x%x", jadid);
	}
	
    return rc;
}

static struct i2c_driver n5600_i2c_driver = {
	.id_table = n5600_i2c_id,
	.probe  = n5600_i2c_probe,
	.driver = {
		.name	= N5600_SENSOR_NAME,
	},
};

static struct msm_camera_i2c_client n5600_sensor_i2c_client = {
	.addr_type = MSM_CAMERA_I2C_BYTE_ADDR,
};

static const struct of_device_id n5600_dt_match[] = {
	{.compatible = "hsm,n5600", .data = &n5600_s_ctrl},
	{}
};
MODULE_DEVICE_TABLE(of, n5600_dt_match);

static int32_t n5600_platform_probe(struct platform_device *pdev)
{
	int32_t rc = 0;
	const struct of_device_id *match;
	match = of_match_device(n5600_dt_match, &pdev->dev);
	rc = msm_sensor_platform_probe(pdev, match->data);
	return rc;
}

static struct platform_driver n5600_platform_driver = {
	.driver = {
		.name = N5600_SENSOR_NAME,
		.owner = THIS_MODULE,
		.of_match_table = n5600_dt_match,
	},
	.probe = n5600_platform_probe,
};

static int __init n5600_module_init(void)
{
	int32_t rc = 0;
	rc = i2c_add_driver(&n5600_i2c_driver);

	if (!rc)
		return rc;
	CDBG("rc = %d\n", rc);
	return platform_driver_register(&n5600_platform_driver);
}

static void __exit n5600_module_exit(void)
{
	if (n5600_s_ctrl.pdev) {
		msm_sensor_free_sensor_data(&n5600_s_ctrl);
		platform_driver_unregister(&n5600_platform_driver);
	} else
		i2c_del_driver(&n5600_i2c_driver);
	return;
}
module_init(n5600_module_init);
module_exit(n5600_module_exit);

MODULE_DESCRIPTION("Camera driver for HSM barcode imagers");
MODULE_AUTHOR("Honewell Scanning & Mobility");
MODULE_LICENSE("GPL");
