/* Copyright (C) 2008 Rodolfo Giometti <giometti@linux.it>
 * Copyright (C) 2008 Eurotech S.p.A. <info@eurotech.it>
 * Based on a previous work by Copyright (C) 2008 Texas Instruments, Inc.
 *
 * Copyright (c) 2011, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
//#define DEBUG 1
#include <linux/module.h>
#include <linux/param.h>
#include <linux/jiffies.h>
#include <linux/workqueue.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/power_supply.h>
#include <linux/idr.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <asm/unaligned.h>
#include <linux/time.h>
#include <linux/gpio.h>
#include <linux/regulator/consumer.h>
#include <linux/regulator/machine.h>
#include <linux/regulator/of_regulator.h>
#include <linux/regulator/driver.h>
#include <linux/err.h>
#include <linux/of_gpio.h>
#include <mach/gpiomux.h>
#include <linux/of_device.h>
#include <linux/debugfs.h>
#include <linux/bitops.h>
#include <linux/irqchip/msm-mpm-irq.h>

#define DEBUG 1
#define READ_ALL_REG 			0
#define DEBUG_BQ24296			0
#define BQ24296_POWER 			96
#define BQ_PLUGIN_CHECK_PERIOD_MS	1000
#define BQ_RESET_PERIOD_MS 		30000

#define CHARGER_AC			2
#define CHARGER_USB			1
#define CHARGER_UNKNOW			0
#define CHARGING			2
#define FULL				1
#define NOT_CHARGING			0
/***************************************************/
/*寄存器宏定义*/
#define BQ24296_CON0      		0x00
#define BQ24296_CON1      		0x01
#define BQ24296_CON2      		0x02
#define BQ24296_CON3     		0x03
#define BQ24296_CON4     		0x04
#define BQ24296_CON5      		0x05
#define BQ24296_CON6      		0x06
#define BQ24296_CON7      		0x07
#define BQ24296_CON8      		0x08

#define BQ24296_REG_WATCHDOG_RESET 	0x4B
#define BQ24296_REG_VAL0		0x36
#define BQ24296_REG_VAL1		0x1B
#define BQ24296_REG_VAL2		0x30
#define BQ24296_REG_VAL3		0x10
#define BQ24296_REG_VAL4		0xE2
#define BQ24296_REG_VAL5		0x86
#define BQ24296_REG_VAL6		0x73
#define BQ24296_REG_VAL7		0x4B

/***************************************************/

#define _BQ27296_MASK(BITS, POS) \
	((unsigned char)(((1 << (BITS)) - 1) << (POS)))
#define BQ27296_MASK(LEFT_BIT_POS, RIGHT_BIT_POS) \
		_BQ27296_MASK((LEFT_BIT_POS) - (RIGHT_BIT_POS) + 1, \
				(RIGHT_BIT_POS))


#define CMD_OTG_EN_BIT			BIT(5)
#define CMD_CHG_EN_BIT			BIT(4)

#define FAST_CHARGE_CURRENT_MASK			BQ27296_MASK(7, 2)
#define FAST_CHARGE_CURRENT_2000MA 		0x60
#define FAST_CHARGE_CURRENT_1500MA 		0x40
#define FAST_CHARGE_CURRENT_1200MA 		0x30
#define FAST_CHARGE_CURRENT_1000MA 		0x20
#define FAST_CHARGE_CURRENT_500MA 		0x00

#define CHARGE_TYPE_MASK		BQ27296_MASK(5, 4)
#define CHARGE_TYPE_SHIFT   4
#define CHARGE_TYPE_NOT_CHARGING           0X0
#define CHARGE_TYPE_PRE_CHARGING           0X1
#define CHARGE_TYPE_FAST_CHARGING           0X2
#define CHARGE_TYPE_CHARGING_DONE           0X3



struct bq24296_device_info;
struct bq24296_access_methods {
        int (*read)(u8 reg, int *rt_value, int b_single,
                struct bq24296_device_info *di);
};

struct bq24296_otg_regulator {
	struct regulator_desc	rdesc;
	struct regulator_dev	*rdev;
};

struct bq24296_device_info {
        struct device                           *dev;
	struct bq24296_access_methods           *bus;
        struct i2c_client                       *client;

        struct power_supply     		dc_psy;


        struct delayed_work                    bq_reset;
        struct delayed_work                    bq_plugin_check;
        uint32_t                                irq;
	 unsigned int		chg_en_gpio;	
	 unsigned int		otg_en_gpio;
	 unsigned int		psel_gpio;
	 unsigned int		chg_int;

	 struct dentry			*debug_root;
	 u32				peek_poke_address;

	struct bq24296_otg_regulator otg_vreg;
	struct power_supply		*usb_psy;

 	int current_usb_type;
	int usb_present;
	
};

struct bq24296_device_info *bq24296_dev;

static char *qpnp_batt_supplicants[] = {
     //"battery",
	  "usb"
};


static void bq24296_init_config(void);
static struct i2c_client *new_client = NULL;

static int bq24296_write_byte(int reg, u8 val)
{
        int retval = 0;
        pr_debug("bq24296_write_byte Writing 0x%02x=0x%02x\n", reg, val);
        retval = i2c_smbus_write_byte_data(new_client, reg, val);
        if (retval < 0) {
                dev_err(&new_client->dev,
                        "%s failed, reg: %d, val: %d, error: %d\n",
                        __func__, reg, val, retval);
                return retval;
        }
        return 0;
}


static int bq24296_read_byte(int reg,u8 *val)
{
        int retval;

        retval = i2c_smbus_read_byte_data(new_client, reg);
        if (retval < 0) {
                dev_err(&new_client->dev, "%s failed, reg: %d, error: %d\n",
                        __func__, reg, retval);
                return retval;
        }
        *val = (u16)retval;
        return retval;
}


static int bq24296_masked_write( int reg,
						u8 mask, u8 val)
{
	s32 rc;
	u8 temp;

	rc = bq24296_read_byte(reg, &temp);
	if (rc < 0) {
		dev_err(&new_client->dev, "read failed: reg=%03X, rc=%d\n", reg, rc);
		return rc;
	}
	temp &= ~mask;
	temp |= val & mask;
	rc = bq24296_write_byte(reg, temp);
	if (rc < 0) {
		dev_err(&new_client->dev,"write failed: reg=%03X, rc=%d\n", reg, rc);
	}
	return rc;
}



u32 bq24296_read_interface (u8 RegNum)
{
        u8 bq24296_reg = 0;
        int ret = 0;

        ret = bq24296_read_byte(RegNum, &bq24296_reg);
#if DEBUG_BQ24296
        printk("[bq24296_read_interface] Reg[%x]=0x%x\n", RegNum, bq24296_reg);
#endif
        return bq24296_reg;
}


u32 bq24296_config_interface (u8 RegNum, u8 val)
{
        int ret = 0;

        ret = bq24296_write_byte(RegNum, val);

        return ret;
}

#if READ_ALL_REG
static void bq24296_dump_reg(void)
{
        bq24296_read_interface(BQ24296_CON0);
	bq24296_read_interface(BQ24296_CON1);
	bq24296_read_interface(BQ24296_CON2);
	bq24296_read_interface(BQ24296_CON3);
	bq24296_read_interface(BQ24296_CON4);
	bq24296_read_interface(BQ24296_CON5);
	bq24296_read_interface(BQ24296_CON6);
	bq24296_read_interface(BQ24296_CON7);
        bq24296_read_interface(BQ24296_CON8);
}
#endif

int charger_type_bq24296 = 0;
int charging_status = 0;
static void bq24296_plugin_check(struct work_struct *work)
{
/*检查AC或USB是否接入*/
        struct bq24296_device_info *di;
	u8 check_plug_reg = 0;
	int status = -1;
	struct power_supply *bms_psy;
	union power_supply_propval ret_psp= {0,};

        di  = container_of(work, struct bq24296_device_info, bq_plugin_check.work);

#if READ_ALL_REG
        bq24296_dump_reg();
#endif

	check_plug_reg = bq24296_read_interface(BQ24296_CON8);
	if((check_plug_reg >> 6)  == 2)
	{
		charger_type_bq24296 = CHARGER_AC;
		//bq24296_init_config();
#if DEBUG_BQ24296
		printk("[bq24296_plugin_check] Reg[08]=0x%x\n",check_plug_reg);
		printk("[bq24296_plugin_check] AC\n");
#endif
	}
	else if((check_plug_reg>>6)  == 1)
	{
               	charger_type_bq24296 = CHARGER_USB;
		//bq24296_init_config();
#if DEBUG_BQ24296
                printk("[bq24296_plugin_check] Reg[08]=0x%x\n",check_plug_reg);
                printk("[bq24296_plugin_check] USB\n");
#endif
	}
	else if((check_plug_reg>>6)  == 0)
	{
           	charger_type_bq24296 = CHARGER_UNKNOW;
#if DEBUG_BQ24296
                printk("[bq24296_plugin_check] Reg[08]=0x%x\n",check_plug_reg);
                printk("[bq24296_plugin_check] UNKNOW\n");
#endif
	}

    status=  ( check_plug_reg & 0x30 ) >> 4 ;
	//printk( KERN_ERR  "get status=0x%x reg=0x%x \n",status,check_plug_reg);
	pr_debug("get status=0x%x reg=0x%x \n",status,check_plug_reg);

	if( status == 0x01 || status == 0x02 ) //precharging or fast charging
	{
		charging_status = CHARGING;
#if DEBUG_BQ24296
                printk("[bq24296_plugin_check] Reg[08]=0x%x\n",check_plug_reg);
                printk("[bq24296_plugin_check] CHARING\n");
#endif
	}
	else if( status == 0x03)
	{
		charging_status = FULL;
#if DEBUG_BQ24296
                printk("[bq24296_plugin_check] Reg[08]=0x%x\n",check_plug_reg);
                printk("[bq24296_plugin_check] FULL\n");
#endif
	}
	else if( status == 0x00) 
	{
		charging_status = NOT_CHARGING;
#if DEBUG_BQ24296
                printk("[bq24296_plugin_check] Reg[08]=0x%x\n",check_plug_reg);
                printk("[bq24296_plugin_check] NOT CHARING\n");
#endif
	}

	bms_psy = power_supply_get_by_name("bms");
	if(bms_psy){
		bms_psy->get_property(bms_psy,POWER_SUPPLY_PROP_CAPACITY, &ret_psp);
		if(ret_psp.intval == 100){
			charging_status = FULL;	 
		}
	}

	power_supply_changed(&di->dc_psy);
       schedule_delayed_work(&di->bq_plugin_check, msecs_to_jiffies(BQ_PLUGIN_CHECK_PERIOD_MS));

}

static void bq24296_watchdog_reset(struct work_struct *work)
{
/*40S内watchdag reset一次,保证寄存器不会恢复默认值(机器死机时)*/
        struct bq24296_device_info *di;

        di  = container_of(work, struct bq24296_device_info, bq_reset.work);

	bq24296_config_interface(BQ24296_CON1,BQ24296_REG_WATCHDOG_RESET);
	udelay(30);//need delay > 20us

        schedule_delayed_work(&di->bq_reset, msecs_to_jiffies(BQ_RESET_PERIOD_MS));
}


static void bq24296_init_config(void)
{
        bq24296_config_interface(BQ24296_CON0,BQ24296_REG_VAL0);
        udelay(30);//need delay > 20us
        bq24296_config_interface(BQ24296_CON1,BQ24296_REG_VAL1);
        udelay(30);//need delay > 20us
        bq24296_config_interface(BQ24296_CON2,BQ24296_REG_VAL2);
        udelay(30);//need delay > 20us
        bq24296_config_interface(BQ24296_CON3,BQ24296_REG_VAL3);
        udelay(30);//need delay > 20us
        bq24296_config_interface(BQ24296_CON4,BQ24296_REG_VAL4);
        udelay(30);//need delay > 20us
        bq24296_config_interface(BQ24296_CON5,BQ24296_REG_VAL5);
        udelay(30);//need delay > 20us
        bq24296_config_interface(BQ24296_CON6,BQ24296_REG_VAL6);
        udelay(30);//need delay > 20us
        bq24296_config_interface(BQ24296_CON7,BQ24296_REG_VAL7);
        udelay(30);//need delay > 20us
}

static void bq24296_power(bool enable,struct bq24296_device_info *di)
{
	if(enable)
	{
		gpio_set_value(di->chg_en_gpio, 0);
	}
	else
	{
		gpio_set_value(di->chg_en_gpio, 1);
	}
}

static int bq24296_get_prop_charge_type(struct bq24296_device_info *chip){
	u8 reg=0;
	u8 chg_type;
	reg =bq24296_read_interface( BQ24296_CON8 );

	chg_type = (reg & CHARGE_TYPE_MASK) >> CHARGE_TYPE_SHIFT;
	pr_debug("charge type =%d \n",chg_type);
	if (chg_type == CHARGE_TYPE_NOT_CHARGING)
		return POWER_SUPPLY_CHARGE_TYPE_NONE;
	else if (chg_type == CHARGE_TYPE_FAST_CHARGING)
		return POWER_SUPPLY_CHARGE_TYPE_FAST;
	else if (chg_type == CHARGE_TYPE_PRE_CHARGING)
		return POWER_SUPPLY_CHARGE_TYPE_TRICKLE;
	return POWER_SUPPLY_CHARGE_TYPE_NONE;
}


static enum power_supply_property pm_power_props_mains[] = {
        POWER_SUPPLY_PROP_PRESENT,
        POWER_SUPPLY_PROP_ONLINE,
        POWER_SUPPLY_PROP_CHARGING_ENABLED,
        POWER_SUPPLY_PROP_STATUS,
        POWER_SUPPLY_PROP_CHARGE_TYPE,
};


static int bq24296_get_property_mains(struct power_supply *psy,
                                  enum power_supply_property psp,
                                  union power_supply_propval *val)
{
        int ret = 0;
	struct bq24296_device_info *di;
        di = container_of(psy,struct bq24296_device_info,dc_psy);
        switch (psp) {
        case POWER_SUPPLY_PROP_PRESENT:
	 case POWER_SUPPLY_PROP_ONLINE:
                if(charger_type_bq24296 == CHARGER_AC)
                {
                        val->intval = 1;
                }
                else
                {
                         val->intval = 0;
                }	
                break;
	case POWER_SUPPLY_PROP_STATUS:
                if(charging_status == CHARGING)
                {
                        val->intval = POWER_SUPPLY_STATUS_CHARGING;
                }
                else if(charging_status == FULL)
                {
                        val->intval = POWER_SUPPLY_STATUS_FULL;
                }
                else
                {
                        val->intval = POWER_SUPPLY_STATUS_NOT_CHARGING;
                }
                break;		
	case POWER_SUPPLY_PROP_CHARGE_TYPE:
		 val->intval = bq24296_get_prop_charge_type(di);
		 break;
	case POWER_SUPPLY_PROP_CHARGING_ENABLED:
		val->intval = 1;
		break;
        default:
                pr_err(" psp %d Not supoprted.\n", psp);
                ret = -EINVAL;
                break;
	}
	return ret;
}


static void bq24296_charging_enable(struct bq24296_device_info *chip,int enable)
{
	bq24296_masked_write(BQ24296_CON1, CMD_CHG_EN_BIT, enable ? 1 : 0);
}

static int bq24296_set_property(struct power_supply *psy,
                                  enum power_supply_property psp,
                                  const union power_supply_propval *val)
{
       
	struct bq24296_device_info *di;
	di = container_of(psy,struct bq24296_device_info,dc_psy);
	pr_debug("psp = %d.val = %d.\n", psp, val->intval);

	switch (psp) {
		case POWER_SUPPLY_PROP_CHARGING_ENABLED:
			bq24296_charging_enable(di,val->intval);
			break;
		case POWER_SUPPLY_PROP_STATUS:
			switch (val->intval) {
				case POWER_SUPPLY_STATUS_FULL:
					bq24296_charging_enable(di,false);
					break;
				case  POWER_SUPPLY_STATUS_DISCHARGING:
					bq24296_charging_enable(di,false);
					break;
				case POWER_SUPPLY_STATUS_CHARGING:
					bq24296_charging_enable(di,true);
					break;
				default:
					return -EINVAL;	
			}
		default:
			return -EINVAL;
	}
	return 0;
}

static int bq24296_battery_is_writeable(struct power_supply *psy,
				       enum power_supply_property prop)
{
	int rc;

	switch (prop) {
	case POWER_SUPPLY_PROP_CHARGING_ENABLED:
	case POWER_SUPPLY_PROP_STATUS:
		rc = 1;
		break;
	default:
		rc = 0;
		break;
	}
	return rc;
}

static void bq24296_external_power_changed(struct power_supply *psy)
{
        struct bq24296_device_info *chip;
	 union power_supply_propval prop = {0, };
	 u8 value;
        chip = container_of(psy,struct bq24296_device_info,dc_psy);

        pr_debug("Notify power_supply_changed.\n");
        /* The battery gauge monitors the current and voltage every 1 second.
         * Therefore a delay from the time that the charger start/stop charging
         * until the battery gauge detects it.
         */
      //msleep(1000);
      value = bq24296_read_interface(BQ24296_CON8);
      if(   value && (0x11<<6) )
	  	chip->usb_present = 1;
      	else
      		chip->usb_present = 0;
	  
   
      if (chip->usb_psy && !chip->usb_psy->get_property(
				chip->usb_psy, POWER_SUPPLY_PROP_TYPE,
						&prop)) {
		if(chip->current_usb_type != prop.intval ){
			chip->current_usb_type = prop.intval;
		}
		
		pr_debug( "charge port type=%d  usb_present=%d \n", prop.intval,chip->usb_present );
		if(chip->usb_present && (prop.intval == POWER_SUPPLY_TYPE_USB_ACA || 
							      prop.intval == POWER_SUPPLY_TYPE_USB_CDP || 
							      prop.intval == POWER_SUPPLY_TYPE_USB_DCP)    ){
#if IS_ENABLED(CONFIG_ARCH_MSM8909_CB03_JP)
			bq24296_masked_write( BQ24296_CON2,FAST_CHARGE_CURRENT_MASK,FAST_CHARGE_CURRENT_1000MA );
#else
			bq24296_masked_write( BQ24296_CON2,FAST_CHARGE_CURRENT_MASK,FAST_CHARGE_CURRENT_2000MA );
#endif
		}else if(chip->usb_present && prop.intval == POWER_SUPPLY_TYPE_USB ) {
			bq24296_masked_write( BQ24296_CON2,FAST_CHARGE_CURRENT_MASK,FAST_CHARGE_CURRENT_500MA );
		}
      	}
        /* Update LEDs and notify uevents */
        //power_supply_changed(&di->dc_psy);
}




static int get_reg(void *data, u64 *val)
{
	struct bq24296_device_info *chip = data;
	int rc;
	u8 temp;

	rc = bq24296_read_byte( chip->peek_poke_address, &temp);
	if (rc < 0) {
		dev_err(chip->dev,
			"Couldn't read reg %x rc = %d\n",
			chip->peek_poke_address, rc);
		return -EAGAIN;
	}
	*val = temp;
	return 0;
}

static int set_reg(void *data, u64 val)
{
	struct bq24296_device_info *chip = data;
	int rc;
	u8 temp;

	temp = (u8) val;
	rc = bq24296_write_byte( chip->peek_poke_address, temp);
	if (rc < 0) {
		dev_err(chip->dev,
			"Couldn't write 0x%02x to 0x%02x rc= %d\n",
			chip->peek_poke_address, temp, rc);
		return -EAGAIN;
	}
	return 0;
}
DEFINE_SIMPLE_ATTRIBUTE(poke_poke_debug_ops, get_reg, set_reg, "0x%02llx\n");


//dumpall bq registers.
static int show_bq_dumpall(struct seq_file *m, void *data)
{
	int i,rc;
	u8 temp;
	struct bq24296_device_info *chip = m->private;
	
	for (i = 0; i < 10; i++){
		rc = bq24296_read_byte(i, &temp);
		if (rc < 0) {
			dev_err(chip->dev,"Couldn't read reg %x rc = %d\n", i, rc);
			return -EAGAIN;
		}
		seq_printf(m, "read reg: 0x%02x = 0x%02x\n", i, temp);
	}
	rc = bq24296_read_byte(9, &temp);
	if (rc < 0) {
		dev_err(chip->dev,"Couldn't read reg %x rc = %d\n",i, rc);
		return -EAGAIN;
	}
	seq_printf(m, "read reg: 0x%02x = 0x%02x\n",9, temp);
	return 0;
}

static int bq_dumpall_open(struct inode *inode, struct file *file)
{
	struct bq24296_device_info *chip = inode->i_private;

	return single_open(file, show_bq_dumpall, chip);
}

static const struct file_operations bq_dumpall_debugfs_ops = {
	.owner		= THIS_MODULE,
	.open		= bq_dumpall_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};


static int create_bq24296_debugfs_entries(struct bq24296_device_info *chip)
{
		
      chip->debug_root = debugfs_create_dir("bq24296", NULL);
      if (!chip->debug_root)
		pr_err( "Couldn't create bq24296 debug dir\n");
      if (chip->debug_root) {
	  	
	  	struct dentry *ent;
		ent = debugfs_create_x32("address", S_IFREG | S_IWUSR | S_IRUGO,
					  chip->debug_root,
					  &(chip->peek_poke_address));
		if (!ent)
			dev_err(chip->dev,
				"Couldn't create address debug file\n");

		ent = debugfs_create_file("data", S_IFREG | S_IWUSR | S_IRUGO,
					  chip->debug_root, chip,
					  &poke_poke_debug_ops);
		if (!ent)
			dev_err(chip->dev,
				"Couldn't create data debug file\n");

		ent = debugfs_create_file("bq_dumpall", S_IFREG | S_IWUSR | S_IRUGO,
					  chip->debug_root, chip,
					  &bq_dumpall_debugfs_ops);
		if (!ent)
			dev_err(chip->dev,
				"Couldn't create data debug file\n");

      }
     return 0;
}


static int bq24296_otg_regulator_enable(struct regulator_dev *rdev)
{
	int rc = 0;
	bq24296_config_interface(BQ24296_CON1,0x2B);
	return rc;
}

static int bq24296_otg_regulator_disable(struct regulator_dev *rdev)
{
	int rc = 0;
	bq24296_config_interface(BQ24296_CON1,BQ24296_REG_VAL1);
	return rc;
}

static int bq24296_otg_regulator_is_enable(struct regulator_dev *rdev)
{
	//u8 reg = 0;
	int rc = 0;

	rc=bq24296_read_interface(BQ24296_CON1);
	pr_debug( "zbs otg is enabled= %d " , (  (rc & CMD_OTG_EN_BIT) ? 1 : 0  )    );
	return  (rc & CMD_OTG_EN_BIT) ? 1 : 0;
}

struct regulator_ops bq24296_otg_reg_ops = {
	.enable		= bq24296_otg_regulator_enable,
	.disable	= bq24296_otg_regulator_disable,
	.is_enabled	= bq24296_otg_regulator_is_enable,
};

static int bq24296_regulator_init(struct bq24296_device_info *chip)
{
	int rc = 0;
	struct regulator_init_data *init_data;
	struct regulator_config cfg = {};

	init_data = of_get_regulator_init_data(chip->dev, chip->dev->of_node);
	if (!init_data) {
		dev_err(chip->dev, "Unable to allocate memory\n");
		return -ENOMEM;
	}

	if (init_data->constraints.name) {
		chip->otg_vreg.rdesc.owner = THIS_MODULE;
		chip->otg_vreg.rdesc.type = REGULATOR_VOLTAGE;
		chip->otg_vreg.rdesc.ops = &bq24296_otg_reg_ops;
		chip->otg_vreg.rdesc.name = init_data->constraints.name;

		cfg.dev = chip->dev;
		cfg.init_data = init_data;
		cfg.driver_data = chip;
		cfg.of_node = chip->dev->of_node;

		init_data->constraints.valid_ops_mask
			|= REGULATOR_CHANGE_STATUS;

		chip->otg_vreg.rdev = regulator_register(
					&chip->otg_vreg.rdesc, &cfg);
		if (IS_ERR(chip->otg_vreg.rdev)) {
			rc = PTR_ERR(chip->otg_vreg.rdev);
			chip->otg_vreg.rdev = NULL;
			if (rc != -EPROBE_DEFER)
				dev_err(chip->dev,
					"OTG reg failed, rc=%d\n", rc);
		}
	}

	return rc;
}

static irqreturn_t bq24296_irq(int irq, void *data)
{
 	//struct bq24296_device_info *di= data;
 	//printk("bq24296 irq handler \n");
	return IRQ_HANDLED;
}


static int bq24296_charger_probe(struct i2c_client *client,
				 const struct i2c_device_id *id)
{
	struct bq24296_device_info *di;	
	struct bq24296_access_methods *bus;
	struct device_node *np;
	int retval = 0;

        if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
                return -ENODEV;

	new_client = client;

        di = kzalloc(sizeof(*di), GFP_KERNEL);
        if (!di) {
                dev_err(&client->dev, "failed to allocate device info data\n");
                retval = -ENOMEM;
                goto charger_failed_1;
        }

        bus = kzalloc(sizeof(*bus), GFP_KERNEL);
        if (!bus) {
                dev_err(&client->dev, "failed to allocate data\n");
                retval = -ENOMEM;
                goto charger_failed_2;
        }

        i2c_set_clientdata(client, di);
        di->dev = &client->dev;
        di->bus = bus;
        di->client = client;

	np = client->dev.of_node;
	di->chg_en_gpio  = of_get_named_gpio( np, "ti,chg-en-gpio", 0);
	di->otg_en_gpio = of_get_named_gpio( np, "ti,otg-en-gpio", 0);
	di->psel_gpio = of_get_named_gpio( np, "ti,psel-gpio", 0);
	di->chg_int = of_get_named_gpio( np, "ti,chg-int", 0);	
	
	if ((!gpio_is_valid(di->chg_en_gpio)))
		return -EINVAL;

	retval = gpio_request(di->chg_en_gpio, "chg_en_gpio");
	if(retval){
	      pr_err("chg_en_gpio request failed retval=%d.\n", retval);
	      goto charger_failed_2;
	}
	
      retval = gpio_request(di->otg_en_gpio, "otg_en_gpio");
	 if(retval){
	      pr_err("otg_en_gpio request failed retval=%d.\n", retval);
	      goto charger_failed_2;
	}
	retval = gpio_request(di->psel_gpio, "psel_gpio");
	 if(retval){
	      pr_err("psel_gpio request failed retval=%d.\n", retval);
	      goto charger_failed_2;
	}
	 retval = gpio_request(di->chg_int, "chg_int");
	 if(retval){
	      pr_err("chg_int request failed retval=%d.\n", retval);
	      goto charger_failed_2;
	}
       di->irq = gpio_to_irq(di->chg_int);
       if (di->irq) {
		retval = request_irq(di->irq,
					  bq24296_irq,
					  IRQF_TRIGGER_RISING |
					  IRQF_TRIGGER_FALLING,
					  "bq24296_irq", di);
		if (retval) {
				pr_err("request irq failed for  BQ24296 IRQ\n");
				goto charger_failed_2;
		}
	} else {
		retval = -ENODEV;
		pr_err("BQ24296 IRQ doesn't exist\n");
		goto charger_failed_2;
	}
	   
	bq24296_regulator_init(di);
	
	bq24296_power(0,di);
	
       gpio_direction_output(di->psel_gpio, 0);
	gpio_direction_output(di->otg_en_gpio, 1);
	   
	bq24296_init_config();

	msleep(5);
	bq24296_power(1,di);

        INIT_DELAYED_WORK(&di->bq_reset, bq24296_watchdog_reset);
	if(0)
	schedule_delayed_work(&di->bq_reset, msecs_to_jiffies(BQ_RESET_PERIOD_MS));

        INIT_DELAYED_WORK(&di->bq_plugin_check, bq24296_plugin_check);
        schedule_delayed_work(&di->bq_plugin_check, msecs_to_jiffies(BQ_PLUGIN_CHECK_PERIOD_MS));


        di->dc_psy.name = "dc_ti";
        di->dc_psy.type = POWER_SUPPLY_TYPE_BATTERY;
	di->dc_psy.num_supplicants = 0;
        di->dc_psy.properties = pm_power_props_mains;
        di->dc_psy.num_properties = ARRAY_SIZE(pm_power_props_mains);
        di->dc_psy.get_property = bq24296_get_property_mains;
        di->dc_psy.set_property = bq24296_set_property;
	di->dc_psy.external_power_changed = bq24296_external_power_changed;
	di->dc_psy.supplied_to = qpnp_batt_supplicants;
	di->dc_psy.num_supplicants = ARRAY_SIZE(qpnp_batt_supplicants);
	di->dc_psy.supplied_from = qpnp_batt_supplicants;
	di->dc_psy.num_supplies =  ARRAY_SIZE(qpnp_batt_supplicants);
	di->dc_psy.property_is_writeable = bq24296_battery_is_writeable;

        retval = power_supply_register(&di->client->dev,
                                &di->dc_psy);
        if (retval) {
                pr_err("failed to register power_supply. ret=%d.\n", retval);
                return retval;
        }


	create_bq24296_debugfs_entries(di);

	di->usb_psy = power_supply_get_by_name("usb");
	if(!di->usb_psy)
	{
		pr_err("couldn't get usb power supply \n");
	}
	return 0;

charger_failed_2:
	kfree(bus);
        kfree(di);
        return -ENOMEM;

charger_failed_1:
	return -ENOMEM;

}

static int bq24296_charger_remove(struct i2c_client *client)
{
        struct bq24296_device_info *di = i2c_get_clientdata(client);
	cancel_delayed_work_sync(&di->bq_reset);
	cancel_delayed_work_sync(&di->bq_plugin_check);
	kfree(di->bus);
	kfree(di);
	return 0;
}

#ifdef CONFIG_PM
static int bq24296_suspend(struct device *dev)
{
	return 0;
}

static int bq24296_resume(struct device *dev)
{
	return 0;
}

static const struct dev_pm_ops bq24296_pm_ops = {
	.suspend = bq24296_suspend,
	.resume = bq24296_resume,
};
#endif

static const struct i2c_device_id bq24296_id[] = {
	{ "ti,bq24296-charger", },
	{},
};

static const struct of_device_id bq24296_match[] = {
        {.compatible = "ti,bq24296-charger"},
        {}
};
MODULE_DEVICE_TABLE(of, bq24296_match);

static struct i2c_driver bq24296_charger_driver = {
	.driver = {
		.name = "ti,bq24296-charger",
		.owner = THIS_MODULE,
		.of_match_table = bq24296_match,
#ifdef CONFIG_PM
		.pm = &bq24296_pm_ops,
#endif
	},
	.probe = bq24296_charger_probe,
	.remove = bq24296_charger_remove,
	.id_table = bq24296_id,
};


static int __init bq24296_charger_init(void)
{
	int ret;

	ret = i2c_add_driver(&bq24296_charger_driver);
	if (ret)
		pr_err(KERN_ERR "Unable to register driver ret = %d\n", ret);

	return ret;
}
module_init(bq24296_charger_init);

static void __exit bq24296_charger_exit(void)
{
	i2c_del_driver(&bq24296_charger_driver);
}
module_exit(bq24296_charger_exit);

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("zhangkai@sim.com");
MODULE_DESCRIPTION("BQ24296 charger monitor driver");
