#define DEBUG 1
#include <linux/i2c.h>
#include <linux/gpio.h>
#include <linux/errno.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/workqueue.h>
#include <linux/interrupt.h>
#include <linux/slab.h>
#include <linux/power_supply.h>
#include <linux/bitops.h>
#include <linux/regulator/consumer.h>
#include <linux/printk.h>

#define BQ27421_NAME 			"ti,bq27421-battery"
#define BQ27241_DEBUG			0
#define READ_FLAG			0
#define CHARGING_AC_STATUS_UEVENT	1


#define ZERO_DEGREE_CELSIUS_IN_TENTH_KELVIN   (-2731)
#define BQ_TERMINATION_CURRENT_MA	200

#define TEMP_COLD			0
#define TEMP_HOT			45

#define BQ27421_REG_CNTL		0x00
#define BQ27421_REG_TEMP		0x02
#define BQ27421_REG_VOLT		0x04
#define BQ27421_REG_FLAGS		0x06
#define BQ27421_REG_NAC			0x08
#define BQ27421_REG_FAC			0x0a
#define BQ27421_REG_RM			0x0c
#define BQ27421_REG_FCC			0x0e
#define BQ27421_REG_AI			0x10
#define BQ27421_REG_SI			0x12
#define BQ27421_REG_MLI			0x14
#define BQ27421_REG_AP			0x18
#define BQ27421_REG_SOH			0x20
#define BQ27421_REG_SOC			0x1c

struct bq27421_device {
	struct i2c_client	*client;
	struct delayed_work	periodic_user_space_update_work;
	struct dentry		*dent;
	struct power_supply	batt_psy;
	struct power_supply	*dc_psy;
	bool			is_charging_enabled;
};

static struct bq27421_device *bq27421_dev;

static enum power_supply_property pm_power_props[] = {
	POWER_SUPPLY_PROP_STATUS,
	//POWER_SUPPLY_PROP_CHARGE_TYPE,
	//POWER_SUPPLY_PROP_PRESENT,
	POWER_SUPPLY_PROP_TECHNOLOGY,
	POWER_SUPPLY_PROP_VOLTAGE_NOW,
	POWER_SUPPLY_PROP_CAPACITY,
	POWER_SUPPLY_PROP_CURRENT_NOW,
	POWER_SUPPLY_PROP_CURRENT_AVG,
	POWER_SUPPLY_PROP_TEMP,
	POWER_SUPPLY_PROP_CHARGE_FULL,
	POWER_SUPPLY_PROP_CHARGE_NOW,
	//POWER_SUPPLY_PROP_MODEL_NAME,
	//POWER_SUPPLY_PROP_MANUFACTURER,
};


static int bq27421_read_reg(struct i2c_client *client, u8 reg)
{
	int val;

	val = i2c_smbus_read_word_data(client, reg);
	if (val < 0)
		pr_err("i2c read fail. reg = 0x%x.ret = %d.\n", reg, val);
	else
		pr_debug("reg = 0x%02X.val = 0x%04X.\n", reg , val);

	return val;
}

static int bq27421_write_reg(struct i2c_client *client, u8 reg, u16 val)
{
	int ret;

	ret = i2c_smbus_write_word_data(client, reg, val);
	if (ret < 0)
		pr_err("i2c read fail. reg = 0x%x.val = 0x%x.ret = %d.\n",
		       reg, val, ret);
	else
		pr_debug("reg = 0x%02X.val = 0x%02X.\n", reg , val);

	return ret;
}

#define BQ27421_INVALID_TEMPERATURE	-999
/*
 * Return the battery temperature in tenths of degree Celsius
 * Or -99.9 C if something fails.
 */
static int bq27421_read_temperature(struct i2c_client *client)
{
	int temp;

	/* temperature resolution 0.1 Kelvin */
	temp = bq27421_read_reg(client, BQ27421_REG_TEMP);
	if (temp < 0)
		return BQ27421_INVALID_TEMPERATURE;

	temp = temp + ZERO_DEGREE_CELSIUS_IN_TENTH_KELVIN;
#if BQ27241_DEBUG
	printk("temp = %d C\n", temp/10);
#endif
	return temp;
}

/*
 * Return the battery Voltage in milivolts 0..20 V
 * Or < 0 if something fails.
 */
static int bq27421_read_voltage(struct i2c_client *client)
{
	int mvolt = 0;

	mvolt = bq27421_read_reg(client, BQ27421_REG_VOLT);
	if (mvolt < 0)
		return mvolt;
#if BQ27241_DEBUG
	printk("volt = %d mV.\n", mvolt);
#endif
	return mvolt;
}

/*
 * Return the battery Current in miliamps
 * Or 0 if something fails.
 * Positive current indicates charging
 * Negative current indicates discharging.
 * Current-now is calculated every second.
 */
static int bq27421_read_current(struct i2c_client *client)
{
	s16 current_ma = 0;

	current_ma = bq27421_read_reg(client, BQ27421_REG_SI);
#if BQ27241_DEBUG
	printk("current = %d mA.\n", current_ma);
#endif
	return current_ma;
}

/*
 * Return the Average battery Current in miliamps
 * Or 0 if something fails.
 * Positive current indicates charging
 * Negative current indicates discharging.
 * Average Current is the rolling 1 minute average current.
 */
static int bq27421_read_avg_current(struct i2c_client *client)
{
	s16 current_ma = 0;

	current_ma = bq27421_read_reg(client, BQ27421_REG_AI);

	pr_debug("avg_current=%d mA.\n", current_ma);

	return current_ma;
}

/*
 * Return the battery Relative-State-Of-Charge 0..100 %
 * Or negative value if something fails.
 */
static int bq27421_read_rsoc(struct i2c_client *client)
{
	int percentage = 0;


	/* This register is only 1 byte */
	percentage = i2c_smbus_read_byte_data(client, BQ27421_REG_SOC);

	if (percentage < 0) {
		pr_err("I2C failure when reading rsoc.\n");
		return percentage;
	}
#if BQ27241_DEBUG
	printk("percentage = %d.\n", percentage);
#endif
	return percentage;
}

/*
 * Return the battery Capacity in mAh.
 * Or 0 if something fails.
 */
static int bq27421_read_full_capacity(struct i2c_client *client)
{
	int capacity = 0;

	capacity = bq27421_read_reg(client, BQ27421_REG_FCC);
	if (capacity < 0)
		return 0;
#if BQ27241_DEBUG
	printk("full-capacity = %d mAh.\n", capacity);
#endif
	return capacity;
}

/*
 * Return the battery Capacity in mAh.
 * Or 0 if something fails.
 */
static int bq27421_read_remain_capacity(struct i2c_client *client)
{
	int capacity = 0;

	capacity = bq27421_read_reg(client, BQ27421_REG_RM);
	if (capacity < 0)
		return 0;
#if BQ27241_DEBUG
	printk("remain-capacity = %d mAh.\n", capacity);
#endif
	return capacity;
}

#if CHARGING_AC_STATUS_UEVENT
static int bq27421_enable_charging(struct bq27421_device *bq27421_dev,
				   bool enable)
{
	int ret;
	static bool is_charging_enabled;

	if (bq27421_dev->dc_psy == NULL) {
		bq27421_dev->dc_psy = power_supply_get_by_name("dc_ti");
		if (bq27421_dev->dc_psy == NULL) {
			pr_err("fail to get dc-psy.\n");
			return -ENODEV;
		}
	}

	if (is_charging_enabled == enable) {
		pr_debug("Charging enable already = %d.\n", enable);
		return 0;
	}

	ret = power_supply_set_online(bq27421_dev->dc_psy, enable);
	if (ret < 0) {
		pr_err("fail to set dc-psy online to %d.\n", enable);
		return ret;
	}

	is_charging_enabled = enable;

	pr_debug("Charging enable = %d.\n", enable);

	return 0;
}
static int bq27421_get_prop_status(struct i2c_client *client)
{
	int status = POWER_SUPPLY_STATUS_UNKNOWN;
	int rsoc;
	s16 current_ma = 0;
	int temperature;


	rsoc = bq27421_read_rsoc(client);
	current_ma = bq27421_read_current(client);
	temperature = bq27421_read_temperature(client);
	temperature = temperature / 10; /* in degree celsius */


	if (rsoc == 100) {
		bq27421_enable_charging(bq27421_dev, false);
		pr_debug("Full.\n");
		return POWER_SUPPLY_STATUS_FULL;
	}

	/* Enable charging when battery is not full and temperature is ok */
	if ((temperature > TEMP_COLD) && (temperature < TEMP_HOT))
		bq27421_enable_charging(bq27421_dev, true);
	else
		bq27421_enable_charging(bq27421_dev, false);

	/*
	* Positive current indicates charging
	* Negative current indicates discharging.
	* Charging is stopped at termination-current.
	*/
	if (current_ma < 0) {
		pr_debug("Discharging.\n");
		status = POWER_SUPPLY_STATUS_DISCHARGING;
	} else if (current_ma > BQ_TERMINATION_CURRENT_MA) {
		pr_debug("Charging.\n");
		status = POWER_SUPPLY_STATUS_CHARGING;
	} else {
		pr_debug("Not Charging.\n");
		status = POWER_SUPPLY_STATUS_NOT_CHARGING;
	}

	return status;
}
#endif


/*
 * User sapce read the battery info.
 * Get data online via I2C from the battery gauge.
 */
static int bq27421_get_property(struct power_supply *psy,
				  enum power_supply_property psp,
				  union power_supply_propval *val)
{
	int ret = 0;
	struct bq27421_device *dev = container_of(psy,
						  struct bq27421_device,
						  batt_psy);
	struct i2c_client *client = dev->client;

	switch (psp) {
#if CHARGING_AC_STATUS_UEVENT
	case POWER_SUPPLY_PROP_STATUS:
		val->intval = bq27421_get_prop_status(client);
		break;
#endif
	case POWER_SUPPLY_PROP_VOLTAGE_NOW:
		val->intval = bq27421_read_voltage(client);
		val->intval *= 1000; /* mV to uV */
		break;
	case POWER_SUPPLY_PROP_CAPACITY:
		val->intval = bq27421_read_rsoc(client);
		if (val->intval < 0)
			ret = -EINVAL;
		break;
	case POWER_SUPPLY_PROP_CURRENT_NOW:
		/* Positive current indicates drawing */
		val->intval = -bq27421_read_current(client);
		val->intval *= 1000; /* mA to uA */
		break;
	case POWER_SUPPLY_PROP_CURRENT_AVG:
		/* Positive current indicates drawing */
		val->intval = -bq27421_read_avg_current(client);
		val->intval *= 1000; /* mA to uA */
		break;
	case POWER_SUPPLY_PROP_TEMP:
		val->intval = bq27421_read_temperature(client);
		break;
	case POWER_SUPPLY_PROP_CHARGE_FULL:
		val->intval = bq27421_read_full_capacity(client);
		break;
	case POWER_SUPPLY_PROP_CHARGE_NOW:
		val->intval = bq27421_read_remain_capacity(client);
		break;
	case POWER_SUPPLY_PROP_TECHNOLOGY:
		val->intval = POWER_SUPPLY_TECHNOLOGY_LION;
		break;
	default:
		pr_err(" psp %d Not supoprted.\n", psp);
		ret = -EINVAL;
		break;
	}

	return ret;
}



static int bq27421_set_property(struct power_supply *psy,
				  enum power_supply_property psp,
				  const union power_supply_propval *val)
{
	pr_debug("psp = %d.val = %d.\n", psp, val->intval);

	return -EINVAL;
}

static void bq27421_external_power_changed(struct power_supply *psy)
{
	pr_debug("Notify power_supply_changed.\n");

	/* The battery gauge monitors the current and voltage every 1 second.
	 * Therefore a delay from the time that the charger start/stop charging
	 * until the battery gauge detects it.
	 */
	msleep(1000);
	/* Update LEDs and notify uevents */
	power_supply_changed(&bq27421_dev->batt_psy);
}

static int  bq27421_register_psy(struct bq27421_device *bq27421_dev)
{
	int ret;

	bq27421_dev->batt_psy.name = "battery_ti";
	bq27421_dev->batt_psy.type = POWER_SUPPLY_TYPE_BATTERY;
	bq27421_dev->batt_psy.num_supplicants = 0;
	bq27421_dev->batt_psy.properties = pm_power_props;
	bq27421_dev->batt_psy.num_properties = ARRAY_SIZE(pm_power_props);
	bq27421_dev->batt_psy.get_property = bq27421_get_property;
	bq27421_dev->batt_psy.set_property = bq27421_set_property;
	bq27421_dev->batt_psy.external_power_changed =
		bq27421_external_power_changed;

	ret = power_supply_register(&bq27421_dev->client->dev,
				&bq27421_dev->batt_psy);
	if (ret) {
		pr_err("failed to register power_supply. ret=%d.\n", ret);
		return ret;
	}

	return 0;
}

#if READ_FLAG
static int bq27421_read_flags(struct i2c_client *client)
{
        int flags = 0;


        flags = i2c_smbus_read_byte_data(client, BQ27421_REG_FLAGS);

        printk("flags = %x.\n", flags);
        return flags;
}

static int bq27421_read_ctrl(struct i2c_client *client)
{
        int ctrl = 0;


        ctrl = i2c_smbus_read_byte_data(client, BQ27421_REG_CNTL);

        printk("ctrl = %x.\n", ctrl);
        return ctrl;
}
#endif

/**
 * Update userspace every 1 minute.
 * Normally it takes more than 120 minutes (two hours) to
 * charge/discahrge the battery,
 * so updating every 1 minute should be enough for 1% change
 * detection.
 * Any immidiate change detected by the DC charger is notified
 * by the bq27421_external_power_changed callback, which notify
 * the user space.
 */
static void bq27421_periodic_user_space_update_worker(struct work_struct *work)
{
	u32 delay_msec = 60*1000;

#if READ_FLAG	
	struct i2c_client *client = bq27421_dev->client;
	bq27421_read_flags(client);
	bq27421_write_reg(client,BQ27421_REG_CNTL,0x0000);
	bq27421_read_ctrl(client);
#endif
	pr_debug("Notify user space.\n");
	/* Notify user space via kobject_uevent change notification */
	power_supply_changed(&bq27421_dev->batt_psy);
	schedule_delayed_work(&bq27421_dev->periodic_user_space_update_work,
                              msecs_to_jiffies(delay_msec));
}

static void bq27421_reg_init(struct i2c_client *client)
{
/*初始化参数*/
/*4000MA/4.2V*/
	bq27421_write_reg(client,0x00,0x13);
        bq27421_write_reg(client,0x01,0x00);
	mdelay(1500);

        bq27421_write_reg(client,0x3e,0x02);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x40,0x02);
        bq27421_write_reg(client,0x41,0x26);
        bq27421_write_reg(client,0x42,0x00);
        bq27421_write_reg(client,0x43,0x00);
        bq27421_write_reg(client,0x44,0x32);
        bq27421_write_reg(client,0x45,0x00);
        bq27421_write_reg(client,0x46,0x00);
        bq27421_write_reg(client,0x47,0x00);
        bq27421_write_reg(client,0x48,0x00);
        bq27421_write_reg(client,0x49,0x00);
        bq27421_write_reg(client,0x4a,0x00);
        bq27421_write_reg(client,0x4b,0x00);
        bq27421_write_reg(client,0x4c,0x00);
        bq27421_write_reg(client,0x4d,0x00);
        bq27421_write_reg(client,0x4e,0x00);
        bq27421_write_reg(client,0x4f,0x00);
        bq27421_write_reg(client,0x50,0x00);
        bq27421_write_reg(client,0x51,0x00);
        bq27421_write_reg(client,0x52,0x00);
        bq27421_write_reg(client,0x53,0x00);
        bq27421_write_reg(client,0x54,0x00);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x00);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x00);
        bq27421_write_reg(client,0x5a,0x00);
        bq27421_write_reg(client,0x5b,0x00);
        bq27421_write_reg(client,0x5c,0x00);
        bq27421_write_reg(client,0x5d,0x00);
        bq27421_write_reg(client,0x5e,0x00);
        bq27421_write_reg(client,0x5f,0x00);

        bq27421_write_reg(client,0x60,0xA5);
	mdelay(200);

        bq27421_write_reg(client,0x3e,0x02);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x3e,0x24);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x40,0x00);
        bq27421_write_reg(client,0x41,0x19);
        bq27421_write_reg(client,0x42,0x28);
        bq27421_write_reg(client,0x43,0x63);
        bq27421_write_reg(client,0x44,0x5F);
        bq27421_write_reg(client,0x45,0xFF);
        bq27421_write_reg(client,0x46,0x62);
        bq27421_write_reg(client,0x47,0x00);
        bq27421_write_reg(client,0x48,0x32);
        bq27421_write_reg(client,0x49,0x00);
        bq27421_write_reg(client,0x4a,0x00);
        bq27421_write_reg(client,0x4b,0x00);
        bq27421_write_reg(client,0x4c,0x00);
        bq27421_write_reg(client,0x4d,0x00);
        bq27421_write_reg(client,0x4e,0x00);
        bq27421_write_reg(client,0x4f,0x00);
        bq27421_write_reg(client,0x50,0x00);
        bq27421_write_reg(client,0x51,0x00);
        bq27421_write_reg(client,0x52,0x00);
        bq27421_write_reg(client,0x53,0x00);
        bq27421_write_reg(client,0x54,0x00);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x00);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x00);
        bq27421_write_reg(client,0x5a,0x00);
        bq27421_write_reg(client,0x5b,0x00);
        bq27421_write_reg(client,0x5c,0x00);
        bq27421_write_reg(client,0x5d,0x00);
        bq27421_write_reg(client,0x5e,0x00);
        bq27421_write_reg(client,0x5f,0x00);

        bq27421_write_reg(client,0x60,0x69);
	mdelay(200);


        bq27421_write_reg(client,0x3e,0x24);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x3e,0x30);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x40,0x0E);
        bq27421_write_reg(client,0x41,0x10);
        bq27421_write_reg(client,0x42,0xFD);
        bq27421_write_reg(client,0x43,0xFF);
        bq27421_write_reg(client,0x44,0x38);
        bq27421_write_reg(client,0x45,0x00);
        bq27421_write_reg(client,0x46,0x00);
        bq27421_write_reg(client,0x47,0x00);
        bq27421_write_reg(client,0x48,0x00);
        bq27421_write_reg(client,0x49,0x00);
        bq27421_write_reg(client,0x4a,0x00);
        bq27421_write_reg(client,0x4b,0x00);
        bq27421_write_reg(client,0x4c,0x00);
        bq27421_write_reg(client,0x4d,0x00);
        bq27421_write_reg(client,0x4e,0x00);
        bq27421_write_reg(client,0x4f,0x00);
        bq27421_write_reg(client,0x50,0x00);
        bq27421_write_reg(client,0x51,0x00);
        bq27421_write_reg(client,0x52,0x00);
        bq27421_write_reg(client,0x53,0x00);
        bq27421_write_reg(client,0x54,0x00);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x00);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x00);
        bq27421_write_reg(client,0x5a,0x00);
        bq27421_write_reg(client,0x5b,0x00);
        bq27421_write_reg(client,0x5c,0x00);
        bq27421_write_reg(client,0x5d,0x00);
        bq27421_write_reg(client,0x5e,0x00);
        bq27421_write_reg(client,0x5f,0x00);

        bq27421_write_reg(client,0x60,0xAD);
	mdelay(200);


        bq27421_write_reg(client,0x3e,0x30);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x3e,0x31);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x40,0x0A);
        bq27421_write_reg(client,0x41,0x0F);
        bq27421_write_reg(client,0x42,0x02);
        bq27421_write_reg(client,0x43,0x05);
        bq27421_write_reg(client,0x44,0x32);
        bq27421_write_reg(client,0x45,0x00);
        bq27421_write_reg(client,0x46,0x00);
        bq27421_write_reg(client,0x47,0x00);
        bq27421_write_reg(client,0x48,0x00);
        bq27421_write_reg(client,0x49,0x00);
        bq27421_write_reg(client,0x4a,0x00);
        bq27421_write_reg(client,0x4b,0x00);
        bq27421_write_reg(client,0x4c,0x00);
        bq27421_write_reg(client,0x4d,0x00);
        bq27421_write_reg(client,0x4e,0x00);
        bq27421_write_reg(client,0x4f,0x00);
        bq27421_write_reg(client,0x50,0x00);
        bq27421_write_reg(client,0x51,0x00);
        bq27421_write_reg(client,0x52,0x00);
        bq27421_write_reg(client,0x53,0x00);
        bq27421_write_reg(client,0x54,0x00);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x00);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x00);
        bq27421_write_reg(client,0x5a,0x00);
        bq27421_write_reg(client,0x5b,0x00);
        bq27421_write_reg(client,0x5c,0x00);
        bq27421_write_reg(client,0x5d,0x00);
        bq27421_write_reg(client,0x5e,0x00);
        bq27421_write_reg(client,0x5f,0x00);

        bq27421_write_reg(client,0x60,0xAD);
	mdelay(200);


        bq27421_write_reg(client,0x3e,0x31);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x3e,0x40);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x40,0x05);
        bq27421_write_reg(client,0x41,0xF8);
        bq27421_write_reg(client,0x42,0x0F);
        bq27421_write_reg(client,0x43,0x00);
        bq27421_write_reg(client,0x44,0x00);
        bq27421_write_reg(client,0x45,0x14);
        bq27421_write_reg(client,0x46,0x04);
        bq27421_write_reg(client,0x47,0x00);
        bq27421_write_reg(client,0x48,0x09);
        bq27421_write_reg(client,0x49,0x00);
        bq27421_write_reg(client,0x4a,0x00);
        bq27421_write_reg(client,0x4b,0x00);
        bq27421_write_reg(client,0x4c,0x00);
        bq27421_write_reg(client,0x4d,0x00);
        bq27421_write_reg(client,0x4e,0x00);
        bq27421_write_reg(client,0x4f,0x00);
        bq27421_write_reg(client,0x50,0x00);
        bq27421_write_reg(client,0x51,0x00);
        bq27421_write_reg(client,0x52,0x00);
        bq27421_write_reg(client,0x53,0x00);
        bq27421_write_reg(client,0x54,0x00);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x00);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x00);
        bq27421_write_reg(client,0x5a,0x00);
        bq27421_write_reg(client,0x5b,0x00);
        bq27421_write_reg(client,0x5c,0x00);
        bq27421_write_reg(client,0x5d,0x00);
        bq27421_write_reg(client,0x5e,0x00);
        bq27421_write_reg(client,0x5f,0x00);

        bq27421_write_reg(client,0x60,0xD2);
	mdelay(200);


        bq27421_write_reg(client,0x3e,0x40);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x3e,0x44);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x40,0x05);
        bq27421_write_reg(client,0x41,0x00);
        bq27421_write_reg(client,0x42,0x32);
        bq27421_write_reg(client,0x43,0x01);
        bq27421_write_reg(client,0x44,0xC2);
        bq27421_write_reg(client,0x45,0x14);
        bq27421_write_reg(client,0x46,0x14);
        bq27421_write_reg(client,0x47,0x00);
        bq27421_write_reg(client,0x48,0x03);
        bq27421_write_reg(client,0x49,0x08);
        bq27421_write_reg(client,0x4a,0x98);
        bq27421_write_reg(client,0x4b,0x01);
        bq27421_write_reg(client,0x4c,0x00);
        bq27421_write_reg(client,0x4d,0x00);
        bq27421_write_reg(client,0x4e,0x00);
        bq27421_write_reg(client,0x4f,0x00);
        bq27421_write_reg(client,0x50,0x00);
        bq27421_write_reg(client,0x51,0x00);
        bq27421_write_reg(client,0x52,0x00);
        bq27421_write_reg(client,0x53,0x00);
        bq27421_write_reg(client,0x54,0x00);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x00);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x00);
        bq27421_write_reg(client,0x5a,0x00);
        bq27421_write_reg(client,0x5b,0x00);
        bq27421_write_reg(client,0x5c,0x00);
        bq27421_write_reg(client,0x5d,0x00);
        bq27421_write_reg(client,0x5e,0x00);
        bq27421_write_reg(client,0x5f,0x00);

        bq27421_write_reg(client,0x60,0x39);
	mdelay(200);



        bq27421_write_reg(client,0x3e,0x44);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x3e,0x50);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x40,0x02);
        bq27421_write_reg(client,0x41,0xBC);
        bq27421_write_reg(client,0x42,0x01);
        bq27421_write_reg(client,0x43,0x2C);
        bq27421_write_reg(client,0x44,0x00);
        bq27421_write_reg(client,0x45,0x1E);
        bq27421_write_reg(client,0x46,0x00);
        bq27421_write_reg(client,0x47,0xC8);
        bq27421_write_reg(client,0x48,0xC8);
        bq27421_write_reg(client,0x49,0x14);
        bq27421_write_reg(client,0x4a,0x08);
        bq27421_write_reg(client,0x4b,0x00);
        bq27421_write_reg(client,0x4c,0x3C);
        bq27421_write_reg(client,0x4d,0x0E);
        bq27421_write_reg(client,0x4e,0x10);
        bq27421_write_reg(client,0x4f,0x00);
        bq27421_write_reg(client,0x50,0x0A);
        bq27421_write_reg(client,0x51,0x46);
        bq27421_write_reg(client,0x52,0x05);
        bq27421_write_reg(client,0x53,0x14);
        bq27421_write_reg(client,0x54,0x05);
        bq27421_write_reg(client,0x55,0x0F);
        bq27421_write_reg(client,0x56,0x03);
        bq27421_write_reg(client,0x57,0x20);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x64);
        bq27421_write_reg(client,0x5a,0x46);
        bq27421_write_reg(client,0x5b,0x50);
        bq27421_write_reg(client,0x5c,0x0A);
        bq27421_write_reg(client,0x5d,0x01);
        bq27421_write_reg(client,0x5e,0x90);
        bq27421_write_reg(client,0x5f,0x00);

        bq27421_write_reg(client,0x60,0xBB);
	mdelay(200);


        bq27421_write_reg(client,0x3e,0x50);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x3e,0x50);
        bq27421_write_reg(client,0x3f,0x01);

        bq27421_write_reg(client,0x40,0x64);
        bq27421_write_reg(client,0x41,0x19);
        bq27421_write_reg(client,0x42,0xDC);
        bq27421_write_reg(client,0x43,0x5C);
        bq27421_write_reg(client,0x44,0x60);
        bq27421_write_reg(client,0x45,0x00);
        bq27421_write_reg(client,0x46,0x7D);
        bq27421_write_reg(client,0x47,0x00);
        bq27421_write_reg(client,0x48,0x04);
        bq27421_write_reg(client,0x49,0x03);
        bq27421_write_reg(client,0x4a,0x19);
        bq27421_write_reg(client,0x4b,0x25);
        bq27421_write_reg(client,0x4c,0x0F);
        bq27421_write_reg(client,0x4d,0x14);
        bq27421_write_reg(client,0x4e,0x0A);
        bq27421_write_reg(client,0x4f,0x78);
        bq27421_write_reg(client,0x50,0x60);
        bq27421_write_reg(client,0x51,0x28);
        bq27421_write_reg(client,0x52,0x01);
        bq27421_write_reg(client,0x53,0xF4);
        bq27421_write_reg(client,0x54,0x00);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x00);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x00);
        bq27421_write_reg(client,0x5a,0x43);
        bq27421_write_reg(client,0x5b,0x80);
        bq27421_write_reg(client,0x5c,0x04);
        bq27421_write_reg(client,0x5d,0x01);
        bq27421_write_reg(client,0x5e,0x14);
        bq27421_write_reg(client,0x5f,0x00);

        bq27421_write_reg(client,0x60,0x2A);
	mdelay(200);


        bq27421_write_reg(client,0x3e,0x50);
        bq27421_write_reg(client,0x3f,0x01);

        bq27421_write_reg(client,0x3e,0x50);
        bq27421_write_reg(client,0x3f,0x02);

        bq27421_write_reg(client,0x40,0x0B);
        bq27421_write_reg(client,0x41,0x0B);
        bq27421_write_reg(client,0x42,0xBB);
        bq27421_write_reg(client,0x43,0x01);
        bq27421_write_reg(client,0x44,0x2C);
        bq27421_write_reg(client,0x45,0x0A);
        bq27421_write_reg(client,0x46,0x01);
        bq27421_write_reg(client,0x47,0x0A);
        bq27421_write_reg(client,0x48,0x00);
        bq27421_write_reg(client,0x49,0x00);
        bq27421_write_reg(client,0x4a,0x00);
        bq27421_write_reg(client,0x4b,0xC8);
        bq27421_write_reg(client,0x4c,0x00);
        bq27421_write_reg(client,0x4d,0x64);
        bq27421_write_reg(client,0x4e,0x02);
        bq27421_write_reg(client,0x4f,0x00);
        bq27421_write_reg(client,0x50,0x00);
        bq27421_write_reg(client,0x51,0x00);
        bq27421_write_reg(client,0x52,0x00);
        bq27421_write_reg(client,0x53,0x00);
        bq27421_write_reg(client,0x54,0x00);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x00);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x00);
        bq27421_write_reg(client,0x5a,0x00);
        bq27421_write_reg(client,0x5b,0x00);
        bq27421_write_reg(client,0x5c,0x00);
        bq27421_write_reg(client,0x5d,0x00);
        bq27421_write_reg(client,0x5e,0x00);
        bq27421_write_reg(client,0x5f,0x00);

        bq27421_write_reg(client,0x60,0xC1);
	mdelay(200);


        bq27421_write_reg(client,0x3e,0x50);
        bq27421_write_reg(client,0x3f,0x02);

        bq27421_write_reg(client,0x3e,0x51);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x40,0x00);
        bq27421_write_reg(client,0x41,0xA7);
        bq27421_write_reg(client,0x42,0x00);
        bq27421_write_reg(client,0x43,0x64);
        bq27421_write_reg(client,0x44,0x00);
        bq27421_write_reg(client,0x45,0xFA);
        bq27421_write_reg(client,0x46,0x00);
        bq27421_write_reg(client,0x47,0x3C);
        bq27421_write_reg(client,0x48,0x3C);
        bq27421_write_reg(client,0x49,0x01);
        bq27421_write_reg(client,0x4a,0xB3);
        bq27421_write_reg(client,0x4b,0xB3);
        bq27421_write_reg(client,0x4c,0x01);
        bq27421_write_reg(client,0x4d,0x90);
        bq27421_write_reg(client,0x4e,0x00);
        bq27421_write_reg(client,0x4f,0x00);
        bq27421_write_reg(client,0x50,0x00);
        bq27421_write_reg(client,0x51,0x00);
        bq27421_write_reg(client,0x52,0x00);
        bq27421_write_reg(client,0x53,0x00);
        bq27421_write_reg(client,0x54,0x00);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x00);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x00);
        bq27421_write_reg(client,0x5a,0x00);
        bq27421_write_reg(client,0x5b,0x00);
        bq27421_write_reg(client,0x5c,0x00);
        bq27421_write_reg(client,0x5d,0x00);
        bq27421_write_reg(client,0x5e,0x00);
        bq27421_write_reg(client,0x5f,0x00);

        bq27421_write_reg(client,0x60,0x8A);
	mdelay(200);


        bq27421_write_reg(client,0x3e,0x51);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x3e,0x52);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x40,0x48);
        bq27421_write_reg(client,0x41,0x44);
        bq27421_write_reg(client,0x42,0x00);
        bq27421_write_reg(client,0x43,0x00);
        bq27421_write_reg(client,0x44,0x00);
        bq27421_write_reg(client,0x45,0x81);
        bq27421_write_reg(client,0x46,0x0E);
        bq27421_write_reg(client,0x47,0xDB);
        bq27421_write_reg(client,0x48,0x0E);
        bq27421_write_reg(client,0x49,0xA8);
        bq27421_write_reg(client,0x4a,0x0F);
        bq27421_write_reg(client,0x4b,0xA0);
        bq27421_write_reg(client,0x4c,0x39);
        bq27421_write_reg(client,0x4d,0xD0);
        bq27421_write_reg(client,0x4e,0x05);
        bq27421_write_reg(client,0x4f,0x3C);
        bq27421_write_reg(client,0x50,0x0C);
        bq27421_write_reg(client,0x51,0x80);
        bq27421_write_reg(client,0x5f,0x00);
        bq27421_write_reg(client,0x52,0xC8);
        bq27421_write_reg(client,0x53,0x00);
        bq27421_write_reg(client,0x54,0x32);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x14);
        bq27421_write_reg(client,0x57,0x03);
        bq27421_write_reg(client,0x58,0xE8);
        bq27421_write_reg(client,0x59,0x01);
        bq27421_write_reg(client,0x5a,0x01);
        bq27421_write_reg(client,0x5b,0x0B);
        bq27421_write_reg(client,0x5c,0x10);
        bq27421_write_reg(client,0x5d,0x04);
        bq27421_write_reg(client,0x5e,0x00);


        bq27421_write_reg(client,0x60,0xB4);
	mdelay(200);

        bq27421_write_reg(client,0x3e,0x52);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x3e,0x52);
        bq27421_write_reg(client,0x3f,0x01);

        bq27421_write_reg(client,0x40,0x0A);
        bq27421_write_reg(client,0x41,0x10);
        bq27421_write_reg(client,0x42,0x5E);
        bq27421_write_reg(client,0x43,0xFF);
        bq27421_write_reg(client,0x44,0xCE);
        bq27421_write_reg(client,0x45,0xFF);
        bq27421_write_reg(client,0x46,0xCE);
        bq27421_write_reg(client,0x47,0x00);
        bq27421_write_reg(client,0x48,0x01);
        bq27421_write_reg(client,0x49,0x02);
        bq27421_write_reg(client,0x4a,0xBC);
        bq27421_write_reg(client,0x4b,0x00);
        bq27421_write_reg(client,0x4c,0x00);
        bq27421_write_reg(client,0x4d,0x00);
        bq27421_write_reg(client,0x4e,0x00);
        bq27421_write_reg(client,0x4f,0x00);
        bq27421_write_reg(client,0x50,0x00);
        bq27421_write_reg(client,0x51,0x00);
        bq27421_write_reg(client,0x5f,0x00);
        bq27421_write_reg(client,0x52,0x00);
        bq27421_write_reg(client,0x53,0x00);
        bq27421_write_reg(client,0x54,0x00);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x00);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x00);
        bq27421_write_reg(client,0x5a,0x00);
        bq27421_write_reg(client,0x5b,0x00);
        bq27421_write_reg(client,0x5c,0x00);
        bq27421_write_reg(client,0x5d,0x00);
        bq27421_write_reg(client,0x5e,0x00);


        bq27421_write_reg(client,0x60,0x2E);
	mdelay(200);


        bq27421_write_reg(client,0x3e,0x52);
        bq27421_write_reg(client,0x3f,0x01);

        bq27421_write_reg(client,0x3e,0x59);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x40,0x00);
        bq27421_write_reg(client,0x41,0x66);
        bq27421_write_reg(client,0x42,0x00);
        bq27421_write_reg(client,0x43,0x66);
        bq27421_write_reg(client,0x44,0x00);
        bq27421_write_reg(client,0x45,0x63);
        bq27421_write_reg(client,0x46,0x00);
        bq27421_write_reg(client,0x47,0x6B);
        bq27421_write_reg(client,0x48,0x00);
        bq27421_write_reg(client,0x49,0x48);
        bq27421_write_reg(client,0x4a,0x00);
        bq27421_write_reg(client,0x4b,0x3B);
        bq27421_write_reg(client,0x4c,0x00);
        bq27421_write_reg(client,0x4d,0x3E);
        bq27421_write_reg(client,0x4e,0x00);
        bq27421_write_reg(client,0x4f,0x3F);
        bq27421_write_reg(client,0x50,0x00);
        bq27421_write_reg(client,0x51,0x35);
        bq27421_write_reg(client,0x5f,0x00);
        bq27421_write_reg(client,0x52,0x2F);
        bq27421_write_reg(client,0x53,0x00);
        bq27421_write_reg(client,0x54,0x3C);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x46);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x8C);
        bq27421_write_reg(client,0x59,0x01);
        bq27421_write_reg(client,0x5a,0x71);
        bq27421_write_reg(client,0x5b,0x02);
        bq27421_write_reg(client,0x5c,0x4C);
        bq27421_write_reg(client,0x5d,0x00);
        bq27421_write_reg(client,0x5e,0x00);


        bq27421_write_reg(client,0x60,0x33);
	mdelay(200);


        bq27421_write_reg(client,0x3e,0x59);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x3e,0x68);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x40,0x00);
        bq27421_write_reg(client,0x41,0x00);
        bq27421_write_reg(client,0x42,0x00);
        bq27421_write_reg(client,0x43,0x00);
        bq27421_write_reg(client,0x44,0x00);
        bq27421_write_reg(client,0x45,0x00);
        bq27421_write_reg(client,0x46,0x00);
        bq27421_write_reg(client,0x47,0xCB);
        bq27421_write_reg(client,0x48,0xD4);
        bq27421_write_reg(client,0x49,0x1A);
        bq27421_write_reg(client,0x4a,0x05);
        bq27421_write_reg(client,0x4b,0x1C);
        bq27421_write_reg(client,0x4c,0x98);
        bq27421_write_reg(client,0x4d,0x02);
        bq27421_write_reg(client,0x4e,0xD3);
        bq27421_write_reg(client,0x4f,0xFF);
        bq27421_write_reg(client,0x50,0xB9);
        bq27421_write_reg(client,0x51,0x30);
        bq27421_write_reg(client,0x5f,0x00);
        bq27421_write_reg(client,0x52,0x00);
        bq27421_write_reg(client,0x53,0x00);
        bq27421_write_reg(client,0x54,0x00);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x00);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x00);
        bq27421_write_reg(client,0x5a,0x00);
        bq27421_write_reg(client,0x5b,0x00);
        bq27421_write_reg(client,0x5c,0x00);
        bq27421_write_reg(client,0x5d,0x00);
        bq27421_write_reg(client,0x5e,0x00);


        bq27421_write_reg(client,0x60,0xD0);
	mdelay(200);

        bq27421_write_reg(client,0x3e,0x68);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x3e,0x69);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x40,0x00);
        bq27421_write_reg(client,0x41,0x00);
        bq27421_write_reg(client,0x42,0x0B);
        bq27421_write_reg(client,0x43,0xAA);
        bq27421_write_reg(client,0x44,0x7F);
        bq27421_write_reg(client,0x45,0x37);
        bq27421_write_reg(client,0x46,0xC2);
        bq27421_write_reg(client,0x47,0x9C);
        bq27421_write_reg(client,0x48,0x93);
        bq27421_write_reg(client,0x49,0x51);
        bq27421_write_reg(client,0x4a,0x14);
        bq27421_write_reg(client,0x4b,0x09);
        bq27421_write_reg(client,0x4c,0x00);
        bq27421_write_reg(client,0x4d,0x00);
        bq27421_write_reg(client,0x4e,0x00);
        bq27421_write_reg(client,0x4f,0x00);
        bq27421_write_reg(client,0x50,0x00);
        bq27421_write_reg(client,0x51,0x00);
        bq27421_write_reg(client,0x5f,0x00);
        bq27421_write_reg(client,0x52,0x00);
        bq27421_write_reg(client,0x53,0x00);
        bq27421_write_reg(client,0x54,0x00);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x00);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x00);
        bq27421_write_reg(client,0x5a,0x00);
        bq27421_write_reg(client,0x5b,0x00);
        bq27421_write_reg(client,0x5c,0x00);
        bq27421_write_reg(client,0x5d,0x00);
        bq27421_write_reg(client,0x5e,0x00);


        bq27421_write_reg(client,0x60,0x35);
	mdelay(200);

        bq27421_write_reg(client,0x3e,0x69);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x3e,0x6B);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x40,0xEF);
        bq27421_write_reg(client,0x41,0x05);
        bq27421_write_reg(client,0x42,0x11);
        bq27421_write_reg(client,0x43,0x05);
        bq27421_write_reg(client,0x44,0x01);
        bq27421_write_reg(client,0x45,0x00);
        bq27421_write_reg(client,0x46,0x00);
        bq27421_write_reg(client,0x47,0x10);
        bq27421_write_reg(client,0x48,0x01);
        bq27421_write_reg(client,0x49,0x00);
        bq27421_write_reg(client,0x4a,0x3C);
        bq27421_write_reg(client,0x4b,0x00);
        bq27421_write_reg(client,0x4c,0x50);
        bq27421_write_reg(client,0x4d,0x3C);
        bq27421_write_reg(client,0x4e,0x00);
        bq27421_write_reg(client,0x4f,0x64);
        bq27421_write_reg(client,0x50,0x3C);
        bq27421_write_reg(client,0x51,0x00);
        bq27421_write_reg(client,0x5f,0x20);
        bq27421_write_reg(client,0x52,0x75);
        bq27421_write_reg(client,0x53,0x4E);
        bq27421_write_reg(client,0x54,0x0B);
        bq27421_write_reg(client,0x55,0x90);
        bq27421_write_reg(client,0x56,0x00);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x00);
        bq27421_write_reg(client,0x5a,0x00);
        bq27421_write_reg(client,0x5b,0x00);
        bq27421_write_reg(client,0x5c,0x00);
        bq27421_write_reg(client,0x5d,0x00);
        bq27421_write_reg(client,0x5e,0x00);


        bq27421_write_reg(client,0x60,0xFD);
	mdelay(200);


        bq27421_write_reg(client,0x3e,0x6B);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x3e,0x70);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x40,0x80);
        bq27421_write_reg(client,0x41,0x00);
        bq27421_write_reg(client,0x42,0x80);
        bq27421_write_reg(client,0x43,0x00);
        bq27421_write_reg(client,0x44,0x00);
        bq27421_write_reg(client,0x45,0x00);
        bq27421_write_reg(client,0x46,0x00);
        bq27421_write_reg(client,0x47,0x00);
        bq27421_write_reg(client,0x48,0x00);
        bq27421_write_reg(client,0x49,0x00);
        bq27421_write_reg(client,0x4a,0x00);
        bq27421_write_reg(client,0x4b,0x00);
        bq27421_write_reg(client,0x4c,0x00);
        bq27421_write_reg(client,0x4d,0x00);
        bq27421_write_reg(client,0x4e,0x00);
        bq27421_write_reg(client,0x4f,0x00);
        bq27421_write_reg(client,0x50,0x00);
        bq27421_write_reg(client,0x51,0x00);
        bq27421_write_reg(client,0x5f,0x00);
        bq27421_write_reg(client,0x52,0x00);
        bq27421_write_reg(client,0x53,0x00);
        bq27421_write_reg(client,0x54,0x00);
        bq27421_write_reg(client,0x55,0x00);
        bq27421_write_reg(client,0x56,0x00);
        bq27421_write_reg(client,0x57,0x00);
        bq27421_write_reg(client,0x58,0x00);
        bq27421_write_reg(client,0x59,0x00);
        bq27421_write_reg(client,0x5a,0x00);
        bq27421_write_reg(client,0x5b,0x00);
        bq27421_write_reg(client,0x5c,0x00);
        bq27421_write_reg(client,0x5d,0x00);
        bq27421_write_reg(client,0x5e,0x00);


        bq27421_write_reg(client,0x60,0xFF);
	mdelay(200);

        bq27421_write_reg(client,0x3e,0x70);
        bq27421_write_reg(client,0x3f,0x00);

        bq27421_write_reg(client,0x00,0x42);
        bq27421_write_reg(client,0x01,0x00);
	mdelay(2000);	

	
	bq27421_write_reg(client,0x00,0x0c);//battery insert
	bq27421_write_reg(client,0x01,0x00);//battery insert	
	
}

static int  bq27421_probe(struct i2c_client *client,
				   const struct i2c_device_id *id)
{
	int ret = 0;
	struct device_node *dev_node = client->dev.of_node;

	if (dev_node == NULL) {
		pr_err("Device Tree node doesn't exist.\n");
		return -ENODEV;
	}

	if (!i2c_check_functionality(client->adapter,
				I2C_FUNC_SMBUS_BYTE_DATA)) {
		pr_err(" i2c func fail.\n");
		return -EIO;
	}

	bq27421_dev = kzalloc(sizeof(*bq27421_dev), GFP_KERNEL);
	if (!bq27421_dev) {
		pr_err(" alloc fail.\n");
		return -ENOMEM;
	}
#if 0
	/* Note: Lithium-ion battery normal temperature range 0..40 C */
	ret = of_property_read_u32(dev_node, "ti,temp-cold",
				   &(bq27421_dev->temp_cold));
	if (ret) {
		pr_err("Unable to read cold temperature. ret=%d.\n", ret);
		goto err_dev_node;
	}
	pr_debug("cold temperature limit = %d C.\n", bq27421_dev->temp_cold);

	ret = of_property_read_u32(dev_node, "ti,temp-hot",
				   &(bq27421_dev->temp_hot));
	if (ret) {
		pr_err("Unable to read hot temperature. ret=%d.\n", ret);
		goto err_dev_node;
	}
	pr_debug("hot temperature limit = %d C.\n", bq27421_dev->temp_hot);
#endif

	

	bq27421_dev->client = client;
	i2c_set_clientdata(client, bq27421_dev);

	bq27421_reg_init(client);

	ret = bq27421_register_psy(bq27421_dev);
	if (ret) {
		pr_err(" bq27421_register_psy fail.\n");
		goto err_register_psy;
	}


	INIT_DELAYED_WORK(&bq27421_dev->periodic_user_space_update_work,
			  bq27421_periodic_user_space_update_worker);

	schedule_delayed_work(&bq27421_dev->periodic_user_space_update_work,
			      msecs_to_jiffies(1000));

	pr_debug("Device is ready.\n");

	return 0;


err_register_psy:
//err_dev_node:
	kfree(bq27421_dev);
	bq27421_dev = NULL;

	pr_info("FAIL.\n");

	return ret;
}

static int  bq27421_remove(struct i2c_client *client)
{
	struct bq27421_device *bq27421_dev = i2c_get_clientdata(client);

	power_supply_unregister(&bq27421_dev->batt_psy);
;
	kfree(bq27421_dev);
	bq27421_dev = NULL;

	return 0;
}

static const struct of_device_id bq27421_match[] = {
	{ .compatible = "ti,bq27421-battery" },
	{ },
	};

static const struct i2c_device_id bq27421_id[] = {
	{BQ27421_NAME, 0},
	{},
};

MODULE_DEVICE_TABLE(i2c, bq27421_id);

static struct i2c_driver bq27421_driver = {
	.driver	= {
		.name	= BQ27421_NAME,
		.owner	= THIS_MODULE,
		.of_match_table = of_match_ptr(bq27421_match),
	},
	.probe		= bq27421_probe,
	.remove		= bq27421_remove,
	.id_table	= bq27421_id,
};

static int __init bq27421_init(void)
{

	return i2c_add_driver(&bq27421_driver);
}
module_init(bq27421_init);

static void __exit bq27421_exit(void)
{
	return i2c_del_driver(&bq27421_driver);
}
module_exit(bq27421_exit);

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("zhangkai@sim.com");
MODULE_DESCRIPTION("BQ27421 battery monitor driver");
