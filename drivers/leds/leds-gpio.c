/*
 * LEDs driver for GPIOs
 *
 * Copyright (C) 2007 8D Technologies inc.
 * Raphael Assenat <raph@8d.com>
 * Copyright (C) 2008 Freescale Semiconductor, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/gpio.h>
#include <linux/leds.h>
#include <linux/of_platform.h>
#include <linux/of_gpio.h>
#include <linux/slab.h>
#include <linux/workqueue.h>
#include <linux/module.h>
#include <linux/pinctrl/consumer.h>
#include <linux/err.h>

struct gpio_led_data {
	struct led_classdev cdev;
	unsigned gpio;
	struct work_struct work;
	u8 new_level;
	u8 can_sleep;
	u8 active_low;
	u8 blinking;
	int (*platform_gpio_blink_set)(unsigned gpio, int state,
			unsigned long *delay_on, unsigned long *delay_off);
};

struct gpio_leds_priv {
	int num_leds;
	struct gpio_led_data leds[];
};

struct gpio_leds_priv *priv;

struct timer_list	leds_red_blink;
struct timer_list	leds_green_blink;
struct timer_list	leds_blue_blink;

enum leds_led_color{
	LEDS_COLOR_RED=0,
	LEDS_COLOR_GREEN,
	LEDS_COLOR_BLUE,
	LEDS_COLOR_MAX,
};

struct leds_status {
	unsigned char red_is_on;
	unsigned char red_is_blink;
	unsigned char green_is_on;
	unsigned char green_is_blink;
	unsigned char blue_is_on;
	unsigned char blue_is_blink;
};

struct leds_status leds_status = {0,0,0,0,0,0};

static void gpio_led_work(struct work_struct *work)
{
	struct gpio_led_data	*led_dat =
		container_of(work, struct gpio_led_data, work);

	if (led_dat->blinking) {
		led_dat->platform_gpio_blink_set(led_dat->gpio,
						 led_dat->new_level,
						 NULL, NULL);
		led_dat->blinking = 0;
	} else
		gpio_set_value_cansleep(led_dat->gpio, led_dat->new_level);
}

static void leds_set_led_state(struct gpio_leds_priv *priv, enum leds_led_color color, int level)
{
	int i;
	char *ledcolor = NULL;
	
	struct led_classdev *led_cdev;
	struct gpio_led_data *led_dat = NULL;

	switch (color)
	{
		case LEDS_COLOR_RED :
			ledcolor = "red";
			break;
		case LEDS_COLOR_GREEN :
			ledcolor = "green";
			break;
		case LEDS_COLOR_BLUE :
			ledcolor = "blue";
			break;
		case LEDS_COLOR_MAX:
			break;
		default:
			break;
	}

	for(i = 0; i < priv->num_leds; i++)
	{
		if (!strcmp(priv->leds[i].cdev.name, ledcolor)) 
		{	
			led_cdev = &priv->leds[i].cdev;
			led_dat = container_of(led_cdev, struct gpio_led_data, cdev);
		}
	}

	if ( led_dat != NULL )
		gpio_set_value(led_dat->gpio, level);

}

static int leds_get_led_state(struct gpio_leds_priv *priv, enum leds_led_color color)
{
	int i;
	char *ledcolor = NULL;
	int level = -1;
	
	struct led_classdev *led_cdev;
	struct gpio_led_data *led_dat = NULL;

	switch (color)
	{
		case LEDS_COLOR_RED :
			ledcolor = "red";
			break;
		case LEDS_COLOR_GREEN :
			ledcolor = "green";
			break;
		case LEDS_COLOR_BLUE :
			ledcolor = "blue";
			break;
		case LEDS_COLOR_MAX:
			ledcolor = NULL;
			break;
		default:
			ledcolor = NULL;
			break;
	}

	for(i = 0; i < priv->num_leds; i++)
	{
		if (!strcmp(priv->leds[i].cdev.name, ledcolor)) 
		{	
			led_cdev = &priv->leds[i].cdev;
			led_dat = container_of(led_cdev, struct gpio_led_data, cdev);
		}
	}

	if ( led_dat != NULL )
		level = gpio_get_value(led_dat->gpio);

	return level;
}

static void leds_red_blink_on(unsigned long data)
{
	u16 reg_val;

	struct gpio_leds_priv *priv = (struct gpio_leds_priv *) data ;
	
	reg_val = leds_get_led_state( priv, LEDS_COLOR_RED);

	if(reg_val < 0)
	{
		printk("========get led state error=============\n");
	}
	else
	{
		if(reg_val) 
			leds_set_led_state( priv, LEDS_COLOR_RED, 0 );
		else
			leds_set_led_state( priv, LEDS_COLOR_RED, 1 );
	}

	mod_timer(&leds_red_blink, jiffies+msecs_to_jiffies(800));
}

static void leds_green_blink_on(unsigned long data)
{
	u16 reg_val;

	struct gpio_leds_priv *priv = (struct gpio_leds_priv *) data ;

	reg_val = leds_get_led_state(priv,LEDS_COLOR_GREEN);

	if(reg_val < 0)
	{
		printk("========get led state error=============\n");
	}
	else
	{
		if(reg_val) 
			leds_set_led_state( priv, LEDS_COLOR_GREEN, 0 );
		else
			leds_set_led_state( priv, LEDS_COLOR_GREEN, 1 );
	}

	mod_timer(&leds_green_blink, jiffies+msecs_to_jiffies(800));
}

static void leds_blue_blink_on(unsigned long data)
{
	u16 reg_val;

	struct gpio_leds_priv *priv = (struct gpio_leds_priv *) data ;

	reg_val = leds_get_led_state( priv, LEDS_COLOR_BLUE);

	if(reg_val < 0)
	{
		printk("========get led state error=============\n");
	}
	else
	{
		if(reg_val) 
			leds_set_led_state( priv, LEDS_COLOR_BLUE, 0 );
		else
			leds_set_led_state( priv, LEDS_COLOR_BLUE, 1 );
	}
	
	mod_timer(&leds_blue_blink, jiffies+msecs_to_jiffies(800));
}

static ssize_t led_blink_solid_store(struct device *dev,
				     struct device_attribute *attr,
				     const char *buf, size_t size)
{
	int blink = 0;
	int color = LEDS_COLOR_MAX;

	struct led_classdev *led_cdev = dev_get_drvdata(dev);

	if (!strcmp(led_cdev->name, "red")) {
		color = LEDS_COLOR_RED;
	} else if (!strcmp(led_cdev->name, "green")) {
		color = LEDS_COLOR_GREEN;
	}else if (!strcmp(led_cdev->name, "blue")){
		color = LEDS_COLOR_BLUE;
	}else {
		color = LEDS_COLOR_MAX;
	}

	sscanf(buf, "%d", &blink);

	if(blink){
		switch(color){
		case LEDS_COLOR_RED:
			leds_status.red_is_blink = 1;
			mod_timer(&leds_red_blink, jiffies+msecs_to_jiffies(500));
			break;
		case LEDS_COLOR_GREEN:
			leds_status.green_is_blink = 1;
			mod_timer(&leds_green_blink, jiffies+msecs_to_jiffies(500));
			break;
		case LEDS_COLOR_BLUE:
			leds_status.blue_is_blink = 1;
			mod_timer(&leds_blue_blink, jiffies+msecs_to_jiffies(500));
			break;
	  	}
	}
	else{
		
		switch(color){
		case LEDS_COLOR_RED:
			leds_status.red_is_blink = 0;
			del_timer(&leds_red_blink);
		
			if (leds_status.red_is_on ){
				leds_set_led_state( priv, LEDS_COLOR_RED, 1);
			}else{
				leds_set_led_state( priv, LEDS_COLOR_RED, 0 );
			}
			break;
		case LEDS_COLOR_GREEN:
			leds_status.green_is_blink = 0;
			del_timer(&leds_green_blink);
			
			if (leds_status.green_is_on) 
				leds_set_led_state( priv, LEDS_COLOR_GREEN, 1 );
			else 
				leds_set_led_state( priv, LEDS_COLOR_GREEN, 0 );
			
			break;
		case LEDS_COLOR_BLUE:
			leds_status.blue_is_blink = 0;
			del_timer(&leds_blue_blink);
			
			if (leds_status.blue_is_on) 
				leds_set_led_state( priv, LEDS_COLOR_BLUE, 1 );
			else
				leds_set_led_state( priv, LEDS_COLOR_BLUE, 0 );
			
			break;
		}
	}

	return size;
}

static DEVICE_ATTR(blink, S_IWUSR, NULL, led_blink_solid_store);

static void gpio_led_set(struct led_classdev *led_cdev,
	enum led_brightness value)
{
	struct gpio_led_data *led_dat =
		container_of(led_cdev, struct gpio_led_data, cdev);
	int level;

	int color;

	if (value == LED_OFF)
		level = 0;
	else
		level = 1;

	if (led_dat->active_low)
		level = !level;

	if (!strcmp(led_cdev->name, "red")) {
		color = LEDS_COLOR_RED;
	} else if (!strcmp(led_cdev->name, "green")) {
		color = LEDS_COLOR_GREEN;
	}else if (!strcmp(led_cdev->name, "blue")){
		color = LEDS_COLOR_BLUE;
	}else {
		color = LEDS_COLOR_MAX;
	}
	
	/* Setting GPIOs with I2C/etc requires a task context, and we don't
	 * seem to have a reliable way to know if we're already in one; so
	 * let's just assume the worst.
	 */
	if (led_dat->can_sleep) {
		led_dat->new_level = level;
		schedule_work(&led_dat->work);
	} else {
		if (led_dat->blinking) {
			led_dat->platform_gpio_blink_set(led_dat->gpio, level,
							 NULL, NULL);
			led_dat->blinking = 0;
		} else{
			switch(color){
				case LEDS_COLOR_RED:
					leds_status.red_is_on =level;
					break;
				case LEDS_COLOR_GREEN:
					leds_status.green_is_on = level;
					break;
				case LEDS_COLOR_BLUE:
					leds_status.blue_is_on = level;
					break;
				case LEDS_COLOR_MAX:
					break;
				default:
					break;
			}
			gpio_set_value(led_dat->gpio, level);
		}
	}
}

static int gpio_blink_set(struct led_classdev *led_cdev,
	unsigned long *delay_on, unsigned long *delay_off)
{
	struct gpio_led_data *led_dat =
		container_of(led_cdev, struct gpio_led_data, cdev);

	led_dat->blinking = 1;
	return led_dat->platform_gpio_blink_set(led_dat->gpio, GPIO_LED_BLINK,
						delay_on, delay_off);
}

static int create_gpio_led(const struct gpio_led *template,
	struct gpio_led_data *led_dat, struct device *parent,
	int (*blink_set)(unsigned, int, unsigned long *, unsigned long *))
{
	int ret, state;

	led_dat->gpio = -1;

	/* skip leds that aren't available */
	if (!gpio_is_valid(template->gpio)) {
		dev_info(parent, "Skipping unavailable LED gpio %d (%s)\n",
				template->gpio, template->name);
		return 0;
	}

	ret = devm_gpio_request(parent, template->gpio, template->name);
	if (ret < 0)
		return ret;

	led_dat->cdev.name = template->name;
	led_dat->cdev.default_trigger = template->default_trigger;
	led_dat->gpio = template->gpio;
	led_dat->can_sleep = gpio_cansleep(template->gpio);
	led_dat->active_low = template->active_low;
	led_dat->blinking = 0;
	if (blink_set) {
		led_dat->platform_gpio_blink_set = blink_set;
		led_dat->cdev.blink_set = gpio_blink_set;
	}
	led_dat->cdev.brightness_set = gpio_led_set;
	if (template->default_state == LEDS_GPIO_DEFSTATE_KEEP)
		state = !!gpio_get_value_cansleep(led_dat->gpio) ^ led_dat->active_low;
	else
		state = (template->default_state == LEDS_GPIO_DEFSTATE_ON);
	led_dat->cdev.brightness = state ? LED_FULL : LED_OFF;
	if (!template->retain_state_suspended)
		led_dat->cdev.flags |= LED_CORE_SUSPENDRESUME;

	ret = gpio_direction_output(led_dat->gpio, led_dat->active_low ^ state);
	if (ret < 0)
		return ret;

	INIT_WORK(&led_dat->work, gpio_led_work);

	ret = led_classdev_register(parent, &led_dat->cdev);
	if (ret < 0)
		return ret;

	return 0;
}

static void delete_gpio_led(struct gpio_led_data *led)
{
	if (!gpio_is_valid(led->gpio))
		return;
	led_classdev_unregister(&led->cdev);
	cancel_work_sync(&led->work);
}

#if 0
struct gpio_leds_priv {
	int num_leds;
	struct gpio_led_data leds[];
};
#endif

static inline int sizeof_gpio_leds_priv(int num_leds)
{
	return sizeof(struct gpio_leds_priv) +
		(sizeof(struct gpio_led_data) * num_leds);
}

/* Code to create from OpenFirmware platform devices */
#ifdef CONFIG_OF_GPIO
static struct gpio_leds_priv *gpio_leds_create_of(struct platform_device *pdev)
{
	struct device_node *np = pdev->dev.of_node, *child;
	struct gpio_leds_priv *priv;
	int count, ret;

	int led_num;

	/* count LEDs in this device, so we know how much to allocate */
	count = of_get_child_count(np);
	if (!count)
		return ERR_PTR(-ENODEV);

	for_each_child_of_node(np, child)
		if (of_get_gpio(child, 0) == -EPROBE_DEFER)
			return ERR_PTR(-EPROBE_DEFER);

	priv = devm_kzalloc(&pdev->dev, sizeof_gpio_leds_priv(count),
			GFP_KERNEL);
	if (!priv)
		return ERR_PTR(-ENOMEM);

	for_each_child_of_node(np, child) {
		struct gpio_led led = {};
		enum of_gpio_flags flags;
		const char *state;

		led.gpio = of_get_gpio_flags(child, 0, &flags);
		led.active_low = flags & OF_GPIO_ACTIVE_LOW;
		led.name = of_get_property(child, "label", NULL) ? : child->name;
		led.default_trigger =
			of_get_property(child, "linux,default-trigger", NULL);
		state = of_get_property(child, "default-state", NULL);
		if (state) {
			if (!strcmp(state, "keep"))
				led.default_state = LEDS_GPIO_DEFSTATE_KEEP;
			else if (!strcmp(state, "on"))
				led.default_state = LEDS_GPIO_DEFSTATE_ON;
			else
				led.default_state = LEDS_GPIO_DEFSTATE_OFF;
		}

		led.retain_state_suspended =
			(unsigned)of_property_read_bool(child,
				"retain-state-suspended");

		ret = create_gpio_led(&led, &priv->leds[priv->num_leds++],
				      &pdev->dev, NULL);
		if (ret < 0) {
			of_node_put(child);
			goto err;
		}
	}

	for(led_num = 0; led_num < count ; led_num++)
	{
		if( (!strcmp(priv->leds[led_num].cdev.name, "red")) 
			|| (!strcmp(priv->leds[led_num].cdev.name, "green"))
			|| (!strcmp(priv->leds[led_num].cdev.name, "blue"))) 
		{
				ret = device_create_file( priv->leds[led_num].cdev.dev , &dev_attr_blink);
				if (ret)
				{
					printk("============%s : device_create blink failed===========\n", __func__);
					goto err_out_attr_blink;
				}	
		}
	}	

	setup_timer(&leds_red_blink, leds_red_blink_on, (unsigned long)priv );
	setup_timer(&leds_green_blink, leds_green_blink_on, (unsigned long)priv );
	setup_timer(&leds_blue_blink, leds_blue_blink_on, (unsigned long)priv );
	return priv;

err:
	for (count = priv->num_leds - 2; count >= 0; count--)
		delete_gpio_led(&priv->leds[count]);
	//return ERR_PTR(-ENODEV);

err_out_attr_blink:
	for(led_num = 0; led_num < count ; led_num++)
	{
		if( (!strcmp(priv->leds[led_num].cdev.name, "red")) || (!strcmp(priv->leds[led_num].cdev.name, "green") )|| (!strcmp(priv->leds[led_num].cdev.name, "blue") )) 
		{
         		device_remove_file( priv->leds[led_num].cdev.dev , &dev_attr_blink);
		}
	}	
	
	return NULL;
}

static const struct of_device_id of_gpio_leds_match[] = {
	{ .compatible = "gpio-leds", },
	{},
};
#else /* CONFIG_OF_GPIO */
static struct gpio_leds_priv *gpio_leds_create_of(struct platform_device *pdev)
{
	return ERR_PTR(-ENODEV);
}
#endif /* CONFIG_OF_GPIO */


static int gpio_led_probe(struct platform_device *pdev)
{
	struct gpio_led_platform_data *pdata = pdev->dev.platform_data;
	//struct gpio_leds_priv *priv;
	struct pinctrl *pinctrl;
	int i, ret = 0;

	pinctrl = devm_pinctrl_get_select_default(&pdev->dev);
	if (IS_ERR(pinctrl))
		dev_warn(&pdev->dev,
			"pins are not configured from the driver\n");

	if (pdata && pdata->num_leds) {
		priv = devm_kzalloc(&pdev->dev,
				sizeof_gpio_leds_priv(pdata->num_leds),
					GFP_KERNEL);
		if (!priv)
			return -ENOMEM;

		priv->num_leds = pdata->num_leds;
		for (i = 0; i < priv->num_leds; i++) {
			ret = create_gpio_led(&pdata->leds[i],
					      &priv->leds[i],
					      &pdev->dev, pdata->gpio_blink_set);
			if (ret < 0) {
				/* On failure: unwind the led creations */
				for (i = i - 1; i >= 0; i--)
					delete_gpio_led(&priv->leds[i]);
				return ret;
			}
		}
	} else {
		priv = gpio_leds_create_of(pdev);
		if (IS_ERR(priv))
			return PTR_ERR(priv);
	}

	platform_set_drvdata(pdev, priv);

	return 0;
}

static int gpio_led_remove(struct platform_device *pdev)
{
	struct gpio_leds_priv *priv = platform_get_drvdata(pdev);
	int i;

	for (i = 0; i < priv->num_leds; i++)
		delete_gpio_led(&priv->leds[i]);

	platform_set_drvdata(pdev, NULL);

	return 0;
}

static struct platform_driver gpio_led_driver = {
	.probe		= gpio_led_probe,
	.remove		= gpio_led_remove,
	.driver		= {
		.name	= "leds-gpio",
		.owner	= THIS_MODULE,
		.of_match_table = of_match_ptr(of_gpio_leds_match),
	},
};

module_platform_driver(gpio_led_driver);

MODULE_AUTHOR("Raphael Assenat <raph@8d.com>, Trent Piepho <tpiepho@freescale.com>");
MODULE_DESCRIPTION("GPIO LED driver");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:leds-gpio");
