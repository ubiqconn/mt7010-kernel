/*
 * Copyright (C) 2010 Trusted Logic S.A.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/i2c.h>
#include <linux/jiffies.h>
#include <linux/uaccess.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/platform_device.h>
#include <linux/gpio.h>
#include <linux/miscdevice.h>
#include <linux/spinlock.h>
#include <linux/of_gpio.h>
#include <mach/gpiomux.h>
#include <linux/of_device.h>
#include "pn547.h"

#define PN547_DEBUG
#if defined(PN547_DEBUG)
static int debug_flag = 0;

#define __NFC_FILE__ strrchr(__FILE__, '/') ? (strrchr(__FILE__, '/')+1) : __FILE__

#define NFC_DEBUG(fmt, args...) \
do { \
	if (debug_flag) \
		printk(KERN_DEBUG "[%s:%s()->Line:%d] " fmt, __NFC_FILE__, __func__, __LINE__, ##args); \
} while(0)
#else
#define NFC_DEBUG(fmt, args...)
#endif

#define NFC_INFO(fmt, args...) \
	pr_info("[%s()->Line:%d] " fmt, __func__, __LINE__, ##args)

#define NFC_ERR(fmt, args...) \
	pr_err("[%s()->Line:%d] " fmt, __func__, __LINE__, ##args)

#define MAX_BUFFER_SIZE	512

struct pn547_dev	{
	wait_queue_head_t	read_wq;
	struct mutex		read_mutex;
	struct i2c_client	*client;
	struct miscdevice	pn547_device;
	unsigned int 		ven_gpio;
	unsigned int 		firm_gpio;
	unsigned int		irq_gpio;
	unsigned int		pvdd_en_gpio;
	bool			    irq_enabled;
	spinlock_t		    irq_enabled_lock;
	unsigned int        reg;
};

#if defined(PN547_DEBUG)
/* sysfs interface */
static ssize_t nfc_show(struct device *dev,struct device_attribute *attr, char *buf)
{
	return sprintf(buf,"%d\n",debug_flag);
}

/* debug fs, "echo @1 > /sys/bus/i2c/devices/xxx/nfc_debug" @1:debug_flag  */
static ssize_t nfc_store(struct device *dev,
        struct device_attribute *attr, const char *buf,size_t count)
{
	sscanf(buf, "%d", &debug_flag);
	return count;
}

static struct device_attribute pn547_dev_attr =
	__ATTR(nfc_debug, 0660, nfc_show, nfc_store);
#endif

static void pn547_disable_irq(struct pn547_dev *pn547_dev)
{
	unsigned long flags;
	
	spin_lock_irqsave(&pn547_dev->irq_enabled_lock, flags);
	if (pn547_dev->irq_enabled) {
		disable_irq_nosync(pn547_dev->client->irq);
		pn547_dev->irq_enabled = false;
	}
	spin_unlock_irqrestore(&pn547_dev->irq_enabled_lock, flags);
}

static irqreturn_t pn547_dev_irq_handler(int irq, void *dev_id)
{
	struct pn547_dev *pn547_dev = dev_id;
	
	if (!gpio_get_value(pn547_dev->irq_gpio)) {
		return IRQ_HANDLED;
	}
	pn547_disable_irq(pn547_dev);

	/* Wake up waiting readers */
	wake_up(&pn547_dev->read_wq);

	return IRQ_HANDLED;
}

static ssize_t pn547_dev_read(struct file *filp, char __user *buf,
		size_t count, loff_t *offset)
{
	struct pn547_dev *pn547_dev = filp->private_data;
	char tmp[MAX_BUFFER_SIZE];
	int ret,i;
	 
	if (count > MAX_BUFFER_SIZE)
		count = MAX_BUFFER_SIZE;

	NFC_DEBUG("reading %zu bytes.\n",  count);

	mutex_lock(&pn547_dev->read_mutex);

	if (!gpio_get_value(pn547_dev->irq_gpio)) {
		if (filp->f_flags & O_NONBLOCK) {
			ret = -EAGAIN;
			goto fail;
		}

		pn547_dev->irq_enabled = true;
		enable_irq(pn547_dev->client->irq);
		ret = wait_event_interruptible(pn547_dev->read_wq,gpio_get_value(pn547_dev->irq_gpio));

		pn547_disable_irq(pn547_dev);

		if (ret)
			goto fail;
	}

	/* Read data */
	ret = i2c_master_recv(pn547_dev->client, tmp, count);
	mutex_unlock(&pn547_dev->read_mutex);

	if (ret < 0) {
		NFC_ERR("i2c_master_recv returned %d\n", ret);
		return ret;
	}
	if (ret > count) {
		NFC_ERR("received too many bytes from i2c (%d)\n", ret);
		return -EIO;
	}
	if (copy_to_user(buf, tmp, ret)) {
		NFC_ERR("failed to copy to user space\n");
		return -EFAULT;
	}

	NFC_DEBUG("IFD->PC:");
	for(i = 0; i < ret; i++){
		NFC_DEBUG(" %02X", tmp[i]);
	}
	NFC_DEBUG("\n");

	return ret;

fail:
	mutex_unlock(&pn547_dev->read_mutex);
	return ret;
}

static ssize_t pn547_dev_write(struct file *filp, const char __user *buf,
		size_t count, loff_t *offset)
{
	struct pn547_dev  *pn547_dev;
	char tmp[MAX_BUFFER_SIZE];
	int ret,i;

	pn547_dev = filp->private_data;

	if (count > MAX_BUFFER_SIZE)
		count = MAX_BUFFER_SIZE;

	if (copy_from_user(tmp, buf, count)) {
		NFC_ERR("failed to copy from user space\n");
		return -EFAULT;
	}

	NFC_DEBUG("writing %zu bytes.\n", count);
	/* Write data */
	ret = i2c_master_send(pn547_dev->client, tmp, count);
	if (ret != count) {
		NFC_ERR("i2c_master_send returned %d\n", ret);
		ret = -EIO;
		goto exit;
	}
	NFC_DEBUG("PC->IFD:");
	for(i = 0; i < count; i++){
		NFC_DEBUG(" %02X", tmp[i]);
	}
	NFC_DEBUG("\n");

exit:
	return ret;
}

static int pn547_dev_open(struct inode *inode, struct file *filp)
{
	struct pn547_dev *pn547_dev = container_of(filp->private_data,
						struct pn547_dev,
						pn547_device);

	filp->private_data = pn547_dev;

	NFC_DEBUG("%d,%d\n", imajor(inode), iminor(inode));

	return 0;
}

static long pn547_dev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	struct pn547_dev *pn547_dev = filp->private_data;

	switch (cmd) {
	case PN547_SET_PWR:
		if (arg == 2) {
			/* power on with firmware download (requires hw reset)
			 */
			NFC_DEBUG("power on with firmware\n");
			gpio_set_value(pn547_dev->ven_gpio, 1);
			gpio_set_value(pn547_dev->firm_gpio, 1);
			msleep(10);
			gpio_set_value(pn547_dev->ven_gpio, 0);
			msleep(50);
			gpio_set_value(pn547_dev->ven_gpio, 1);
			msleep(10);
		} else if (arg == 1) {
			/* power on */
			NFC_DEBUG("power on\n");
			gpio_set_value(pn547_dev->firm_gpio, 0);
			gpio_set_value(pn547_dev->ven_gpio, 1);
			msleep(10);
		} else  if (arg == 0) {
			/* power off */
			NFC_DEBUG("power off\n");
			gpio_set_value(pn547_dev->firm_gpio, 0);
			gpio_set_value(pn547_dev->ven_gpio, 0);
			msleep(10);
		} else {
			NFC_DEBUG("bad arg %lu\n", arg);
			return -EINVAL;
		}
		break;
	default:
		NFC_DEBUG("bad ioctl %u\n", cmd);
		return -EINVAL;
	}

	return 0;
}

static const struct file_operations pn547_dev_fops = {
	.owner	= THIS_MODULE,
	.llseek	= no_llseek,
	.read	= pn547_dev_read,
	.write	= pn547_dev_write,
	.open	= pn547_dev_open,
	.unlocked_ioctl  = pn547_dev_ioctl,
};

static void pn547_power_init(struct pn547_dev *pn547_dev, int on)
{
 	if(on) {
		gpio_direction_output(pn547_dev->pvdd_en_gpio, 1);
	}else {
		gpio_direction_output(pn547_dev->pvdd_en_gpio, 0);	
	}	
}

static int pn547_parse_dt(struct device *dev, struct pn547_dev *pn547_dev)
{
	int r = 0;
	struct device_node *np = dev->of_node;
	 
	r = of_property_read_u32(np, "reg", &pn547_dev->reg);
	if (r)
		return -EINVAL;

	pn547_dev->pvdd_en_gpio = of_get_named_gpio(np, "nxp,pvdd-en-gpio", 0);
	if ((!gpio_is_valid(pn547_dev->pvdd_en_gpio)))
		return -EINVAL;

	pn547_dev->ven_gpio = of_get_named_gpio(np, "nxp,enable-gpio", 0);
	if ((!gpio_is_valid(pn547_dev->ven_gpio)))
		return -EINVAL;

	pn547_dev->irq_gpio = of_get_named_gpio(np, "nxp,irq-gpio", 0);
	if ((!gpio_is_valid(pn547_dev->irq_gpio)))
		return -EINVAL;

	pn547_dev->firm_gpio = of_get_named_gpio(np, "nxp,dwl-gpio", 0);
	if ((!gpio_is_valid(pn547_dev->firm_gpio)))
		return -EINVAL;
	
	NFC_DEBUG("pvdd = %d, ven = %d, irq = %d, firm = %d\n", pn547_dev->pvdd_en_gpio,
		pn547_dev->ven_gpio, pn547_dev->irq_gpio, pn547_dev->firm_gpio);
	return r;
}

static int pn547_probe(struct i2c_client *client,const struct i2c_device_id *id)
{
	int ret;
	struct pn547_dev *pn547_dev;

    printk("pn547 enter probe\n");
	 
	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
		NFC_ERR("need I2C_FUNC_I2C\n");
		return -ENODEV;
	}

	pn547_dev = kzalloc(sizeof(*pn547_dev), GFP_KERNEL);
	if (pn547_dev == NULL) {
		NFC_ERR("failed to allocate memory for module data\n");
		return -ENOMEM;
	}

	ret = pn547_parse_dt(&client->dev, pn547_dev);
	if (ret)
		goto err_exit;
	
   	ret = gpio_request(pn547_dev->pvdd_en_gpio, "nfc_pvdd_enable");
	if(ret){
		NFC_ERR("nfc_pvdd_enable request error\n");
		goto err_exit;
	}

	pn547_power_init(pn547_dev, 1);
	if(!gpio_get_value(pn547_dev->pvdd_en_gpio)){
		NFC_ERR("nfc-pn547 power( = %d )failed.\n", gpio_get_value(pn547_dev->pvdd_en_gpio));
		ret = -ENODEV;
		goto err_power;
	}
	
	pn547_dev->client = client;

	/* init mutex and queues */
	init_waitqueue_head(&pn547_dev->read_wq);
	mutex_init(&pn547_dev->read_mutex);
	spin_lock_init(&pn547_dev->irq_enabled_lock);

	pn547_dev->pn547_device.minor = MISC_DYNAMIC_MINOR;
	pn547_dev->pn547_device.name = PN547_NAME;
	pn547_dev->pn547_device.fops = &pn547_dev_fops;
	
	ret = misc_register(&pn547_dev->pn547_device);
	if (ret) {
		NFC_ERR("misc_register failed\n");
		goto err_misc_register;
	}
	
    ret = gpio_request(pn547_dev->irq_gpio, "nfc_irq");
	if(ret){
		NFC_ERR("gpio_IRQ_request error\n");
		goto err_irq;
	}

    ret = gpio_request(pn547_dev->ven_gpio, "nfc_ven");
	if(ret){
	    NFC_ERR("gpio_VEN_request error\n");
	    goto err_ven;
	}

    ret = gpio_request(pn547_dev->firm_gpio, "nfc_firm");
	if(ret){
	  	NFC_ERR("gpio_firm_request error\n");
	  	goto err_firm;
	}

	gpio_direction_output(pn547_dev->firm_gpio, 0);
	gpio_direction_output(pn547_dev->ven_gpio, 1);
	gpio_direction_input(pn547_dev->irq_gpio);
	client->irq = gpio_to_irq(pn547_dev->irq_gpio);
	NFC_INFO("%s : requesting IRQ %d\n", __func__, client->irq);
	pn547_dev->irq_enabled = true;
	
	NFC_DEBUG("%s, irq_gpio = %d, irq_gpio_value = %d\n",__func__,pn547_dev->irq_gpio,gpio_get_value(pn547_dev->irq_gpio));

	
	ret = request_irq(client->irq, pn547_dev_irq_handler,IRQF_TRIGGER_HIGH, client->name, pn547_dev);
//	ret = request_threaded_irq(client->irq, pn547_dev_irq_handler, NULL, IRQF_TRIGGER_HIGH, client->name, pn547_dev);
	NFC_DEBUG("FUNC = %s, LINE = %d, ret = %d\n", __func__, __LINE__, ret);
	if (ret) {
		NFC_ERR("request_irq failed\n");
		goto err_request_irq_failed;
	}

	pn547_disable_irq(pn547_dev);

	i2c_set_clientdata(client, pn547_dev);
	
#if defined(PN547_DEBUG)
	ret = device_create_file(&client->dev, &pn547_dev_attr);
	if (ret) {
		NFC_ERR("sysfs registration failed, error %d \n", ret);
		ret = 0;
	}
#endif

	NFC_INFO("nfc-pn547 probe successful\n");

	return 0;

err_request_irq_failed:
	gpio_free(pn547_dev->firm_gpio);
err_firm:
	gpio_free(pn547_dev->ven_gpio);
err_ven:
    gpio_free(pn547_dev->irq_gpio);
err_irq:
	misc_deregister(&pn547_dev->pn547_device);
err_misc_register:
	mutex_destroy(&pn547_dev->read_mutex);
	pn547_power_init(pn547_dev, 0);
err_power:
	gpio_free(pn547_dev->pvdd_en_gpio);
err_exit:
	kfree(pn547_dev);
	return ret;
}

static int pn547_remove(struct i2c_client *client)
{
	struct pn547_dev *pn547_dev;

	pn547_dev = i2c_get_clientdata(client);

#if defined(PN547_DEBUG)
	device_remove_file(&client->dev, &pn547_dev_attr);
#endif
	free_irq(client->irq, pn547_dev);
	misc_deregister(&pn547_dev->pn547_device);
	mutex_destroy(&pn547_dev->read_mutex);
	gpio_free(pn547_dev->irq_gpio);
	gpio_free(pn547_dev->ven_gpio);
	gpio_free(pn547_dev->firm_gpio);
	pn547_power_init(pn547_dev, 0);
	kfree(pn547_dev);

	return 0;
}

static struct of_device_id pn547_match_table[] = {
	{.compatible = "nxp,pn547"},
	{}
};
MODULE_DEVICE_TABLE(of, pn547_match_table);

static const struct i2c_device_id pn547_id[] = {
	{ PN547_NAME, 0 },
	{ }
};

static struct i2c_driver pn547_driver = {
	.id_table	= pn547_id,
	.probe		= pn547_probe,
	.remove		= pn547_remove,
	.driver		= {
		.owner	= THIS_MODULE,
		.name	= PN547_NAME,
		.of_match_table = pn547_match_table,
	},
};

/*
 * module load/unload record keeping
 */

static int __init pn547_dev_init(void)
{
	NFC_DEBUG("Loading pn547 driver\n");
	return i2c_add_driver(&pn547_driver);
}
module_init(pn547_dev_init);

static void __exit pn547_dev_exit(void)
{
	NFC_DEBUG("Unloading pn547 driver\n");	
	i2c_del_driver(&pn547_driver);
}
module_exit(pn547_dev_exit);

MODULE_AUTHOR("joco zhang");
MODULE_DESCRIPTION("NFC PN547 driver");
MODULE_LICENSE("GPL");
