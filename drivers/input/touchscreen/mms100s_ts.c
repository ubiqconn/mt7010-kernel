/*
 * MELFAS MMS100S Touchscreen Driver
 *
 * Copyright (C) 2014 MELFAS Inc.
 *
 *	Modified records
 *	1.Function mms_reboot date:20150602
 *		add gpio request by xiaopeng.li@sim.com
 *	2.
 *
 */

#include "mms100s_ts.h"

#define PINCTRL_STATE_ACTIVE	"pmx_ts_active"
#define PINCTRL_STATE_SUSPEND	"pmx_ts_suspend"
#define PINCTRL_STATE_RELEASE	"pmx_ts_release"  //linfeng

static int esd_cnt;
#ifdef CONFIG_HAS_EARLYSUSPEND
static void mms_ts_early_suspend(struct early_suspend *h);
static void mms_ts_late_resume(struct early_suspend *h);
#endif
static int mms_ts_config(struct mms_ts_info *info);
static int fb_notifier_callback(struct notifier_block *self,
				 unsigned long event, void *data);

/* mms_ts_enable - wake-up func (VDD on)  */
static void mms_ts_enable(struct mms_ts_info *info)
{
	if (info->enabled)
		return;
	
	mutex_lock(&info->lock);

	gpio_direction_output(info->pdata->gpio_reset, 1);
	msleep(50);
	
	info->enabled = true;
	enable_irq(info->irq);

	mutex_unlock(&info->lock);
}

/* mms_ts_disable - sleep func (VDD off) */
static void mms_ts_disable(struct mms_ts_info *info)
{
	if (!info->enabled)
		return;

	i2c_smbus_write_byte_data(info->client, MMS_POWER_CONTROL, 1);

	mutex_lock(&info->lock);

	disable_irq(info->irq);

	msleep(50);
	gpio_direction_output(info->pdata->gpio_reset, 0);
	msleep(50);

	info->enabled = false;

	mutex_unlock(&info->lock);
}

/* mms_reboot - IC reset */ 
void mms_reboot(struct mms_ts_info *info)
{
	struct i2c_adapter *adapter = to_i2c_adapter(info->client->dev.parent);
	
	int rc;
	rc = gpio_request(info->pdata->gpio_reset, "melfas,reset-gpio");
	
	if (rc) {
		dev_err(&info->client->dev, "%s [ERROR] gpio_request : reset-gpio\n", __func__);
		goto ERROR_RESET;
	}
	
	i2c_lock_adapter(adapter);
	msleep(50);
	gpio_direction_output(info->pdata->gpio_reset, 0);
	msleep(150);

	gpio_direction_output(info->pdata->gpio_reset, 1);
	msleep(50);

	i2c_unlock_adapter(adapter);
ERROR_RESET:
	if (gpio_is_valid(info->pdata->gpio_reset))		
		gpio_free(info->pdata->gpio_reset);	
}

/*
 * mms_clear_input_data - all finger point release
 */
void mms_clear_input_data(struct mms_ts_info *info)
{
	int i;

	for (i = 0; i < MAX_FINGER_NUM; i++) {
		input_mt_slot(info->input_dev, i);
		input_mt_report_slot_state(info->input_dev, MT_TOOL_FINGER, false);
	}
	input_sync(info->input_dev);

	return;
}

/* mms_report_input_data - The position of a touch send to platfrom  */
void mms_report_input_data(struct mms_ts_info *info, u8 sz, u8 *buf)
{
	int i;
	struct i2c_client *client = info->client;
	int id;
	int x;
	int y;
	int touch_major;
	int pressure;
	int key_code;
	int key_state;
	u8 *tmp;

	if (buf[0] == MMS_NOTIFY_EVENT) {
		dev_info(&client->dev, "TSP mode changed (%d)\n", buf[1]);
		goto out;
	} else if (buf[0] == MMS_ERROR_EVENT) {
		dev_info(&client->dev, "Error detected, restarting TSP\n");
		mms_clear_input_data(info);
		mms_reboot(info);
		esd_cnt++;
		if (esd_cnt>= ESD_DETECT_COUNT)
		{
			i2c_smbus_write_byte_data(info->client, MMS_MODE_CONTROL, 0x04);
			esd_cnt =0;
		}
		goto out;
	}

	for (i = 0; i < sz; i += FINGER_EVENT_SZ) {
		tmp = buf + i;
		esd_cnt =0;
		if (tmp[0] & MMS_TOUCH_KEY_EVENT) {
			switch (tmp[0] & 0xf) {
			case 1:
				//key_code = KEY_BACK;
				key_code = KEY_MENU;
				break;
			case 2:
				key_code = KEY_HOMEPAGE;
				break;
			case 3:
				//key_code = KEY_MENU;
				key_code = KEY_BACK;
				break;
			default:
				dev_err(&client->dev, "unknown key type\n");
				goto out;
				break;
			}
			key_state = (tmp[0] & 0x80) ? 1 : 0;
			input_report_key(info->input_dev, key_code, key_state);

		} else {
			id = (tmp[0] & 0xf) -1;

			x = tmp[2] | ((tmp[1] & 0xf) << 8);
			y = tmp[3] | (((tmp[1] >> 4 ) & 0xf) << 8);
			touch_major = tmp[4];
			pressure = tmp[5];

			input_mt_slot(info->input_dev, id);
			
			if (!(tmp[0] & 0x80)) {
				input_mt_report_slot_state(info->input_dev, MT_TOOL_FINGER, false);
				continue;
			}

			input_mt_report_slot_state(info->input_dev, MT_TOOL_FINGER, true);
			input_report_abs(info->input_dev, ABS_MT_POSITION_X, x);
			input_report_abs(info->input_dev, ABS_MT_POSITION_Y, y);
			input_report_abs(info->input_dev, ABS_MT_TOUCH_MAJOR, touch_major);
			input_report_abs(info->input_dev, ABS_MT_PRESSURE, pressure);
		}
	}

	input_sync(info->input_dev);

out:
	return;

}

/* mms_ts_interrupt - interrupt thread */
static irqreturn_t mms_ts_interrupt(int irq, void *dev_id)
{
	struct mms_ts_info *info = dev_id;
	struct i2c_client *client = info->client;
	u8 buf[MAX_FINGER_NUM * FINGER_EVENT_SZ] = { 0, };
	int ret;
	int sz;
	u8 reg = MMS_INPUT_EVENT;
	
	struct i2c_msg msg[] = {
		{
			.addr = client->addr,
			.flags = 0,
			.buf = &reg,
			.len = 1,
		}, {
			.addr = client->addr,
			.flags = I2C_M_RD,
			.buf = buf,
		},
	};
//	dev_err(&client->dev,"mms_ts_interrupt client->addr =0x%02X\n",client->addr);
	
	sz = i2c_smbus_read_byte_data(client, MMS_EVENT_PKT_SZ);
	
	if(0 > sz){
		mms_ts_disable(info);
		return sz;
	}
	msg[1].len = sz;
	
	ret = i2c_transfer(client->adapter, msg, ARRAY_SIZE(msg));

	
	if (ret != ARRAY_SIZE(msg)) {
		dev_err(&client->dev,"failed to read %d bytes of touch data (%d)\n",sz, ret);		
	} else {
		mms_report_input_data(info, sz, buf);
	}

	return IRQ_HANDLED;
}

/*
 * mms_ts_input_open - Register input device after call this function 
 * this function is wait firmware flash wait
 */ 
#if MMS_USE_INIT_DONE
static int mms_ts_input_open(struct input_dev *dev)
{
	struct mms_ts_info *info = input_get_drvdata(dev);
	int ret = 1;

#if MMS_USE_INIT_DONE
	ret = wait_for_completion_interruptible_timeout(&info->init_done,
			msecs_to_jiffies(90 * MSEC_PER_SEC));
#endif

	if (ret > 0) {
		if (info->irq != -1) {
			mms_ts_enable(info);
			ret = 0;
		} else {
			ret = -ENXIO;
		}
	} else {
		dev_err(&dev->dev, "error while waiting for device to init\n");
		ret = -ENXIO;
	}

	return ret;
}

/*
 * mms_ts_input_close -If device power off state call this function
 */
static void mms_ts_input_close(struct input_dev *dev)
{
	struct mms_ts_info *info = input_get_drvdata(dev);

	mms_ts_disable(info);
}
#endif
void mms_fw_update_controller(const struct firmware *fw, void * context)
{
	struct mms_ts_info *info = context;
	int retires = 3;
	int ret;

	if (!fw) {
		dev_err(&info->client->dev, "failed to read firmware\n");
#if MMS_USE_INIT_DONE
		complete_all(&info->init_done);
#endif
		return;
	}

	do {
		ret = mms_flash_fw(info,fw->data,fw->size,false);
	} while (ret && --retires);

	if (!retires) {
		dev_err(&info->client->dev, "failed to flash firmware after retires\n");
	}
	release_firmware(fw);
}
/*
 * mms_ts_config - f/w check download & irq thread register
 */
static int mms_ts_config(struct mms_ts_info *info)
{
	struct i2c_client *client = info->client;
	int ret;

#ifdef MMS_USE_DEVICETREE
	//Set IRQ
	dev_err(&client->dev, "%s - gpio_to_irq [%d]\n", __func__, info->pdata->gpio_intr);
	//client->irq = info->pdata->gpio_intr;
//	client->irq = gpio_to_irq(info->pdata->gpio_intr);
//	dev_info(&info->client->dev, "%s - gpio_to_irq [%d]\n", __func__, client->irq);
	
#endif
	dev_info(&info->client->dev, "%s\n", __func__);
	
	ret = request_threaded_irq(info->client->irq,NULL, mms_ts_interrupt,IRQF_TRIGGER_LOW | IRQF_ONESHOT,"mms_ts", info);

/*  				
	ret = request_irq(info->client->irq, mms_ts_interrupt,IRQF_TRIGGER_LOW | IRQF_ONESHOT,"mms_ts", info);
*/
	dev_info(&info->client->dev, "request_threaded_irq ret = %d\n", ret);
	
	if (ret) {
		dev_err(&client->dev, "failed to register irq\n");
		goto out;
	}
	disable_irq(client->irq);
	info->irq = client->irq;
	barrier();

  	info->tx_num = i2c_smbus_read_byte_data(client, MMS_TX_NUM);
	info->rx_num = i2c_smbus_read_byte_data(client, MMS_RX_NUM);
	info->key_num = i2c_smbus_read_byte_data(client, MMS_KEY_NUM);

	dev_info(&client->dev, "Melfas touch controller initialized\n");
	mms_reboot(info);
#if MMS_USE_INIT_DONE
	complete_all(&info->init_done);
#else
	enable_irq(client->irq);
#endif

out:
	return ret;
}

//melfas test mode


static ssize_t mms_force_update(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct mms_ts_info *info = dev_get_drvdata(dev);
        struct i2c_client *client = to_i2c_client(dev);
	struct file *fp; 
	mm_segment_t old_fs;
	size_t fw_size, nread;
	int error = 0;
	int result = 0;
 	disable_irq(client->irq);
	old_fs = get_fs();
	set_fs(KERNEL_DS);  
 
	fp = filp_open(EXTRA_FW_PATH, O_RDONLY, S_IRUSR);
	if (IS_ERR(fp)) {
		dev_err(&info->client->dev,
		"%s: failed to open %s.\n", __func__, EXTRA_FW_PATH);
		error = -ENOENT;
		goto open_err;
	}
 	fw_size = fp->f_path.dentry->d_inode->i_size;
	if (0 < fw_size) {
		unsigned char *fw_data;
		fw_data = kzalloc(fw_size, GFP_KERNEL);
		nread = vfs_read(fp, (char __user *)fw_data,fw_size, &fp->f_pos);
		dev_info(&info->client->dev,
		"%s: start, file path %s, size %u Bytes\n", __func__,EXTRA_FW_PATH, fw_size);
		if (nread != fw_size) {
			    dev_err(&info->client->dev,
			    "%s: failed to read firmware file, nread %u Bytes\n", __func__, nread);
		    error = -EIO;
		} else{
			result=mms_flash_fw(info,fw_data,fw_size, true); // last argument is full update
		}
		kfree(fw_data);
	}
 	filp_close(fp, current->files);
open_err:
	enable_irq(client->irq);
	set_fs(old_fs);
	return result;
}
static DEVICE_ATTR(fw_update, 0666, mms_force_update, NULL);

static struct attribute *mms_attrs[] = {
	&dev_attr_fw_update.attr,
	NULL,
};

static const struct attribute_group mms_attr_group = {
	.attrs = mms_attrs,
};

#if MMS_USE_DEVICETREE
static int mms_parse_dt(struct device *dev, struct mms_ts_platform_data *pdata)
{
	int rc;
	u32 temp_val;
	struct device_node *np = dev->of_node;
	


	//Read property
	rc = of_property_read_u32(np, "melfas,max_x", &temp_val);
	if (rc) {
		dev_err(dev, "Failed to read max_x\n");
		pdata->max_x = 1080;
	} else {
		pdata->max_x = temp_val;
	}

	rc = of_property_read_u32(np, "melfas,max_y", &temp_val);
	if (rc) {
		dev_err(dev, "Failed to read max_y\n");
		pdata->max_y = 1920;
	} else {
		pdata->max_y = temp_val;
	}
/* 
	rc = of_property_read_u32(np, "melfas,max_id", &temp_val);
	if (rc) {
		dev_err(dev, "Failed to read max_id\n");
		pdata->max_id = MAX_FINGER_NUM;
	} else {
		pdata->max_id = temp_val;
	}
 */
	//interrupt GPIO
	pdata->gpio_intr = of_get_named_gpio(np, "melfas,irq-gpio", 0);
	
	if (!gpio_is_valid(pdata->gpio_intr)) {
		dev_err(dev, "%s [ERROR] irq-gpio\n", __func__);
		rc = pdata->gpio_intr;
		goto ERROR;
	//	return -1;
	}
/* 
	else{
		pdata->gpio_intr = pdata->gpio_intr ;
	} 
*/
	
 	
	//reset GPIO
	pdata->gpio_reset = of_get_named_gpio(np, "melfas,reset-gpio", 0);
	if (!gpio_is_valid(pdata->gpio_reset)) {
		dev_err(dev, "%s [ERROR] reset-gpio\n", __func__);
		rc = pdata->gpio_reset;
		goto ERROR;
	}
/*	
	else{
		pdata->gpio_reset = pdata->gpio_reset;
	}
*/	

 //interrupt GPIO
	rc = gpio_request(pdata->gpio_intr, "melfas,irq-gpio");
	if (rc){
		dev_err(dev, "%s [ERROR] gpio_request : irq-gpio\n", __func__);
		goto ERROR;
	}
	rc = gpio_direction_input(pdata->gpio_intr);
	if (rc) {
		dev_err(dev,"set_direction for irq gpio failed\n");
		goto ERROR_INTR;
	}
	
	//reset GPIO	
	rc = gpio_request(pdata->gpio_reset, "melfas,reset-gpio");
	if (rc) {
		dev_err(dev, "%s [ERROR] gpio_request : reset-gpio\n", __func__);
		goto ERROR_INTR;
	}		
	rc = gpio_direction_output(pdata->gpio_reset, 1);
	if (rc) {
		dev_err(dev,"set_direction for reset-gpio failed\n");
		goto ERROR_RESET;
	}

	dev_err(dev,"request gpio reset and irq successful\n");
 
ERROR_RESET:
	if (gpio_is_valid(pdata->gpio_reset))		
		gpio_free(pdata->gpio_reset);
ERROR_INTR:	
	if (gpio_is_valid(pdata->gpio_intr))
		gpio_free(pdata->gpio_intr);	
ERROR:
	return rc;
}
#endif
#ifdef PINCTRL_STATE
static int mms_ts_pinctrl_init(struct mms_ts_info *mms_data)
{
	int retval;

	/* Get pinctrl if target uses pinctrl */
	mms_data->ts_pinctrl = devm_pinctrl_get(&(mms_data->client->dev));
	if (IS_ERR_OR_NULL(mms_data->ts_pinctrl)) {
		retval = PTR_ERR(mms_data->ts_pinctrl);
		dev_dbg(&mms_data->client->dev,
			"Target does not use pinctrl %d\n", retval);
		goto err_pinctrl_get;
	}

	mms_data->pinctrl_state_active
		= pinctrl_lookup_state(mms_data->ts_pinctrl,
				PINCTRL_STATE_ACTIVE);
	if (IS_ERR_OR_NULL(mms_data->pinctrl_state_active)) {
		retval = PTR_ERR(mms_data->pinctrl_state_active);
		dev_err(&mms_data->client->dev,
			"Can not lookup %s pinstate %d\n",
			PINCTRL_STATE_ACTIVE, retval);
		goto err_pinctrl_lookup;
	}

	mms_data->pinctrl_state_suspend
		= pinctrl_lookup_state(mms_data->ts_pinctrl,
			PINCTRL_STATE_SUSPEND);
	if (IS_ERR_OR_NULL(mms_data->pinctrl_state_suspend)) {
		retval = PTR_ERR(mms_data->pinctrl_state_suspend);
		dev_err(&mms_data->client->dev,
			"Can not lookup %s pinstate %d\n",
			PINCTRL_STATE_SUSPEND, retval);
		goto err_pinctrl_lookup;
	}

	mms_data->pinctrl_state_release
		= pinctrl_lookup_state(mms_data->ts_pinctrl,
			PINCTRL_STATE_RELEASE);
	if (IS_ERR_OR_NULL(mms_data->pinctrl_state_release)) {
		retval = PTR_ERR(mms_data->pinctrl_state_release);
		dev_dbg(&mms_data->client->dev,
			"Can not lookup %s pinstate %d\n",
			PINCTRL_STATE_RELEASE, retval);
	}

	return 0;

err_pinctrl_lookup:
	devm_pinctrl_put(mms_data->ts_pinctrl);
err_pinctrl_get:
	mms_data->ts_pinctrl = NULL;
	return retval;
} //linfeng
#endif
static int mms_ts_probe(struct i2c_client *client,
				  const struct i2c_device_id *id)
{
	struct i2c_adapter *adapter = to_i2c_adapter(client->dev.parent);
	struct mms_ts_info *info;
	struct input_dev *input_dev;
	int ret = 0;
//	const char *fw_name = FW_NAME;
	
	if (!i2c_check_functionality(adapter, I2C_FUNC_I2C))
		return -ENODEV;

	info = kzalloc(sizeof(*info), GFP_KERNEL);

#if MMS_USE_DEVICETREE
	if (client->dev.of_node) {
		info->pdata  = devm_kzalloc(&client->dev, sizeof(struct mms_ts_platform_data), GFP_KERNEL);
		if (!info->pdata) {
			dev_err(&client->dev, "Failed to allocate memory\n");
			return -ENOMEM;
		}

		ret = mms_parse_dt(&client->dev, info->pdata);
		if (ret)
			return ret;
	} else
#endif
	{
		info->pdata = client->dev.platform_data;
		if (info->pdata == NULL) {
			dev_err(&client->dev, "pdata is NULL\n");
			return -EINVAL;
		}
	}
	
	input_dev = input_allocate_device();

	if (!info || !input_dev) {
		dev_err(&client->dev, "Failed to allocated memory\n");
		return -ENOMEM;
	}
	
	info->client = client;
	info->input_dev = input_dev;
#if MMS_USE_INIT_DONE
	init_completion(&info->init_done);
#endif
	info->irq = -1;
	
	mutex_init(&info->lock);

	snprintf(info->phys, sizeof(info->phys),
		"%s/input0", dev_name(&client->dev));

	input_dev->name = "MELFAS MMS100S TP";
	input_dev->phys = info->phys;
	input_dev->id.bustype = BUS_I2C;
	input_dev->dev.parent = &client->dev;
#if MMS_USE_INIT_DONE
	input_dev->open = mms_ts_input_open;
	input_dev->close = mms_ts_input_close;
#endif
	
	input_set_drvdata(input_dev, info);
	i2c_set_clientdata(client, info);
	
	__set_bit(EV_ABS, input_dev->evbit);
	__set_bit(INPUT_PROP_DIRECT, input_dev->propbit);

	
#if MMS_HAS_TOUCH_KEY
	__set_bit(EV_KEY, input_dev->evbit);
	__set_bit(KEY_MENU, input_dev->keybit);
	__set_bit(KEY_BACK, input_dev->keybit);
//	__set_bit(BTN_TOUCH, input_dev->keybit);
	__set_bit(KEY_HOMEPAGE, input_dev->keybit);
#endif
	
	input_mt_init_slots(input_dev, MAX_FINGER_NUM,0);

	input_set_abs_params(input_dev, ABS_MT_POSITION_X, 0, info->pdata->max_x, 0, 0);
	input_set_abs_params(input_dev, ABS_MT_POSITION_Y, 0, info->pdata->max_y, 0, 0);
	input_set_abs_params(input_dev, ABS_MT_TOUCH_MAJOR, 0, MAX_WIDTH, 0, 0);
	input_set_abs_params(input_dev, ABS_MT_PRESSURE, 0, MAX_PRESSURE, 0, 0);	
	
	ret = input_register_device(input_dev);
	
	if (ret) {
		dev_err(&client->dev, "failed to register input dev\n");
		input_free_device(input_dev);
		return ret;
	}	
	
	#ifdef PINCTRL_STATE
	ret = mms_ts_pinctrl_init(info);
	if (!ret && info->ts_pinctrl) {
		ret = pinctrl_select_state(info->ts_pinctrl,
					info->pinctrl_state_active);
		if (ret < 0) {
			dev_err(&client->dev,
				"failed to select pin to active state");
			goto pinctrl_deinit;
		}
	} else {
		goto ERROR;
	} //linfeng
	#endif
	mms_reboot(info);
/*
	info->fw_name = kstrdup(fw_name, GFP_KERNEL);

 	ret = request_firmware_nowait(THIS_MODULE, true, fw_name, &client->dev,
					GFP_KERNEL, info, mms_fw_update_controller);
	
	if (ret) {
		dev_err(&client->dev, "failed to schedule firmware update\n");
		return -EIO;
	}
*/

	ret = mms_ts_interrupt(info->client->irq,info);
	if(0 > ret)
	{
		dev_err(&client->dev, "mms_ts_interrupt Read I2C failed: %d\n",ret);	
		return ret;
	}
	

	ret = mms_ts_config(info);
	if(0 > ret){
		return ret;
	}

	esd_cnt = 0;

#if defined(CONFIG_FB)
	info->fb_notif.notifier_call = fb_notifier_callback;

	ret = fb_register_client(&info->fb_notif);

	if (ret)
		dev_err(&client->dev, "Unable to register fb_notifier: %d\n",
			ret);	
#elif defined(CONFIG_HAS_EARLYSUSPEND)
	info->early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN + MMS_SUSPEND_LEVEL;
	info->early_suspend.suspend = mms_ts_early_suspend;
	info->early_suspend.resume = mms_ts_late_resume;
	register_early_suspend(&info->early_suspend);
#endif

#ifdef __MMS_LOG_MODE__
	if(mms_ts_log(info)){
		dev_err(&client->dev, "failed to create Log mode.\n");
		return -EAGAIN;
	}

	info->class = class_create(THIS_MODULE, "mms_ts");
	device_create(info->class, NULL, info->mms_dev, NULL, "mms_ts");
#endif

#ifdef __MMS_TEST_MODE__
	if (mms_sysfs_test_mode(info)){
		dev_err(&client->dev, "failed to create sysfs test mode\n");
		return -EAGAIN;
	}

#endif
	if (sysfs_create_group(&client->dev.kobj, &mms_attr_group)) {
		dev_err(&client->dev, "failed to create sysfs group\n");
		return -EAGAIN;
	}

	if (sysfs_create_link(NULL, &client->dev.kobj, "mms_ts")) {
		dev_err(&client->dev, "failed to create sysfs symlink\n");
		return -EAGAIN;
	}


	dev_notice(&client->dev, "mms dev initialized\n");
	return 0;
		
#ifdef PINCTRL_STATE		
pinctrl_deinit:
	if (info->ts_pinctrl) {
		if (IS_ERR_OR_NULL(info->pinctrl_state_release)) {
			devm_pinctrl_put(info->ts_pinctrl);
			info->ts_pinctrl = NULL;
		} else {
			ret = pinctrl_select_state(info->ts_pinctrl,
					info->pinctrl_state_release);
			if (ret)
				pr_err("failed to select relase pinctrl state\n");
		}
	}

ERROR:
	dev_dbg(&client->dev, "%s [ERROR]\n", __func__);
	dev_err(&client->dev, "MELFAS Touchscreen initialization failed.\n");	
#endif		
	return ret;
}

static int mms_ts_remove(struct i2c_client *client)
{
	struct mms_ts_info *info = i2c_get_clientdata(client);
	int retval;

#ifdef __MMS_TEST_MODE__
	mms_sysfs_remove(info);
#endif
	sysfs_remove_group(&info->client->dev.kobj, &mms_attr_group);	
	sysfs_remove_link(NULL, "mms_ts");
	kfree(info->get_data);


#ifdef __MMS_LOG_MODE__
	device_destroy(info->class, info->mms_dev);
	class_destroy(info->class);
#endif
#ifdef PINCTRL_STATE
	if (info->ts_pinctrl) {
		if (IS_ERR_OR_NULL(info->pinctrl_state_release)) {
			devm_pinctrl_put(info->ts_pinctrl);
			info->ts_pinctrl = NULL;
		} else {
			retval = pinctrl_select_state(info->ts_pinctrl,
					info->pinctrl_state_release);
			if (retval < 0)
				pr_err("failed to select release pinctrl state\n");
		}
	}
#endif	
	
	#if defined(CONFIG_FB)
	if (fb_unregister_client(&info->fb_notif))
		dev_err(&client->dev, "Error occurred while unregistering fb_notifier.\n");
	#elif defined(CONFIG_HAS_EARLYSUSPEND)
	unregister_early_suspend(&info->early_suspend);
	#endif
	if (client->irq >= 0)
		free_irq(client->irq, info);
	
	if (gpio_is_valid(info->pdata->gpio_intr))
		gpio_free(info->pdata->gpio_intr);
	if (gpio_is_valid(info->pdata->gpio_reset))
		gpio_free(info->pdata->gpio_reset);
	
	input_unregister_device(info->input_dev);
//	kfree(info->fw_name);
	kfree(info);

	return 0;
}

#if defined(CONFIG_FB) || defined(CONFIG_HAS_EARLYSUSPEND)
static int mms_ts_suspend(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct mms_ts_info *info = i2c_get_clientdata(client);

	mutex_lock(&info->input_dev->mutex);

	if (info->input_dev->users) {
		mms_ts_disable(info);
		mms_clear_input_data(info);
	}

	mutex_unlock(&info->input_dev->mutex);
	return 0;
}

static int mms_ts_resume(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct mms_ts_info *info = i2c_get_clientdata(client);

	mutex_lock(&info->input_dev->mutex);

	if (info->input_dev->users)
		mms_ts_enable(info);

	mutex_unlock(&info->input_dev->mutex);

	return 0;
}
#endif
#if defined(CONFIG_FB)
static int fb_notifier_callback(struct notifier_block *self,
				 unsigned long event, void *data)
{
	struct fb_event *evdata = data;
	int *blank;
	struct mms_ts_info *info =
		container_of(self, struct mms_ts_info, fb_notif);

	if (evdata && evdata->data && event == FB_EVENT_BLANK &&
			info && info->client) {
		blank = evdata->data;
		if (*blank == FB_BLANK_UNBLANK)
			mms_ts_resume(&info->client->dev);
		else if (*blank == FB_BLANK_POWERDOWN)
			mms_ts_suspend(&info->client->dev);
	}
	return 0;
}
#elif defined(CONFIG_HAS_EARLYSUSPEND)
static void mms_ts_early_suspend(struct early_suspend *h)
{
	struct mms_ts_info *info;
	info = container_of(h, struct mms_ts_info, early_suspend);
	mms_ts_suspend(&info->client->dev);
}

static void mms_ts_late_resume(struct early_suspend *h)
{
	struct mms_ts_info *info;
	info = container_of(h, struct mms_ts_info, early_suspend);
	mms_ts_resume(&info->client->dev);
}
#endif


static const struct dev_pm_ops mms_ts_pm_ops = {
#if !defined(CONFIG_FB) && !defined(CONFIG_HAS_EARLYSUSPEND)	
	.suspend	= mms_ts_suspend,
	.resume		= mms_ts_resume,
#endif	
};


#if MMS_USE_DEVICETREE
static struct of_device_id mms_match_table[] = { 
	{ .compatible = "melfas,mms_ts",},
	{ },
};
#endif

static const struct i2c_device_id mms_ts_id[] = {
	{"mms_ts", 0},
	{ }
};

MODULE_DEVICE_TABLE(i2c, mms_ts_id);


static struct i2c_driver mms_ts_driver = {
	.probe		= mms_ts_probe,
	//.remove		= __devexit_p(mms_ts_remove),
	.remove		= mms_ts_remove,
	.driver		= {
				.name	= "mms_ts",
				.owner    = THIS_MODULE,
				#if MMS_USE_DEVICETREE					
				.of_match_table = mms_match_table,
				#endif
				#ifdef CONFIG_PM
				.pm	= &mms_ts_pm_ops,
				#endif
				},
	.id_table	= mms_ts_id,
};

static int __init mms_ts_init(void)
{
	return i2c_add_driver(&mms_ts_driver);
}

static void __exit mms_ts_exit(void)
{
	return i2c_del_driver(&mms_ts_driver);
}

module_init(mms_ts_init);
module_exit(mms_ts_exit);

MODULE_VERSION("2014.11.14");
MODULE_DESCRIPTION("MELFAS MMS100S Touchscreen Driver");
MODULE_LICENSE("GPL");


