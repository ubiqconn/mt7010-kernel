/*
 * egalax_usb.c
 *
 * Copyright 2011 Heiko Stuebner <heiko@sntech.de>
 *
 * based on fixed.c
 *
 * Copyright 2008 Wolfson Microelectronics PLC.
 *
 * Author: Mark Brown <broonie@opensource.wolfsonmicro.com>
 *
 * Copyright (c) 2009 Nokia Corporation
 * Roger Quadros <ext-roger.quadros@nokia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This is useful for systems with mixed controllable and
 * non-controllable regulators, as well as for allowing testing on
 * systems with no controllable regulators.
 */

#include <linux/err.h>
#include <linux/mutex.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/regulator/driver.h>
#include <linux/regulator/machine.h>
#include <linux/regulator/of_regulator.h>
#include <linux/gpio.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/of_gpio.h>

#define TOUCH_PWR_VDD_MIN_UV	3300000
#define TOUCH_PWR_VDD_MAX_UV	3300000

struct egalax_usb_platform_data {
	struct regulator *vdd;
};
struct egalax_usb_info {
	struct platform_device *pdev;
};
struct egalax_usb_info *touch_info;

static int egalax_usb_parse_dt(struct device *dev, struct egalax_usb_platform_data *pdata) {
	int ret = 0;
	pdata->vdd = devm_regulator_get(dev, "vdd");
	if (IS_ERR(pdata->vdd)) {
		ret = PTR_ERR(pdata->vdd);
		dev_err(dev, "Regulator get failed vdd rc=%d\n", ret);
	}
	return ret;
}

static int egalax_usb_probe(struct platform_device *pdev) {
	int ret = 0;
	struct egalax_usb_platform_data *pdata;

	touch_info = kzalloc(sizeof(struct egalax_usb_info), GFP_KERNEL);
	if (!touch_info) {
		dev_err(&pdev->dev, "%s: Failed to alloc memory.\n", __func__);
		return -ENOMEM;
	}

	touch_info->pdev = pdev;

	if (&pdev->dev.of_node) {
		pdata = devm_kzalloc(&pdev->dev, sizeof(*pdata), GFP_KERNEL);
		if (!pdata) {
			dev_err(&pdev->dev, "Failed to allocate memory for pdata\n");
			ret = -ENOMEM;
			goto err_platform_data_null;
		}

		ret = egalax_usb_parse_dt(&pdev->dev, pdata);
		if (ret) {
			dev_err(&pdev->dev, "Failed to get pdata from device tree\n");
			goto err_platform_data_null;
		}
	} else {
		pdata = pdev->dev.platform_data;
		if (!pdata) {
			dev_err(&pdev->dev, "%s: Assign platform_data error!!\n",
				__func__);
			ret = -EBUSY;
			goto err_platform_data_null;
		}
	}
	// Try to enable regulator.
	ret = regulator_enable(pdata->vdd);
	if (ret) {
		dev_err(&pdev->dev, "%s: Failed to enable vdd regulator: %d\n", __func__, ret);
		goto err_regulator_enable;
	}
	return 0;

err_regulator_enable:
	if (pdev->dev.of_node && (pdata != NULL))
		devm_kfree(&pdev->dev, pdata);
err_platform_data_null:
	kfree(touch_info);
	return ret;
}

static int egalax_usb_remove(struct platform_device *pdev) {
	return 0;
}

static const struct of_device_id egalax_usb_of_match[] = {
	{.compatible = "eeti,egalax-usb", },
	{},
};

static struct platform_driver egalax_usb_driver = {
	.probe = egalax_usb_probe,
	.remove = egalax_usb_remove,
	.driver = {
		.name = "egalax_usb",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(egalax_usb_of_match),
	},
};

static int __init egalax_usb_init(void) {
	pr_info("%s", __func__);
	return platform_driver_register(&egalax_usb_driver);
}
subsys_initcall(egalax_usb_init);

static void __exit egalax_usb_exit(void) {
	pr_info("%s", __func__);
	platform_driver_unregister(&egalax_usb_driver);
}
module_exit(egalax_usb_exit);

MODULE_AUTHOR("Heiko Stuebner <heiko@sntech.de>");
MODULE_DESCRIPTION("gpio voltage regulator");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:egalax_usb");
