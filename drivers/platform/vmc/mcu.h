#include <linux/power_supply.h>
#include <linux/i2c.h>

//#define VIRTUAL_THERMAL // for thermal virtual debug

#define MCU_BAT_THERMAL		0x00
#define MCU_MODULE_CONTROL  0x9D
	#define WATCHDOG_EN		(1 << 0)
	#define OBD_ON			(1 << 1)
	#define EXTEND_USB_ON	(1 << 2)
	#define IO_USB_ON		(1 << 3)
	#define RF_LED_ON		(1 << 4)
#define MCU_CPU_THERMAL     0x70
#define MCU_SYSTEM_THERMAL  0x71
#define MCU_DIGITAL_IO	    0x7A
#define MCU_VEHICALE_STATUS	0x99
	#define ACC_ON 			(1 << 0)
	#define ACC_12V			(1 << 1)
	#define ACC_24V			(1 << 2)
	#define ACC_UVP			(1 << 3)
#define MCU_RS232_RI		0xA0
	#define RI_0V_RING		(1 << 0)
	#define RI_5V			(1 << 1)
	#define RI_12V			(1 << 2)	
#define MUC_KEYS_STATUS 	0xA3
	#define FN1 			(1 << 0)
	#define FN2 			(1 << 1)
	#define FN3 			(1 << 2)
	#define FN4 			(1 << 3)
	#define FN5 			(1 << 4)
	#define SLP_LCD			(1 << 5)
	#define SOS 			(1 << 6)
	#define RST 			(1 << 7)
#define MCU_SYSTEM_STATE	0xA4
	#define	S0				(1 << 0)
	#define S5				(1 << 1)
#define MCU_VERSION			0xFE
#define MCU_SUBVERSION		0xFF

struct mcu_class_dev {
	const char	*name;
	struct device	*dev;
	int		index;
	int		state;
	int     (*read_word)(struct i2c_client *client, int addr);
	void    (*write_word)(struct i2c_client *client, int addr, int data);
};

struct ac_info {
	int					id;
	struct device 		*dev;
	struct i2c_client	*client;
	struct power_supply	ac;
};

struct mcu_platform_data {
	int irq;
	int poll_delay;
	int version;
	int sub_version;
	int vehicle_status;
	int key_status;
	int cpu_thermal;
	int sys_thermal;
	int bat_thermal;
	int digital_IO;	
	struct ac_info ai;
};

struct mcu_info {
	struct i2c_client		*client;
	struct mutex			lock;
	struct mcu_platform_data	*pdata;
	struct workqueue_struct	*workqueue;
	struct delayed_work dwork;
	struct mcu_class_dev mcu_class;
};