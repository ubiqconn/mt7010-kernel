/* Includes ------------------------------------------------------------------*/
#include <linux/interrupt.h>
#include <linux/slab.h>
#include <linux/irq.h>
#include <linux/of_gpio.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/workqueue.h>
#include <linux/kobject.h>
#include <linux/platform_device.h>
#include <linux/idr.h>
#include "mcu.h"

extern int class_dev_register(struct mcu_info *mcu_info);
extern void mcu_send_event(struct device *dev, int event, int state);

#define MCU_NAME "mcu"
static DEFINE_IDR(ac_id);
static struct mcu_info *mcu_info = NULL;
static const struct i2c_device_id mcu_i2c_id[] = {{MCU_NAME,0},{}};

static struct of_device_id mcu_match_table[] = {
	{.compatible = "vmc,mcu"},
	{}
};

static int mcu_read_word_data(struct i2c_client *client, int addr)
{
	int temp;
	temp = i2c_smbus_read_word_data(client, addr);
	return ((temp & 0xFF) << 8) | ((temp >> 8) & 0xFF);
}

static void mcu_write_word_data(struct i2c_client *client, int addr, int data)
{
	int temp;
	temp =((data & 0xFF) << 8) | ((data >> 8) & 0xFF);
	i2c_smbus_write_word_data(client, addr, temp);
}

static void mcu_workqueue_poll(struct work_struct *work)
{
	int vehicleStatus, keyStatus, DigitalIO;
	#ifndef VIRTUAL_THERMAL
	int cpuThermal, sysThermal, batThermal;
	#endif

	vehicleStatus = mcu_read_word_data(mcu_info->client, MCU_VEHICALE_STATUS);
	if (vehicleStatus != mcu_info->pdata->vehicle_status) {
		mcu_info->pdata->vehicle_status = vehicleStatus;
		mcu_send_event(mcu_info->mcu_class.dev, MCU_VEHICALE_STATUS, vehicleStatus);
		power_supply_changed(&mcu_info->pdata->ai.ac);
	}

    keyStatus = mcu_read_word_data(mcu_info->client, MUC_KEYS_STATUS);
    if (keyStatus != mcu_info->pdata->key_status) {
    	mcu_info->pdata->key_status = keyStatus;
    	mcu_send_event(mcu_info->mcu_class.dev, MUC_KEYS_STATUS, keyStatus);
    }

    //Thermal
    #ifndef VIRTUAL_THERMAL
    cpuThermal = mcu_read_word_data(mcu_info->client, MCU_CPU_THERMAL);
    if (cpuThermal != mcu_info->pdata->cpu_thermal) {
    	mcu_info->pdata->cpu_thermal = cpuThermal;
    	mcu_send_event(mcu_info->mcu_class.dev, MCU_CPU_THERMAL, cpuThermal);
    }

    sysThermal = mcu_read_word_data(mcu_info->client, MCU_SYSTEM_THERMAL);
    if (sysThermal != mcu_info->pdata->sys_thermal) {
    	mcu_info->pdata->sys_thermal = sysThermal;
    	mcu_send_event(mcu_info->mcu_class.dev, MCU_SYSTEM_THERMAL, sysThermal);
    }

    batThermal = mcu_read_word_data(mcu_info->client, MCU_BAT_THERMAL);
    if (batThermal != mcu_info->pdata->bat_thermal) {
    	mcu_info->pdata->bat_thermal = batThermal;
    	mcu_send_event(mcu_info->mcu_class.dev, MCU_BAT_THERMAL, batThermal);
    }
    #endif

    //Digital input/output
    DigitalIO = mcu_read_word_data(mcu_info->client, MCU_DIGITAL_IO);
    if (DigitalIO != mcu_info->pdata->digital_IO) {
    	mcu_info->pdata->digital_IO = DigitalIO;
    	mcu_send_event(mcu_info->mcu_class.dev, MCU_DIGITAL_IO, DigitalIO);
    }

    queue_delayed_work(mcu_info->workqueue,	&mcu_info->dwork,
            msecs_to_jiffies(mcu_info->pdata->poll_delay));
}

static int ac_get_property(struct power_supply *psy,
					enum power_supply_property psp,
					union power_supply_propval *val)
{
	//struct ac_info *ai = container_of(psy, struct ac_info, ac);
	
	mutex_lock(&mcu_info->lock);
	switch (psp) {
	case POWER_SUPPLY_PROP_ONLINE:
		if ((mcu_info->pdata->vehicle_status & ACC_ON) == 0 ||
			(mcu_info->pdata->vehicle_status & ACC_UVP)) {
		 	val->intval = 0;
		} else {
            val->intval = 1;
		}
		break;
	case POWER_SUPPLY_PROP_STATUS:
		val->intval = POWER_SUPPLY_STATUS_UNKNOWN;
		break;
	default:
		mutex_unlock(&mcu_info->lock);
		return -EINVAL;
	}
	mutex_unlock(&mcu_info->lock);
	return 0;
}

static enum power_supply_property ac_props[] = {
	POWER_SUPPLY_PROP_ONLINE,
	POWER_SUPPLY_PROP_STATUS,
};

static void ac_power_init(struct ac_info *ai)
{
	ai->ac.type = POWER_SUPPLY_TYPE_MAINS;
	ai->ac.properties = ac_props;
	ai->ac.num_properties = ARRAY_SIZE(ac_props);
	ai->ac.get_property = ac_get_property;
	ai->ac.external_power_changed = NULL;
}


static int mcu_parse_dt(struct device *dev, struct mcu_platform_data *pdata) {
	struct device_node *np = dev->of_node;
	int rc, tmp;

	dev_info(&mcu_info->client->dev, "%s\n", __func__);
	rc = of_property_read_u32(np, "mcu,polltime", &tmp);
	if (rc < 0) {
		dev_err(dev, "mcu, not support polling!!\n");
	} else {
		pdata->poll_delay = tmp;
		INIT_DELAYED_WORK(&mcu_info->dwork, mcu_workqueue_poll);
		mcu_info->workqueue = create_singlethread_workqueue("mcu_wq");
	    if (mcu_info->workqueue) {
            queue_delayed_work(mcu_info->workqueue,	&mcu_info->dwork,
                               msecs_to_jiffies(mcu_info->pdata->poll_delay));
	    }
	}

	return 0;
}

static int mcu_driver_probe(struct i2c_client *client, const struct i2c_device_id *id) {
	int err = 0, ac_num;
	char *name;

	dev_info(&client->dev, "%s\n", __func__);
	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
		dev_err(&client->dev, "need I2C_FUNC_I2C\n");
		return -ENODEV;
	}
	if (!(mcu_info = kzalloc(sizeof(struct mcu_info), GFP_KERNEL))) {
		err = -ENOMEM;
		goto exit;
	}
	memset(mcu_info, 0, sizeof(struct mcu_info));
	mcu_info->client = client;
	mcu_info->pdata = devm_kzalloc(&client->dev, sizeof(struct mcu_platform_data), GFP_KERNEL);
	if (!mcu_info->pdata) {
		dev_err(&client->dev, "Failed to allocate memory\n");
		err = -ENOMEM;
		goto err_kzalloc;
	}

	err = mcu_parse_dt(&client->dev, mcu_info->pdata);
	if (err) {
		goto dt_failed;
	}
	i2c_set_clientdata(client, mcu_info);

	mutex_init(&mcu_info->lock);

	mcu_info->pdata->version = mcu_read_word_data(client, MCU_VERSION);
	mcu_info->pdata->sub_version = mcu_read_word_data(client, MCU_SUBVERSION);
	dev_info(&client->dev, "version:0x%x, sub version:0x%x\n", mcu_info->pdata->version, mcu_info->pdata->sub_version);
	mcu_write_word_data(client, MCU_SYSTEM_STATE, S0);

	mcu_info->pdata->vehicle_status = mcu_read_word_data(client, MCU_VEHICALE_STATUS);
	dev_info(&client->dev, "VehicleStatus status:0x%x\n", mcu_info->pdata->vehicle_status);

	mutex_lock(&mcu_info->lock);
	ac_num = idr_alloc(&ac_id, client, 0, 0, GFP_KERNEL);
	mutex_unlock(&mcu_info->lock);

	name = kasprintf(GFP_KERNEL, "ac");
	if (!name) {
		dev_err(&client->dev, "failed to allocate ac device name\n");
		idr_remove(&ac_id, ac_num);
	} else {
		mcu_info->pdata->ai.id = ac_num;
		mcu_info->pdata->ai.dev = &client->dev;
		mcu_info->pdata->ai.ac.name = name;
		mcu_info->pdata->ai.client = client;

		ac_power_init(&mcu_info->pdata->ai);
		err = power_supply_register(&client->dev, &mcu_info->pdata->ai.ac);
		if (err) {
			dev_err(&client->dev, "failed to register ac\n");
			idr_remove(&ac_id, ac_num);
		}
	}

	mcu_info->mcu_class.name = MCU_NAME;
	mcu_info->mcu_class.read_word = mcu_read_word_data;
    mcu_info->mcu_class.write_word = mcu_write_word_data;
	err = class_dev_register(mcu_info);
	if (err < 0) {
		dev_err(&client->dev, "create class fail!!\n");
	}

	return 0;

dt_failed:
	kfree(mcu_info->pdata);
err_kzalloc:
	kfree(mcu_info);
	mcu_info = NULL;
exit:
	return err;
}

//static void mcu_shutdown(struct i2c_client *client)
void mcu_shutdown(void)
{
	int value, mode=S5;
	do
	{
		mcu_write_word_data(mcu_info->client, MCU_SYSTEM_STATE, mode); 
		
		value = mcu_read_word_data(mcu_info->client, MCU_SYSTEM_STATE);
		printk("SYS_PWR_STATUS:0x%x\n",value);
		msleep(10);
	} while(value != mode);
}
EXPORT_SYMBOL_GPL(mcu_shutdown);

static struct i2c_driver mcu_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name = MCU_NAME,
		.of_match_table = mcu_match_table,
	},
	.probe = mcu_driver_probe,
	.id_table = mcu_i2c_id,
	//.suspend     = mcu_suspend,
	//.resume      = mcu_resume,
	//.shutdown    = mcu_shutdown,
};

static int __init mcu_init(void) {
	if (i2c_add_driver(&mcu_driver) != 0) {
		pr_err("mcu_init failed to register i2c driver.\n");
	}
	return 0;
}

static void __exit mcu_exit(void) {
	i2c_del_driver(&mcu_driver);
}

module_init(mcu_init);
module_exit(mcu_exit);