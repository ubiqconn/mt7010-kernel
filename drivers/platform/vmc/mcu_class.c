#include <linux/module.h>
#include <linux/types.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/err.h>
#include <linux/switch.h>
#include <linux/kernel.h>
#include "mcu.h"

static struct class *mcu_class;
static struct mcu_info *mcu_info;
static atomic_t device_count;

static ssize_t event_name(int event, char *buf)
{
	return sprintf(buf, "%d\n", event);
}

static ssize_t event_value(int state, char *buf)
{
	return sprintf(buf, "%d\n", state);
}

void mcu_send_event(struct device *dev, int event, int state)
{
	char name_buf[120];
	char state_buf[120];
	char *prop_buf;
	char *envp[3];
	int env_offset = 0;
	int length;

	prop_buf = (char *)get_zeroed_page(GFP_KERNEL);
	if (prop_buf) {
		length = event_name(event, prop_buf);
		if (length > 0) {
			if (prop_buf[length - 1] == '\n')
				prop_buf[length - 1] = 0;
			snprintf(name_buf, sizeof(name_buf), "MCU_EVENT=%s", prop_buf);
			envp[env_offset++] = name_buf;
		}

		length = event_value(state, prop_buf);
		if (length > 0) {
			if (prop_buf[length - 1] == '\n')
				prop_buf[length - 1] = 0;
			snprintf(state_buf, sizeof(state_buf), "EVENT_VALUE=%s", prop_buf);
			envp[env_offset++] = state_buf;
		}

		envp[env_offset] = NULL;
		kobject_uevent_env(&dev->kobj, KOBJ_CHANGE, envp);
		free_page((unsigned long)prop_buf);
	} else {
		printk(KERN_ERR "out of memory in mcu_send_state\n");
		kobject_uevent(&dev->kobj, KOBJ_CHANGE);
	}
}
EXPORT_SYMBOL_GPL(mcu_send_event);

static ssize_t cpuThermal_show(struct device *dev, struct device_attribute *attr,
		char *buf)
{
	//0x00 = -40 degree, 0x28 = 0 degree
	int temp = mcu_info->pdata->cpu_thermal - 40;
	return sprintf(buf, "%d degree\n", temp);
}

static ssize_t sysThermal_show(struct device *dev, struct device_attribute *attr,
		char *buf)
{
	//0x00 = -40 degree, 0x28 = 0 degree
	int temp = mcu_info->pdata->sys_thermal - 40;
	return sprintf(buf, "%d degree\n", temp);
}

static ssize_t batThermal_show(struct device *dev, struct device_attribute *attr,
		char *buf)
{
	//0.1 K
	int temp = mcu_info->pdata->bat_thermal - 2730;
	return sprintf(buf, "%d.%d degree\n", temp/10 , temp%10);
}

#ifdef VIRTUAL_THERMAL
static ssize_t cpuThermal_set(struct device *device, struct device_attribute *attr,
								const char *buff, size_t size)
{
	int tmp = simple_strtol(buff, NULL, 0);
	mcu_info->pdata->cpu_thermal = tmp + 40;
	mcu_send_event(mcu_info->mcu_class.dev, MCU_CPU_THERMAL, mcu_info->pdata->cpu_thermal);
	return size;
}

static ssize_t sysThermal_set(struct device *device, struct device_attribute *attr,
								const char *buff, size_t size)
{
	int tmp = simple_strtol(buff, NULL, 0);
	mcu_info->pdata->sys_thermal = tmp + 40;
	mcu_send_event(mcu_info->mcu_class.dev, MCU_SYSTEM_THERMAL, mcu_info->pdata->sys_thermal);
	return size;
}

static ssize_t batThermal_set(struct device *device, struct device_attribute *attr,
								const char *buff, size_t size)
{
	int tmp = simple_strtol(buff, NULL, 0);
	mcu_info->pdata->bat_thermal = (tmp * 10) + 2730;
	mcu_send_event(mcu_info->mcu_class.dev, MCU_BAT_THERMAL, mcu_info->pdata->bat_thermal);
	return size;
}
#endif

char strMcuEvent[8];
static ssize_t mcuEvent_report(struct device *dev, struct device_attribute *attr,
		char *buf)
{
	int i;
	int	index = (strMcuEvent[2] << 8) | strMcuEvent[1];	
	int data = mcu_info->mcu_class.read_word(mcu_info->client, index);
	strMcuEvent[3] = data & 0xff;
	strMcuEvent[4] = (data >> 8) & 0xff;
	//printk(KERN_ERR "read index:0x%x data:0x%x\n", index, data);
	for (i = 0; i < 5; i++) {
		buf[i] = strMcuEvent[i];
	}
	return 5;
}

static ssize_t mcuEvent_parse(struct device *device, struct device_attribute *attr,
								const char *buff, size_t size)
{
	int len, read, index, data;

	len = size > sizeof(strMcuEvent) ? sizeof(strMcuEvent) : size; 
	memset(strMcuEvent, 0, sizeof(strMcuEvent));
	memcpy(strMcuEvent, buff, len);
	//printk(KERN_ERR "buff: 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, size:%d len:%d\n", buff[0], buff[1], buff[2], buff[3], buff[4], size, len);
	//printk(KERN_ERR "strMcuEvent: 0x%x, 0x%x, 0x%x, 0x%x, 0x%x\n", strMcuEvent[0], strMcuEvent[1], strMcuEvent[2], strMcuEvent[3], strMcuEvent[4]);

	read = strMcuEvent[0];
	index = (strMcuEvent[2] << 8) | strMcuEvent[1];
	if (read == 1) {
		//read command
	} else if (read == 0) {
		//write command
		data = (strMcuEvent[4] << 8) | strMcuEvent[3];
		mcu_info->mcu_class.write_word(mcu_info->client, index, data);
		//printk(KERN_ERR "write index:0x%x data:0x%x\n", index, data);
		mcu_send_event(mcu_info->mcu_class.dev, index, data);
	} 

	return size;
}

#ifdef VIRTUAL_THERMAL
static DEVICE_ATTR(cpuThermal, S_IWUSR | S_IRUSR, cpuThermal_show, cpuThermal_set);
static DEVICE_ATTR(sysThermal, S_IWUSR | S_IRUSR, sysThermal_show, sysThermal_set);
static DEVICE_ATTR(batThermal, S_IWUSR | S_IRUSR, batThermal_show, batThermal_set);
#else
static DEVICE_ATTR(cpuThermal, S_IRUGO, cpuThermal_show, NULL);
static DEVICE_ATTR(sysThermal, S_IRUGO, sysThermal_show, NULL);
static DEVICE_ATTR(batThermal, S_IRUGO, batThermal_show, NULL);
#endif
static DEVICE_ATTR(mcuEvent, S_IWUSR | S_IRUSR, mcuEvent_report, mcuEvent_parse);

static int create_mcu_class(char *name)
{
	if (!mcu_class) {
		mcu_class = class_create(THIS_MODULE, name);
		if (IS_ERR(mcu_class))
			return PTR_ERR(mcu_class);
		atomic_set(&device_count, 0);
	}

	return 0;
}

int class_dev_register(struct mcu_info *info)
{
	int ret = 0;
	struct mcu_class_dev *class_dev = &info->mcu_class;
	mcu_info = info;

	if (!mcu_class) {
		ret = create_mcu_class("vmc");
		if (ret < 0)
			return ret;
	}

	class_dev->index = atomic_inc_return(&device_count);
	class_dev->dev = device_create(mcu_class, NULL, MKDEV(0, class_dev->index), NULL, class_dev->name);
	if (IS_ERR(class_dev->dev))
		return PTR_ERR(class_dev->dev);

	//thermal
	ret = device_create_file(class_dev->dev, &dev_attr_cpuThermal);
    if (ret < 0)
        printk(KERN_ERR "MCU: Failed to register cpuThermal\n");

    ret = device_create_file(class_dev->dev, &dev_attr_sysThermal);
    if (ret < 0)
        printk(KERN_ERR "MCU: Failed to register sysThermal\n");

    ret = device_create_file(class_dev->dev, &dev_attr_batThermal);
    if (ret < 0)
        printk(KERN_ERR "MCU: Failed to register batThermal\n");

	//mcu event
	ret = device_create_file(class_dev->dev, &dev_attr_mcuEvent);
    if (ret < 0)
        printk(KERN_ERR "MCU: Failed to register mcuEvent\n");

	return 0;
}
EXPORT_SYMBOL_GPL(class_dev_register);