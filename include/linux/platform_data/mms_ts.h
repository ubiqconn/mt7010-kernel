/*
 * MELFAS MMS Touchscreen Driver - Platform Data
 *
 * Copyright (C) 2014 MELFAS Inc.
 *
 * Default path : linux/platform_data/mms_ts.h
 *
 */

#ifndef _LINUX_MMS_TOUCH_H
#define _LINUX_MMS_TOUCH_H

struct mms_ts_platform_data {
	int max_x;
	int max_y;
	int max_id;

	int gpio_intr;		//required (interrupt signal)
	int gpio_reset;		//required (reset signal)
	
	int gpio_vdd_en;	//required (power control)

	int gpio_sda;		//optional
	int gpio_scl;		//optional
};

#endif /* _LINUX_MMS_TOUCH_H */
